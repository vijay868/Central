﻿Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Imports Rebex.Net
Imports System.Data

Public Class frmFTPSync
    Dim Host As String
   
    Dim ftpCredential As System.Net.NetworkCredential
    Dim MstopTime As DateTime
    Private _showTransferProgress As Boolean
    Private _currentFileLength As Long
    Dim client As New Ftp
    Dim App_Start_MilliSeconds As DateTime
    Private Shared _bytesPerSecond As Long
    Private Shared _eventTime As Integer
    Dim SQLDirPath, FileDirPath, JPEGDirPath, LNKPath As String

    Public Function FTPLiveConnection() As Boolean
        Dim returnVal As Boolean
        Try
            Status_Strip_text.Text = "Checking FTP Connection..."
            Application.DoEvents()

            If client.State = FtpState.Disconnected Then
                Status_Strip_text.Text = "Connecting to FTP..."
                Application.DoEvents()
                'client.Connect("124.153.75.128", "21")
                client.Connect(FTPIPAddress, 21)
                client.Login(FTPUserName, FTPPassword)
                'client.Login("newdev", "new_dev")
                client.KeepAliveDuringTransferInterval = 300
                Status_Strip_text.Text = "Connected"
                Application.DoEvents()
                If FTPRootPath <> "/" Then client.ChangeDirectory(FTPRootPath)
                AddHandler client.TransferProgress, AddressOf _ftp_TransferProgress
                returnVal = True
            Else
                Status_Strip_text.Text = "FTP Connection Alive"
                Application.DoEvents()
                returnVal = True
            End If
        Catch ex As Exception
            returnVal = False
        End Try
        Return returnVal
    End Function

    Public Sub WorkWithDirectory(ByVal aDir As DirectoryInfo, ByVal CustomerName As String, ByVal StoreName As String, ByVal MonthYear As String)
        Dim nextDir As DirectoryInfo
        Dim CurDir As String
        Dim FtpPath As String
        Try
            lbl_curstorename.Text = StoreName
            If aDir.Name = "Songs" Then
                FtpPath = FTPRootPath & "/" & CustomerName & "/" & StoreName & "/" & MonthYear & "/Songs"
                lbl_type.Text = "Songs"
                WorkWithFilesInDir(aDir, "Songs", FtpPath)
            ElseIf aDir.Name = "Promos" Then
                FtpPath = FTPRootPath & "/" & CustomerName & "/" & StoreName & "/" & MonthYear & "/Promos"
                lbl_type.Text = "Promos"
                WorkWithFilesInDir(aDir, "Promos", FtpPath)
            Else
                FtpPath = FTPRootPath & "/" & CustomerName & "/" & StoreName & "/" & MonthYear
                lbl_type.Text = "Play List"
                WorkWithFilesInDir(aDir, "PlayList", FtpPath)
            End If

            If lbl_Status.Text = "Status: FTP Connection failure" Then Exit Sub

            For Each nextDir In aDir.GetDirectories
                CurDir = FTPRootPath & "/" & CustomerName & "/" & StoreName & "/" & MonthYear & "/" & nextDir.Name
                If client.DirectoryExists(CurDir) = False Then
                    client.CreateDirectory(CurDir)
                End If
                WorkWithDirectory(nextDir, CustomerName, StoreName, MonthYear)
                lbl_curstorename.Text = ""
            Next
            'lbl_Status.Text = "Status: Uploading complete"
            'Application.DoEvents()

        Catch ex As Exception
            lbl_Status.Text = "Status: FTP Connection failure"
            Application.DoEvents()
        End Try
    End Sub

    Public Sub WorkWithFilesInDir(ByVal aDir As DirectoryInfo, ByVal FolderType As String, ByVal FTPPath As String)
        Dim aFile As FileInfo
        Dim i As Integer = 1
        Try
            For Each aFile In aDir.GetFiles()
                Console.WriteLine(aFile.FullName)
                _currentFileLength = aFile.Length
                _showTransferProgress = True
                lbl_fname.Text = aFile.Name & " (" & i & "/" & aDir.GetFiles.Length & ")"
                lbl_size.Text = SetBytes(aFile.Length)
                _currentFileLength = aFile.Length
                _showTransferProgress = True
                Status_Strip_text.Text = "Upload File: " & aFile.Name & " (" & i & "/" & aDir.GetFiles.Length & ")"
                Application.DoEvents()
                'lblFilesCount.Text = "Uploading File(s)   " & i & "/" & aDir.GetFiles.Length
                ' lblFilesCount.Text = i & "/" & aDir.GetFiles.Length
                UploadFiles(aFile.DirectoryName, FTPPath, client, aFile.Name, aFile.Length)
                If lbl_Status.Text = "Status: FTP Connection failure" Then Exit For
                i = i + 1
            Next
        Catch ex As Exception
            lbl_Status.Text = "Status: FTP Connection failure"
            Application.DoEvents()
        End Try
    End Sub

    Public Function UploadFiles(ByVal LocalDirPath As String, ByVal CurrentDirectory As String, ByRef client As Ftp, ByVal StrFileName As String, ByVal LocalSize As Integer) As Boolean
        Dim returnval As Boolean
        Dim resStr As String
        Dim rSize As Integer
        Try
            client.PutFile(LocalDirPath & "\" & StrFileName, CurrentDirectory & "/" & StrFileName)
            'client.PutFiles(LocalDirPath, CurrentDirectory, FtpBatchTransferOptions.Recursive, FtpActionOnExistingFiles.OverwriteAll)
            rSize = client.GetFileLength(CurrentDirectory & "/" & StrFileName)

            If rSize = LocalSize Then
                returnval = True
            Else
                returnval = False
            End If
        Catch ex As Exception

            'If ex.Message.ToString = "Cannot send command to the server because the response for previous one was not received." Or ex.Message.ToString = "An existing connection was forcibly closed by the remote host." Then
            lbl_Status.Text = "Status: FTP Connection failure"
            Application.DoEvents()
            'End If
            returnval = False
        End Try
        Return returnval
    End Function

    'Public Function ReadFiles(ByVal LocalDirPath As String) As Boolean
    '    Dim FileNames As String()
    '    Dim OnlyFilename As String
    '    Dim finfo As FileInfo
    '    Dim i As Integer
    '    Dim returnval As Boolean

    '    Try

    '        Pbar.EditValue = 0
    '        lbl_fname.Text = ""
    '        lbl_type.Text = ""
    '        lbl_size.Text = ""
    '        lblFilesCount.Text = ""
    '        lbl_percentage.Text = ""

    '        FileNames = Directory.GetFiles(LocalDirPath)
    '        If Not FileNames.Length <= 0 Then
    '            i = 1
    '            For Each OnlyFilename In FileNames
    '                Application.DoEvents()
    '                finfo = New FileInfo(OnlyFilename)
    '                lbl_fname.Text = finfo.Name
    '                lbl_type.Text = finfo.Extension.ToString
    '                lbl_size.Text = SetBytes(finfo.Length)

    '                _currentFileLength = finfo.Length
    '                _showTransferProgress = True
    '                Status_Strip_text.Text = "Upload File: " & finfo.Name
    '                Application.DoEvents()
    '                '   lblFilesCount.Text = "Uploading File(s)   " & i & "/" & FileNames.Length - 1
    '                FTPConnect(Trim(LocalDirPath), finfo.Name, finfo.Length)
    '                Status_Strip_text.Text = "Done"
    '                Application.DoEvents()
    '                i = i + 1
    '            Next
    '        End If
    '    Catch ex As Exception
    '        client.Disconnect()
    '        returnval = False
    '    End Try
    '    Return returnval
    'End Function

    'Public Sub FTPConnect(ByVal LocalDirPath As String, ByVal StrFileName As String, ByVal fsize As String)
    '    Dim docroot As String
    '    Dim CurrentDirectory As String
    '    Dim resStr As String
    '    Dim fFilename As String
    '    'docroot = "/testing/"
    '    fFilename = (Mid(LocalDirPath, InStrRev(LocalDirPath, "\") + 1))
    '    docroot = "/"
    '    client.ChangeDirectory(docroot)
    '    client.GetCurrentDirectory()
    '    Try
    '        client.ChangeDirectory(docroot & "/" & fFilename)
    '    Catch
    '        client.CreateDirectory(docroot & "/" & fFilename)
    '    End Try

    '    '     client.ChangeDirectory(docroot & "/" & DoctorName & "/" & MonthName() & "/" & DateName)
    '    '  CurrentDirectory = client.GetCurrentDirectory

    '    If UploadBatchFiles(LocalDirPath, docroot & "/" & fFilename, client, StrFileName, fsize) = True Then
    '        ' client.CreateDirectory(docroot & "/databaselog")
    '        ' If UploadLogFile(LocalDirPath, CurrentDirectory, client) = True Then

    '        'End If

    '        If Not Directory.Exists("C:\UploadBackUp\JPGFiles\" & fFilename) Then
    '            Directory.CreateDirectory("C:\UploadBackUp\JPGFiles\" & fFilename)
    '        End If

    '        Try
    '            File.Copy(LocalDirPath & "\" & StrFileName, "C:\UploadBackUp\JPGFiles\" & fFilename & "\" & StrFileName, True)
    '            File.Delete(LocalDirPath & "\" & StrFileName)
    '        Catch ex As Exception
    '        End Try
    '    End If
    '    'client.PutFiles(LocalDirPath, CurrentDirectory, FtpBatchTransferOptions.Recursive, FtpActionOnExistingFiles.OverwriteAll)
    '    Pbar.EditValue = 0
    'End Sub
    'Public Function UploadBatchFiles(ByVal LocalDirPath As String, ByVal CurrentDirectory As String, ByRef client As Ftp, ByVal StrFileName As String, ByVal LocalSize As String) As Boolean
    '    Dim returnval As Boolean
    '    Dim resStr As String
    '    Dim rSize As String
    '    Try
    '        client.PutFile(LocalDirPath & "\" & StrFileName, CurrentDirectory & "/" & StrFileName)
    '        'client.PutFiles(LocalDirPath, CurrentDirectory, FtpBatchTransferOptions.Recursive, FtpActionOnExistingFiles.OverwriteAll)
    '        rSize = client.GetFileLength(CurrentDirectory & "/" & StrFileName)

    '        If rSize = LocalSize Then
    '            returnval = True
    '        Else
    '            returnval = False
    '        End If
    '    Catch ex As Exception
    '        returnval = False
    '    End Try
    '    Return returnval
    'End Function

    Private Sub _ftp_TransferProgress(ByVal sender As Object, ByVal e As FtpTransferProgressEventArgs)
        If _showTransferProgress AndAlso _currentFileLength > 0 Then
            Dim percentage As Integer = CInt(Pbar.Properties.Maximum * CDec(e.BytesTransferred) / CDec(_currentFileLength))
            ' WriteErrorLog("Bytes Transferred :" & e.BytesTransferred)
            lbl_percentage.Text = percentage & "%"
            ' handle the file length changes during transfer
            If percentage > Pbar.Properties.Maximum Then
                percentage = Pbar.Properties.Maximum
            End If
            If percentage <> Pbar.EditValue Then
                Pbar.EditValue = percentage
            End If

            '------------------

            If e.State = FtpTransferState.None OrElse e.BytesSinceLastEvent = 0 Then
                Return
            End If
            Dim time As Integer = Environment.TickCount
            Dim timeSinceLastEvent As Integer
            timeSinceLastEvent = time - _eventTime
            _eventTime = time
            If timeSinceLastEvent = 0 Then
                Return
            End If
            Dim bytesPerSecond As Long = e.BytesSinceLastEvent * 1000 \ timeSinceLastEvent
            If _bytesPerSecond = 0 Then
                _bytesPerSecond = bytesPerSecond
            Else
                _bytesPerSecond = (_bytesPerSecond * 15 + bytesPerSecond) / 16
            End If
            '    Me.Text = "Transfer speed: " & _bytesPerSecond / 1024 & " KB/s"
            'TStrip.Text = "Transfer Progress : " & SetBytes(e.BytesTransferred) & " / " & SetBytes(_currentFileLength) & "     |   Transfer Rate : " & SetBytes(_bytesPerSecond) & "/s"
            Status_Strip_text.Text = "Transfer Progress : " & SetBytes(e.BytesTransferred) & " of " & SetBytes(_currentFileLength) & " (" & SetBytes(_bytesPerSecond) & "/sec) "
        End If

        ' process any application events to prevent the window from freezing
        Application.DoEvents()
    End Sub
    Function SetBytes(ByVal Bytes) As String
        On Error GoTo hell
        If Bytes >= 1073741824 Then
            SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") _
                 & " GB"
        ElseIf Bytes >= 1048576 Then
            SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " MB"
        ElseIf Bytes >= 1024 Then
            SetBytes = Format(Bytes / 1024, "#0.00") & " KB"
        ElseIf Bytes < 1024 Then
            SetBytes = Fix(Bytes) & " Bytes"
        End If
        Exit Function
hell:
        SetBytes = "0 Bytes"
    End Function

    Private Sub startprocess()
        Dim nameOfDirectory As String
        Dim myDirectory As DirectoryInfo
        Dim ProcessMonth, LocalPath As String

        'Dim cmdDBUpdate As SqlCommand
        lbl_Status.Text = "Status: Establishing FTP Connection"
        Application.DoEvents()
        Try
            If FTPLiveConnection() = True Then
                lbl_Status.Text = "Status: FTPSync in progress"
                Me.Height = 296
                Application.DoEvents()
                Pbar.EditValue = 0
                lbl_fname.Text = ""
                lbl_type.Text = ""
                lbl_size.Text = ""
                lblFilesCount.Text = ""
                lbl_percentage.Text = ""
                ProcessMonth = cmb_Month.Text & "-" & cmb_Year.Text
                LocalPath = FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & ProcessMonth & "\"
                If CreateRootDirectories(cmb_Customer.Text, cmb_Store.Text, ProcessMonth) = True Then
                    nameOfDirectory = ""
                    myDirectory = New DirectoryInfo(LocalPath)
                    WorkWithDirectory(myDirectory, cmb_Customer.Text, cmb_Store.Text, ProcessMonth)
                End If
                'ReadDirectories("D-Mart Kukatpally", "September-2012")
                If lbl_Status.Text <> "Status: FTP Connection failure" Then lbl_Status.Text = "Status: Uploading complete" Else lbl_Status.Text = "Status: FTP Connection failed, try after sometime"
                Application.DoEvents()
            Else
                lbl_Status.Text = "Status: FTP Connection failed, try after sometime"
                Application.DoEvents()
            End If

            client.Disconnect()

        Catch ex As Exception
            lbl_Status.Text = "Status: FTP Connection failed, try after sometime"
            Application.DoEvents()
        End Try
    End Sub
    Public Function CreateRootDirectories(ByVal CustomerName As String, ByVal StoreName As String, ByVal MonthYear As String) As Boolean
        Dim returnval As Boolean
        Dim DirPath As String
        Try
            If client.DirectoryExists(FTPRootPath & "/" & CustomerName) = False Then
                client.CreateDirectory(FTPRootPath & "/" & CustomerName)
            End If
            DirPath = FTPRootPath & "/" & CustomerName

            If client.DirectoryExists(DirPath & "/" & StoreName) = False Then
                client.CreateDirectory(DirPath & "/" & StoreName)
            End If
            DirPath = DirPath & "/" & StoreName

            If client.DirectoryExists(DirPath & "/" & MonthYear) = False Then
                client.CreateDirectory(DirPath & "/" & MonthYear)
            End If
            DirPath = DirPath & "/" & MonthYear
            If client.DirectoryExists(DirPath & "/Songs") = False Then
                client.CreateDirectory(DirPath & "/Songs")
            End If

            If client.DirectoryExists(DirPath & "/Promos") = False Then
                client.CreateDirectory(DirPath & "/Promos")
            End If

            returnval = True
        Catch ex As Exception
            XtraMessageBox.Show(ex.ToString, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            returnval = False
        End Try
        Return returnval
    End Function

    Private Sub AppTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AppTimer.Tick
        Dim remaining As TimeSpan = MstopTime.Subtract(Now)
        remaining = New TimeSpan(remaining.Hours, remaining.Minutes, remaining.Seconds)
        If remaining.Minutes < 0 Then remaining = TimeSpan.Zero
        lbl_Status.Text = "Status: Next auto check will be after 00:" & remaining.Minutes.ToString("00") & " minute(s)" '& ":" & remaining.Seconds.ToString("00") & " ]"
        If remaining.Minutes <= 0 Then
            AppTimer.Enabled = False
            Call AutoProcess()
            MstopTime = Now.AddMinutes(60)
            AppTimer.Enabled = True
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'AppTimer.Enabled = False

        startprocess()
        'MstopTime = Now.AddMinutes(5)
        'AppTimer.Enabled = True
    End Sub

    Private Sub frmFTPSync_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If btn_Generate.Enabled = False Then
            e.Cancel = True
        ElseIf btn_Quit.Visible = False AndAlso XtraMessageBox.Show("Due to importance of this application, this is not to be closed. Select 'Yes' if you really want to close", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            e.Cancel = True
        Else
            End
        End If
        '       e.Cancel = True
    End Sub

    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        If (Me.WindowState = FormWindowState.Minimized) AndAlso FTPAutoSync = "True" Then
            Me.Hide()
            'Else
            '    If resizeboo = True Then
            '        Me.Width = 589
            '        Me.Height = 432
            '        'Else
            '        '    Me.Width = 589
            '        '    Me.Height = 244
            '        'End If
        End If
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.TopMost = True
        Me.WindowState = FormWindowState.Normal

    End Sub

    'Public Sub OpenDBConnection()
    '    DBConn = New SqlConnection

    '    DBConn.ConnectionString = "MUDRAGADA-PC\SQLEXPRESS;Database=INSCentral;UID=sa;PWD=mudragada"
    '    '"server=MUDRAGADA-PC;Database=INSCentral;UID=storeadmin;PWD=central"

    '    'DBConn.ConnectionString = "server=" & DBServer & ";Database=INSCentral;UID=" & DBUserName & ";PWD=" & DBPassword
    '    DBConn.Open()

    'End Sub

    'Public Sub CloseDBConnection()
    '    If Not DBConn Is Nothing Then
    '        DBConn.Close()
    '        DBConn = Nothing
    '    End If
    'End Sub
    Private Sub btn_Generate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Generate.Click

        If PromosArchivePath = "" OrElse SongsArchivePath = "" Then
            XtraMessageBox.Show("FilePrep path is not configured", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If cmb_Customer.EditValue = Nothing Then
            XtraMessageBox.Show("Customer is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Customer.Focus()
            Exit Sub
        End If
        If cmb_Store.EditValue = Nothing Then
            XtraMessageBox.Show("Store is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Store.Focus()
            Exit Sub
        End If

        'If dt_From.EditValue = Nothing Or dt_To.EditValue = Nothing Then
        '    XtraMessageBox.Show("'From' date or 'To' date can not be null", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    If dt_From.EditValue = Nothing Then
        '        dt_From.Focus()
        '    Else
        '        dt_To.Focus()
        '    End If
        '    Exit Sub
        'End If

        'If dt_From.EditValue > dt_To.EditValue Then
        '    XtraMessageBox.Show("'From' date can not be later than 'To' date'", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    dt_From.Focus()
        '    Exit Sub
        'End If

        'Dim DateCount As Integer = DateDiff(DateInterval.Day, dt_From.EditValue, dt_To.EditValue) + 1
        'If DateCount > 31 Then
        '    XtraMessageBox.Show("Range of dates can not exceed 31 days", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    dt_To.Focus()
        '    Exit Sub
        'End If
        AppTimer.Enabled = False
        Dim Playlistdate As Date
        Playlistdate = DateSerial(cmb_Year.EditValue, cmb_Month.EditValue, 1)

        Dim FTPEnabled, PlaylistStatus As String

        Dim cmdCheckIfFTPEnabled As New SqlCommand
        Call OpenDBConnection()
        With cmdCheckIfFTPEnabled
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckForFTPSync"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Date", Playlistdate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
            .Parameters.Add("@FTPEnabled", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            .Parameters.Add("@Playlist_Status", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .Parameters.Add("@Upload_Status", SqlDbType.VarChar, 25).Direction = ParameterDirection.Output
            .ExecuteNonQuery()
            FTPEnabled = .Parameters("@FTPEnabled").Value
            PlaylistStatus = .Parameters("@Playlist_Status").Value
        End With
        cmdCheckIfFTPEnabled.Dispose()
        cmdCheckIfFTPEnabled = Nothing
        Call CloseDBConnection()

        If FTPEnabled = "No" Then
            XtraMessageBox.Show("FTPSync is not enabled for this store.", "Alert", MessageBoxButtons.OK)
        ElseIf PlaylistStatus = "Not generated" Then
            XtraMessageBox.Show("Playlist for the selected store and month doesn't exist", "Alert", MessageBoxButtons.OK)
        ElseIf Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & cmb_Month.Text & "-" & cmb_Year.Text) Then
            XtraMessageBox.Show(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & cmb_Month.Text & "-" & cmb_Year.Text & " directory not found", "Alert", MessageBoxButtons.OK)
        Else
            btn_Generate.Enabled = False
            btn_Quit.Enabled = False
            lbl_type.Text = ""
            lbl_fname.Text = ""
            Call FTPSync()
            Me.Height = 208
            btn_Generate.Enabled = True
            btn_Quit.Enabled = True
        End If
      
        If btn_Quit.Visible = False Then AppTimer.Enabled = True
    End Sub
    Private Sub AutoProcess()
        AppTimer.Enabled = False
        If PromosArchivePath = "" OrElse SongsArchivePath = "" Then
            lbl_Status.Text = "Status: FilePrep path is not configured, auto FTP cancelled"
            AppTimer.Enabled = True
            Exit Sub
        End If

        Dim PendingListForFTPSync As New SqlCommand
        Dim drList As SqlDataReader

        Call OpenDBConnection()
        With PendingListForFTPSync
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcPendingListForFTPSync"
            drList = .ExecuteReader
        End With

        If drList.HasRows Then
            btn_Generate.Enabled = False
            lbl_type.Text = ""
            lbl_fname.Text = ""

            While drList.Read
                cmb_Customer.Text = drList("Customer_Name")
                cmb_Store.Text = drList("Store_Name")
                cmb_Month.Text = drList("Month_Name")
                cmb_Year.Text = drList("Year_Number")
                Application.DoEvents()
                Me.Refresh()
                Call FTPSync()

            End While

            Me.Height = 208
            btn_Generate.Enabled = True
        End If

        AppTimer.Enabled = True
        PendingListForFTPSync.Dispose()
        PendingListForFTPSync = Nothing
        Call CloseDBConnection()

        If lbl_Status.Text = "Status: FTPSync completed" Then
            lbl_Status.Text = "Status: Autorun will execute after 01:00 hour"
        Else
            lbl_Status.Text = "Status: FTP connection failed, next check will be after 01:00 hour"
        End If





    End Sub
    Private Sub FTPSync()
        lbl_Status.Text = "Status: FTPSync in progress"
        Application.DoEvents()

        startprocess()
        If lbl_Status.Text = "Status: Uploading complete" Then Call UpdateDB()
        If lbl_Status.Text = "Status: Database updation completed" Then lbl_Status.Text = "Status: FTPSync completed"
        Application.DoEvents()

    End Sub

   
    Private Sub UpdateDB()

        lbl_Status.Text = "Status: Updating database"
        Application.DoEvents()
        Call OpenDBConnection()
        Dim cmdUpdateDB As New SqlCommand
        With cmdUpdateDB
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcUpdatePlayListStatus"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Mode", "MonthWise").Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Month", Convert.ToInt32(cmb_Month.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Year", Convert.ToInt32(cmb_Year.EditValue.ToString)).Direction = ParameterDirection.Input
            .ExecuteNonQuery()
        End With

        Call CloseDBConnection()
        cmdUpdateDB.Dispose()
        lbl_Status.Text = "Status: Database updation completed"
        Application.DoEvents()
        
    End Sub
    Private Function StoreConfigured() As Boolean

        Dim cmdCheckStore As New SqlCommand
        Dim result As String

        Call OpenDBConnection()
        With cmdCheckStore
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckStoreConfiguration"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .ExecuteNonQuery()
            result = .Parameters("@Result").Value
        End With

        If result <> "Success" Then

            XtraMessageBox.Show(result, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False

        Else
            Return True
        End If
        cmdCheckStore.Dispose()

    End Function

    Private Sub LoadCustomers()

        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With

        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)
        cmb_Customer.Properties.Columns(0).Visible = False
        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""

        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub

    Private Sub LoadStores(ByVal Customer_Id As Integer)

        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = dbConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")


        'Creating Columns for Display in lookup
        cmb_Store.Properties.DisplayMember = "Store_Name"
        cmb_Store.Properties.ValueMember = "Store_ID"
        cmb_Store.Properties.DataSource = dsStores.Tables(0)
        cmb_Store.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Store.Properties.PopulateColumns()
        cmb_Store.Properties.Columns(0).Visible = False
        cmb_Store.Properties.ShowFooter = False
        cmb_Store.Properties.ShowHeader = False
        cmb_Store.EditValue = ""

        dsStores.Dispose()
        dapStores.Dispose()


        Call CloseDBConnection()
    End Sub

    Private Sub cmb_Customer_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If cmb_Customer.EditValue.ToString <> "" Then Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString))
    End Sub
    Private Sub btn_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Quit.Click
        End
    End Sub
    Private Sub InitializeDropDowns()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Customer.Properties.DataSource = dt
        cmb_Month.Properties.DataSource = dt
        cmb_Year.Properties.DataSource = dt
        cmb_Store.Properties.DataSource = dt
        cmb_Month.EditValue = ""
        cmb_Year.EditValue = ""
        dt.Dispose()
        btn_Generate.Enabled = False
    End Sub
    Private Sub InitializeStore()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Store.Properties.DataSource = dt
        dt.Dispose()
    End Sub
    Private Sub LoadMonths()
        Dim dt As New DataTable
        dt.Columns.Add("MonthNo")
        dt.Columns.Add("MonthName")

        dt.Rows.Add(1, "January")
        dt.Rows.Add(2, "February")
        dt.Rows.Add(3, "March")
        dt.Rows.Add(4, "April")
        dt.Rows.Add(5, "May")
        dt.Rows.Add(6, "June")
        dt.Rows.Add(7, "July")
        dt.Rows.Add(8, "August") : dt.Rows.Add(9, "September") : dt.Rows.Add(10, "October") : dt.Rows.Add(11, "November") : dt.Rows.Add(12, "December")

        cmb_Month.Properties.DisplayMember = "MonthName"
        cmb_Month.Properties.ValueMember = "MonthNo"
        cmb_Month.Properties.DataSource = dt
        cmb_Month.Properties.ShowFooter = False
        cmb_Month.Properties.ShowHeader = False
        cmb_Month.Properties.PopulateColumns()
        cmb_Month.Properties.Columns(0).Visible = False

        If Today.Day <= 5 Then
            cmb_Month.Text = Today.ToString("MMMM")
        Else
            cmb_Month.Text = Today.AddMonths(1).ToString("MMMM")
        End If

        cmb_Month.Properties.ForceInitialize()
        dt.Dispose()
        dt = Nothing

        Dim dt1 As New DataTable

        dt1.Columns.Add("YearNo")

        For i As Integer = 2012 To 2020
            dt1.Rows.Add(i)
        Next
        cmb_Year.Properties.DataSource = dt1
        cmb_Year.Properties.DisplayMember = "YearNo"
        cmb_Year.Properties.ValueMember = "YearNo"
        cmb_Year.Properties.PopulateColumns()
        cmb_Year.Properties.ShowFooter = False
        cmb_Year.Properties.ShowHeader = False

        If Now.Date.Day <= 5 Then
            cmb_Year.Text = Today.Year.ToString
        Else
            cmb_Year.Text = Today.AddMonths(1).Year.ToString
        End If
        cmb_Year.Properties.ForceInitialize()

        dt1.Dispose()
    End Sub
    Private Sub frmftpsync_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Height = 208

        If Not ReadConfig() Then
           
            Call InitializeDropDowns()
            Exit Sub
        End If

        If FTPAutoSync = "True" Then
            btn_Quit.Visible = False
            btn_Generate.Top = 140
            NotifyIcon1.Visible = True
            Me.ShowInTaskbar = False
            MstopTime = Now.AddMinutes(60)
            AppTimer.Enabled = True
            lbl_Status.Text = "Status: Autorun will execute after 01:00 hour"
        End If

        Call LoadSettings()

        Call LoadCustomers()
        Call InitializeStore()
        Call LoadMonths()

        'ArchivePath = "D:\Works\Audio Repository\Clients"
        FTPRootPath = "/newdev/Instore/Archive"
        client.LogWriter = New RichTextBoxLogWriter(lbox, lbox.MaxLength, Rebex.LogLevel.Info)
        Application.DoEvents()
        'Me.Hide()
        'Application.DoEvents()
        'AppTimer.Enabled = False
        ''startprocess()
     
    End Sub

    Private Sub frmFTPSync_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus

    End Sub
End Class