﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFTPSync1
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFTPSync1))
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.Panel = New DevExpress.XtraEditors.PanelControl()
        Me.cmb_Year = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmb_Month = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmb_Store = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_Generate = New DevExpress.XtraEditors.SimpleButton()
        Me.lbl_Status = New System.Windows.Forms.Label()
        Me.btn_Quit = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.FTP_Panel = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Close = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Minimize = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.lblFilesCount = New System.Windows.Forms.Label()
        Me.lbl_type = New System.Windows.Forms.Label()
        Me.lbl_fname = New System.Windows.Forms.Label()
        Me.lbl_percentage = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Pbar = New DevExpress.XtraEditors.ProgressBarControl()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lbl_curstorename = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Pbar1 = New System.Windows.Forms.ProgressBar()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lbl_counter = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.Status_Strip_text = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txt_filepath = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lbox = New System.Windows.Forms.RichTextBox()
        Me.lbl_size = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.AppTimer = New System.Windows.Forms.Timer(Me.components)
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        CType(Me.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel.SuspendLayout()
        CType(Me.cmb_Year.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Month.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.FTP_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FTP_Panel.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.Pbar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Panel
        '
        Me.Panel.Controls.Add(Me.cmb_Year)
        Me.Panel.Controls.Add(Me.cmb_Month)
        Me.Panel.Controls.Add(Me.Label2)
        Me.Panel.Controls.Add(Me.Label1)
        Me.Panel.Controls.Add(Me.cmb_Store)
        Me.Panel.Controls.Add(Me.Label7)
        Me.Panel.Controls.Add(Me.cmb_Customer)
        Me.Panel.Controls.Add(Me.Label5)
        Me.Panel.Controls.Add(Me.btn_Generate)
        Me.Panel.Controls.Add(Me.lbl_Status)
        Me.Panel.Controls.Add(Me.btn_Quit)
        Me.Panel.Location = New System.Drawing.Point(6, 27)
        Me.Panel.Name = "Panel"
        Me.Panel.Size = New System.Drawing.Size(521, 176)
        Me.Panel.TabIndex = 58
        '
        'cmb_Year
        '
        Me.cmb_Year.Location = New System.Drawing.Point(248, 96)
        Me.cmb_Year.Name = "cmb_Year"
        Me.cmb_Year.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Year.Properties.Appearance.Options.UseFont = True
        Me.cmb_Year.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Year.Size = New System.Drawing.Size(100, 22)
        Me.cmb_Year.TabIndex = 76
        '
        'cmb_Month
        '
        Me.cmb_Month.Location = New System.Drawing.Point(75, 96)
        Me.cmb_Month.Name = "cmb_Month"
        Me.cmb_Month.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Month.Properties.Appearance.Options.UseFont = True
        Me.cmb_Month.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Month.Size = New System.Drawing.Size(114, 22)
        Me.cmb_Month.TabIndex = 75
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(207, 99)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 15)
        Me.Label2.TabIndex = 72
        Me.Label2.Text = "Year:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(28, 99)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 15)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "Month:"
        '
        'cmb_Store
        '
        Me.cmb_Store.EditValue = ""
        Me.cmb_Store.Location = New System.Drawing.Point(75, 58)
        Me.cmb_Store.Name = "cmb_Store"
        Me.cmb_Store.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Store.Properties.Appearance.Options.UseFont = True
        Me.cmb_Store.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Store.Size = New System.Drawing.Size(273, 22)
        Me.cmb_Store.TabIndex = 70
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(33, 60)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 15)
        Me.Label7.TabIndex = 69
        Me.Label7.Text = "Store:"
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(75, 20)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Size = New System.Drawing.Size(273, 22)
        Me.cmb_Customer.TabIndex = 67
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "Customer:"
        '
        'btn_Generate
        '
        Me.btn_Generate.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Generate.Appearance.Options.UseFont = True
        Me.btn_Generate.Location = New System.Drawing.Point(400, 109)
        Me.btn_Generate.Name = "btn_Generate"
        Me.btn_Generate.Size = New System.Drawing.Size(108, 23)
        Me.btn_Generate.TabIndex = 66
        Me.btn_Generate.Text = "Run"
        '
        'lbl_Status
        '
        Me.lbl_Status.AutoSize = True
        Me.lbl_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Status.Location = New System.Drawing.Point(34, 140)
        Me.lbl_Status.Name = "lbl_Status"
        Me.lbl_Status.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Status.TabIndex = 65
        Me.lbl_Status.Text = "Status:"
        '
        'btn_Quit
        '
        Me.btn_Quit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Quit.Appearance.Options.UseFont = True
        Me.btn_Quit.Location = New System.Drawing.Point(400, 140)
        Me.btn_Quit.Name = "btn_Quit"
        Me.btn_Quit.Size = New System.Drawing.Size(108, 23)
        Me.btn_Quit.TabIndex = 62
        Me.btn_Quit.Text = "Exit"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(1, -13)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(1313, 948)
        Me.XtraTabControl1.TabIndex = 59
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage2})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.FTP_Panel)
        Me.XtraTabPage2.Controls.Add(Me.GroupBox1)
        Me.XtraTabPage2.Controls.Add(Me.lbl_counter)
        Me.XtraTabPage2.Controls.Add(Me.StatusStrip1)
        Me.XtraTabPage2.Controls.Add(Me.txt_filepath)
        Me.XtraTabPage2.Controls.Add(Me.GroupBox2)
        Me.XtraTabPage2.Controls.Add(Me.lbl_size)
        Me.XtraTabPage2.Controls.Add(Me.Button1)
        Me.XtraTabPage2.Controls.Add(Me.Label9)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1307, 929)
        '
        'FTP_Panel
        '
        Me.FTP_Panel.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FTP_Panel.Appearance.Options.UseFont = True
        Me.FTP_Panel.AppearanceCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FTP_Panel.AppearanceCaption.Options.UseFont = True
        Me.FTP_Panel.Controls.Add(Me.btn_Close)
        Me.FTP_Panel.Controls.Add(Me.btn_Minimize)
        Me.FTP_Panel.Controls.Add(Me.Panel)
        Me.FTP_Panel.Controls.Add(Me.PanelControl1)
        Me.FTP_Panel.Location = New System.Drawing.Point(-2, -1)
        Me.FTP_Panel.Name = "FTP_Panel"
        Me.FTP_Panel.Size = New System.Drawing.Size(531, 294)
        Me.FTP_Panel.TabIndex = 68
        Me.FTP_Panel.Text = "PlayTM FTPSync"
        '
        'btn_Close
        '
        Me.btn_Close.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Close.Appearance.Options.UseFont = True
        Me.btn_Close.Location = New System.Drawing.Point(503, 0)
        Me.btn_Close.Name = "btn_Close"
        Me.btn_Close.Size = New System.Drawing.Size(24, 21)
        Me.btn_Close.TabIndex = 78
        Me.btn_Close.Text = "X"
        '
        'btn_Minimize
        '
        Me.btn_Minimize.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Minimize.Appearance.Options.UseFont = True
        Me.btn_Minimize.Location = New System.Drawing.Point(478, 0)
        Me.btn_Minimize.Name = "btn_Minimize"
        Me.btn_Minimize.Size = New System.Drawing.Size(24, 21)
        Me.btn_Minimize.TabIndex = 77
        Me.btn_Minimize.Text = "--"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.lblFilesCount)
        Me.PanelControl1.Controls.Add(Me.lbl_type)
        Me.PanelControl1.Controls.Add(Me.lbl_fname)
        Me.PanelControl1.Controls.Add(Me.lbl_percentage)
        Me.PanelControl1.Controls.Add(Me.Label12)
        Me.PanelControl1.Controls.Add(Me.Label11)
        Me.PanelControl1.Controls.Add(Me.Label10)
        Me.PanelControl1.Controls.Add(Me.Pbar)
        Me.PanelControl1.Location = New System.Drawing.Point(7, 206)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(520, 85)
        Me.PanelControl1.TabIndex = 67
        '
        'lblFilesCount
        '
        Me.lblFilesCount.AutoSize = True
        Me.lblFilesCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilesCount.Location = New System.Drawing.Point(446, 54)
        Me.lblFilesCount.Name = "lblFilesCount"
        Me.lblFilesCount.Size = New System.Drawing.Size(31, 15)
        Me.lblFilesCount.TabIndex = 86
        Me.lblFilesCount.Text = "1/10"
        Me.lblFilesCount.Visible = False
        '
        'lbl_type
        '
        Me.lbl_type.AutoSize = True
        Me.lbl_type.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_type.Location = New System.Drawing.Point(75, 12)
        Me.lbl_type.Name = "lbl_type"
        Me.lbl_type.Size = New System.Drawing.Size(33, 15)
        Me.lbl_type.TabIndex = 85
        Me.lbl_type.Text = "Type"
        '
        'lbl_fname
        '
        Me.lbl_fname.AutoSize = True
        Me.lbl_fname.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fname.Location = New System.Drawing.Point(73, 33)
        Me.lbl_fname.Name = "lbl_fname"
        Me.lbl_fname.Size = New System.Drawing.Size(41, 15)
        Me.lbl_fname.TabIndex = 84
        Me.lbl_fname.Text = "Name"
        '
        'lbl_percentage
        '
        Me.lbl_percentage.AutoSize = True
        Me.lbl_percentage.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_percentage.Location = New System.Drawing.Point(405, 54)
        Me.lbl_percentage.Name = "lbl_percentage"
        Me.lbl_percentage.Size = New System.Drawing.Size(18, 15)
        Me.lbl_percentage.TabIndex = 83
        Me.lbl_percentage.Text = "%"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(10, 12)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(59, 15)
        Me.Label12.TabIndex = 82
        Me.Label12.Text = "File Type:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(4, 33)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(67, 15)
        Me.Label11.TabIndex = 81
        Me.Label11.Text = "File Name:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(13, 54)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 15)
        Me.Label10.TabIndex = 80
        Me.Label10.Text = "Progress:"
        '
        'Pbar
        '
        Me.Pbar.Location = New System.Drawing.Point(74, 55)
        Me.Pbar.Name = "Pbar"
        Me.Pbar.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.Pbar.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid
        Me.Pbar.Size = New System.Drawing.Size(325, 16)
        Me.Pbar.TabIndex = 79
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lbl_curstorename)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Pbar1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 302)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(478, 120)
        Me.GroupBox1.TabIndex = 59
        Me.GroupBox1.TabStop = False
        '
        'lbl_curstorename
        '
        Me.lbl_curstorename.AutoSize = True
        Me.lbl_curstorename.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_curstorename.Location = New System.Drawing.Point(96, 18)
        Me.lbl_curstorename.Name = "lbl_curstorename"
        Me.lbl_curstorename.Size = New System.Drawing.Size(0, 13)
        Me.lbl_curstorename.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Store Name :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Pbar1
        '
        Me.Pbar1.Location = New System.Drawing.Point(98, 81)
        Me.Pbar1.Name = "Pbar1"
        Me.Pbar1.Size = New System.Drawing.Size(329, 13)
        Me.Pbar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.Pbar1.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(25, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Progress :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(51, 37)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Type :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(18, 57)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "File Name :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_counter
        '
        Me.lbl_counter.AutoSize = True
        Me.lbl_counter.Location = New System.Drawing.Point(5, 312)
        Me.lbl_counter.Name = "lbl_counter"
        Me.lbl_counter.Size = New System.Drawing.Size(47, 15)
        Me.lbl_counter.TabIndex = 64
        Me.lbl_counter.Text = "asdsda"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Status_Strip_text})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 907)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1307, 22)
        Me.StatusStrip1.TabIndex = 63
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'Status_Strip_text
        '
        Me.Status_Strip_text.Name = "Status_Strip_text"
        Me.Status_Strip_text.Size = New System.Drawing.Size(0, 17)
        '
        'txt_filepath
        '
        Me.txt_filepath.Location = New System.Drawing.Point(481, 302)
        Me.txt_filepath.Name = "txt_filepath"
        Me.txt_filepath.Size = New System.Drawing.Size(122, 21)
        Me.txt_filepath.TabIndex = 66
        Me.txt_filepath.Text = "Z:\PACSUPLOAD_Express"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lbox)
        Me.GroupBox2.Location = New System.Drawing.Point(2, 441)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(478, 128)
        Me.GroupBox2.TabIndex = 62
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "History"
        '
        'lbox
        '
        Me.lbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(237, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.lbox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lbox.Location = New System.Drawing.Point(6, 16)
        Me.lbox.Name = "lbox"
        Me.lbox.Size = New System.Drawing.Size(466, 104)
        Me.lbox.TabIndex = 18
        Me.lbox.Text = ""
        '
        'lbl_size
        '
        Me.lbl_size.AutoSize = True
        Me.lbl_size.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_size.Location = New System.Drawing.Point(580, 238)
        Me.lbl_size.Name = "lbl_size"
        Me.lbl_size.Size = New System.Drawing.Size(0, 13)
        Me.lbl_size.TabIndex = 65
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(557, 374)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(79, 23)
        Me.Button1.TabIndex = 61
        Me.Button1.Text = "Send &Now"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(540, 238)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(42, 13)
        Me.Label9.TabIndex = 60
        Me.Label9.Text = "Size :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'AppTimer
        '
        Me.AppTimer.Interval = 60000
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "In-Store FTPSync"
        '
        'frmFTPSync1
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(533, 208)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmFTPSync1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PlayTM FTPSync"
        CType(Me.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel.ResumeLayout(False)
        Me.Panel.PerformLayout()
        CType(Me.cmb_Year.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Month.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.FTP_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FTP_Panel.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.Pbar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_Generate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbl_Status As System.Windows.Forms.Label
    Friend WithEvents btn_Quit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cmb_Store As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmb_Year As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmb_Month As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lbl_curstorename As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Pbar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lbl_counter As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents Status_Strip_text As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txt_filepath As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lbox As System.Windows.Forms.RichTextBox
    Friend WithEvents lbl_size As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents AppTimer As System.Windows.Forms.Timer
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Pbar As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents lbl_percentage As System.Windows.Forms.Label
    Friend WithEvents lbl_fname As System.Windows.Forms.Label
    Friend WithEvents lbl_type As System.Windows.Forms.Label
    Friend WithEvents lblFilesCount As System.Windows.Forms.Label
    Friend WithEvents FTP_Panel As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_Close As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Minimize As DevExpress.XtraEditors.SimpleButton
End Class
