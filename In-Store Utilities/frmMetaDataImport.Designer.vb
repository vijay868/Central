﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMetaDataImport
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMetaDataImport))
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.BrowseFile = New System.Windows.Forms.OpenFileDialog()
        Me.Genre_Panel = New DevExpress.XtraEditors.PanelControl()
        Me.cmb_Genre_Category = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_showhideerrors = New DevExpress.XtraEditors.SimpleButton()
        Me.Conversion_Grid = New System.Windows.Forms.DataGridView()
        Me.Song_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Title = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.file_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Comments = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConversionBar = New DevExpress.XtraEditors.MarqueeProgressBarControl()
        Me.btn_Generate = New DevExpress.XtraEditors.SimpleButton()
        Me.cmb_Genre = New DevExpress.XtraEditors.LookUpEdit()
        Me.btn_Browse = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_FilePath = New DevExpress.XtraEditors.TextEdit()
        Me.lbl_Status = New System.Windows.Forms.Label()
        Me.btn_Quit = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Import = New DevExpress.XtraEditors.SimpleButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.Group_Data = New DevExpress.XtraEditors.GroupControl()
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Genre_Panel.SuspendLayout()
        CType(Me.cmb_Genre_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Conversion_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConversionBar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Genre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_FilePath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.Group_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Group_Data.SuspendLayout()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Genre_Panel
        '
        Me.Genre_Panel.Controls.Add(Me.cmb_Genre_Category)
        Me.Genre_Panel.Controls.Add(Me.Label3)
        Me.Genre_Panel.Controls.Add(Me.btn_showhideerrors)
        Me.Genre_Panel.Controls.Add(Me.Conversion_Grid)
        Me.Genre_Panel.Controls.Add(Me.ConversionBar)
        Me.Genre_Panel.Controls.Add(Me.btn_Generate)
        Me.Genre_Panel.Controls.Add(Me.cmb_Genre)
        Me.Genre_Panel.Controls.Add(Me.btn_Browse)
        Me.Genre_Panel.Controls.Add(Me.txt_FilePath)
        Me.Genre_Panel.Controls.Add(Me.lbl_Status)
        Me.Genre_Panel.Controls.Add(Me.btn_Quit)
        Me.Genre_Panel.Controls.Add(Me.btn_Import)
        Me.Genre_Panel.Controls.Add(Me.Label2)
        Me.Genre_Panel.Controls.Add(Me.Label1)
        Me.Genre_Panel.Location = New System.Drawing.Point(3, 21)
        Me.Genre_Panel.Name = "Genre_Panel"
        Me.Genre_Panel.Size = New System.Drawing.Size(564, 375)
        Me.Genre_Panel.TabIndex = 58
        '
        'cmb_Genre_Category
        '
        Me.cmb_Genre_Category.EditValue = ""
        Me.cmb_Genre_Category.Location = New System.Drawing.Point(335, 20)
        Me.cmb_Genre_Category.Name = "cmb_Genre_Category"
        Me.cmb_Genre_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Genre_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Genre_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Genre_Category.Properties.NullText = ""
        Me.cmb_Genre_Category.Size = New System.Drawing.Size(172, 22)
        Me.cmb_Genre_Category.TabIndex = 69
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(274, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 15)
        Me.Label3.TabIndex = 70
        Me.Label3.Text = "Category:"
        '
        'btn_showhideerrors
        '
        Me.btn_showhideerrors.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_showhideerrors.Appearance.Options.UseFont = True
        Me.btn_showhideerrors.Location = New System.Drawing.Point(435, 139)
        Me.btn_showhideerrors.Name = "btn_showhideerrors"
        Me.btn_showhideerrors.Size = New System.Drawing.Size(108, 23)
        Me.btn_showhideerrors.TabIndex = 68
        Me.btn_showhideerrors.Text = "Show Errors"
        Me.btn_showhideerrors.Visible = False
        '
        'Conversion_Grid
        '
        Me.Conversion_Grid.AllowUserToAddRows = False
        Me.Conversion_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.Conversion_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.Conversion_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Conversion_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Conversion_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Conversion_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Conversion_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.Conversion_Grid.ColumnHeadersHeight = 25
        Me.Conversion_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.Conversion_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Song_ID, Me.Title, Me.file_name, Me.Comments})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Conversion_Grid.DefaultCellStyle = DataGridViewCellStyle3
        Me.Conversion_Grid.EnableHeadersVisualStyles = False
        Me.Conversion_Grid.Location = New System.Drawing.Point(5, 176)
        Me.Conversion_Grid.Name = "Conversion_Grid"
        Me.Conversion_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Conversion_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.Conversion_Grid.RowHeadersVisible = False
        Me.Conversion_Grid.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Conversion_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Conversion_Grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Conversion_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Conversion_Grid.Size = New System.Drawing.Size(554, 195)
        Me.Conversion_Grid.TabIndex = 67
        '
        'Song_ID
        '
        Me.Song_ID.HeaderText = "Song ID"
        Me.Song_ID.Name = "Song_ID"
        '
        'Title
        '
        Me.Title.HeaderText = "Title"
        Me.Title.Name = "Title"
        '
        'file_name
        '
        Me.file_name.HeaderText = "File Name"
        Me.file_name.Name = "file_name"
        '
        'Comments
        '
        Me.Comments.HeaderText = "Comments"
        Me.Comments.Name = "Comments"
        Me.Comments.Width = 250
        '
        'ConversionBar
        '
        Me.ConversionBar.EditValue = 0
        Me.ConversionBar.Location = New System.Drawing.Point(319, 140)
        Me.ConversionBar.Name = "ConversionBar"
        Me.ConversionBar.Properties.ProgressAnimationMode = DevExpress.Utils.Drawing.ProgressAnimationMode.Cycle
        Me.ConversionBar.Size = New System.Drawing.Size(224, 21)
        Me.ConversionBar.TabIndex = 60
        Me.ConversionBar.Visible = False
        '
        'btn_Generate
        '
        Me.btn_Generate.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Generate.Appearance.Options.UseFont = True
        Me.btn_Generate.Location = New System.Drawing.Point(554, 82)
        Me.btn_Generate.Name = "btn_Generate"
        Me.btn_Generate.Size = New System.Drawing.Size(26, 23)
        Me.btn_Generate.TabIndex = 66
        Me.btn_Generate.Text = "Generate"
        Me.btn_Generate.Visible = False
        '
        'cmb_Genre
        '
        Me.cmb_Genre.EditValue = ""
        Me.cmb_Genre.Location = New System.Drawing.Point(90, 20)
        Me.cmb_Genre.Name = "cmb_Genre"
        Me.cmb_Genre.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Genre.Properties.Appearance.Options.UseFont = True
        Me.cmb_Genre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Genre.Properties.NullText = ""
        Me.cmb_Genre.Size = New System.Drawing.Size(172, 22)
        Me.cmb_Genre.TabIndex = 58
        '
        'btn_Browse
        '
        Me.btn_Browse.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Browse.Appearance.Options.UseFont = True
        Me.btn_Browse.Location = New System.Drawing.Point(507, 61)
        Me.btn_Browse.Name = "btn_Browse"
        Me.btn_Browse.Size = New System.Drawing.Size(36, 22)
        Me.btn_Browse.TabIndex = 60
        Me.btn_Browse.Text = "..."
        '
        'txt_FilePath
        '
        Me.txt_FilePath.EditValue = ""
        Me.txt_FilePath.Location = New System.Drawing.Point(90, 61)
        Me.txt_FilePath.Name = "txt_FilePath"
        Me.txt_FilePath.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_FilePath.Properties.Appearance.Options.UseFont = True
        Me.txt_FilePath.Size = New System.Drawing.Size(415, 22)
        Me.txt_FilePath.TabIndex = 59
        '
        'lbl_Status
        '
        Me.lbl_Status.AutoSize = True
        Me.lbl_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Status.Location = New System.Drawing.Point(34, 140)
        Me.lbl_Status.Name = "lbl_Status"
        Me.lbl_Status.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Status.TabIndex = 65
        Me.lbl_Status.Text = "Status:"
        '
        'btn_Quit
        '
        Me.btn_Quit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Quit.Appearance.Options.UseFont = True
        Me.btn_Quit.Location = New System.Drawing.Point(434, 105)
        Me.btn_Quit.Name = "btn_Quit"
        Me.btn_Quit.Size = New System.Drawing.Size(108, 23)
        Me.btn_Quit.TabIndex = 62
        Me.btn_Quit.Text = "Exit"
        '
        'btn_Import
        '
        Me.btn_Import.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Import.Appearance.Options.UseFont = True
        Me.btn_Import.Enabled = False
        Me.btn_Import.Location = New System.Drawing.Point(319, 105)
        Me.btn_Import.Name = "btn_Import"
        Me.btn_Import.Size = New System.Drawing.Size(108, 23)
        Me.btn_Import.TabIndex = 61
        Me.btn_Import.Text = "Import"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(11, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 15)
        Me.Label2.TabIndex = 64
        Me.Label2.Text = "Select  File:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(37, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 15)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "Genre:"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(1, -13)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(1313, 948)
        Me.XtraTabControl1.TabIndex = 59
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage2})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.Group_Data)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1307, 929)
        '
        'Group_Data
        '
        Me.Group_Data.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Group_Data.Controls.Add(Me.Genre_Panel)
        Me.Group_Data.Location = New System.Drawing.Point(-2, 0)
        Me.Group_Data.Name = "Group_Data"
        Me.Group_Data.Size = New System.Drawing.Size(568, 190)
        Me.Group_Data.TabIndex = 59
        Me.Group_Data.Text = "Metadata Import"
        '
        'frmMetaDataImport
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(570, 195)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMetaDataImport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Meta Data Import"
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Genre_Panel.ResumeLayout(False)
        Me.Genre_Panel.PerformLayout()
        CType(Me.cmb_Genre_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Conversion_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConversionBar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Genre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_FilePath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.Group_Data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Group_Data.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents BrowseFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Genre_Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_Generate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmb_Genre As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btn_Browse As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_FilePath As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl_Status As System.Windows.Forms.Label
    Friend WithEvents btn_Quit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Import As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ConversionBar As DevExpress.XtraEditors.MarqueeProgressBarControl
    Friend WithEvents Conversion_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents btn_showhideerrors As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Song_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents file_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Comments As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Group_Data As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cmb_Genre_Category As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
