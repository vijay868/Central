﻿Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Public Class frmGeneratePlaylist
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer
    Private Sub Group_Utilities_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Group_Utilities.MouseDown
        drag = True
        mousex = Windows.Forms.Cursor.Position.X - Me.Left
        mousey = Windows.Forms.Cursor.Position.Y - Me.Top
    End Sub

    Private Sub Group_Utilities_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Group_Utilities.MouseMove
        If drag Then
            Me.Top = Windows.Forms.Cursor.Position.Y - mousey
            Me.Left = Windows.Forms.Cursor.Position.X - mousex
        End If
    End Sub

    Private Sub Group_Utilities_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Group_Utilities.MouseUp
        drag = False
    End Sub
    Private Sub btn_Generate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Generate.Click

        If PromosArchivePath = "" OrElse SongsArchivePath = "" Then
            XtraMessageBox.Show("FilePrep path is not configured", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If cmb_Customer.EditValue = Nothing Then
            XtraMessageBox.Show("Customer is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Customer.Focus()
            Exit Sub
        End If
        If cmb_Store.EditValue = Nothing Then
            XtraMessageBox.Show("Store is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Store.Focus()
            Exit Sub
        End If

        If dt_from.EditValue = Nothing Or dt_to.EditValue = Nothing Then
            XtraMessageBox.Show("'From' date or 'To' date can not be null", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            If dt_from.EditValue = Nothing Then
                dt_from.Focus()
            Else
                dt_to.Focus()
            End If
            Exit Sub
        End If

        If dt_from.EditValue > dt_to.EditValue Then
            XtraMessageBox.Show("'From' date can not be later than 'To' date'", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            dt_from.Focus()
            Exit Sub
        End If

        If Me.Text = "Generete Playlist" Then
            Call GeneratePlaylists()
        ElseIf Me.Text = "Delete Playlist" Then
            Call DeletePlaylists()
        ElseIf Me.Text = "FTPSync" Then
            XtraMessageBox.Show("Not yet done")
        ElseIf Me.Text = "FilePrep" Then
            Call FileSync("Direct")
        End If

        Call FileSync("Direct")
    End Sub
    Private Sub FileSync(ByVal Mode As String)

        'If Not CheckIfPlayListGenerated() Then
        '    XtraMessageBox.Show("Playlist for the selected store and month doesn't exist", "Alert", MessageBoxButtons.OK)
        '    Exit Sub
        'End If

        If Mode = "Direct" Then
            lbl_Status.Text = "Status: Creating folders"
        ElseIf Mode = "Generate" Then
            lbl_Status.Text = "Status: Executing Fileprep - Creating folders"
        End If
        Application.DoEvents()

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text) Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text)
        End If

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text) Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text)
        End If

        
        If Mode = "Direct" Then
            lbl_Status.Text = "Status: Preparing playlist XML files"
        ElseIf Mode = "Generate" Then
            lbl_Status.Text = "Status: Executing Fileprep - Preparing playlist XML files"
        End If
        Application.DoEvents()

        Dim cmdPlaylist As New SqlCommand
        Dim dapPlaylist As New SqlDataAdapter, dsPlaylist As New DataSet, dsSongs As New DataSet, dsPromos As New DataSet

        Dim clsInstore As New ClsEncryption

        Call OpenDBConnection()
        Dim cmdUpdateDB As New SqlCommand
        With cmdUpdateDB
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcUpdatePlayListStatus"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
        End With

        With cmdPlaylist
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcGetPlaylistForSelectedDate"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 2).Direction = ParameterDirection.Input
        End With

        Dim StartDate, EndDate As Date
        Dim CurDate As String, bOverwrite As Boolean = False


        StartDate = dt_from.EditValue : EndDate = dt_to.EditValue
        'StartDate = DateSerial(Convert.ToInt32(cmb_Year.EditValue.ToString), Convert.ToInt32(cmb_Month.EditValue.ToString), 1)
        'EndDate = StartDate.AddMonths(1).AddDays(-1)

        Dim FileCopyPath As String
        If Mode = "Direct" Then
            While StartDate <= EndDate
                lbl_Status.Text = "Status: Checking if already generated"

                If CheckIfPlayListExists(StartDate) Then
                    If XtraMessageBox.Show("Playlists were already generated for few of the selected dates, do you want to overwrite?", "Alert", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    Else
                        Exit While
                    End If
                End If
                StartDate = DateAdd(DateInterval.Day, 1, StartDate)
            End While

        End If

        StartDate = dt_from.EditValue : EndDate = dt_to.EditValue
        While StartDate <= EndDate
            If Mode = "Direct" Then
                lbl_Status.Text = "Status: Creating playlist XML file for '" & StartDate.ToString("dd/MM/yyyy").Replace("-", "/") & "'"
            ElseIf Mode = "Generate" Then
                lbl_Status.Text = "Status: Executing Fileprep - Creating playlist XML file for '" & StartDate.ToString("dd/MM/yyyy").Replace("-", "/") & "'"
            End If

            Application.DoEvents()
            CurDate = StartDate.ToString("MM/dd/yyyy").Replace("-", "/")
            cmdPlaylist.Parameters("@Playlist_Date").Value = CurDate

            If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & StartDate.ToString("MMMM") & "-" & StartDate.ToString("yyyy")) Then
                Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & StartDate.ToString("MMMM") & "-" & StartDate.ToString("yyyy"))
            End If

            If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & StartDate.ToString("MMMM") & "-" & StartDate.ToString("yyyy") & "\Songs") Then
                Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & StartDate.ToString("MMMM") & "-" & StartDate.ToString("yyyy") & "\Songs")
            End If

            If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & StartDate.ToString("MMMM") & "-" & StartDate.ToString("yyyy") & "\Promos") Then
                Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & StartDate.ToString("MMMM") & "-" & StartDate.ToString("yyyy") & "\Promos")
            End If

            FileCopyPath = FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & StartDate.ToString("MMMM") & "-" & StartDate.ToString("yyyy")



            ' MsgBox(cmdPlaylist.Parameters("@Store_Id").Value.ToString)
            ' MsgBox(cmdPlaylist.Parameters("@Playlist_Date").Value.ToString)
            dapPlaylist.SelectCommand = cmdPlaylist
            dsPlaylist.Clear()
            dapPlaylist.Fill(dsPlaylist, "Playlist")

            'If File.Exists(FileCopyPath & "\" & CurDate.Replace("/", "-") & ".xml") Then
            '    File.Delete(FileCopyPath & "\" & CurDate.Replace("/", "-") & ".xml")
            'End If

            If File.Exists(FileCopyPath & "\" & StartDate.ToString("dd-MM-yyyy").Replace("/", "-") & ".xml") Then
                File.Delete(FileCopyPath & "\" & StartDate.ToString("dd-MM-yyyy").Replace("/", "-") & ".xml")
            End If

            dsPlaylist.WriteXml(FileCopyPath & "\" & StartDate.ToString("dd-MM-yyyy").Replace("/", "-") & ".xml")
            Application.DoEvents()

            'update database
            cmdUpdateDB.Parameters("@Playlist_Date").Value = CurDate
            cmdUpdateDB.ExecuteNonQuery()

            StartDate = DateAdd(DateInterval.Day, 1, StartDate)
        End While

        'Active playlist and file copying
        If Mode = "Direct" Then
            lbl_Status.Text = "Status: Preparing Active List XML file"
        ElseIf Mode = "Generate" Then
            lbl_Status.Text = "Status: Executing Fileprep - Preparing Active List XML file"
        End If


        Application.DoEvents()

        With cmdPlaylist
            .Parameters.Clear()
            .CommandText = "ProcGetStoreSongPromoList"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Filter_Criteria", "Songs").Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 1).Direction = ParameterDirection.Input
        End With

        dapPlaylist.SelectCommand = cmdPlaylist
        dsPlaylist.Clear()
        dapPlaylist.Fill(dsPlaylist, "ActiveSongs")

        cmdPlaylist.Parameters("@Filter_Criteria").Value = "Promos"
        dapPlaylist.SelectCommand = cmdPlaylist
        dapPlaylist.Fill(dsPlaylist, "ActivePromos")

        Dim rCount As Integer, totRows As Integer
        Dim FileName As String

        ' MsgBox(dsPromos.Tables("ActivePromos").Rows.Count)
        'MsgBox(dsSongs.Tables("ActiveSongs").Rows.Count)

        cmdPlaylist.CommandText = "ProcGetPlaylistDatesForStore"
        cmdPlaylist.Parameters.Clear()
        cmdPlaylist.Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        'cmdPlaylist.Parameters.AddWithValue("@Month_Number", Convert.ToInt32(cmb_Month.EditValue.ToString)).Direction = ParameterDirection.Input
        'cmdPlaylist.Parameters.AddWithValue("@Year_Number", Convert.ToInt32(cmb_Year.EditValue.ToString)).Direction = ParameterDirection.Input
        cmdPlaylist.Parameters.AddWithValue("@Month_Number", SqlDbType.Int).Direction = ParameterDirection.Input
        cmdPlaylist.Parameters.AddWithValue("@Year_Number", SqlDbType.Int).Direction = ParameterDirection.Input


        Dim DtTable As DataTable
        Dim SelMonth, SelYear As Integer

        StartDate = dt_from.EditValue : EndDate = dt_to.EditValue
        'StartDate = DateSerial(Convert.ToInt32(cmb_Year.EditValue.ToString), Convert.ToInt32(cmb_Month.EditValue.ToString), 1)
        'EndDate = StartDate.AddMonths(1).AddDays(-1)

        While StartDate <= EndDate

            If SelMonth <> CInt(StartDate.ToString("MM")) Then
                FileCopyPath = FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & StartDate.ToString("MMMM") & "-" & StartDate.ToString("yyyy")

                SelMonth = CInt(StartDate.ToString("MM"))
                SelYear = CInt(StartDate.ToString("yyyy"))
                cmdPlaylist.Parameters("@Month_Number").Value = SelMonth
                cmdPlaylist.Parameters("@Year_Number").Value = SelYear
                dapPlaylist.SelectCommand = cmdPlaylist
                'dsPlaylist.Tables.Clear()
                If dsPlaylist.Tables.Contains("PlaylistDates") Then
                    dsPlaylist.Tables.Remove("PlaylistDates")
                    dsPlaylist.Tables.Remove("PlaylistMonth")
                End If

                'dsPlaylist.Clear()
                dapPlaylist.Fill(dsPlaylist, "PlaylistDates")

                DtTable = New DataTable("PlaylistMonth")
                DtTable.Columns.Add("MonthNumber")
                DtTable.Columns.Add("YearNumber")

                DtTable.Rows.Add(StartDate.ToString("MMMM"), StartDate.ToString("yyyy"))

                dsPlaylist.Tables.Add(DtTable)

                If File.Exists(FileCopyPath & "\Active List.xml") Then
                    File.Delete(FileCopyPath & "\Active List.xml")
                End If

                dsPlaylist.WriteXml(FileCopyPath & "\Active List.xml")
                DtTable = Nothing

                Application.DoEvents()

                If Mode = "Direct" Then
                    lbl_Status.Text = "Status: Copying Songs"
                ElseIf Mode = "Generate" Then
                    lbl_Status.Text = "Status: Executing Fileprep -  Copying Songs"
                End If

                Application.DoEvents()
                'MsgBox(dsPlaylist.Tables(1).TableName.ToString)
                totRows = dsPlaylist.Tables("ActiveSongs").Rows.Count
                For rCount = 0 To dsPlaylist.Tables("ActiveSongs").Rows.Count - 1
                    If Mode = "Direct" Then
                        lbl_Status.Text = "Status: Copying Songs (" & (rCount + 1).ToString & "/" & totRows.ToString & ")"
                    ElseIf Mode = "Generate" Then
                        lbl_Status.Text = "Status: Executing Fileprep -  Copying Songs (" & (rCount + 1).ToString & "/" & totRows.ToString & ")"
                    End If

                    Application.DoEvents()
                    FileName = dsPlaylist.Tables("ActiveSongs").Rows(rCount)("FileName").ToString
                    If File.Exists(SongsArchivePath & "\" & FileName) Then
                        'File.Copy(SongsArchivePath & "\" & FileName, FileCopyPath & "\Songs\" & FileName, True)
                        Call clsInstore.EncryptFile(SongsArchivePath & "\" & FileName, FileCopyPath & "\Songs\" & FileName)
                        Application.DoEvents()
                    End If
                Next


                totRows = dsPlaylist.Tables("ActivePromos").Rows.Count
                If Mode = "Direct" Then
                    lbl_Status.Text = "Status: Copying Promos"
                ElseIf Mode = "Generate" Then
                    lbl_Status.Text = "Status: Executing Fileprep - Copying Promos"
                End If

                Application.DoEvents()

                For rCount = 0 To dsPlaylist.Tables("ActivePromos").Rows.Count - 1
                    If Mode = "Direct" Then
                        lbl_Status.Text = "Status: Copying Promos (" & (rCount + 1).ToString & "/" & totRows.ToString & ")"
                    ElseIf Mode = "Generate" Then
                        lbl_Status.Text = "Status: Executing Fileprep - Copying Promos (" & (rCount + 1).ToString & "/" & totRows.ToString & ")"
                    End If

                    Application.DoEvents()
                    FileName = dsPlaylist.Tables("ActivePromos").Rows(rCount)("FileName").ToString
                    If File.Exists(PromosArchivePath & "\" & FileName) Then
                        File.Copy(PromosArchivePath & "\" & FileName, FileCopyPath & "\Promos\" & FileName, True)
                        Application.DoEvents()
                    End If
                Next
            End If

            StartDate = DateAdd(DateInterval.Day, 1, StartDate)
        End While

        lbl_Status.Text = "Status: Audio copying completed"
        Application.DoEvents()

        clsInstore = Nothing
        dsPlaylist.Dispose()
        'dsSongs.Dispose()
        'dsPromos.Dispose()
        dapPlaylist.Dispose()
        cmdPlaylist.Dispose()
        Call CloseDBConnection()
        lbl_Status.Text = "Status: Fileprep completed"
        Application.DoEvents()

        'exec ProcGetPlaylistDatesForStore 1
        'exec ProcGetPlaylistForSelectedDate 1,'08/01/2012'

    End Sub
    Private Function CheckIfPlayListGenerated() As Boolean
        Dim Playlistdate As Date, Res As Boolean = True
        Playlistdate = DateSerial(cmb_Year.EditValue, cmb_Month.EditValue, 1)

        Dim FTPEnabled, PlaylistStatus As String

        Dim cmdCheckIfFTPEnabled As New SqlCommand
        Call OpenDBConnection()
        With cmdCheckIfFTPEnabled
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckForFTPSync"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Date", Playlistdate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
            .Parameters.Add("@FTPEnabled", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            .Parameters.Add("@Playlist_Status", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .Parameters.Add("@Upload_Status", SqlDbType.VarChar, 25).Direction = ParameterDirection.Output
            .Parameters.AddWithValue("@From_Utility", 0).Direction = ParameterDirection.Input
            .ExecuteNonQuery()
            FTPEnabled = .Parameters("@FTPEnabled").Value
            PlaylistStatus = .Parameters("@Playlist_Status").Value
        End With
        cmdCheckIfFTPEnabled.Dispose()
        cmdCheckIfFTPEnabled = Nothing
        Call CloseDBConnection()

        If PlaylistStatus = "Not generated" Then
            Res = False
        End If
        Return Res
    End Function
    Private Function CheckIfPlayListExists(ByVal PlaylistDate As Date) As Boolean
        Dim Res As Boolean = True
        'Playlistdate = DateSerial(cmb_Year.EditValue, cmb_Month.EditValue, 1)

        Dim FTPEnabled, PlaylistStatus As String

        Dim cmdCheckIfFTPEnabled As New SqlCommand
        Call OpenDBConnection()
        With cmdCheckIfFTPEnabled
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckForFTPSync"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Date", Playlistdate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
            .Parameters.Add("@FTPEnabled", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            .Parameters.Add("@Playlist_Status", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .Parameters.Add("@Upload_Status", SqlDbType.VarChar, 25).Direction = ParameterDirection.Output
            .Parameters.AddWithValue("@From_Utility", 0).Direction = ParameterDirection.Input
            .ExecuteNonQuery()
            FTPEnabled = .Parameters("@FTPEnabled").Value
            PlaylistStatus = .Parameters("@Playlist_Status").Value
        End With
        cmdCheckIfFTPEnabled.Dispose()
        cmdCheckIfFTPEnabled = Nothing
        Call CloseDBConnection()

        If PlaylistStatus = "Not generated" Then
            Res = False
        End If
        Return Res
    End Function
    Private Sub DeletePlaylists()

        'If Not CheckIfPlayListGenerated() Then
        '    XtraMessageBox.Show("Playlist for the selected store and month doesn't exist", "Alert", MessageBoxButtons.OK)
        '    Exit Sub
        'End If

        lbl_Status.Text = "Status: Deleting playlists for the selected dates"
        Application.DoEvents()

        btn_Generate.Enabled = False
        btn_Quit.Enabled = False

        Dim cmdPlaylist As New SqlCommand

        Call OpenDBConnection()
        With cmdPlaylist
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcDeletePlayList"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
        End With

        Dim StartDate, EndDate As Date
        Dim CurDate As String
        StartDate = dt_from.EditValue : EndDate = dt_to.EditValue
        'StartDate = DateSerial(Convert.ToInt32(cmb_Year.EditValue.ToString), Convert.ToInt32(cmb_Month.EditValue.ToString), 1)
        'EndDate = StartDate.AddMonths(1).AddDays(-1)

        While StartDate <= EndDate
            lbl_Status.Text = "Status: Currently deleting playlist for '" & StartDate.ToString("dd/MM/yyyy").Replace("-", "/") & "'"
            Application.DoEvents()
            CurDate = StartDate.ToString("MM/dd/yyyy").Replace("-", "/")
            cmdPlaylist.Parameters("@Playlist_Date").Value = CurDate
            cmdPlaylist.ExecuteNonQuery()
            If File.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & CurDate.Replace("/", "-") & ".xml") Then
                File.Delete(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & CurDate.Replace("/", "-") & ".xml")
                Application.DoEvents()
            End If
            'MsgBox(cmdPlaylist.Parameters("@New_Playlist_Id").Value.ToString)
            StartDate = DateAdd(DateInterval.Day, 1, StartDate)
        End While

        cmdPlaylist.Dispose()
        Call CloseDBConnection()

        lbl_Status.Text = "Status: Currently deleting Songs and Promos audio files"
        Application.DoEvents()

        'If Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & cmb_Month.Text & "-" & cmb_Year.Text) Then
        '    Directory.Delete(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & cmb_Month.Text & "-" & cmb_Year.Text, True)
        '    Application.DoEvents()
        'End If
        lbl_Status.Text = "Status: Playlist deletion completed"
        Application.DoEvents()

        btn_Generate.Enabled = True
        btn_Quit.Enabled = True


    End Sub
    Private Function StoreConfigured() As Boolean

        Dim cmdCheckStore As New SqlCommand
        Dim result As String

        Call OpenDBConnection()
        With cmdCheckStore
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckStoreConfiguration"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .ExecuteNonQuery()
            result = .Parameters("@Result").Value
        End With

        If result <> "Success" Then

            XtraMessageBox.Show(result, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False

        Else
            Return True
        End If
        cmdCheckStore.Dispose()

    End Function
    Private Sub GeneratePlaylists()

        lbl_Status.Text = "Status: Checking Store Configuration"
        Application.DoEvents()

        If Not StoreConfigured() Then
            lbl_Status.Text = "Status:"
            Application.DoEvents()
            Exit Sub
        End If

        lbl_Status.Text = "Status: Checking database"
        Application.DoEvents()

        'MsgBox(DateSerial(cmb_Year.EditValue.ToString, cmb_Month.EditValue.ToString, 1).ToString("MM/dd/yyyy").Replace("-", "/"))
        'Checking if playlist already generated
        Dim cmdPlaylist As New SqlCommand
        Dim StartDate, EndDate As Date
        Dim CurDate As String

        Call OpenDBConnection()
        With cmdPlaylist
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckIfPlaylistGenerated"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            '.ExecuteNonQuery()
        End With

       
        StartDate = dt_from.EditValue : EndDate = dt_to.EditValue
        'StartDate = DateSerial(Convert.ToInt32(cmb_Year.EditValue.ToString), Convert.ToInt32(cmb_Month.EditValue.ToString), 1)
        'EndDate = StartDate.AddMonths(1).AddDays(-1)

        While StartDate <= EndDate
            lbl_Status.Text = "Status: Checking if already generated"
            cmdPlaylist.Parameters("@Playlist_Date").Value = StartDate.ToString("MM/dd/yyyy").Replace("-", "/")
            cmdPlaylist.ExecuteNonQuery()
            If cmdPlaylist.Parameters("@Result").Value.ToString = "Already generated" Then
                If XtraMessageBox.Show("Playlists were already generated for few of the selected dates, do you want to overwrite?", "Alert", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then
                    lbl_Status.Text = "Status: Playlist generation cancelled"
                    Exit Sub
                Else
                    Exit While
                End If
            End If
            StartDate = DateAdd(DateInterval.Day, 1, StartDate)
        End While

        'If cmdPlaylist.Parameters("@Result").Value.ToString = "Already generated" Then
        '    If XtraMessageBox.Show("Playlist for the " & cmb_Month.Text & "-" & cmb_Year.Text & " exits. Do you want to overwrite?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
        '        lbl_Status.Text = "Status: Regenerating"
        '        Application.DoEvents()
        '    Else
        '        lbl_Status.Text = "Status:"
        '        Application.DoEvents()
        '        Exit Sub
        '    End If
        'End If

        lbl_Status.Text = "Status: Generating playlists for the selected dates"
        Application.DoEvents()

        btn_Generate.Enabled = False
        btn_Quit.Enabled = False

        'Dim cmdPlaylist As New SqlCommand

        'Call OpenDBConnection()
        'With cmdPlaylist
        '    .Connection = DBConn
        '    .CommandTimeout = 300
        '    .CommandType = CommandType.StoredProcedure
        '    .CommandText = "ProcGeneratePlayList"
        '    .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        '    .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
        '    .Parameters.Add("@New_Playlist_Id", SqlDbType.BigInt).Direction = ParameterDirection.Output
        'End With


        cmdPlaylist.Parameters.Clear()
        With cmdPlaylist
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcGeneratePlayList"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
            .Parameters.Add("@New_Playlist_Id", SqlDbType.BigInt).Direction = ParameterDirection.Output
        End With

        StartDate = dt_from.EditValue : EndDate = dt_to.EditValue
        While StartDate <= EndDate
            lbl_Status.Text = "Status: Currently generating playlist for '" & StartDate.ToString("dd/MM/yyyy").Replace("-", "/") & "'"
            Application.DoEvents()
            CurDate = StartDate.ToString("MM/dd/yyyy").Replace("-", "/")
            cmdPlaylist.Parameters("@Playlist_Date").Value = CurDate
            cmdPlaylist.ExecuteNonQuery()
            'MsgBox(cmdPlaylist.Parameters("@New_Playlist_Id").Value.ToString)
            StartDate = DateAdd(DateInterval.Day, 1, StartDate)
        End While

        cmdPlaylist.Dispose()
        Call CloseDBConnection()

        lbl_Status.Text = "Status: Executing FilePrep"
        Application.DoEvents()
        Call FileSync("Generate")

        lbl_Status.Text = "Status: Playlist generation and Fileprep completed"
        Application.DoEvents()

        btn_Generate.Enabled = True
        btn_Quit.Enabled = True

    End Sub
    Private Sub LoadCustomers()

        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With

        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)
        cmb_Customer.Properties.Columns(0).Visible = False
        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""

        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub

    Private Sub LoadStores(ByVal Customer_Id As Integer)

        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")


        'Creating Columns for Display in lookup
        cmb_Store.Properties.DisplayMember = "Store_Name"
        cmb_Store.Properties.ValueMember = "Store_ID"
        cmb_Store.Properties.DataSource = dsStores.Tables(0)
        cmb_Store.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Store.Properties.PopulateColumns()
        cmb_Store.Properties.Columns(0).Visible = False
        cmb_Store.Properties.ShowFooter = False
        cmb_Store.Properties.ShowHeader = False
        cmb_Store.EditValue = ""

        dsStores.Dispose()
        dapStores.Dispose()


        Call CloseDBConnection()
    End Sub

    Private Sub cmb_Customer_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If cmb_Customer.EditValue.ToString <> "" Then Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString))
    End Sub
    Private Sub btn_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Quit.Click
        End
    End Sub
    Private Sub InitializeStore()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Store.Properties.DataSource = dt
        dt.Dispose()
    End Sub
    Private Sub LoadMonths()
        Dim dt As New DataTable
        dt.Columns.Add("MonthNo")
        dt.Columns.Add("MonthName")

        dt.Rows.Add(1, "January")
        dt.Rows.Add(2, "February")
        dt.Rows.Add(3, "March")
        dt.Rows.Add(4, "April")
        dt.Rows.Add(5, "May")
        dt.Rows.Add(6, "June")
        dt.Rows.Add(7, "July")
        dt.Rows.Add(8, "August") : dt.Rows.Add(9, "September") : dt.Rows.Add(10, "October") : dt.Rows.Add(11, "November") : dt.Rows.Add(12, "December")

        cmb_Month.Properties.DisplayMember = "MonthName"
        cmb_Month.Properties.ValueMember = "MonthNo"
        cmb_Month.Properties.DataSource = dt
        cmb_Month.Properties.ShowFooter = False
        cmb_Month.Properties.ShowHeader = False
        cmb_Month.Properties.PopulateColumns()
        cmb_Month.Properties.Columns(0).Visible = False

        If Today.Day <= 5 Then
            cmb_Month.Text = Today.ToString("MMMM")
        Else
            cmb_Month.Text = Today.AddMonths(1).ToString("MMMM")
        End If

        cmb_Month.Properties.ForceInitialize()
        dt.Dispose()
        dt = Nothing

        Dim dt1 As New DataTable

        dt1.Columns.Add("YearNo")

        For i As Integer = 2012 To 2020
            dt1.Rows.Add(i)
        Next
        cmb_Year.Properties.DataSource = dt1
        cmb_Year.Properties.DisplayMember = "YearNo"
        cmb_Year.Properties.ValueMember = "YearNo"
        cmb_Year.Properties.PopulateColumns()
        cmb_Year.Properties.ShowFooter = False
        cmb_Year.Properties.ShowHeader = False


        If Now.Date.Day <= 5 Then
            cmb_Year.Text = Today.Year.ToString
        Else
            cmb_Year.Text = Today.AddMonths(1).Year.ToString
        End If
        cmb_Year.Properties.ForceInitialize()

        dt1.Dispose()
    End Sub
    Public Sub InitializeDropDowns()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Customer.Properties.DataSource = dt
        cmb_Month.Properties.DataSource = dt
        cmb_Year.Properties.DataSource = dt
        cmb_Store.Properties.DataSource = dt
        cmb_Month.EditValue = ""
        cmb_Year.EditValue = ""
        dt.Dispose()
        btn_Generate.Enabled = False
    End Sub
    Private Sub frmGeneratePlaylist_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If AppMode = "Generate Playlist" Then
            Me.Text = "Generate Playlist"
            Group_Utilities.Text = "Generate Playlist"
        ElseIf AppMode = "Delete Playlist" Then
            Me.Text = "Delete Playlist"
            btn_Generate.Text = "Delete"
            Group_Utilities.Text = "Delete Playlist"
        ElseIf AppMode = "FilePrep" Then
            Me.Text = "FilePrep"
            Group_Utilities.Text = "FilePrep"
        End If

        If Not ReadConfig() Then
            Call InitializeDropDowns()
            Exit Sub
        End If

        Call LoadSettings()
        Call LoadCustomers()
        Call InitializeStore()
        Call LoadMonths()

    End Sub
End Class