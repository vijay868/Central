﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO
Imports DevExpress.Utils
Imports NAudio
Imports DevExpress.XtraEditors
Imports aTrimmer
Public Class frmMetaDataImport
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer
    Private Sub Group_Data_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Group_Data.MouseDown
        drag = True
        mousex = Windows.Forms.Cursor.Position.X - Me.Left
        mousey = Windows.Forms.Cursor.Position.Y - Me.Top
    End Sub

    Private Sub Group_Data_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Group_Data.MouseMove
        If drag Then
            Me.Top = Windows.Forms.Cursor.Position.Y - mousey
            Me.Left = Windows.Forms.Cursor.Position.X - mousex
        End If
    End Sub

    Private Sub Group_Data_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Group_Data.MouseUp
        drag = False
    End Sub

    Private Sub LoadGenres()


        Dim cmdGenre As New SqlCommand, adpGenre As New SqlDataAdapter, dsGenre As New DataSet

        Call OpenDBConnection()

        With cmdGenre
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdGenre.CommandText = "ProcGetGenresList"
        adpGenre.SelectCommand = cmdGenre
        adpGenre.Fill(dsGenre, "GenreList")

        cmb_Genre.Properties.DisplayMember = "Genre_Name"
        cmb_Genre.Properties.ValueMember = "Genre_ID"
        cmb_Genre.Properties.DataSource = dsGenre.Tables("GenreList")
        cmb_Genre.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Genre.Properties.PopulateColumns()
        cmb_Genre.Properties.Columns(0).Visible = False
        cmb_Genre.Properties.ShowFooter = False
        cmb_Genre.Properties.ShowHeader = False

        cmb_Genre.EditValue = ""


        Call CloseDBConnection()

        dsGenre.Dispose()
        adpGenre.Dispose()

    End Sub
    
    'Public connetionString As String
    'Private Sub Utilities_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    connetionString = "Server=192.168.0.220; Database=INSCentral; User ID=sa; Password=manager"
    '    bindGenreGrid()
    '    bindCustomersGrid()
    'End Sub
    'Private Sub bindGenreGrid()
    '    Dim connection As SqlConnection
    '    Dim dr As SqlDataReader
    '    Dim command As New SqlCommand
    '    Dim ds As New DataSet

    '    connection = New SqlConnection(connetionString)
    '    Genre_Info_Grid.Rows.Clear()
    '    Try
    '        connection.Open()
    '        command.Connection = connection
    '        command.CommandType = CommandType.StoredProcedure
    '        command.CommandText = "ProcGetGenresList"
    '        dr = command.ExecuteReader
    '        If dr.HasRows Then
    '            While dr.Read
    '                Genre_Info_Grid.Rows.Add(dr(0).ToString, dr(1).ToString)
    '            End While
    '        End If
    '        connection.Close()
    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try
    'End Sub
    'Private Sub bindCustomersGrid()
    '    Dim connection As SqlConnection
    '    Dim dr As SqlDataReader
    '    Dim command As New SqlCommand
    '    Dim ds As New DataSet

    '    connection = New SqlConnection(connetionString)
    '    Customers_Info_Grid.Rows.Clear()
    '    Try
    '        connection.Open()
    '        command.Connection = connection
    '        command.CommandType = CommandType.StoredProcedure
    '        command.CommandText = "ProcGetCustomersList"
    '        dr = command.ExecuteReader
    '        If dr.HasRows Then
    '            While dr.Read
    '                Customers_Info_Grid.Rows.Add(dr(0).ToString, dr(1).ToString)
    '            End While
    '        End If
    '        connection.Close()
    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try
    'End Sub
    'Private Sub Genre_Info_Grid_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(0).ReadOnly = True
    'End Sub

    'Private Sub btn_save_edit_Genre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim connection As SqlConnection
    '    Dim StrMsg, StrEditMsg As String
    '    Dim OutResult As String
    '    Dim EditOutResult As String
    '    connection = New SqlConnection(connetionString)
    '    connection.Open()
    '    For i = 0 To Genre_Info_Grid.Rows.Count - 1
    '        If Genre_Info_Grid.Rows(i).Cells(0).Value = "" Then
    '            If Not Genre_Info_Grid.Rows(i).Cells(1).Value = "" Then
    '                OutResult = AddNewGenre(Genre_Info_Grid.Rows(i).Cells(1).Value, Genre_Info_Grid.CurrentRow.Index, connection)
    '                If OutResult <> "Success" Then
    '                    StrMsg = StrMsg + vbNewLine + Genre_Info_Grid.Rows(i).Cells(1).Value + " : " + OutResult
    '                End If
    '            End If
    '        Else
    '            If Not Genre_Info_Grid.Rows(i).Cells(1).Value = "" Then
    '                EditOutResult = EditGenre(Genre_Info_Grid.Rows(i).Cells(1).Value, Genre_Info_Grid.Rows(i).Cells(0).Value, i, connection)
    '                If EditOutResult <> "Success" Then
    '                    StrEditMsg = StrEditMsg + vbNewLine + Genre_Info_Grid.Rows(i).Cells(1).Value + " : " + EditOutResult
    '                End If
    '            End If
    '        End If
    '    Next
    '    If StrMsg = "" Then
    '        MessageBox.Show("Data saved successfully", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    Else
    '        MessageBox.Show(StrMsg, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    End If
    '    connection.Close()
    '    bindGenreGrid()
    'End Sub
    'Public Function EditGenre(ByVal GenreName As String, ByVal GenreID As Integer, ByVal SelectedRowIndex As Integer, ByRef connection As SqlConnection) As String
    '    Dim command As New SqlCommand
    '    Dim ds As New DataSet
    '    Try
    '        command.Connection = connection
    '        command.CommandType = CommandType.StoredProcedure
    '        command.CommandText = "ProcModifyGenre"
    '        command.Parameters.AddWithValue("@Genre_New_Name", GenreName).Direction = ParameterDirection.Input
    '        command.Parameters.AddWithValue("@Genre_Id", GenreID).Direction = ParameterDirection.Input
    '        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
    '        command.ExecuteNonQuery()
    '        Return command.Parameters("@Result").Value
    '    Catch ex As Exception
    '        Return "Failed"
    '    End Try
    'End Function
    'Public Sub DeleteGenre(ByVal GenreName As String, ByVal GenreID As Integer, ByVal SelectedRowIndex As Integer)
    '    Dim connection As SqlConnection
    '    Dim command As New SqlCommand
    '    Dim ds As New DataSet
    '    connection = New SqlConnection(connetionString)
    '    Try
    '        connection.Open()
    '        command.Connection = connection
    '        command.CommandType = CommandType.StoredProcedure
    '        command.CommandText = "ProcDeleteGenre"
    '        command.Parameters.AddWithValue("@Genre_Id", GenreID).Direction = ParameterDirection.Input
    '        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
    '        command.ExecuteNonQuery()
    '        MsgBox(command.Parameters("@Result").Value.ToString)
    '        connection.Close()
    '        bindGenreGrid()
    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try
    'End Sub
    'Public Function AddNewGenre(ByVal GenreName As String, ByVal SelectedRowIndex As Integer, ByRef connection As SqlConnection) As String
    '    Dim command As New SqlCommand
    '    Dim ds As New DataSet
    '    Try
    '        command.Connection = connection
    '        command.CommandType = CommandType.StoredProcedure
    '        command.CommandText = "ProcAddGenre"
    '        command.Parameters.AddWithValue("@Genre_Name", GenreName).Direction = ParameterDirection.Input
    '        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
    '        command.ExecuteNonQuery()
    '        Return command.Parameters("@Result").Value
    '    Catch ex As Exception
    '        Return "Failed"
    '    End Try
    'End Function

    'Private Sub Genre_Info_Grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    '    If e.KeyCode = Keys.Delete Then
    '        If MessageBox.Show("Are you sure to delete the selected record?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) Then
    '            DeleteGenre(Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(1).Value, Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(0).Value, Genre_Info_Grid.CurrentRow.Index)
    '        End If
    '    End If
    'End Sub

    'Private Sub btn_save_edit_customer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim connection As SqlConnection
    '    Dim StrMsg, StrEditMsg As String
    '    Dim OutResult As String
    '    Dim EditOutResult As String
    '    connection = New SqlConnection(connetionString)
    '    connection.Open()
    '    For i = 0 To Customers_Info_Grid.Rows.Count - 1
    '        If Customers_Info_Grid.Rows(i).Cells(0).Value = "" Then
    '            If Not Customers_Info_Grid.Rows(i).Cells(1).Value = "" Then
    '                OutResult = AddNewCustomer(Customers_Info_Grid.Rows(i).Cells(1).Value, Customers_Info_Grid.CurrentRow.Index, connection)
    '                If OutResult <> "Success" Then
    '                    StrMsg = StrMsg + vbNewLine + Customers_Info_Grid.Rows(i).Cells(1).Value + " : " + OutResult
    '                End If
    '            End If
    '        Else
    '            If Not Customers_Info_Grid.Rows(i).Cells(1).Value = "" Then
    '                EditOutResult = EditCustomer(Customers_Info_Grid.Rows(i).Cells(1).Value, Customers_Info_Grid.Rows(i).Cells(0).Value, i, connection)
    '                If EditOutResult <> "Success" Then
    '                    StrEditMsg = StrEditMsg + vbNewLine + Customers_Info_Grid.Rows(i).Cells(1).Value + " : " + EditOutResult
    '                End If
    '            End If
    '        End If
    '    Next
    '    If StrMsg = "" Then
    '        MessageBox.Show("Data saved successfully", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    Else
    '        MessageBox.Show(StrMsg, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    End If
    '    connection.Close()
    '    bindCustomersGrid()



    'End Sub
    'Private Sub Customers_Info_Grid_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(0).ReadOnly = True
    'End Sub
    'Public Function EditCustomer(ByVal CustomerName As String, ByVal CustomerID As Integer, ByVal SelectedRowIndex As Integer, ByRef connection As SqlConnection) As String
    '    Dim command As New SqlCommand
    '    Dim ds As New DataSet
    '    Try
    '        command.Connection = connection
    '        command.CommandType = CommandType.StoredProcedure
    '        command.CommandText = "ProcModifyCustomer"
    '        command.Parameters.AddWithValue("@Customer_New_Name", CustomerName).Direction = ParameterDirection.Input
    '        command.Parameters.AddWithValue("@Customer_Id", CustomerID).Direction = ParameterDirection.Input
    '        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
    '        command.ExecuteNonQuery()
    '        Return command.Parameters("@Result").Value
    '    Catch ex As Exception
    '        Return "Failed"
    '    End Try
    'End Function
    'Public Sub DeleteCustomer(ByVal CustomerName As String, ByVal CustomerID As Integer, ByVal SelectedRowIndex As Integer)
    '    Dim connection As SqlConnection
    '    Dim command As New SqlCommand
    '    Dim ds As New DataSet
    '    connection = New SqlConnection(connetionString)
    '    Try
    '        connection.Open()
    '        command.Connection = connection
    '        command.CommandType = CommandType.StoredProcedure
    '        command.CommandText = "ProcDeleteCustomer"
    '        command.Parameters.AddWithValue("@Customer_Id", CustomerID).Direction = ParameterDirection.Input
    '        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
    '        command.ExecuteNonQuery()
    '        MsgBox(command.Parameters("@Result").Value.ToString)
    '        connection.Close()
    '        bindGenreGrid()
    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try
    'End Sub
    'Public Function AddNewCustomer(ByVal CustomerName As String, ByVal SelectedRowIndex As Integer, ByRef connection As SqlConnection) As String
    '    Dim command As New SqlCommand
    '    Dim ds As New DataSet
    '    Try
    '        command.Connection = connection
    '        command.CommandType = CommandType.StoredProcedure
    '        command.CommandText = "ProcAddCustomer"
    '        command.Parameters.AddWithValue("@Customer_Name", CustomerName).Direction = ParameterDirection.Input
    '        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
    '        command.ExecuteNonQuery()
    '        Return command.Parameters("@Result").Value
    '    Catch ex As Exception
    '        Return "Failed"
    '    End Try
    'End Function

    'Private Sub Customers_Info_Grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    '    If e.KeyCode = Keys.Delete Then
    '        If MessageBox.Show("Are you sure to delete the selected record?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) Then
    '            DeleteCustomer(Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(1).Value, Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(0).Value, Customers_Info_Grid.CurrentRow.Index)
    '        End If
    '    End If
    'End Sub

    'Private Sub btn_save_edit_Genre_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    'Private Sub Genre_Sub_Tab_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    'End Sub

    Private Sub frmMetaDataImport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not ReadConfig() Then
            Exit Sub
        End If
        Call LoadSettings()
        Call LoadGenres()


    End Sub
   
    Private Sub btn_Import_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Import.Click

        If SongsArchivePath = "" Then
            XtraMessageBox.Show("Songs archive is path is not defined", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        ElseIf cmb_Genre.EditValue = Nothing Then
            XtraMessageBox.Show("Select Genre", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        ElseIf txt_FilePath.Text.Trim = "" Then
            XtraMessageBox.Show("Select meta data file to be imported", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        ElseIf Not File.Exists(txt_FilePath.Text.Trim) Then
            XtraMessageBox.Show("Playlist file not found", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        btn_Import.Enabled = False
        btn_Quit.Enabled = False

        ConversionBar.Visible = True
        Cursor = Cursors.WaitCursor
        Conversion_Grid.Rows.Clear()
        Call OpenDBConnection()


        Dim strLine As String, strValues() As String
        Dim Song_ID As Integer, Genre_ID As Integer, Title As String, Album As String, Artist1 As String = "", Artist2 As String = "", SourceFileName As String
        Dim Genre_Category_ID As Integer = 0
        Dim ActFilename As String
        Dim FileDuration As String
        Dim showerrors As Boolean
        Dim TempActFilename As String
        Dim FileHeaderRead As Boolean

        Dim i As Integer, Result As String = ""
        showerrors = False
        Dim MetaDataCount, CurFile As Integer
        Dim FileReader As New StreamReader(txt_FilePath.Text)
        While Not FileReader.EndOfStream
            FileReader.ReadLine()
            MetaDataCount += 1
        End While
        FileReader.Close()
        FileReader = Nothing
        'FileReader.Dispose()

        FileReader = New StreamReader(txt_FilePath.Text)

        Genre_ID = Convert.ToInt32(cmb_Genre.EditValue.ToString)
        Genre_Category_ID = Convert.ToInt32(cmb_Genre_Category.EditValue.ToString)

        While Not FileReader.EndOfStream
            CurFile += 1
            FileDuration = ""
            strLine = FileReader.ReadLine
            strValues = strLine.Split(";")

            TempActFilename = ""
            ActFilename = strValues(4)
            'ActFilename = Replace(ActFilename, "G:\", "D:\wav\")
            Song_ID = strValues(0).Split("-")(0)
            Title = strValues(1)
            Album = strValues(2)
            If strValues(3).IndexOf("[+]") > 0 Then
                Artist1 = strValues(3).Split("[+]")(0).Trim
                Artist2 = strValues(3).Split("[+]")(1).Replace("+]", "").Trim
            Else
                Artist1 = strValues(3)
            End If

            'checking if exists
            If Not File.Exists(strValues(4)) Then
                showerrors = True
                Conversion_Grid.Rows.Add(Song_ID, Title, strValues(4), "Source file not found")
                GoTo nxtfile
            End If

            SourceFileName = Path.GetFileName(strValues(4))
            SourceFileName = SourceFileName.ToLower
            If SourceFileName.Contains("{") Then GoTo nxtfile

            lbl_Status.Text = "Processing : " & SourceFileName & " (" & CurFile & "/" & MetaDataCount & ")"
            Application.DoEvents()

            'If Not Directory.Exists(SongsArchivePath & "\Temp") = True Then
            '    Directory.CreateDirectory(SongsArchivePath & "\Temp")
            'End If

            If Not Directory.Exists(Application.StartupPath & "\Temp") = True Then
                Directory.CreateDirectory(Application.StartupPath & "\Temp")
            End If

            TempActFilename = Application.StartupPath & "\Temp\" & Song_ID & Path.GetExtension(SourceFileName) '.Replace(Strings.Right(FileName, 4), ".aac")
            'TempActFilename = SongsArchivePath & "\Temp\" & SourceFileName

            If File.Exists(TempActFilename) Then
                File.Delete(TempActFilename)
                Application.DoEvents()
            End If

            FileDuration = GetWavFileDuration(ActFilename)
            FileHeaderRead = False

            If FileDuration <> "Error while reading wav file" And FileDuration <> "Source File Not Found" Then
                FileHeaderRead = True
                GoTo AacConversion
            Else
                File.Copy(ActFilename, TempActFilename)
                Application.DoEvents()
            End If

AudioNormalization:
            If ReWriteWavFile(AudioConverterpath, ActFilename, TempActFilename) = True Then

                '  FileDuration = GetWavFileDuration(ActFilename)
                FileDuration = GetWavFileDuration(TempActFilename)
AacConversion:
                If CheckforAACFile(TempActFilename) = False Then
                    If FileDuration <> "Error while reading wav file" Then
                        If Convert_Wav_AACPlus(TempActFilename) Then

                            Call WriteData(Song_ID, Genre_ID, Genre_Category_ID, Title, Album, Artist1, Artist2, Path.GetFileName(TempActFilename), FileDuration, Result)
                            If Result <> "Success" Then
                                'MessageBox.Show(Result, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Debug.WriteLine(SourceFileName)
                            End If
                        Else
                            If FileHeaderRead Then
                                GoTo AudioNormalization
                            Else
                                showerrors = True
                                Conversion_Grid.Rows.Add(Song_ID, Title, SourceFileName, "AAC file conversion error")
                                Application.DoEvents()
                            End If
                        End If
                        File.Delete(Application.StartupPath & "\Temp\" & SourceFileName)
                        'File.Delete(SongsArchivePath & "\Temp\" & SourceFileName)
                        Application.DoEvents()
                    Else
                        showerrors = True
                        Conversion_Grid.Rows.Add(Song_ID, Title, SourceFileName, "Error while reading duration")
                        Application.DoEvents()
                    End If
                Else
                    'showerrors = True
                    'Conversion_Grid.Rows.Add(Song_ID, Title, SourceFileName, "File already exists")
                    Application.DoEvents()
                    If FileDuration <> "Error while reading wav file" Then
                        Call WriteData(Song_ID, Genre_ID, Genre_Category_ID, Title, Album, Artist1, Artist2, SourceFileName, FileDuration, Result)
                        If Result <> "Success" Then
                            'MessageBox.Show(Result, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Debug.WriteLine(SourceFileName)
                        End If
                    Else
                        showerrors = True
                        Conversion_Grid.Rows.Add(Song_ID, Title, SourceFileName, "Error while reading duration")
                        Application.DoEvents()
                    End If
                End If
            End If


            'i += 1
nxtfile:
        End While
        ConversionBar.Visible = False

        If DeleteTempFolder(Application.StartupPath & "\Temp") Then
            'Write Log---Temp folder Delete Sucessfully
        Else
            'Write Log --- Error while deleteing temp folder
        End If

        If showerrors = True Then
            btn_showhideerrors.Visible = True
            lbl_Status.Text = "Status: MetaDataImport completed with some errors"
            btn_Quit.Enabled = True
        Else
            lbl_Status.Text = "Status: Meta data import is complete"
            btn_Quit.Enabled = True
        End If

        FileReader.Close()
        Call CloseDBConnection()
        Cursor = Cursors.Default

       

    End Sub
    Public Function DeleteTempFolder(ByVal Temppath As String) As Boolean
        Try
            If Directory.Exists(Temppath) = True Then
                Directory.Delete(Temppath, True)
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
       
    End Function
    Public Function CheckforAACFile(ByVal Filename As String) As Boolean
        Dim returnval As Boolean
        Dim aacFilename As String
        aacFilename = Replace(Filename.ToLower, ".wav", "") & ".aac"

        If File.Exists(SongsArchivePath & "\" & aacFilename) Then
            returnval = True
        Else
            returnval = False
        End If
        Return returnval
    End Function

    Private Sub WriteData(ByVal Song_ID As Integer, ByVal Genre_ID As Integer, ByVal Genre_Category_ID As Integer, ByVal Title As String, ByVal Album As String, ByVal Artist1 As String, ByVal Artist2 As String, ByVal FileName As String, ByVal Duration As String, ByRef Result As String)
        Dim cmd As New SqlCommand
        Try
            cmd.CommandText = "ProcAddNewSong"
            With cmd
                .Parameters.Clear()
                .CommandType = CommandType.StoredProcedure
                .Connection = DBConn
                .CommandTimeout = 500

                .Parameters.Add("@Song_ID", SqlDbType.BigInt).Direction = ParameterDirection.Input
                .Parameters.Add("@Genre_ID", SqlDbType.BigInt).Direction = ParameterDirection.Input
                .Parameters.Add("@Title", SqlDbType.VarChar, 150).Direction = ParameterDirection.Input
                .Parameters.Add("@Album", SqlDbType.VarChar, 250).Direction = ParameterDirection.Input
                .Parameters.Add("@Artist1", SqlDbType.VarChar, 150).Direction = ParameterDirection.Input
                .Parameters.Add("@Artist2", SqlDbType.VarChar, 150).Direction = ParameterDirection.Input
                .Parameters.Add("@FiileName", SqlDbType.VarChar, 250).Direction = ParameterDirection.Input
                .Parameters.Add("@Duration", SqlDbType.VarChar, 20).Direction = ParameterDirection.Input
                .Parameters.Add("@Genre_Categrory_Id", SqlDbType.BigInt).Direction = ParameterDirection.Input
                .Parameters.Add("@Normalization_ID", SqlDbType.BigInt).Direction = ParameterDirection.Input
                ''-6 db'
                '.Parameters.Add("@File_TimeStamp", SqlDbType.DateTime).Direction = ParameterDirection.Input
                .Parameters.Add("@Result", SqlDbType.VarChar, 70).Direction = ParameterDirection.Output

                .Parameters("@Song_ID").Value = Song_ID
                .Parameters("@Genre_ID").Value = Genre_ID
                .Parameters("@Title").Value = Title
                .Parameters("@Album").Value = Album
                .Parameters("@Artist1").Value = Artist1
                .Parameters("@Artist2").Value = Artist2
                .Parameters("@FileName").Value = Replace(FileName.ToLower, ".wav", ".aac")
                .Parameters("@Genre_Categrory_Id").Value = Genre_Category_ID
                .Parameters("@Normalization_ID").Value = 7
                .Parameters("@Duration").Value = Duration
                '.Parameters("@File_TimeStamp").Value = File_TimeStamp
            End With

            cmd.ExecuteNonQuery()
            Result = cmd.Parameters("@Result").Value.ToString
        Catch
        End Try

    End Sub

    

    Private Sub btn_Generate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Generate.Click
        Call OpenDBConnection()

        Dim FileReader As New StreamReader(txt_FilePath.Text)
        Dim strLine As String, strValues() As String
        Dim Song_ID As Integer, Genre_ID As Integer, Title As String, Album As String, Artist1 As String, Artist2 As String, FileName As String

        Dim i As Integer, Result As String

        Genre_ID = 3
        While Not FileReader.EndOfStream
            strLine = FileReader.ReadLine
            strValues = strLine.Split(";")
            Song_ID = strValues(0).Split("-")(0)
            Title = strValues(1)
            Album = strValues(2)
            If strValues(3).IndexOf("[+]") > 0 Then
                Artist1 = strValues(3).Split("[+]")(0).Trim
                Artist2 = strValues(3).Split("[+]")(1).Replace("+]", "").Trim
            Else
                Artist1 = strValues(3)
            End If

            FileName = Path.GetFileName(strValues(4))
            If FileName.Contains("{") Then GoTo nxtfile

            lbl_Status.Text = "Processing : " & FileName
            Application.DoEvents()
            File.Copy("D:\Works\Audio Source\009000101.wav", "D:\Works\Source Files\" & FileName, True)
            Application.DoEvents()
            'Call WriteData(Con, Song_ID, Genre_ID, Title, Album, Artist1, Artist2, FileName, "03:24", Result)
            'If Result <> "Success" Then
            '    'MessageBox.Show(Result, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    Debug.WriteLine(FileName)
            'End If
            'i += 1
nxtfile:
        End While

        FileReader.Close()
        Call CloseDBConnection()
        XtraMessageBox.Show("Done", "Alert", MessageBoxButtons.OK)

    End Sub

   

    Public Function GetWavFileDuration(ByVal FileNameStr As String) As String
        Dim returnval As String
        If File.Exists(FileNameStr) Then
            Try
                Dim wd As NAudio.Wave.WaveFileReader = New Wave.WaveFileReader(FileNameStr)
                returnval = wd.TotalTime.ToString.Split(".")(0)
                wd.Close()
                wd = Nothing
            Catch ex As Exception
                returnval = "Error while reading wav file"
            End Try
        Else
            returnval = "Source File Not Found"
        End If
        Return returnval
    End Function
    Public Function Convert_Wav_AACPlus(ByVal FileNameStr As String) As Boolean
        Dim ConversionProcess As Process
        Dim StrOutput As String
        Dim finfo As FileInfo
        Dim wavfile As String
        Dim aacfile As String
        Dim frmStr As String
        Dim returnval As Boolean

        If File.Exists(FileNameStr) Then
            finfo = New FileInfo(FileNameStr)
            wavfile = finfo.FullName
            aacfile = SongsArchivePath & "\" & Replace(finfo.Name.ToLower, ".wav", ".aac")
            ConversionProcess = New Process
            ConversionProcess.StartInfo.UseShellExecute = False
            ConversionProcess.StartInfo.RedirectStandardError = True
            ConversionProcess.StartInfo.CreateNoWindow = True
            ConversionProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            ConversionProcess.StartInfo.FileName = Application.StartupPath & "\enc_aacPlus.exe"
            ' ConversionProcess.EnableRaisingEvents = True
            frmStr = """" & wavfile & """" & " """ & aacfile & """ --br 128000 --he"
            ConversionProcess.StartInfo.Arguments = frmStr
            ConversionProcess.Start()

            ' StrOutput = ConversionProcess.StandardOutput.ReadToEnd
            Application.DoEvents()
            Do Until ConversionProcess.HasExited
                Application.DoEvents()
            Loop
            'ConversionProcess.WaitForExit()
            If File.Exists(wavfile) Then
                File.Delete(wavfile)
                Application.DoEvents()
            End If
            If File.Exists(aacfile) Then
                returnval = True
            Else
                returnval = False
            End If
        End If
        Return returnval
    End Function
    Private Sub btn_Browse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Browse.Click
        BrowseFile.Filter = "CSV Files|*.csv"
        BrowseFile.ShowDialog()
        txt_FilePath.Text = BrowseFile.FileName
       
    End Sub

    Private Sub btn_showhideerrors_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_showhideerrors.Click
        If btn_showhideerrors.Text = "Show Errors" Then
            btn_showhideerrors.Text = "Hide Errors"
            Me.Width = 585
            Me.Height = 428
            Group_Data.Height = 398
        Else
            btn_showhideerrors.Text = "Show Errors"
            Me.Width = 585
            Me.Height = 223
            Group_Data.Height = 190
        End If
    End Sub

    Private Sub btn_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Quit.Click
        End
    End Sub

    Public Function ReWriteWavFile(ByVal ExePath As String, ByVal InFile As String, ByVal OutFile As String) As Boolean
        Dim StrProcessPath As String = String.Empty
        Dim tempOutFile, tempPath As String
        Dim Res As Boolean

        Try
            If File.Exists(OutFile) = True Then
                File.Delete(OutFile)
                Application.DoEvents()
            End If
        Catch ex As Exception
            Return True

        End Try
        'if file not getting deleted skip the file
        tempPath = Path.GetDirectoryName(OutFile)
        tempOutFile = Path.GetFileNameWithoutExtension(OutFile) & "-trim" & Path.GetExtension(OutFile)

        'StrProcessPath = """" & InFile & """ """ & OutFile & "" & """ -c wav -sr 44100 -ch Stereo"
        'If ShellandWait(ExePath, StrProcessPath) = True Then
        '    If File.Exists(OutFile) = True Then
        '        Return True
        '    Else
        '        Return False
        '    End If
        'End If


        StrProcessPath = """" & InFile & """ """ & tempPath & "\" & tempOutFile & "" & """ -c wav -sr 44100 -ch Stereo"
        If ShellandWait(ExePath, StrProcessPath) = True Then
            If File.Exists(tempPath & "\" & tempOutFile) = True Then
                Dim wa As New aTrim
                'lbl_Status.Text = "Status: Trimming silence"
                Application.DoEvents()
                If (Not wa.StripSilence(tempPath & "\" & tempOutFile, OutFile)) Then
                    'lbl_Status.Text = "Status: Trimming failed"

                    wa = Nothing
                    Application.DoEvents()

                    Res = False
                Else
                    wa = Nothing
                    Res = True
                End If
            Else
                Res = False
            End If
        Else
            Res = False
        End If

        If File.Exists(tempPath & "\" & tempOutFile) Then
            File.Delete(tempPath & "\" & tempOutFile)
            Application.DoEvents()
        End If
        Return Res
    End Function
    Public Function ShellandWait(ByVal ProcessPath As String, ByVal Arguments As String) As Boolean
        Dim objProcess As System.Diagnostics.Process
        Dim returnval As Boolean
        Try
            objProcess = New System.Diagnostics.Process()
            objProcess.StartInfo.FileName = ProcessPath
            objProcess.StartInfo.Arguments = Arguments
            objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
            objProcess.Start()
            objProcess.WaitForExit()
            objProcess.Close()
            returnval = True
        Catch
            returnval = False
        End Try
        Return returnval
    End Function
    Private Sub LoadGenreCategories()
        Dim cmdGenre As New SqlCommand, adpGenre As New SqlDataAdapter, dsGenre As New DataSet
        'Try
        Call OpenDBConnection()

        With cmdGenre
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdGenre.CommandText = "ProcGetGenreCategoriesList"
        cmdGenre.Parameters.AddWithValue("@Genre_Id", Convert.ToInt32(cmb_Genre.EditValue.ToString))
        adpGenre.SelectCommand = cmdGenre
        adpGenre.Fill(dsGenre, "GenreCategoryList")

        cmb_Genre_Category.Properties.DisplayMember = "Category_Code"
        cmb_Genre_Category.Properties.ValueMember = "Genre_Category_ID"
        cmb_Genre_Category.Properties.DataSource = dsGenre.Tables("GenreCategoryList")
        cmb_Genre_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Genre_Category.Properties.PopulateColumns()
        'MsgBox(cmb_Genre.Properties.Columns.VisibleCount)
        cmb_Genre_Category.Properties.Columns(0).Visible = False
        cmb_Genre_Category.Properties.Columns(1).Visible = False
        cmb_Genre_Category.Properties.ShowFooter = False
        cmb_Genre_Category.Properties.ShowHeader = False

        'cmb_Parent_Category.EditValue = ""


        Call CloseDBConnection()

        dsGenre.Dispose()
        adpGenre.Dispose()

    End Sub
    Private Sub cmb_Genre_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Genre.EditValueChanged
        If cmb_Genre.EditValue = Nothing Then Exit Sub
        Call LoadGenreCategories()


        btn_Import.Enabled = True
    End Sub

    Private Sub txt_FilePath_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_FilePath.EditValueChanged
        If txt_FilePath.Text.Trim <> "" Then
            btn_Import.Enabled = True
        Else
            btn_Import.Enabled = False
        End If
    End Sub
End Class