﻿Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Public Class frmPlaylistCopier
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer
    Dim PlaylistDates(), TargetPlaylistDates() As String
    Private Sub Group_Utilities_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Playlist_Copier.MouseDown
        drag = True
        mousex = Windows.Forms.Cursor.Position.X - Me.Left
        mousey = Windows.Forms.Cursor.Position.Y - Me.Top
    End Sub

    Private Sub Group_Utilities_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Playlist_Copier.MouseMove
        If drag Then
            Me.Top = Windows.Forms.Cursor.Position.Y - mousey
            Me.Left = Windows.Forms.Cursor.Position.X - mousex
        End If
    End Sub

    Private Sub Group_Utilities_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Playlist_Copier.MouseUp
        drag = False
    End Sub
    Private Sub btn_Generate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Generate.Click
        
        'Exit Sub

        If PromosArchivePath = "" OrElse SongsArchivePath = "" Then
            XtraMessageBox.Show("FilePrep path is not configured", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If cmb_Customer.EditValue = Nothing Then
            XtraMessageBox.Show("Customer is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Customer.Focus()
            Exit Sub
        End If
        If cmb_Source_Store.EditValue = Nothing Then
            XtraMessageBox.Show("Store is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Source_Store.Focus()
            Exit Sub
        End If

        If cmb_Target_Stores.EditValue = Nothing Then
            XtraMessageBox.Show("Target Stores are not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Target_Stores.Focus()
            Exit Sub
        End If

        If source_playlist_start_date.EditValue = Nothing Then
            XtraMessageBox.Show("'From' date is  not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            source_playlist_start_date.Focus()
            Exit Sub
        End If

        If source_playlist_end_date.EditValue = Nothing Then
            XtraMessageBox.Show("To' date is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            source_playlist_end_date.Focus()
            Exit Sub
        End If

        If target_playlist_start_date.EditValue = Nothing Then
            XtraMessageBox.Show("'From' date is  not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            target_playlist_start_date.Focus()
            Exit Sub
        End If

        If target_playlist_end_date.EditValue = Nothing Then
            XtraMessageBox.Show("To' date is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            target_playlist_end_date.Focus()
            Exit Sub
        End If


        Dim stDate, endDate, targetStDate, targetEndDate As Date
        Dim sDayCount, tDayCount As Integer

        If OptionGroup.SelectedIndex = 0 Then
            source_playlist_end_date.EditValue = source_playlist_start_date.EditValue
            target_playlist_end_date.EditValue = target_playlist_start_date.EditValue
        End If


        stDate = source_playlist_start_date.EditValue
        endDate = source_playlist_end_date.EditValue

        targetStDate = target_playlist_start_date.EditValue
        targetEndDate = target_playlist_end_date.EditValue

        sDayCount = DateDiff(DateInterval.Day, stDate, endDate) + 1
        tDayCount = DateDiff(DateInterval.Day, targetStDate, targetEndDate) + 1

        If DateDiff(DateInterval.Day, stDate, endDate) <> 0 AndAlso sDayCount <> tDayCount Then
            XtraMessageBox.Show("Source date must be either 'one' or the number of source days should be equal to target days", "Alert", MessageBoxButtons.OK)
            Exit Sub
        End If

        'If dt_From.EditValue = Nothing Or dt_To.EditValue = Nothing Then
        '    XtraMessageBox.Show("'From' date or 'To' date can not be null", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    If dt_From.EditValue = Nothing Then
        '        dt_From.Focus()
        '    Else
        '        dt_To.Focus()
        '    End If
        '    Exit Sub
        'End If

        'If dt_From.EditValue > dt_To.EditValue Then
        '    XtraMessageBox.Show("'From' date can not be later than 'To' date'", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    dt_From.Focus()
        '    Exit Sub
        'End If

        'Dim DateCount As Integer = DateDiff(DateInterval.Day, dt_From.EditValue, dt_To.EditValue) + 1
        'If DateCount > 31 Then
        '    XtraMessageBox.Show("Range of dates can not exceed 31 days", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    dt_To.Focus()
        '    Exit Sub
        'End If


        lbl_Status.Text = "Status: Checking database"
        Application.DoEvents()

        'MsgBox(DateSerial(cmb_Year.EditValue.ToString, cmb_Month.EditValue.ToString, 1).ToString("MM/dd/yyyy").Replace("-", "/"))
        'Checking if playlist already generated
        Dim cmdPlaylist As New SqlCommand

        Call OpenDBConnection()
        With cmdPlaylist
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckIfPlaylistGenerated"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Source_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            '.ExecuteNonQuery()
        End With


        Dim totDays, i, p As Integer, CurSourceDate, CurTargetDate As Date
        Dim vSourceDays(), vTargetDays() As Date

        ReDim vSourceDays(tDayCount - 1)
        ReDim vTargetDays(tDayCount - 1)

        ' If sDayCount = 1 Then p = 0 Else p = 1

        For i = 0 To tDayCount - 1
            If sDayCount = 1 Then
                vSourceDays(i) = stDate
            Else
                vSourceDays(i) = DateAdd(DateInterval.Day, i, stDate)
            End If
        Next

        For i = 0 To tDayCount - 1
            vTargetDays(i) = DateAdd(DateInterval.Day, i, targetStDate)
        Next

        ' MsgBox(vTargetDays)
        'MsgBox(vSourceDays)



        For i = 0 To cmb_Target_Stores.Properties.Items.Count - 1
            If cmb_Target_Stores.Properties.Items(i).CheckState = CheckState.Checked Then 'AndAlso Convert.ToInt32(cmb_Source_Store.EditValue.ToString) <> cmb_Target_Stores.Properties.Items(i).Value Then

                totDays = tDayCount

                For p = 0 To totDays - 1
                    CurSourceDate = vSourceDays(p)
                    CurTargetDate = vTargetDays(p)
                    ' MsgBox(CurDate.ToString("MM/dd/yyyy"))

                    cmdPlaylist.Parameters("@Playlist_Date").Value = CurSourceDate.ToString("MM/dd/yyyy")
                    cmdPlaylist.ExecuteNonQuery()
                    If cmdPlaylist.Parameters("@Result").Value.ToString = "Already generated" Then
                        lbl_Status.Text = "Status: Generating playlists for " & cmb_Target_Stores.Properties.Items(i).ToString & " - " & CurTargetDate.ToString("dd/MM/yyyy")
                        Application.DoEvents()
                        Call GeneratePlaylists(cmb_Target_Stores.Properties.Items(i).Value, CurSourceDate, CurTargetDate)
                    End If
                Next
                lbl_Status.Text = "Status: Executing FilePrep for " & cmb_Target_Stores.Properties.Items(i).ToString
                Application.DoEvents()
                Call FileSync(Convert.ToInt32(cmb_Source_Store.EditValue.ToString), cmb_Target_Stores.Properties.Items(i).Value, cmb_Target_Stores.Properties.Items(i).ToString, targetStDate, targetEndDate)

                lbl_Status.Text = "Status: Playlist generation and Fileprep completed"
                Application.DoEvents()
            End If
        Next


        'For i = 0 To cmb_Target_Stores.Properties.Items.Count - 1
        '    If cmb_Target_Stores.Properties.Items(i).CheckState = CheckState.Checked AndAlso Convert.ToInt32(cmb_Source_Store.EditValue.ToString) <> cmb_Target_Stores.Properties.Items(i).Value Then

        '        totDays = DateDiff(DateInterval.Day, stDate, endDate) + 1
        '        CurDate = DateAdd(DateInterval.Day, -1, stDate)
        '        For p = 1 To totDays
        '            CurDate = DateAdd(DateInterval.Day, 1, CurDate)
        '            ' MsgBox(CurDate.ToString("MM/dd/yyyy"))

        '            cmdPlaylist.Parameters("@Playlist_Date").Value = CurDate.ToString("MM/dd/yyyy")
        '            cmdPlaylist.ExecuteNonQuery()
        '            If cmdPlaylist.Parameters("@Result").Value.ToString = "Already generated" Then
        '                lbl_Status.Text = "Status: Generating playlists for " & cmb_Target_Stores.Properties.Items(i).ToString & " - " & CurDate.ToString("dd/MM/yyyy")
        '                Application.DoEvents()
        '                Call GeneratePlaylists(cmb_Target_Stores.Properties.Items(i).Value, CurDate)
        '            End If
        '        Next
        '        lbl_Status.Text = "Status: Executing FilePrep for " & cmb_Target_Stores.Properties.Items(i).ToString
        '        Application.DoEvents()
        '        Call FileSync(Convert.ToInt32(cmb_Source_Store.EditValue.ToString), cmb_Target_Stores.Properties.Items(i).Value, cmb_Target_Stores.Properties.Items(i).ToString, stDate, endDate)

        '        lbl_Status.Text = "Status: Playlist generation and Fileprep completed"
        '        Application.DoEvents()
        '    End If
        'Next

        btn_Generate.Enabled = True
        btn_Quit.Enabled = True

        'adding dates to array of the store
        'Call AddDates(targetStDate, targetEndDate, tDayCount - 1)



    End Sub
    Private Sub AddDates(ByVal stDate As Date, ByVal endDate As Date, ByVal totDays As Integer)
        Dim CheckedCount, SelectedStore As Integer

        For i = 0 To cmb_Target_Stores.Properties.Items.Count - 1
            If cmb_Target_Stores.Properties.Items(i).CheckState = CheckState.Checked Then
                CheckedCount += 1
                If SelectedStore = 0 Then SelectedStore = cmb_Target_Stores.Properties.Items(i).Value
            End If
        Next

        If CheckedCount > 1 OrElse CheckedCount = 0 Then ReDim TargetPlaylistDates(1) : Exit Sub

        Dim z, m As Integer
        If TargetPlaylistDates(0) <> "" Then
            z = TargetPlaylistDates.Length - 1

        End If
        ReDim Preserve TargetPlaylistDates(z + totDays)
        m = 0
        For i = z + 1 To z + totDays
            TargetPlaylistDates(i - 1) = DateAdd(DateInterval.Day, m, stDate)
            m += 1
        Next

    End Sub
    Private Sub FileSync(ByVal ParentStoreId As Integer, ByVal StoreId As Integer, ByVal CurStore As String, ByVal stDate As Date, ByVal EndDate As Date)

        'If Not CheckIfPlayListGenerated() Then
        '    XtraMessageBox.Show("Playlist for the selected store and month doesn't exist", "Alert", MessageBoxButtons.OK)
        '    Exit Sub
        'End If

        lbl_Status.Text = "Status: Executing Fileprep ( " & CurStore & ") - Creating folders"

        Application.DoEvents()

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text) Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text)
        End If

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore) Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore)
        End If

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore & "\" & stDate.ToString("MMMM-yyyy")) Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore & "\" & stDate.ToString("MMMM-yyyy"))
        End If

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore & "\" & stDate.ToString("MMMM-yyyy") & "\Songs") Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore & "\" & stDate.ToString("MMMM-yyyy") & "\Songs")
        End If

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore & "\" & stDate.ToString("MMMM-YYYY") & "\Promos") Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore & "\" & stDate.ToString("MMMM-yyyy") & "\Promos")
        End If

        Dim FileCopyPath As String = FileprepPath & "\" & cmb_Customer.Text & "\" & CurStore & "\" & stDate.ToString("MMMM-yyyy")


        lbl_Status.Text = "Status: Executing Fileprep (" & CurStore & ")- Preparing playlist XML files"

        Application.DoEvents()

        Dim cmdPlaylist As New SqlCommand
        Dim dapPlaylist As New SqlDataAdapter, dsPlaylist As New DataSet

        Dim clsInstore As New ClsEncryption

        Call OpenDBConnection()

        'Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        Dim cmdFilePrepDates As New SqlCommand, drFilePrepDates As SqlDataReader
        With cmdFilePrepDates
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcGetPlaylistDatesForStore"
            .Parameters.AddWithValue("@Store_Id", StoreId).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Start_Date", stDate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@End_Date", EndDate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
        End With

        drFilePrepDates = cmdFilePrepDates.ExecuteReader
        cmdFilePrepDates = Nothing

        Dim cmdUpdateDB As New SqlCommand
        With cmdUpdateDB
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcUpdatePlayListStatus"
            .Parameters.AddWithValue("@Store_Id", StoreId).Direction = ParameterDirection.Input
            .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
        End With

        ' Dim cmdPlaylist1 As New SqlCommand

        With cmdPlaylist
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcGetPlaylistForSelectedDate"
            .Parameters.AddWithValue("@Store_Id", StoreId).Direction = ParameterDirection.Input
            .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 1).Direction = ParameterDirection.Input
        End With

        Dim CurDate As Date
        If drFilePrepDates.HasRows Then
            Dim PrepDates(1000) As String, z As Integer

            While drFilePrepDates.Read
                PrepDates(z) = drFilePrepDates(0)
                z += 1
            End While
            ReDim Preserve PrepDates(z - 1)
            drFilePrepDates.Close()

            For j As Integer = 0 To PrepDates.Length - 1
                'Genre_Info_Grid.Rows.Add(dr(0).ToString, dr(1).ToString)
                CurDate = PrepDates(j)
                lbl_Status.Text = "Status: Executing Fileprep - Creating playlist XML file for '" & CurStore & " - " & CurDate.ToString("dd/MM/yyyy") & "'"

                Application.DoEvents()
                cmdPlaylist.Parameters("@Playlist_Date").Value = CurDate.ToString("MM/dd/yyyy").Replace("-", "/")
                dapPlaylist.SelectCommand = cmdPlaylist
                dsPlaylist.Clear()
                dapPlaylist.Fill(dsPlaylist, "Playlist")

                If File.Exists(FileCopyPath & "\" & CurDate.ToString("dd-MM-yyyy").Replace("/", "-") & ".xml") Then
                    File.Delete(FileCopyPath & "\" & CurDate.ToString("dd-MM-yyyy").Replace("/", "-") & ".xml")
                End If

                dsPlaylist.WriteXml(FileCopyPath & "\" & CurDate.ToString("dd-MM-yyyy").Replace("/", "-") & ".xml")
                Application.DoEvents()

                'update database
                cmdUpdateDB.Parameters("@Playlist_Date").Value = CurDate.ToString("MM/dd/yyyy")
                cmdUpdateDB.ExecuteNonQuery()

            Next
        End If

        Application.DoEvents()
        drFilePrepDates.Close()
        With cmdPlaylist
            .Parameters.Clear()
            .CommandText = "ProcGetStoreSongPromoList"
            .Parameters.AddWithValue("@Store_Id", ParentStoreId).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Filter_Criteria", "Songs").Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 1).Direction = ParameterDirection.Input
        End With

        dapPlaylist.SelectCommand = cmdPlaylist
        dsPlaylist.Clear()
        dapPlaylist.Fill(dsPlaylist, "ActiveSongs")

        cmdPlaylist.Parameters("@Filter_Criteria").Value = "Promos"
        dapPlaylist.SelectCommand = cmdPlaylist
        dapPlaylist.Fill(dsPlaylist, "ActivePromos")

        cmdPlaylist.CommandText = "ProcGetPlaylistDatesForStore"
        cmdPlaylist.Parameters.Clear()
        cmdPlaylist.Parameters.AddWithValue("@Store_Id", StoreId).Direction = ParameterDirection.Input
        cmdPlaylist.Parameters.AddWithValue("@Start_Date", stDate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
        cmdPlaylist.Parameters.AddWithValue("@End_Date", EndDate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input

        dapPlaylist.SelectCommand = cmdPlaylist
        dapPlaylist.Fill(dsPlaylist, "PlaylistDates")

        Dim DtTable As New DataTable("PlaylistMonth")
        DtTable.Columns.Add("MonthNumber")
        DtTable.Columns.Add("YearNumber")

        DtTable.Rows.Add(stDate.ToString("MMMM"), stDate.ToString("yyyy"))

        dsPlaylist.Tables.Add(DtTable)

        If File.Exists(FileCopyPath & "\Active List.xml") Then
            File.Delete(FileCopyPath & "\Active List.xml")
        End If

        dsPlaylist.WriteXml(FileCopyPath & "\Active List.xml")
        Application.DoEvents()

        lbl_Status.Text = "Status: Executing Fileprep -  Copying Songs"

        Application.DoEvents()

        Dim rCount As Integer, totRows As Integer
        Dim FileName As String
        totRows = dsPlaylist.Tables("ActiveSongs").Rows.Count
        ' 
        For rCount = 0 To dsPlaylist.Tables("ActiveSongs").Rows.Count - 1
            lbl_Status.Text = "Status: Executing Fileprep - " & CurStore & " -  Copying Songs (" & (rCount + 1).ToString & "/" & totRows.ToString & ")"
            Application.DoEvents()
            FileName = dsPlaylist.Tables("ActiveSongs").Rows(rCount)("FileName").ToString
            If File.Exists(SongsArchivePath & "\" & FileName) Then
                'File.Copy(SongsArchivePath & "\" & FileName, FileCopyPath & "\Songs\" & FileName, True)
                Call clsInstore.EncryptFile(SongsArchivePath & "\" & FileName, FileCopyPath & "\Songs\" & FileName)
                Application.DoEvents()
            End If
        Next

        lbl_Status.Text = "Status: Executing Fileprep - Copying Promos"

        Application.DoEvents()

        totRows = dsPlaylist.Tables("ActivePromos").Rows.Count

        For rCount = 0 To dsPlaylist.Tables("ActivePromos").Rows.Count - 1

            lbl_Status.Text = "Status: Executing Fileprep - Copying Promos (" & (rCount + 1).ToString & "/" & totRows.ToString & ")"
            Application.DoEvents()
            FileName = dsPlaylist.Tables("ActivePromos").Rows(rCount)("FileName").ToString
            If File.Exists(PromosArchivePath & "\" & FileName) Then
                File.Copy(PromosArchivePath & "\" & FileName, FileCopyPath & "\Promos\" & FileName, True)
                Application.DoEvents()
            End If
        Next

        lbl_Status.Text = "Status: Audio copying completed"
        Application.DoEvents()

        clsInstore = Nothing
        dsPlaylist.Dispose()
        dapPlaylist.Dispose()
        cmdPlaylist.Dispose()
        Call CloseDBConnection()
        lbl_Status.Text = "Status: Fileprep completed"
        Application.DoEvents()

        'exec ProcGetPlaylistDatesForStore 1
        'exec ProcGetPlaylistForSelectedDate 1,'08/01/2012'

    End Sub
    Private Function CheckIfPlayListGenerated() As Boolean
        Dim Playlistdate As Date, Res As Boolean = True
        Playlistdate = DateSerial(cmb_Year.EditValue, cmb_Month.EditValue, 1)

        Dim FTPEnabled, PlaylistStatus As String

        Dim cmdCheckIfFTPEnabled As New SqlCommand
        Call OpenDBConnection()
        With cmdCheckIfFTPEnabled
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckForFTPSync"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Source_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Date", Playlistdate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
            .Parameters.Add("@FTPEnabled", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            .Parameters.Add("@Playlist_Status", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .Parameters.Add("@Upload_Status", SqlDbType.VarChar, 25).Direction = ParameterDirection.Output
            .Parameters.AddWithValue("@From_Utility", 0).Direction = ParameterDirection.Input
            .ExecuteNonQuery()
            FTPEnabled = .Parameters("@FTPEnabled").Value
            PlaylistStatus = .Parameters("@Playlist_Status").Value
        End With
        cmdCheckIfFTPEnabled.Dispose()
        cmdCheckIfFTPEnabled = Nothing
        Call CloseDBConnection()

        If PlaylistStatus = "Not generated" Then
            Res = False
        End If
        Return Res
    End Function
    Private Function StoreConfigured() As Boolean

        Dim cmdCheckStore As New SqlCommand
        Dim result As String

        Call OpenDBConnection()
        With cmdCheckStore
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckStoreConfiguration"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Source_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .ExecuteNonQuery()
            result = .Parameters("@Result").Value
        End With

        If result <> "Success" Then

            XtraMessageBox.Show(result, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False

        Else
            Return True
        End If
        cmdCheckStore.Dispose()

    End Function
    Private Sub GeneratePlaylists(ByVal StoreId As Integer, ByVal CurSourceDate As Date, ByVal CurTargetDate As Date)

        lbl_Status.Text = "Status: Checking Store Configuration"
        Application.DoEvents()

        'If Not StoreConfigured() Then
        '    lbl_Status.Text = "Status:"
        '    Application.DoEvents()
        '    Exit Sub
        'End If

        lbl_Status.Text = "Status: Checking database"
        Application.DoEvents()

        'MsgBox(DateSerial(cmb_Year.EditValue.ToString, cmb_Month.EditValue.ToString, 1).ToString("MM/dd/yyyy").Replace("-", "/"))
        'Checking if playlist already generated
        Dim cmdPlaylist As New SqlCommand

        Call OpenDBConnection()
        'With cmdPlaylist
        '    .Connection = DBConn
        '    .CommandTimeout = 300
        '    .CommandType = CommandType.StoredProcedure
        '    .CommandText = "ProcCheckIfPlaylistGenerated"
        '    .Parameters.AddWithValue("@Store_Id", StoreId).Direction = ParameterDirection.Input
        '    .Parameters.AddWithValue("@Playlist_Date", curdate.ToString("MM/dd/yyyy").Replace("-", "/")).Direction = ParameterDirection.Input
        '    .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
        '    .ExecuteNonQuery()
        'End With

        'If cmdPlaylist.Parameters("@Result").Value.ToString = "Already generated" Then
        '    If XtraMessageBox.Show("Playlist for the " & cmb_Month.Text & "-" & cmb_Year.Text & " exits. Do you want to overwrite?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
        '        lbl_Status.Text = "Status: Regenerating"
        '        Application.DoEvents()
        '    Else
        '        lbl_Status.Text = "Status:"
        '        Application.DoEvents()
        '        Exit Sub
        '    End If
        'End If

        lbl_Status.Text = "Status: Generating playlists for the selected dates"
        Application.DoEvents()

        btn_Generate.Enabled = False
        btn_Quit.Enabled = False


        'Dim cmdPlaylist As New SqlCommand

        'Call OpenDBConnection()
        'With cmdPlaylist
        '    .Connection = DBConn
        '    .CommandTimeout = 300
        '    .CommandType = CommandType.StoredProcedure
        '    .CommandText = "ProcGeneratePlayList"
        '    .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        '    .Parameters.Add("@Playlist_Date", SqlDbType.VarChar, 10).Direction = ParameterDirection.Input
        '    .Parameters.Add("@New_Playlist_Id", SqlDbType.BigInt).Direction = ParameterDirection.Output
        'End With

        lbl_Status.Text = "Status: Currently generating playlist for '" & CurTargetDate.ToString("dd/MM/yyyy").Replace("-", "/") & "'"
        Application.DoEvents()

        cmdPlaylist.Parameters.Clear()
        With cmdPlaylist
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCopyPlayList"
            .Parameters.AddWithValue("@CopyStore_Id", Convert.ToInt32(cmb_Source_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Store_Id", StoreId).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Source_Playlist_Date", CurSourceDate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Target_Playlist_Date", CurTargetDate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
            .Parameters.Add("@New_Playlist_Id", SqlDbType.BigInt).Direction = ParameterDirection.Output
            .ExecuteNonQuery()
        End With

        cmdPlaylist.Dispose()
        Call CloseDBConnection()



    End Sub
    Private Sub LoadCustomers()

        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With

        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)
        cmb_Customer.Properties.Columns(0).Visible = False
        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""

        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub

    Private Sub LoadStores(ByVal Customer_Id As Integer)

        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")


        'Creating Columns for Display in lookup
        cmb_Source_Store.Properties.DisplayMember = "Store_Name"
        cmb_Source_Store.Properties.ValueMember = "Store_ID"
        cmb_Source_Store.Properties.DataSource = dsStores.Tables(0)
        cmb_Source_Store.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Source_Store.Properties.PopulateColumns()
        cmb_Source_Store.Properties.Columns(0).Visible = False
        cmb_Source_Store.Properties.ShowFooter = False
        cmb_Source_Store.Properties.ShowHeader = False
        cmb_Source_Store.EditValue = ""

        cmb_Target_Stores.Properties.DisplayMember = "Store_Name"
        cmb_Target_Stores.Properties.ValueMember = "Store_ID"
        cmb_Target_Stores.Properties.DataSource = dsStores.Tables(0)
        'cmb_Target_Stores.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        'cmb_Target_Stores.Properties.PopulateColumns()
        'cmb_Target_Stores.Properties.Columns(0).Visible = False

        cmb_Target_Stores.Properties.ShowButtons = False
        cmb_Target_Stores.Properties.ShowPopupCloseButton = False
        cmb_Target_Stores.Properties.PopupSizeable = False

        'cmb_Store.Properties.ShowHeader = False
        cmb_Source_Store.EditValue = ""

        dsStores.Dispose()
        dapStores.Dispose()


        Call CloseDBConnection()
    End Sub

    Private Sub cmb_Customer_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If cmb_Customer.EditValue.ToString <> "" Then Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString))
    End Sub
    Private Sub btn_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Quit.Click
        End
    End Sub
    Private Sub InitializeStore()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Source_Store.Properties.DataSource = dt
        dt.Dispose()
    End Sub
    Private Sub LoadMonths()
        Dim dt As New DataTable
        dt.Columns.Add("MonthNo")
        dt.Columns.Add("MonthName")

        dt.Rows.Add(1, "January")
        dt.Rows.Add(2, "February")
        dt.Rows.Add(3, "March")
        dt.Rows.Add(4, "April")
        dt.Rows.Add(5, "May")
        dt.Rows.Add(6, "June")
        dt.Rows.Add(7, "July")
        dt.Rows.Add(8, "August") : dt.Rows.Add(9, "September") : dt.Rows.Add(10, "October") : dt.Rows.Add(11, "November") : dt.Rows.Add(12, "December")

        cmb_Month.Properties.DisplayMember = "MonthName"
        cmb_Month.Properties.ValueMember = "MonthNo"
        cmb_Month.Properties.DataSource = dt
        cmb_Month.Properties.ShowFooter = False
        cmb_Month.Properties.ShowHeader = False
        cmb_Month.Properties.PopulateColumns()
        cmb_Month.Properties.Columns(0).Visible = False

        If Today.Day <= 5 Then
            cmb_Month.Text = Today.ToString("MMMM")
        Else
            cmb_Month.Text = Today.AddMonths(1).ToString("MMMM")
        End If

        cmb_Month.Properties.ForceInitialize()
        dt.Dispose()
        dt = Nothing

        Dim dt1 As New DataTable

        dt1.Columns.Add("YearNo")

        For i As Integer = 2012 To 2020
            dt1.Rows.Add(i)
        Next
        cmb_Year.Properties.DataSource = dt1
        cmb_Year.Properties.DisplayMember = "YearNo"
        cmb_Year.Properties.ValueMember = "YearNo"
        cmb_Year.Properties.PopulateColumns()
        cmb_Year.Properties.ShowFooter = False
        cmb_Year.Properties.ShowHeader = False


        If Now.Date.Day <= 5 Then
            cmb_Year.Text = Today.Year.ToString
        Else
            cmb_Year.Text = Today.AddMonths(1).Year.ToString
        End If
        cmb_Year.Properties.ForceInitialize()

        dt1.Dispose()
    End Sub
    Public Sub InitializeDropDowns()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Customer.Properties.DataSource = dt
        cmb_Month.Properties.DataSource = dt
        cmb_Year.Properties.DataSource = dt
        cmb_Source_Store.Properties.DataSource = dt
        cmb_Month.EditValue = ""
        cmb_Year.EditValue = ""
        dt.Dispose()
        btn_Generate.Enabled = False
    End Sub
    Private Sub frmGeneratePlaylist_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReDim PlaylistDates(100)

        If Not ReadConfig() Then
            Call InitializeDropDowns()
            Exit Sub
        End If
        ReDim PlaylistDates(1)
        ReDim TargetPlaylistDates(1)


        Call LoadSettings()
        Call LoadCustomers()
        Call InitializeStore()
        Call LoadMonths()

    End Sub
    Private Sub cmb_Source_Store_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Source_Store.EditValueChanged

        Application.DoEvents()
        If cmb_Source_Store.EditValue.ToString = "" Then Exit Sub
        Dim Cmd As New SqlCommand, dsPlaylist As New DataSet, dapPlaylist As New SqlDataAdapter
        Call OpenDBConnection()

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetPlaylistDatesForStore"
            .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Source_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        End With
        dapPlaylist.SelectCommand = Cmd
        dapPlaylist.Fill(dsPlaylist, "PlayListDates")

        If dsPlaylist.Tables("PlayListDates").Rows.Count <> 0 Then
            ReDim PlaylistDates(0 To dsPlaylist.Tables("PlayListDates").Rows.Count - 1)
            For i As Integer = 0 To dsPlaylist.Tables("PlayListDates").Rows.Count - 1
                PlaylistDates(i) = dsPlaylist.Tables("PlayListDates").Rows(i).Item(0).ToString
            Next
        Else
            source_playlist_start_date.Text = ""
            source_playlist_end_date.Text = ""
            target_playlist_start_date.Text = ""
            target_playlist_end_date.Text = ""
        End If
        dsPlaylist.Dispose()
        dapPlaylist.Dispose()

        For i As Integer = 0 To cmb_Target_Stores.Properties.Items.Count - 1
            cmb_Target_Stores.Properties.Items(i).Enabled = True
            cmb_Target_Stores.Properties.Items(i).CheckState = CheckState.Unchecked
            'If cmb_Target_Stores.Properties.Items(i).Value.ToString = cmb_Source_Store.EditValue.ToString Then
            '    cmb_Target_Stores.Properties.Items(i).Enabled = False
            'End If
        Next
        Call CloseDBConnection()
    End Sub

    Private Sub Source_playlist_start_date_DrawItem(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs) Handles source_playlist_start_date.DrawItem
        Dim rect As New RectangleF(e.Bounds.Location, e.Bounds.Size)
        ' MsgBox(e.Date.ToString("dd/MM/yyyy"))
        ' MsgBox(e.Style.BackColor.ToArgb)

        If PlaylistDates.Contains(e.Date.ToString("dd-MM-yyyy").Replace("/", "-")) Then
            Dim backColor As Color = Color.LightYellow
            ' your color here
            e.Style.ForeColor = Color.Black
            e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
            'Else
            '    Dim backColor As Color = RGB(255, 95, 95)
            '     your color here
            '    e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
        End If
        e.Graphics.DrawString(e.[Date].Day.ToString(), e.Style.Font, New SolidBrush(e.Style.ForeColor), rect, e.Style.GetStringFormat())
        e.Handled = True
        Return

    End Sub
    Private Sub target_playlist_start_date_DrawItem(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs) Handles target_playlist_start_date.DrawItem
        Dim rect As New RectangleF(e.Bounds.Location, e.Bounds.Size)
        ' MsgBox(e.Date.ToString("dd/MM/yyyy"))
        ' MsgBox(e.Style.BackColor.ToArgb)

        If TargetPlaylistDates(0) = "" Then Exit Sub

        'If TargetPlaylistDates = Nothing OrElse TargetPlaylistDates.Length = 0 Then Exit Sub
        Try
            If TargetPlaylistDates.Contains(e.Date.ToString("dd-MM-yyyy").Replace("/", "-")) Then
                Dim backColor As Color = Color.LightYellow
                ' your color here
                e.Style.ForeColor = Color.Black
                e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
                'Else
                '    Dim backColor As Color = RGB(255, 95, 95)
                '     your color here
                '    e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
            End If
            e.Graphics.DrawString(e.[Date].Day.ToString(), e.Style.Font, New SolidBrush(e.Style.ForeColor), rect, e.Style.GetStringFormat())
            e.Handled = True
            Return
        Catch
            Return
        End Try


    End Sub

    Private Sub Source_playlist_end_date_DrawItem(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs) Handles source_playlist_end_date.DrawItem
        Dim rect As New RectangleF(e.Bounds.Location, e.Bounds.Size)
        ' MsgBox(e.Date.ToString("dd/MM/yyyy"))
        ' MsgBox(e.Style.BackColor.ToArgb)

        If PlaylistDates.Contains(e.Date.ToString("dd-MM-yyyy").Replace("/", "-")) Then
            Dim backColor As Color = Color.LightYellow
            ' your color here
            e.Style.ForeColor = Color.Black
            e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
            'Else
            '    Dim backColor As Color = RGB(255, 95, 95)
            '     your color here
            '    e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
        End If
        e.Graphics.DrawString(e.[Date].Day.ToString(), e.Style.Font, New SolidBrush(e.Style.ForeColor), rect, e.Style.GetStringFormat())
        e.Handled = True
        Return

    End Sub
    Private Sub target_playlist_end_date_DrawItem(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs) Handles target_playlist_end_date.DrawItem
        Dim rect As New RectangleF(e.Bounds.Location, e.Bounds.Size)
        ' MsgBox(e.Date.ToString("dd/MM/yyyy"))
        ' MsgBox(e.Style.BackColor.ToArgb)

        ' Call CheckDates()
        If TargetPlaylistDates(0) = "" Then Exit Sub

        If TargetPlaylistDates.Contains(e.Date.ToString("dd-MM-yyyy").Replace("/", "-")) Then
            Dim backColor As Color = Color.LightYellow
            ' your color here
            e.Style.ForeColor = Color.Black
            e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
            'Else
            '    Dim backColor As Color = RGB(255, 95, 95)
            '     your color here
            '    e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
        End If
        e.Graphics.DrawString(e.[Date].Day.ToString(), e.Style.Font, New SolidBrush(e.Style.ForeColor), rect, e.Style.GetStringFormat())
        e.Handled = True
        Return
    End Sub
    Private Sub Source_playlist_start_date_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles source_playlist_start_date.EditValueChanged
        If source_playlist_start_date.EditValue = Nothing Then Exit Sub
       
        target_playlist_start_date.EditValue = source_playlist_start_date.EditValue
        source_playlist_end_date.EditValue = source_playlist_start_date.EditValue
    End Sub
    Private Sub Source_playlist_end_date_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles source_playlist_end_date.EditValueChanged
        If source_playlist_end_date.EditValue = Nothing Then Exit Sub
        Dim stDate, endDate As Date
        stDate = source_playlist_start_date.EditValue
        endDate = source_playlist_end_date.EditValue
        If endDate < stDate Then
            XtraMessageBox.Show("Invalid 'To' date", "Date Error")
        Else
            target_playlist_start_date.EditValue = source_playlist_start_date.EditValue
            target_playlist_end_date.EditValue = source_playlist_end_date.EditValue
        End If
    End Sub
    Private Sub target_playlist_end_date_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles target_playlist_end_date.EditValueChanged
        If target_playlist_end_date.EditValue = Nothing Then Exit Sub
        Dim stDate, endDate As Date
        stDate = target_playlist_start_date.EditValue
        endDate = target_playlist_end_date.EditValue
        If endDate < stDate Then
            XtraMessageBox.Show("Invalid 'To' date", "Date Error")
        End If
    End Sub

    Private Sub OptionGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OptionGroup.SelectedIndexChanged
        Dim Edit As RadioGroup = CType(sender, RadioGroup)
        lbl_Source_stDate.Text = "Start Date:"
        lbl_Target_stDate.Text = "Start Date:"
        lbl_Source_endDate.Visible = True
        lbl_Target_endDate.Visible = True
        source_playlist_end_date.Visible = True
        target_playlist_end_date.Visible = True

        If Edit.SelectedIndex = 0 Then
            lbl_Source_stDate.Text = "Date:"
            lbl_Target_stDate.Text = "Date:"
            lbl_Source_endDate.Visible = False
            lbl_Target_endDate.Visible = False
            source_playlist_end_date.Visible = False
            target_playlist_end_date.Visible = False
        Else
            If Edit.SelectedIndex = 1 Then
                lbl_Source_stDate.Text = "Date:"
                lbl_Source_endDate.Visible = False
                target_playlist_end_date.Enabled = True
                source_playlist_end_date.Visible = False
            Else
                target_playlist_end_date.Enabled = False
                target_playlist_end_date.EditValue = source_playlist_end_date.EditValue
            End If


        End If

    End Sub

    Private Sub target_playlist_start_date_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles target_playlist_start_date.EditValueChanged
        If target_playlist_start_date.EditValue = Nothing Then Exit Sub
        If OptionGroup.SelectedIndex = 2 Then
            target_playlist_end_date.EditValue = DateAdd(DateInterval.Day, DateDiff(DateInterval.Day, source_playlist_start_date.EditValue, source_playlist_end_date.EditValue), target_playlist_start_date.EditValue)
        End If
    End Sub

    Private Sub CheckDates()
        Application.DoEvents()
        If cmb_Source_Store.EditValue.ToString = "" Then Exit Sub

        Dim CheckedCount, SelectedStore As Integer

        For i = 0 To cmb_Target_Stores.Properties.Items.Count - 1
            If cmb_Target_Stores.Properties.Items(i).CheckState = CheckState.Checked Then
                CheckedCount += 1
                If SelectedStore = 0 Then SelectedStore = cmb_Target_Stores.Properties.Items(i).Value
            End If
        Next

        If CheckedCount > 1 OrElse CheckedCount = 0 Then ReDim TargetPlaylistDates(1) : Exit Sub

        Dim Cmd As New SqlCommand, dsPlaylist As New DataSet, dapPlaylist As New SqlDataAdapter
        Call OpenDBConnection()

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetPlaylistDatesForStore"
            .Parameters.AddWithValue("@Store_ID", SelectedStore).Direction = ParameterDirection.Input
        End With
        dapPlaylist.SelectCommand = Cmd
        dapPlaylist.Fill(dsPlaylist, "PlayListDates")

        If dsPlaylist.Tables("PlayListDates").Rows.Count <> 0 Then
            ReDim TargetPlaylistDates(0 To dsPlaylist.Tables("PlayListDates").Rows.Count - 1)
            For i As Integer = 0 To dsPlaylist.Tables("PlayListDates").Rows.Count - 1
                TargetPlaylistDates(i) = dsPlaylist.Tables("PlayListDates").Rows(i).Item(0).ToString
            Next
            'Else
            '    source_playlist_start_date.Text = ""
            '    source_playlist_end_date.Text = ""
            '    target_playlist_start_date.Text = ""
            '    target_playlist_end_date.Text = ""
        End If
        dsPlaylist.Dispose()
        dapPlaylist.Dispose()

        Call CloseDBConnection()
    End Sub

    Private Sub cmb_Target_Stores_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Target_Stores.EditValueChanged
        Call CheckDates()
    End Sub
End Class