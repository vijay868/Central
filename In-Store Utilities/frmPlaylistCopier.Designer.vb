﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlaylistCopier
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPlaylistCopier))
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel()
        Me.Genre_Panel = New DevExpress.XtraEditors.PanelControl()
        Me.OptionGroup = New DevExpress.XtraEditors.RadioGroup()
        Me.Target_Group = New DevExpress.XtraEditors.GroupControl()
        Me.target_playlist_end_date = New DevExpress.XtraEditors.DateEdit()
        Me.cmb_Target_Stores = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.lbl_Target_endDate = New System.Windows.Forms.Label()
        Me.lbl_Target_stDate = New System.Windows.Forms.Label()
        Me.target_playlist_start_date = New DevExpress.XtraEditors.DateEdit()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Source_Group = New DevExpress.XtraEditors.GroupControl()
        Me.lbl_Source_endDate = New System.Windows.Forms.Label()
        Me.source_playlist_end_date = New DevExpress.XtraEditors.DateEdit()
        Me.lbl_Source_stDate = New System.Windows.Forms.Label()
        Me.source_playlist_start_date = New DevExpress.XtraEditors.DateEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmb_Source_Store = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmb_Year = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmb_Month = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_Generate = New DevExpress.XtraEditors.SimpleButton()
        Me.lbl_Status = New System.Windows.Forms.Label()
        Me.btn_Quit = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.Playlist_Copier = New DevExpress.XtraEditors.GroupControl()
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Genre_Panel.SuspendLayout()
        CType(Me.OptionGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Target_Group, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Target_Group.SuspendLayout()
        CType(Me.target_playlist_end_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.target_playlist_end_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Target_Stores.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.target_playlist_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.target_playlist_start_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Source_Group, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Source_Group.SuspendLayout()
        CType(Me.source_playlist_end_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.source_playlist_end_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.source_playlist_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.source_playlist_start_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Source_Store.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Year.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Month.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.Playlist_Copier, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Playlist_Copier.SuspendLayout()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Genre_Panel
        '
        Me.Genre_Panel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.Genre_Panel.Controls.Add(Me.OptionGroup)
        Me.Genre_Panel.Controls.Add(Me.Target_Group)
        Me.Genre_Panel.Controls.Add(Me.Source_Group)
        Me.Genre_Panel.Controls.Add(Me.cmb_Year)
        Me.Genre_Panel.Controls.Add(Me.cmb_Month)
        Me.Genre_Panel.Controls.Add(Me.Label2)
        Me.Genre_Panel.Controls.Add(Me.Label1)
        Me.Genre_Panel.Controls.Add(Me.cmb_Customer)
        Me.Genre_Panel.Controls.Add(Me.Label5)
        Me.Genre_Panel.Controls.Add(Me.btn_Generate)
        Me.Genre_Panel.Controls.Add(Me.lbl_Status)
        Me.Genre_Panel.Controls.Add(Me.btn_Quit)
        Me.Genre_Panel.Location = New System.Drawing.Point(1, 21)
        Me.Genre_Panel.Name = "Genre_Panel"
        Me.Genre_Panel.Size = New System.Drawing.Size(660, 271)
        Me.Genre_Panel.TabIndex = 58
        '
        'OptionGroup
        '
        Me.OptionGroup.EditValue = CType(1, Short)
        Me.OptionGroup.Location = New System.Drawing.Point(289, 24)
        Me.OptionGroup.Name = "OptionGroup"
        Me.OptionGroup.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OptionGroup.Properties.Appearance.Options.UseFont = True
        Me.OptionGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.OptionGroup.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "Single Day"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(2, Short), "One - Many Days"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(3, Short), "Many - Many Days")})
        Me.OptionGroup.Size = New System.Drawing.Size(364, 26)
        Me.OptionGroup.TabIndex = 98
        '
        'Target_Group
        '
        Me.Target_Group.Controls.Add(Me.target_playlist_end_date)
        Me.Target_Group.Controls.Add(Me.cmb_Target_Stores)
        Me.Target_Group.Controls.Add(Me.lbl_Target_endDate)
        Me.Target_Group.Controls.Add(Me.lbl_Target_stDate)
        Me.Target_Group.Controls.Add(Me.target_playlist_start_date)
        Me.Target_Group.Controls.Add(Me.Label9)
        Me.Target_Group.Location = New System.Drawing.Point(289, 57)
        Me.Target_Group.Name = "Target_Group"
        Me.Target_Group.Size = New System.Drawing.Size(250, 172)
        Me.Target_Group.TabIndex = 97
        Me.Target_Group.Text = "Target Store(s)"
        '
        'target_playlist_end_date
        '
        Me.target_playlist_end_date.EditValue = Nothing
        Me.target_playlist_end_date.Location = New System.Drawing.Point(8, 140)
        Me.target_playlist_end_date.Name = "target_playlist_end_date"
        Me.target_playlist_end_date.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.target_playlist_end_date.Properties.Appearance.Options.UseFont = True
        Me.target_playlist_end_date.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.target_playlist_end_date.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.target_playlist_end_date.Properties.AppearanceDisabled.Options.UseFont = True
        Me.target_playlist_end_date.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.target_playlist_end_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.target_playlist_end_date.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.target_playlist_end_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.target_playlist_end_date.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.target_playlist_end_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.target_playlist_end_date.Size = New System.Drawing.Size(112, 22)
        Me.target_playlist_end_date.TabIndex = 86
        Me.target_playlist_end_date.Visible = False
        '
        'cmb_Target_Stores
        '
        Me.cmb_Target_Stores.Location = New System.Drawing.Point(8, 49)
        Me.cmb_Target_Stores.Name = "cmb_Target_Stores"
        Me.cmb_Target_Stores.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Target_Stores.Properties.Appearance.Options.UseFont = True
        Me.cmb_Target_Stores.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Target_Stores.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Target_Stores.TabIndex = 85
        '
        'lbl_Target_endDate
        '
        Me.lbl_Target_endDate.AutoSize = True
        Me.lbl_Target_endDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Target_endDate.Location = New System.Drawing.Point(8, 121)
        Me.lbl_Target_endDate.Name = "lbl_Target_endDate"
        Me.lbl_Target_endDate.Size = New System.Drawing.Size(61, 15)
        Me.lbl_Target_endDate.TabIndex = 84
        Me.lbl_Target_endDate.Text = "End Date:"
        Me.lbl_Target_endDate.Visible = False
        '
        'lbl_Target_stDate
        '
        Me.lbl_Target_stDate.AutoSize = True
        Me.lbl_Target_stDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Target_stDate.Location = New System.Drawing.Point(5, 76)
        Me.lbl_Target_stDate.Name = "lbl_Target_stDate"
        Me.lbl_Target_stDate.Size = New System.Drawing.Size(36, 15)
        Me.lbl_Target_stDate.TabIndex = 82
        Me.lbl_Target_stDate.Text = "Date:"
        '
        'target_playlist_start_date
        '
        Me.target_playlist_start_date.EditValue = Nothing
        Me.target_playlist_start_date.Location = New System.Drawing.Point(8, 95)
        Me.target_playlist_start_date.Name = "target_playlist_start_date"
        Me.target_playlist_start_date.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.target_playlist_start_date.Properties.Appearance.Options.UseFont = True
        Me.target_playlist_start_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.target_playlist_start_date.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.target_playlist_start_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.target_playlist_start_date.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.target_playlist_start_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.target_playlist_start_date.Size = New System.Drawing.Size(112, 22)
        Me.target_playlist_start_date.TabIndex = 81
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(5, 31)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(53, 15)
        Me.Label9.TabIndex = 72
        Me.Label9.Text = "Store(s):"
        '
        'Source_Group
        '
        Me.Source_Group.Controls.Add(Me.lbl_Source_endDate)
        Me.Source_Group.Controls.Add(Me.source_playlist_end_date)
        Me.Source_Group.Controls.Add(Me.lbl_Source_stDate)
        Me.Source_Group.Controls.Add(Me.source_playlist_start_date)
        Me.Source_Group.Controls.Add(Me.Label7)
        Me.Source_Group.Controls.Add(Me.cmb_Source_Store)
        Me.Source_Group.Location = New System.Drawing.Point(13, 57)
        Me.Source_Group.Name = "Source_Group"
        Me.Source_Group.Size = New System.Drawing.Size(250, 172)
        Me.Source_Group.TabIndex = 96
        Me.Source_Group.Text = "Source Store"
        '
        'lbl_Source_endDate
        '
        Me.lbl_Source_endDate.AutoSize = True
        Me.lbl_Source_endDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Source_endDate.Location = New System.Drawing.Point(8, 121)
        Me.lbl_Source_endDate.Name = "lbl_Source_endDate"
        Me.lbl_Source_endDate.Size = New System.Drawing.Size(61, 15)
        Me.lbl_Source_endDate.TabIndex = 84
        Me.lbl_Source_endDate.Text = "End Date:"
        Me.lbl_Source_endDate.Visible = False
        '
        'source_playlist_end_date
        '
        Me.source_playlist_end_date.EditValue = Nothing
        Me.source_playlist_end_date.Location = New System.Drawing.Point(8, 140)
        Me.source_playlist_end_date.Name = "source_playlist_end_date"
        Me.source_playlist_end_date.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.source_playlist_end_date.Properties.Appearance.Options.UseFont = True
        Me.source_playlist_end_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.source_playlist_end_date.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.source_playlist_end_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.source_playlist_end_date.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.source_playlist_end_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.source_playlist_end_date.Size = New System.Drawing.Size(112, 22)
        Me.source_playlist_end_date.TabIndex = 83
        Me.source_playlist_end_date.Visible = False
        '
        'lbl_Source_stDate
        '
        Me.lbl_Source_stDate.AutoSize = True
        Me.lbl_Source_stDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Source_stDate.Location = New System.Drawing.Point(5, 76)
        Me.lbl_Source_stDate.Name = "lbl_Source_stDate"
        Me.lbl_Source_stDate.Size = New System.Drawing.Size(36, 15)
        Me.lbl_Source_stDate.TabIndex = 82
        Me.lbl_Source_stDate.Text = "Date:"
        '
        'source_playlist_start_date
        '
        Me.source_playlist_start_date.EditValue = Nothing
        Me.source_playlist_start_date.Location = New System.Drawing.Point(8, 95)
        Me.source_playlist_start_date.Name = "source_playlist_start_date"
        Me.source_playlist_start_date.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.source_playlist_start_date.Properties.Appearance.Options.UseFont = True
        Me.source_playlist_start_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.source_playlist_start_date.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.source_playlist_start_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.source_playlist_start_date.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.source_playlist_start_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.source_playlist_start_date.Size = New System.Drawing.Size(112, 22)
        Me.source_playlist_start_date.TabIndex = 81
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(5, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 15)
        Me.Label7.TabIndex = 72
        Me.Label7.Text = "Store:"
        '
        'cmb_Source_Store
        '
        Me.cmb_Source_Store.EditValue = ""
        Me.cmb_Source_Store.Location = New System.Drawing.Point(5, 49)
        Me.cmb_Source_Store.Name = "cmb_Source_Store"
        Me.cmb_Source_Store.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Source_Store.Properties.Appearance.Options.UseFont = True
        Me.cmb_Source_Store.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Source_Store.Properties.NullText = ""
        Me.cmb_Source_Store.Size = New System.Drawing.Size(237, 22)
        Me.cmb_Source_Store.TabIndex = 71
        '
        'cmb_Year
        '
        Me.cmb_Year.Location = New System.Drawing.Point(230, 302)
        Me.cmb_Year.Name = "cmb_Year"
        Me.cmb_Year.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Year.Properties.Appearance.Options.UseFont = True
        Me.cmb_Year.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Year.Properties.NullText = ""
        Me.cmb_Year.Size = New System.Drawing.Size(100, 22)
        Me.cmb_Year.TabIndex = 76
        '
        'cmb_Month
        '
        Me.cmb_Month.Location = New System.Drawing.Point(57, 302)
        Me.cmb_Month.Name = "cmb_Month"
        Me.cmb_Month.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Month.Properties.Appearance.Options.UseFont = True
        Me.cmb_Month.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Month.Properties.NullText = ""
        Me.cmb_Month.Size = New System.Drawing.Size(114, 22)
        Me.cmb_Month.TabIndex = 75
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(189, 305)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 15)
        Me.Label2.TabIndex = 72
        Me.Label2.Text = "Year:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 305)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 15)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "Month:"
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(13, 26)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.NullText = ""
        Me.cmb_Customer.Size = New System.Drawing.Size(250, 22)
        Me.cmb_Customer.TabIndex = 67
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "Customer:"
        '
        'btn_Generate
        '
        Me.btn_Generate.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Generate.Appearance.Options.UseFont = True
        Me.btn_Generate.Location = New System.Drawing.Point(545, 171)
        Me.btn_Generate.Name = "btn_Generate"
        Me.btn_Generate.Size = New System.Drawing.Size(108, 23)
        Me.btn_Generate.TabIndex = 66
        Me.btn_Generate.Text = "Generate"
        '
        'lbl_Status
        '
        Me.lbl_Status.AutoSize = True
        Me.lbl_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Status.Location = New System.Drawing.Point(18, 244)
        Me.lbl_Status.Name = "lbl_Status"
        Me.lbl_Status.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Status.TabIndex = 65
        Me.lbl_Status.Text = "Status:"
        '
        'btn_Quit
        '
        Me.btn_Quit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Quit.Appearance.Options.UseFont = True
        Me.btn_Quit.Location = New System.Drawing.Point(545, 206)
        Me.btn_Quit.Name = "btn_Quit"
        Me.btn_Quit.Size = New System.Drawing.Size(108, 23)
        Me.btn_Quit.TabIndex = 62
        Me.btn_Quit.Text = "Exit"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(1, -13)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(1313, 948)
        Me.XtraTabControl1.TabIndex = 59
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage2})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.Playlist_Copier)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1307, 929)
        '
        'Playlist_Copier
        '
        Me.Playlist_Copier.Controls.Add(Me.Genre_Panel)
        Me.Playlist_Copier.Location = New System.Drawing.Point(0, 0)
        Me.Playlist_Copier.Name = "Playlist_Copier"
        Me.Playlist_Copier.Size = New System.Drawing.Size(661, 292)
        Me.Playlist_Copier.TabIndex = 59
        Me.Playlist_Copier.Text = "Copy Playlist"
        '
        'frmPlaylistCopier
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(664, 291)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPlaylistCopier"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generate/Delete Playlist/FilePrep"
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Genre_Panel.ResumeLayout(False)
        Me.Genre_Panel.PerformLayout()
        CType(Me.OptionGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Target_Group, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Target_Group.ResumeLayout(False)
        Me.Target_Group.PerformLayout()
        CType(Me.target_playlist_end_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.target_playlist_end_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Target_Stores.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.target_playlist_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.target_playlist_start_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Source_Group, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Source_Group.ResumeLayout(False)
        Me.Source_Group.PerformLayout()
        CType(Me.source_playlist_end_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.source_playlist_end_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.source_playlist_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.source_playlist_start_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Source_Store.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Year.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Month.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.Playlist_Copier, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Playlist_Copier.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents Genre_Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_Generate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbl_Status As System.Windows.Forms.Label
    Friend WithEvents btn_Quit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmb_Year As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmb_Month As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Playlist_Copier As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Target_Group As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cmb_Target_Stores As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents lbl_Target_endDate As System.Windows.Forms.Label
    Friend WithEvents lbl_Target_stDate As System.Windows.Forms.Label
    Friend WithEvents target_playlist_start_date As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Source_Group As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lbl_Source_endDate As System.Windows.Forms.Label
    Friend WithEvents source_playlist_end_date As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lbl_Source_stDate As System.Windows.Forms.Label
    Friend WithEvents source_playlist_start_date As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmb_Source_Store As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents OptionGroup As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents target_playlist_end_date As DevExpress.XtraEditors.DateEdit
End Class
