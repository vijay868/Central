﻿Module ModUtilities
    Public AppMode As String
    Public Sub Main()

        Dim Param() As String, ExePath, FormName As String, LoginId As String
        FormName = ""
        LoginId = ""
        Param = System.Environment.GetCommandLineArgs
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False

        If UBound(Param) <> 0 Then
            ExePath = Param(0)
            'FormName = UCase(Param(1))
            FormName = "CP"

        End If

        If FormName = "MDI" Then
            Application.Run(frmMetaDataImport)
        ElseIf FormName = "GPL" Then
            AppMode = "Generate Playlist"
            Application.Run(frmGeneratePlaylist)
        ElseIf FormName = "DPL" Then
            AppMode = "Delete Playlist"
            Application.Run(frmGeneratePlaylist)
        ElseIf FormName = "FP" Then
            AppMode = "FilePrep"
            Application.Run(frmGeneratePlaylist)
        ElseIf FormName = "CP" Then
            AppMode = "Playlist Copier"
            Application.Run(frmPlaylistCopier)
        End If
    End Sub
End Module
