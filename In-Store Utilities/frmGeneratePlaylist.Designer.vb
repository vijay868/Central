﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeneratePlaylist
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGeneratePlaylist))
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.Genre_Panel = New DevExpress.XtraEditors.PanelControl()
        Me.lbl_Source_endDate = New System.Windows.Forms.Label()
        Me.dt_to = New DevExpress.XtraEditors.DateEdit()
        Me.lbl_Source_stDate = New System.Windows.Forms.Label()
        Me.dt_from = New DevExpress.XtraEditors.DateEdit()
        Me.cmb_Year = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmb_Month = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmb_Store = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_Generate = New DevExpress.XtraEditors.SimpleButton()
        Me.lbl_Status = New System.Windows.Forms.Label()
        Me.btn_Quit = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.Group_Utilities = New DevExpress.XtraEditors.GroupControl()
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Genre_Panel.SuspendLayout()
        CType(Me.dt_to.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dt_to.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dt_from.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dt_from.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Year.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Month.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.Group_Utilities, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Group_Utilities.SuspendLayout()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Genre_Panel
        '
        Me.Genre_Panel.Controls.Add(Me.lbl_Source_endDate)
        Me.Genre_Panel.Controls.Add(Me.dt_to)
        Me.Genre_Panel.Controls.Add(Me.lbl_Source_stDate)
        Me.Genre_Panel.Controls.Add(Me.dt_from)
        Me.Genre_Panel.Controls.Add(Me.cmb_Year)
        Me.Genre_Panel.Controls.Add(Me.cmb_Month)
        Me.Genre_Panel.Controls.Add(Me.Label2)
        Me.Genre_Panel.Controls.Add(Me.Label1)
        Me.Genre_Panel.Controls.Add(Me.cmb_Store)
        Me.Genre_Panel.Controls.Add(Me.Label7)
        Me.Genre_Panel.Controls.Add(Me.cmb_Customer)
        Me.Genre_Panel.Controls.Add(Me.Label5)
        Me.Genre_Panel.Controls.Add(Me.btn_Generate)
        Me.Genre_Panel.Controls.Add(Me.lbl_Status)
        Me.Genre_Panel.Controls.Add(Me.btn_Quit)
        Me.Genre_Panel.Location = New System.Drawing.Point(1, 21)
        Me.Genre_Panel.Name = "Genre_Panel"
        Me.Genre_Panel.Size = New System.Drawing.Size(564, 175)
        Me.Genre_Panel.TabIndex = 58
        '
        'lbl_Source_endDate
        '
        Me.lbl_Source_endDate.AutoSize = True
        Me.lbl_Source_endDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Source_endDate.Location = New System.Drawing.Point(182, 99)
        Me.lbl_Source_endDate.Name = "lbl_Source_endDate"
        Me.lbl_Source_endDate.Size = New System.Drawing.Size(61, 15)
        Me.lbl_Source_endDate.TabIndex = 88
        Me.lbl_Source_endDate.Text = "End Date:"
        '
        'dt_to
        '
        Me.dt_to.EditValue = Nothing
        Me.dt_to.Location = New System.Drawing.Point(242, 96)
        Me.dt_to.Name = "dt_to"
        Me.dt_to.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dt_to.Properties.Appearance.Options.UseFont = True
        Me.dt_to.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dt_to.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.dt_to.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dt_to.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.dt_to.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dt_to.Size = New System.Drawing.Size(105, 22)
        Me.dt_to.TabIndex = 87
        '
        'lbl_Source_stDate
        '
        Me.lbl_Source_stDate.AutoSize = True
        Me.lbl_Source_stDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Source_stDate.Location = New System.Drawing.Point(7, 99)
        Me.lbl_Source_stDate.Name = "lbl_Source_stDate"
        Me.lbl_Source_stDate.Size = New System.Drawing.Size(64, 15)
        Me.lbl_Source_stDate.TabIndex = 86
        Me.lbl_Source_stDate.Text = "Start Date:"
        '
        'dt_from
        '
        Me.dt_from.EditValue = Nothing
        Me.dt_from.Location = New System.Drawing.Point(75, 96)
        Me.dt_from.Name = "dt_from"
        Me.dt_from.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dt_from.Properties.Appearance.Options.UseFont = True
        Me.dt_from.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dt_from.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.dt_from.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dt_from.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.dt_from.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dt_from.Size = New System.Drawing.Size(105, 22)
        Me.dt_from.TabIndex = 85
        '
        'cmb_Year
        '
        Me.cmb_Year.Location = New System.Drawing.Point(248, 189)
        Me.cmb_Year.Name = "cmb_Year"
        Me.cmb_Year.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Year.Properties.Appearance.Options.UseFont = True
        Me.cmb_Year.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Year.Properties.NullText = ""
        Me.cmb_Year.Size = New System.Drawing.Size(100, 22)
        Me.cmb_Year.TabIndex = 76
        '
        'cmb_Month
        '
        Me.cmb_Month.Location = New System.Drawing.Point(75, 194)
        Me.cmb_Month.Name = "cmb_Month"
        Me.cmb_Month.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Month.Properties.Appearance.Options.UseFont = True
        Me.cmb_Month.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Month.Properties.NullText = ""
        Me.cmb_Month.Size = New System.Drawing.Size(114, 22)
        Me.cmb_Month.TabIndex = 75
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(207, 99)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 15)
        Me.Label2.TabIndex = 72
        Me.Label2.Text = "Year:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(28, 99)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 15)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "Month:"
        '
        'cmb_Store
        '
        Me.cmb_Store.EditValue = ""
        Me.cmb_Store.Location = New System.Drawing.Point(75, 58)
        Me.cmb_Store.Name = "cmb_Store"
        Me.cmb_Store.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Store.Properties.Appearance.Options.UseFont = True
        Me.cmb_Store.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Store.Properties.NullText = ""
        Me.cmb_Store.Size = New System.Drawing.Size(273, 22)
        Me.cmb_Store.TabIndex = 70
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(33, 60)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 15)
        Me.Label7.TabIndex = 69
        Me.Label7.Text = "Store:"
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(75, 20)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.NullText = ""
        Me.cmb_Customer.Size = New System.Drawing.Size(273, 22)
        Me.cmb_Customer.TabIndex = 67
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "Customer:"
        '
        'btn_Generate
        '
        Me.btn_Generate.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Generate.Appearance.Options.UseFont = True
        Me.btn_Generate.Location = New System.Drawing.Point(400, 99)
        Me.btn_Generate.Name = "btn_Generate"
        Me.btn_Generate.Size = New System.Drawing.Size(108, 23)
        Me.btn_Generate.TabIndex = 66
        Me.btn_Generate.Text = "Generate"
        '
        'lbl_Status
        '
        Me.lbl_Status.AutoSize = True
        Me.lbl_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Status.Location = New System.Drawing.Point(29, 140)
        Me.lbl_Status.Name = "lbl_Status"
        Me.lbl_Status.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Status.TabIndex = 65
        Me.lbl_Status.Text = "Status:"
        '
        'btn_Quit
        '
        Me.btn_Quit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Quit.Appearance.Options.UseFont = True
        Me.btn_Quit.Location = New System.Drawing.Point(400, 140)
        Me.btn_Quit.Name = "btn_Quit"
        Me.btn_Quit.Size = New System.Drawing.Size(108, 23)
        Me.btn_Quit.TabIndex = 62
        Me.btn_Quit.Text = "Exit"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(1, -13)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(1313, 948)
        Me.XtraTabControl1.TabIndex = 59
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage2})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.Group_Utilities)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1307, 929)
        '
        'Group_Utilities
        '
        Me.Group_Utilities.Controls.Add(Me.Genre_Panel)
        Me.Group_Utilities.Location = New System.Drawing.Point(0, 0)
        Me.Group_Utilities.Name = "Group_Utilities"
        Me.Group_Utilities.Size = New System.Drawing.Size(535, 196)
        Me.Group_Utilities.TabIndex = 59
        Me.Group_Utilities.Text = "Generate/Delete Playlist/FilePrep"
        '
        'frmGeneratePlaylist
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(538, 198)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGeneratePlaylist"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generate/Delete Playlist/FilePrep"
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Genre_Panel.ResumeLayout(False)
        Me.Genre_Panel.PerformLayout()
        CType(Me.dt_to.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dt_to.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dt_from.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dt_from.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Year.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Month.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.Group_Utilities, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Group_Utilities.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents Genre_Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_Generate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbl_Status As System.Windows.Forms.Label
    Friend WithEvents btn_Quit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cmb_Store As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmb_Year As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmb_Month As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Group_Utilities As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lbl_Source_endDate As System.Windows.Forms.Label
    Friend WithEvents dt_to As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lbl_Source_stDate As System.Windows.Forms.Label
    Friend WithEvents dt_from As DevExpress.XtraEditors.DateEdit
End Class
