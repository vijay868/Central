﻿
Imports DevExpress.XtraEditors

Public Class frmUtilities
    
    Private Sub frmUtilities_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cmb_Utilities.Properties.Items.Add("Generate Playlist")
        cmb_Utilities.Properties.Items.Add("Delete Playlist")
        cmb_Utilities.Properties.Items.Add("Metadata Import")
        cmb_Utilities.Properties.Items.Add("FTPSync")
        cmb_Utilities.Properties.Items.Add("FilePrep")
        cmb_Utilities.Properties.Items.Add("Copy Playlist")
    End Sub

    Private Sub btn_Run_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Run.Click

        If cmb_Utilities.Text = "" Then
            XtraMessageBox.Show("Select the Utility to run", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        ElseIf PromosArchivePath = "" OrElse SongsArchivePath = "" Then
            XtraMessageBox.Show("Configuration of paths was not done. Add paths in 'Config->Audio'", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If cmb_Utilities.Text = "Metadata Import" Then
            'frmMetaDataImport.Show()
            Process.Start(Application.StartupPath & "\PlayTM Utilities.exe", "MDI")
        ElseIf cmb_Utilities.Text = "Delete Playlist" Then
            'frmGeneratePlaylist.Show()
            'frmGeneratePlaylist.Text = "Delete Playlist"
            'frmGeneratePlaylist.btn_Generate.Text = "Delete"
            Process.Start(Application.StartupPath & "\PlayTM Utilities.exe", "DPL")
        ElseIf cmb_Utilities.Text = "Generate Playlist" Then
            'frmGeneratePlaylist.Show()
            'frmGeneratePlaylist.Text = "Generate Playlist"
            Process.Start(Application.StartupPath & "\PlayTM Utilities.exe", "GPL")
        ElseIf cmb_Utilities.Text = "FTPSync" Then
            'XtraMessageBox.Show("Not yet done")
            'frmFTPSync.Show()
            'frmGeneratePlaylist.Text = "FTP Sync"
            Process.Start(Application.StartupPath & "\PlayTM FTPSync.exe")
        ElseIf cmb_Utilities.Text = "FilePrep" Then
            'frmGeneratePlaylist.Show()
            'frmGeneratePlaylist.Text = "File Prep"
            Process.Start(Application.StartupPath & "\PlayTM Utilities.exe", "FP")
        ElseIf cmb_Utilities.Text = "Copy Playlist" Then
            'frmGeneratePlaylist.Show()
            'frmGeneratePlaylist.Text = "File Prep"
            Process.Start(Application.StartupPath & "\PlayTM Utilities.exe", "CP")
        End If
    End Sub

    Private Sub btn_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Quit.Click
        Call MDIInstoreCentral.CloseChild()
        Dim w As New frmBackGround

        With w
            .MdiParent = MDIInstoreCentral
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
        End With
    End Sub

    Private Sub XtraTabPage2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles XtraTabPage2.Paint

    End Sub
End Class