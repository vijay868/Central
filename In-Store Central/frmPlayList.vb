﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO
Imports DevExpress.Utils
Imports INSCentral.libZPlay
Imports DevExpress.XtraEditors

Public Class frmPlayList
    Dim PlaylistDates() As String
    Dim CurFileRowIndex As Integer = 0
    Dim bPlayMedia As Boolean = False
    Dim iRowIndex As Integer = 0
    Dim Searchstring As String = ""
    Dim StringFound As Boolean = False
    Dim insPlaylistId, insMediaId As Integer
    Dim insMediaType, insAirTime, insCutAirTime As String
    Dim StoreStTime, StoreEndTime As Integer

    Private Sub LoadCustomers()

        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        cmb_Customer.Properties.Columns(0).Visible = False
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)

        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""
        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub
    Private Sub FormatSongsGrid(ByVal ResetStructure As Boolean)

        If ResetStructure Then
            Dim gridData As New DataTable
            gridData.Columns.Add("Playlist_ID")
            gridData.Columns.Add("Media_ID")
            gridData.Columns.Add("Media_Type")
            gridData.Columns.Add("Airtime")
            gridData.Columns.Add("Category")
            gridData.Columns.Add("Title")
            gridData.Columns.Add("Album")
            gridData.Columns.Add("Artists")
            gridData.Columns.Add("FileName")
            gridData.Columns.Add("Duration")

            'For i As Integer = 1 To 30
            '    ' PromosGrid.Rows.Add("", "", "", "")
            '    gridData.Rows.Add("", "", "", "", "", "", "", "", "")
            'Next

            SongsGrid.Columns.Clear()
            SongsGrid.DataSource = gridData

        End If

        SongsGrid.Columns("Playlist_ID").Visible = False
        SongsGrid.Columns("Media_ID").Visible = False
        SongsGrid.Columns("Media_Type").Visible = False
        SongsGrid.Columns("Airtime").Width = 80
        SongsGrid.Columns("Airtime").SortMode = DataGridViewColumnSortMode.NotSortable
        SongsGrid.Columns("Category").Width = 80
        SongsGrid.Columns("Category").SortMode = DataGridViewColumnSortMode.NotSortable
        SongsGrid.Columns("Title").Width = 360
        SongsGrid.Columns("Title").SortMode = DataGridViewColumnSortMode.NotSortable
        SongsGrid.Columns("Album").Width = 360
        SongsGrid.Columns("Album").SortMode = DataGridViewColumnSortMode.NotSortable
        SongsGrid.Columns("Artists").Width = 360
        SongsGrid.Columns("Artists").SortMode = DataGridViewColumnSortMode.NotSortable
        SongsGrid.Columns("FileName").Visible = False
        SongsGrid.Columns("Duration").Width = 80
        SongsGrid.Columns("Duration").SortMode = DataGridViewColumnSortMode.NotSortable
        SongsGrid.Refresh()
    End Sub
    Private Sub FormatSongsReplaceGrid(ByVal ResetStructure As Boolean)

        If ResetStructure Then
            Dim gridData As New DataTable
            gridData.Columns.Add("Replace_Song_Id")
            gridData.Columns.Add("Replace_Song_Cetegory")
            gridData.Columns.Add("Replace_Song_Title")
            gridData.Columns.Add("Replace_Song_Album")
            gridData.Columns.Add("Replace_Song_Artists")
            gridData.Columns.Add("Replace_Song_FileName")
            gridData.Columns.Add("Replace_Song_Duration")

            'For i As Integer = 1 To 30
            '    ' PromosGrid.Rows.Add("", "", "", "")
            '    gridData.Rows.Add("", "", "", "", "", "", "", "", "")
            'Next

            Grid_Song_Replace.Columns.Clear()
            Grid_Song_Replace.DataSource = gridData

        End If

        Grid_Song_Replace.Columns(0).Width = 80
        Grid_Song_Replace.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
        Grid_Song_Replace.Columns(0).HeaderText = "Song ID"
        Grid_Song_Replace.Columns(1).Width = 80
        Grid_Song_Replace.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
        Grid_Song_Replace.Columns(1).HeaderText = "Category"
        Grid_Song_Replace.Columns(2).Width = 200
        Grid_Song_Replace.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
        Grid_Song_Replace.Columns(2).HeaderText = "Title"
        Grid_Song_Replace.Columns(3).Width = 200
        Grid_Song_Replace.Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
        Grid_Song_Replace.Columns(3).HeaderText = "Album"
        Grid_Song_Replace.Columns(4).Width = 200
        Grid_Song_Replace.Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
        Grid_Song_Replace.Columns(4).HeaderText = "Artist"
        Grid_Song_Replace.Columns(5).Visible = False
        Grid_Song_Replace.Columns(6).Width = 80
        Grid_Song_Replace.Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
        Grid_Song_Replace.Columns(6).HeaderText = "Duration"
        Grid_Song_Replace.Refresh()
    End Sub
    Private Sub SongsGrid_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles SongsGrid.CellDoubleClick
        CurFileRowIndex = e.RowIndex
        If CurFileRowIndex <> -1 AndAlso SongsGrid.Rows(e.RowIndex).Cells("Media_Type").Value.ToString <> "H" Then
            Call LoadFile()
            TmrPlayState.Enabled = True
        End If
    End Sub

    Private Sub SongsGrid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles SongsGrid.KeyDown
        If SongsGrid.RowCount = 0 Then Exit Sub

        If SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_Type").Value.ToString = "H" Then Exit Sub

        If e.KeyCode = Keys.Delete AndAlso SongsGrid.RowCount > 0 Then
            If Not SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells(1).Value Is Nothing Then
                Dim CurRow As Integer = SongsGrid.CurrentRow.Index
                Dim CurFirstRowIndex As Integer = SongsGrid.FirstDisplayedScrollingRowIndex
                DeleteFromPlaylist(CInt(SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Playlist_ID").Value.ToString), SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Airtime").Value.ToString, CurRow, CurFirstRowIndex)
            End If
        ElseIf e.KeyCode = Keys.C And Keys.ControlKey Then
            CopyMenuItem.PerformClick()
        ElseIf e.KeyCode = Keys.X And Keys.ControlKey Then
            CutMenuItem.PerformClick()
        ElseIf e.KeyCode = Keys.V And Keys.ControlKey Then
            PasteMenuItem.PerformClick()
        ElseIf e.KeyCode = Keys.F7 Then
            If Grp_History.Visible = True Then Grp_History.Visible = False : SongsGrid.Height = 552 Else LoadHistory()
        End If
    End Sub
    Public Sub DeleteFromPlaylist(ByVal PlaylistId As String, ByVal AirTime As String, ByVal SelectedRowIndex As Integer, ByVal CurFirstRowIndex As Integer)

        Dim command As New SqlCommand
        Dim ds As New DataSet
        Call OpenDBConnection()
        'Try

        command.Connection = DBConn
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "ProcDeleteFromPlaylist"
        command.Parameters.AddWithValue("@Playlist_ID", PlaylistId).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@Airtime", AirTime).Direction = ParameterDirection.Input
        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
        command.ExecuteNonQuery()

        If command.Parameters("@Result").Value.ToString <> "Success" Then
            XtraMessageBox.Show(command.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK)
        Else
            LoadData(SelectedRowIndex, CurFirstRowIndex)
        End If

        Call CloseDBConnection()

        'Catch ex As Exception
        'MsgBox(ex.ToString)
        'End Try
    End Sub
    Private Sub SongsGrid_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles SongsGrid.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim ht As DataGridView.HitTestInfo
            ht = Me.SongsGrid.HitTest(e.X, e.Y)
            
            If ht.Type = DataGridViewHitTestType.RowHeader Then
                SongsGrid.ContextMenuStrip = Nothing

            ElseIf ht.Type = DataGridViewHitTestType.ColumnHeader Then
                SongsGrid.ContextMenuStrip = Nothing

            ElseIf ht.Type = DataGridViewHitTestType.Cell Then
                SongsGrid.CurrentCell = SongsGrid(ht.ColumnIndex, ht.RowIndex)
                ReplaceSongMenuItem.Visible = False
                InsertSongMenuItem.Visible = False
                ReplacePromoMenuItem.Visible = False
                If SongsGrid.Rows(ht.RowIndex).Cells("Media_Type").Value = "H" Then
                    SongsGrid.ContextMenuStrip = Nothing
                    Exit Sub
                ElseIf SongsGrid.Rows(ht.RowIndex).Cells("Media_Type").Value = "P" Then 'SongsGrid.Rows(ht.RowIndex).Cells("Album").Value = "Promo" Then
                    ReplacePromoMenuItem.Visible = True
                ElseIf SongsGrid.Rows(ht.RowIndex).Cells("Media_Type").Value = "S" Then
                    InsertSongMenuItem.Visible = True
                    ReplaceSongMenuItem.Visible = True
                End If
                If insMediaId = 0 Then
                    PasteMenuItem.Enabled = False
                Else
                    PasteMenuItem.Enabled = True
                End If
                SongsGrid.ContextMenuStrip = PopUpMenu
            Else
                SongsGrid.ContextMenuStrip = Nothing
                'mnuCell.Items(0).Text = String.Format("This is the cell at {0}, {1}", ht.ColumnIndex, ht.RowIndex)
            End If
        End If
    End Sub

    Private Sub frmPlayList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReDim PlaylistDates(100)
        Call FormatSongsGrid(True)
        Call LoadCustomers()
        Call InitializeStore()
    End Sub
    Private Sub InitializeStore()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Store.Properties.DataSource = dt
        dt.Dispose()
    End Sub


    Private Sub LoadStores(ByVal Customer_Id As Integer)

        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")
        'Creating Columns for Display in lookup
        cmb_Store.Properties.DisplayMember = "Store_Name"
        cmb_Store.Properties.ValueMember = "Store_ID"
        cmb_Store.Properties.DataSource = dsStores.Tables(0)
        cmb_Store.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Store.Properties.PopulateColumns()
        cmb_Store.Properties.Columns(0).Visible = False
        cmb_Store.Properties.ShowFooter = False
        cmb_Store.Properties.ShowHeader = False
        cmb_Store.EditValue = ""

        dsStores.Dispose()
        dapStores.Dispose()
        Call CloseDBConnection()
    End Sub
    Private Sub InitializeCopyVaribales()
        insMediaId = 0
        insMediaType = ""
        insCutAirTime = ""
        insAirTime = ""
    End Sub
    Private Sub cmb_Customer_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If Not cmb_Customer.EditValue = Nothing Then
            InitializeCopyVaribales()
            If Grp_History.Visible = True Then SongsGrid.Height = 552 : Grp_History.Visible = False
            Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString)) : Call FormatSongsGrid(True) : Dt_playlist_date.Text = ""
            ActiveCustomer = Convert.ToInt32(cmb_Customer.EditValue.ToString)
        End If

    End Sub

    Private Sub cmb_Store_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Store.EditValueChanged
        InitializeCopyVaribales()
        'ProcGetPlaylistDatesForStore
        If Grp_History.Visible = True Then SongsGrid.Height = 552 : Grp_History.Visible = False
        Application.DoEvents()
        If cmb_Store.EditValue.ToString = "" Then Exit Sub
        Dim Cmd As New SqlCommand, dsPlaylist As New DataSet, dapPlaylist As New SqlDataAdapter
        Call OpenDBConnection()

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetPlaylistDatesForStore"
            .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        End With
        dapPlaylist.SelectCommand = Cmd
        dapPlaylist.Fill(dsPlaylist, "PlayListDates")

        If dsPlaylist.Tables("PlayListDates").Rows.Count <> 0 Then
            ReDim PlaylistDates(0 To dsPlaylist.Tables("PlayListDates").Rows.Count - 1)
            For i As Integer = 0 To dsPlaylist.Tables("PlayListDates").Rows.Count - 1
                PlaylistDates(i) = dsPlaylist.Tables("PlayListDates").Rows(i).Item(0).ToString
            Next
        Else
            Call FormatSongsGrid(True)
            Dt_playlist_date.Text = ""
        End If
        dsPlaylist.Dispose()
        dapPlaylist.Dispose()

        'New Code loading trackbar with store timings
        Dim CmdTimings As New SqlCommand, dsTimings As New DataSet, dapTimings As New SqlDataAdapter

        With CmdTimings
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoreDetails"
            .Parameters.AddWithValue("@Customer_Id", Convert.ToInt32(cmb_Customer.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        End With
        dapTimings.SelectCommand = CmdTimings
        dapTimings.Fill(dsTimings, "StoreTimings")

        If dsTimings.Tables(0).Rows.Count <> 0 Then
            Dim StTime, EndTime As String
            Dim iStTime, iEndTime As Integer
            stTime = dsTimings.Tables(0).Rows(0).Item("Store_Opens_At").ToString
            EndTime = dsTimings.Tables(0).Rows(0).Item("Store_Closed_At").ToString
            If Strings.Right(StTime, 2) = "AM" OrElse StTime = "12 PM" Then
                If StTime = "12 AM" Then
                    iStTime = 0
                Else
                    iStTime = CInt(Strings.Replace(StTime, Strings.Right(StTime, 2), ""))
                End If
            ElseIf Strings.Right(StTime, 2) = "PM" Then
                iStTime = CInt(Strings.Replace(StTime, Strings.Right(StTime, 2), "")) + 12
            End If
            If Strings.Right(EndTime, 2) = "AM" OrElse EndTime = "12 PM" Then
                If EndTime = "12 AM" Then
                    iEndTime = 24
                Else
                    iEndTime = CInt(Strings.Replace(EndTime, Strings.Right(EndTime, 2), ""))
                End If
            ElseIf Strings.Right(EndTime, 2) = "PM" Then
                iEndTime = CInt(Strings.Replace(EndTime, Strings.Right(EndTime, 2), "")) + 12
            End If
            StoreStTime = iStTime : StoreEndTime = iEndTime
            HourBar.Value = iStTime
            'HourBar.Properties.Minimum = iStTime
            'HourBar.Properties.Maximum = iEndTime - 1
            'HourBar.EditValue=iStTime
        Else
            StoreStTime = 9 : StoreEndTime = 23
            HourBar.Value = 9
            'HourBar.Properties.Minimum = 9
            'HourBar.Properties.Maximum = 23
            'HourBar.EditValue = 9
        End If

        dsTimings.Dispose()
        dapTimings.Dispose()
        Call CloseDBConnection()

        'initializing date picker
        Dt_playlist_date.EditValue = Nothing
        FormatSongsGrid(True)
    End Sub

    Private Sub Dt_playlist_date_DrawItem(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs) Handles Dt_playlist_date.DrawItem
        Dim rect As New RectangleF(e.Bounds.Location, e.Bounds.Size)
        ' MsgBox(e.Date.ToString("dd/MM/yyyy"))
        ' MsgBox(e.Style.BackColor.ToArgb)

        If PlaylistDates.Contains(e.Date.ToString("dd-MM-yyyy").Replace("/", "-")) Then
            Dim backColor As Color = Color.LightYellow
            ' your color here
            e.Style.ForeColor = Color.Black
            e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
            'Else
            '    Dim backColor As Color = RGB(255, 95, 95)
            '     your color here
            '    e.Graphics.FillRectangle(New SolidBrush(backColor), rect)
        End If
        e.Graphics.DrawString(e.[Date].Day.ToString(), e.Style.Font, New SolidBrush(e.Style.ForeColor), rect, e.Style.GetStringFormat())
        e.Handled = True
        Return

    End Sub
    Private Sub LoadGenreCategories()
        Dim cmdStore As New SqlCommand, dapStore As New SqlDataAdapter, dsStore As New DataSet

        Call OpenDBConnection()
        With cmdStore
            .Connection = DBConn
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 300
            .CommandText = "ProcGetStoreDetails"
            .Parameters.AddWithValue("@Customer_Id", Convert.ToInt32(cmb_Customer.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        End With
        dapStore.SelectCommand = cmdStore
        dapStore.Fill(dsStore)
        tvw_Genre_Category.Nodes.Clear()

        'loading genre categories of activate songs of this store
        Dim CurGenre As String = ""
        Dim NodeCount As Integer = 0

        tvw_Genre_Category.Nodes.Add("(Select All)")
        For i As Integer = 0 To dsStore.Tables(3).Rows.Count - 1
            If CurGenre <> dsStore.Tables(3).Rows(i).Item("genre_name").ToString Then
                NodeCount += 1
                CurGenre = dsStore.Tables(3).Rows(i).Item("genre_name").ToString
                tvw_Genre_Category.Nodes.Add(CurGenre)
            End If
            Dim ChildNode As New TreeNode
            ChildNode.Tag = dsStore.Tables(3).Rows(i).Item("Genre_Category_Id").ToString
            ChildNode.Text = dsStore.Tables(3).Rows(i).Item("Category_Code").ToString
            tvw_Genre_Category.Nodes(NodeCount).Nodes.Add(ChildNode)
            ChildNode = Nothing
        Next
    End Sub
    Private Sub LoadData(Optional ByVal RowIndex As Integer = 0, Optional ByVal CurFirstRowIndex As Integer = 0)
        Application.DoEvents()
        If Dt_playlist_date.EditValue = Nothing OrElse cmb_Store.EditValue = Nothing Then Exit Sub
        Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter

        Cursor=Cursors.WaitCursor
        Call OpenDBConnection()
        ' MsgBox(Dt_playlist_date.EditValue.ToString)
        Dim dt As Date = Dt_playlist_date.EditValue.ToString
        '  MsgBox(dt.ToString("MM/dd/yyyy").Replace("-", "/"))

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetPlaylistForSelectedDate"
            .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Date", dt.ToString("MM/dd/yyyy").Replace("-", "/")).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 0).Direction = ParameterDirection.Input
        End With
        dapSongs.SelectCommand = Cmd
        'MsgBox(Cmd.Parameters("@Playlist_Date").Value.ToString)
        dapSongs.Fill(dsSongs, "PlayList")


        If dsSongs.Tables("PlayList").Rows.Count <> 0 Then
            SongsGrid.Columns.Clear()
            SongsGrid.DataSource = dsSongs.Tables("PlayList")
            FormatSongsGrid(False)

            For i = 0 To SongsGrid.Rows.Count - 1
                If SongsGrid.Rows(i).Cells("Playlist_ID").Value.ToString = "" AndAlso SongsGrid.Rows(i).Cells("Media_Type").Value.ToString <> "H" Then Exit For

                If SongsGrid.Rows(i).Cells("Media_Type").Value.ToString = "H" Then
                    SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 255)
                    SongsGrid.Rows(i).Cells(6).Style.Font = New Font("Microsoft Sans Serif", 11, FontStyle.Bold)
                    SongsGrid.Rows(i).Height = SongsGrid.Rows(i).Height + 10
                    SongsGrid.Rows(i).Cells(6).Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    
                ElseIf SongsGrid.Rows(i).Cells("Media_Type").Value.ToString = "P" Then

                    If Not File.Exists(PromosArchivePath & "\" & SongsGrid.Rows(i).Cells("FileName").Value) Then
                        SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    Else
                        SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 204, 102)
                    End If
                ElseIf SongsGrid.Rows(i).Cells("Media_Type").Value.ToString = "S" Then
                    If Not File.Exists(SongsArchivePath & "\" & SongsGrid.Rows(i).Cells("FileName").Value) Then
                        SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    End If
                End If
        Application.DoEvents()
            Next
            If SongsGrid.Rows.Count = RowIndex Then RowIndex -= 1

            If RowIndex = 0 Then
                SongsGrid.Rows(1).Selected = True
            Else
                SongsGrid.Rows(RowIndex).Selected = True
            End If
            SongsGrid.SelectedCells(3).Selected = True
            SongsGrid.FirstDisplayedScrollingRowIndex = CurFirstRowIndex
            'btn_Save.Enabled = False
        Else 'no records

            FormatSongsGrid(True)
            'btn_Save.Enabled = False
            'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If

        dsSongs.Dispose()
        dapSongs.Dispose()
        Call CloseDBConnection()
        Cursor = Cursors.Default

        'If SongsGrid.RowCount <> 0 Then
        'SongsGrid.Sort(SongsGrid.Columns("Playlist_ID"), ListSortDirection.Ascending)
        '    'TmrPlayState.Enabled = True
        '    bPlayMedia = False
        '    Call LoadFile()
        '    MDIInstoreCentral.player.StopPlayback()
        '    PlayMode = "Stopped"
        'End If

    End Sub

    Private Sub Dt_playlist_date_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dt_playlist_date.EditValueChanged
        InitializeCopyVaribales()
        If Grp_History.Visible = True Then SongsGrid.Height = 552 : Grp_History.Visible = False
        HourBar.Value = StoreStTime
        Call LoadData()
    End Sub
    'Private Sub SongsGrid_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs)
    '    txt_Playlist_Search.Text = "<Search " & SongsGrid.Columns(e.ColumnIndex).HeaderText & ">"
    'End Sub

    'Private Sub txt_Playlist_Search_GotFocus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Playlist_Search.GotFocus, txt_Playlist_Search.MouseClick
    '    txt_Playlist_Search.SelectAll()
    'End Sub

    'Private Sub Songs_Search_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    '    If SongsGrid.RowCount <> 0 AndAlso StringFound = True AndAlso e.KeyValue = Keys.F3 Then
    '        iRowIndex += 1
    '        StringFound = False
    '        If Not FindString(SongsGrid, SongsGrid.SortedColumn.Index, txt_Playlist_Search.Text.Trim) Then
    '            XtraMessageBox.Show("No more matching '" & SongsGrid.Columns(SongsGrid.SortedColumn.Index).HeaderText & "' with the given search text, ' " & txt_Playlist_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        End If
    '    End If
    'End Sub

    'Private Sub txt_Songs_Search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '    If SongsGrid.RowCount <> 0 AndAlso txt_Playlist_Search.Text.Trim <> "" AndAlso e.KeyChar = Chr(13) Then
    '        iRowIndex = 0
    '        StringFound = False
    '        If Not FindString(SongsGrid, SongsGrid.SortedColumn.Index, txt_Playlist_Search.Text.Trim) Then
    '            XtraMessageBox.Show("No matching '" & SongsGrid.Columns(SongsGrid.SortedColumn.Index).HeaderText & "'  with the given search text, ' " & txt_Playlist_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        End If
    '    End If
    'End Sub
    'Private Function FindString(ByRef GridSearch As DataGridView, ByVal SeachColumnIndex As Integer, ByVal Searchstring As String) As Boolean

    '    Dim i As Integer
    '    Dim TopRow As Integer, LowerRow As Integer
    '    Dim strpos As Integer = 0

    '    Dim strFind As String = ""

    '    'For i = 0 To dgData.Rows.Count - 2
    '    '    dgData.Rows(i).Selected = False
    '    'Next

    '    With GridSearch
    '        For i = iRowIndex To .Rows.Count - 1
    '            strFind = LCase(.Rows(i).Cells(SeachColumnIndex).Value.ToString())
    '            strpos = InStr(strFind, LCase(Searchstring))
    '            If strpos > 0 Then
    '                .Rows(i).Selected = True
    '                iRowIndex = i
    '                StringFound = True

    '                TopRow = .FirstDisplayedScrollingRowIndex
    '                LowerRow = TopRow + (.DisplayedRowCount(False) - 1)
    '                If i <> -1 Then
    '                    .Rows(i).Selected = True
    '                    If i < TopRow Or i > LowerRow Then
    '                        .FirstDisplayedScrollingRowIndex = i
    '                    End If
    '                End If
    '                Exit For
    '            End If
    '        Next
    '    End With

    '    If StringFound = True Then
    '        Return True
    '    Else
    '        Return False
    '    End If

    'End Function

    'Private Sub SongsGrid_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles SongsGrid.ColumnHeaderMouseClick
    '    Dim newColumn As DataGridViewColumn = _
    '   SongsGrid.Columns(e.ColumnIndex)
    '    Dim oldColumn As DataGridViewColumn = SongsGrid.SortedColumn
    '    Dim direction As ListSortDirection

    '    ' If oldColumn is null, then the DataGridView is not currently sorted. 
    '    If oldColumn IsNot Nothing Then

    '        ' Sort the same column again, reversing the SortOrder. 
    '        If oldColumn Is newColumn AndAlso SongsGrid.SortOrder = Windows.Forms.SortOrder.Ascending Then
    '            direction = ListSortDirection.Descending
    '        Else

    '            ' Sort a new column and remove the old SortGlyph.
    '            direction = ListSortDirection.Ascending
    '            oldColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.None
    '        End If
    '    Else
    '        direction = ListSortDirection.Ascending
    '    End If

    '    ' Sort the selected column.
    '    SongsGrid.Sort(newColumn, direction)
    '    If direction = ListSortDirection.Ascending Then
    '        newColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.Ascending
    '    Else
    '        newColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.Descending
    '    End If
    'End Sub

    'Private Sub SongsGrid_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles SongsGrid.DataBindingComplete
    '    For Each column As DataGridViewColumn In SongsGrid.Columns
    '        column.SortMode = DataGridViewColumnSortMode.Programmatic
    '    Next
    'End Sub

    'Private Sub SongsGrid_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles SongsGrid.RowPostPaint
    '    If SongsGrid.Rows(e.RowIndex).Cells("Media_Type").Value.ToString = "P" Then

    '        If Not File.Exists(PromosArchivePath & "\" & SongsGrid.Rows(e.RowIndex).Cells("FileName").Value) Then
    '            SongsGrid.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red
    '        Else
    '            SongsGrid.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Yellow
    '        End If
    '    ElseIf SongsGrid.Rows(e.RowIndex).Cells("Media_Type").Value.ToString = "S" Then
    '        If Not File.Exists(SongsArchivePath & "\" & SongsGrid.Rows(e.RowIndex).Cells("FileName").Value) Then
    '            SongsGrid.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red
    '        End If
    '    End If
    'End Sub

    Private Sub ReplaceMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReplacePromoMenuItem.Click

        If Grp_History.Visible = True Then SongsGrid.Height = 552 : Grp_History.Visible = False

        lbl_Date_Display.Text = Dt_playlist_date.Text
        lbl_Promo_Display.Text = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Title").Value.ToString
        lblPromoId.Text = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_ID").Value.ToString
        lbl_Airtime.Text = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Airtime").Value.ToString
        lbl_Playlist_Id.Text = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Playlist_ID").Value.ToString
        PanelPromoReplace.Left = 508
        PanelPromoReplace.Top = 203
        PanelPromoReplace.Visible = True

        Dim dr As SqlDataReader
        Dim command As New SqlCommand
        Dim ds As New DataSet

        Call OpenDBConnection()
        Grid_Replace.Rows.Clear()
        Try
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcGetPromosListForReplace"
            command.Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Playlist_ID", SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Playlist_ID").Value.ToString).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Promo_Id", SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_ID").Value.ToString).Direction = ParameterDirection.Input

            dr = command.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    Grid_Replace.Rows.Add(dr(0).ToString, dr(1).ToString)
                End While
            End If
            Call CloseDBConnection()
            Grid_Replace.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btn_Exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Exit.Click
        PanelPromoReplace.Visible = False
        txt_Promos_Search.Text = "<Search Promo>"
    End Sub

    Private Sub btn_ReplacePromo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ReplacePromo.Click

        'ActiveCustomer = Convert.ToInt32(cmb_Customer.EditValue.ToString)
        'ActiveStore = Convert.ToInt32(cmb_Store.EditValue.ToString)
        'frmreplacepromos.ShowDialog()

        'MsgBox(Grid_Replace.CurrentRow.Index.ToString)
        'MsgBox(Grid_Replace.Rows(Grid_Replace.CurrentRow.Index).Cells(0).Value.ToString)

        Cursor = Cursors.WaitCursor
        Dim CurRow As Integer = SongsGrid.CurrentRow.Index
        Dim CurFirstRowIndex As Integer = SongsGrid.FirstDisplayedScrollingRowIndex
        Dim dr As SqlDataReader
        Dim command As New SqlCommand
        Dim ds As New DataSet

        Call OpenDBConnection()

        'Try
        command.Connection = DBConn
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "ProcReplacePromo"

        command.Parameters.AddWithValue("@Playlist_ID", lbl_Playlist_Id.Text).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@AirTime", "1900-01-01 " & lbl_Airtime.Text).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@Old_Media_ID", lblPromoId.Text).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@New_Media_ID", Grid_Replace.Rows(Grid_Replace.CurrentRow.Index).Cells(0).Value.ToString).Direction = ParameterDirection.Input
        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output

        command.ExecuteNonQuery()
        PanelPromoReplace.Visible = False
        If command.Parameters("@Result").Value.ToString = "Replacement successful" Then
            'Call FileSyncWhenPromoReplace()
            Call LoadData(CurRow, CurFirstRowIndex)
        Else
            XtraMessageBox.Show(command.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
        Call CloseDBConnection()
        txt_Promos_Search.Text = "<Search Promo>"
        Cursor = Cursors.Default
        'Catch ex As Exception
        'MsgBox(ex.ToString)
        'End Try
    End Sub
    Private Sub FileSyncWhenPromoReplace()

        Dim CurDate As Date

        CurDate = Dt_playlist_date.EditValue
        Dim CurMonth As String = CurDate.ToString("MMMM")
        Dim CurYear As String = CurDate.ToString("YYYY")

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text) Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text)
        End If

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text) Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text)
        End If

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & CurMonth & "-" & CurYear) Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & CurMonth & "-" & CurYear)
        End If

        If Not Directory.Exists(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & CurMonth & "-" & CurYear & "\Promos") Then
            Directory.CreateDirectory(FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & CurMonth & "-" & CurYear & "\Promos")
        End If

        Dim FileCopyPath As String = FileprepPath & "\" & cmb_Customer.Text & "\" & cmb_Store.Text & "\" & CurMonth & "-" & CurYear

        Application.DoEvents()

        Dim cmdPlaylist As New SqlCommand
        Dim dapPlaylist As New SqlDataAdapter, dsPlaylist As New DataSet
        Dim Playdate As String, playlistdate As Date

        Dim clsInstore As New ClsEncryption
        Playdate = Dt_playlist_date.Text
        Playdate = Playdate.Replace("-", "/")
        playlistdate = Dt_playlist_date.EditValue

        Call OpenDBConnection()
        With cmdPlaylist
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcGetPlaylistForSelectedDate"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Date", playlistdate.ToString("MM/dd/yyyy")).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 0).Direction = ParameterDirection.Input
        End With

        Application.DoEvents()
        dapPlaylist.SelectCommand = cmdPlaylist
        dsPlaylist.Clear()
        dapPlaylist.Fill(dsPlaylist, "Playlist")

        If File.Exists(FileCopyPath & "\" & Playdate.Replace("/", "-") & ".xml") Then
            File.Delete(FileCopyPath & "\" & Playdate.Replace("/", "-") & ".xml")
        End If

        dsPlaylist.WriteXml(FileCopyPath & "\" & Playdate.Replace("/", "-") & ".xml")
        Application.DoEvents()

        Application.DoEvents()

        With cmdPlaylist
            .Parameters.Clear()
            .CommandText = "ProcGetStoreSongPromoList"
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Filter_Criteria", "Songs").Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 1).Direction = ParameterDirection.Input
        End With

        dapPlaylist.SelectCommand = cmdPlaylist
        dsPlaylist.Clear()
        dapPlaylist.Fill(dsPlaylist, "ActiveSongs")

        cmdPlaylist.Parameters("@Filter_Criteria").Value = "Promos"
        dapPlaylist.SelectCommand = cmdPlaylist
        dapPlaylist.Fill(dsPlaylist, "ActivePromos")

        If File.Exists(FileCopyPath & "\Active List.xml") Then
            File.Delete(FileCopyPath & "\Active List.xml")
        End If

        dsPlaylist.WriteXml(FileCopyPath & "\Active List.xml")
        Application.DoEvents()

        Application.DoEvents()

        Dim rCount As Integer, totRows As Integer
        Dim FileName As String
        totRows = dsPlaylist.Tables("ActiveSongs").Rows.Count
        ' 
        Application.DoEvents()

        totRows = dsPlaylist.Tables("ActivePromos").Rows.Count

        For rCount = 0 To dsPlaylist.Tables("ActivePromos").Rows.Count - 1
            Application.DoEvents()
            FileName = dsPlaylist.Tables("ActivePromos").Rows(rCount)("FileName").ToString
            If File.Exists(PromosArchivePath & "\" & FileName) Then
                File.Copy(PromosArchivePath & "\" & FileName, FileCopyPath & "\Promos\" & FileName, True)
                Application.DoEvents()
            End If
        Next


        Application.DoEvents()
        clsInstore = Nothing
        dsPlaylist.Dispose()
        dapPlaylist.Dispose()
        cmdPlaylist.Dispose()
        Call CloseDBConnection()

        Application.DoEvents()

        'exec ProcGetPlaylistDatesForStore 1
        'exec ProcGetPlaylistForSelectedDate 1,'08/01/2012'

    End Sub


    Private Sub Grid_Replace_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Replace.DoubleClick
        Call btn_ReplacePromo.PerformClick()
    End Sub


    'Private Sub PlaylistPlayer_PlayStateChange(ByVal sender As Object, ByVal e As AxWMPLib._WMPOCXEvents_PlayStateChangeEvent) Handles PlaylistPlayer.PlayStateChange
    '    Static Dim PlayMe As Boolean = True
    '    Select Case PlaylistPlayer.playState
    '        Case 3
    '            If Not bPlayMedia Then PlaylistPlayer.Ctlcontrols.pause() : bPlayMedia = True
    '        Case 10
    '            If PlayMe Then
    '                PlaylistPlayer.Ctlcontrols.play()
    '            End If
    '        Case 8
    '            PlayMe = False

    '            If LoadFile() Then
    '                PlaylistPlayer.Ctlcontrols.play()
    '                PlayMe = True
    '            End If
    '    End Select
    'End Sub
    Public Function LoadFile() As Boolean
        Dim res As Boolean = True
NxtFile:
        TmrPlayState.Enabled = False
        MDIInstoreCentral.StopStream()
        MDIInstoreCentral.lbl_TotalTime.Text = ""
        MDIInstoreCentral.lbl_curtime.Text = ""
        PlayMode = "Play"

        If CurFileRowIndex <= SongsGrid.RowCount - 1 Then

            If SongsGrid.Rows(CurFileRowIndex).Cells("Media_Type").Value.ToString = "P" AndAlso File.Exists(PromosArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells("FileName").Value) Then
                'PlaylistPlayer.URL = PromosArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells("FileName").Value
                MDIInstoreCentral.PlayAudioFile(PromosArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells("FileName").Value)
                PlayMode = "Play"
                SongsGrid.Rows(CurFileRowIndex).Selected = True
                SongsGrid.FirstDisplayedScrollingRowIndex = CurFileRowIndex
                res = True
                CurFileRowIndex += 1
                TmrPlayState.Enabled = True
                'If Not File.Exists(PromosArchivePath & "\" & SongsGrid.Rows(i).Cells("FileName").Value) Then
                '    SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                'Else
                '    SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 204, 102)
                'End If
            ElseIf SongsGrid.Rows(CurFileRowIndex).Cells("Media_Type").Value.ToString = "S" AndAlso File.Exists(SongsArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells("FileName").Value) Then
                'PlaylistPlayer.URL = SongsArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells("FileName").Value
                MDIInstoreCentral.PlayAudioFile(SongsArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells("FileName").Value)
                PlayMode = "Play"
                SongsGrid.Rows(CurFileRowIndex).Selected = True
                SongsGrid.FirstDisplayedScrollingRowIndex = CurFileRowIndex
                res = True
                TmrPlayState.Enabled = True
                CurFileRowIndex += 1
            Else
                CurFileRowIndex += 1
                GoTo nxtfile
            End If
        Else
            res = False
        End If
        Return res
    End Function

    Private Sub TmrPlayState_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TmrPlayState.Tick

        If PlayMode = "Stopped" Then TmrPlayState.Enabled = False : Exit Sub

        If MDIInstoreCentral.CheckIfChannelActive = False Then
            TmrPlayState.Enabled = False
            LoadFile()
            TmrPlayState.Enabled = True
        End If


        'Dim Status As New TStreamStatus()

        'If PlayMode = "Stopped" Then TmrPlayState.Enabled = False : Exit Sub
        'MDIInstoreCentral.player.GetStatus(Status)

        'If Status.fPlay = False Then
        '    TmrPlayState.Enabled = False
        '    LoadFile()
        '    TmrPlayState.Enabled = True
        'End If

    End Sub
    Private Sub PopulateSongReplaceDetails()
        lbl_Date_Replace.Text = Dt_playlist_date.Text
        'lbl_Promo_Display.Text = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Title").Value.ToString
        lbl_song_Id.Text = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_ID").Value.ToString
        lbl_song_airtime.Text = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Airtime").Value.ToString
        lbl_song_playlist_id.Text = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Playlist_ID").Value.ToString
        WriteGenreSelection(SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Category").Value.ToString)
        'PanelPromoReplace.Left = 508
        'PanelPromoReplace.Top = 203
        'PanelPromoReplace.Visible = True

        'Dim dr As SqlDataReader
        'Dim command As New SqlCommand
        'Dim ds As New DataSet

        'Call OpenDBConnection()
        'Grid_Replace.Rows.Clear()
        'Try
        '    command.Connection = DBConn
        '    command.CommandType = CommandType.StoredProcedure
        '    command.CommandText = "ProcGetPromosListForReplace"
        '    command.Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        '    command.Parameters.AddWithValue("@Playlist_ID", SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Playlist_ID").Value.ToString).Direction = ParameterDirection.Input
        '    command.Parameters.AddWithValue("@Promo_Id", SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_ID").Value.ToString).Direction = ParameterDirection.Input

        '    dr = command.ExecuteReader
        '    If dr.HasRows Then
        '        While dr.Read
        '            Grid_Replace.Rows.Add(dr(0).ToString, dr(1).ToString)
        '        End While
        '    End If
        '    Call CloseDBConnection()
        '    Grid_Replace.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
        'Catch ex As Exception
        '    MsgBox(ex.ToString)
        'End Try
    End Sub
    Private Sub ReplaceSongMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReplaceSongMenuItem.Click
        If Grp_History.Visible = True Then SongsGrid.Height = 552 : Grp_History.Visible = False
        LoadGenreCategories()
        PopulateSongReplaceDetails()
        btn_Song_Replace.Text = "Replace"
        PanelSongReplace.Visible = True
       
    End Sub

    Private Sub btn_Song_Replace_Exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Song_Replace_Exit.Click
        PanelSongReplace.Visible = False
        txt_Songs_Search.Text = "<Search Song ID>"
    End Sub

    Private Sub btn_Song_Replace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Song_Replace.Click
        If btn_Song_Replace.Text = "Replace" Then
            SaveSongsReplace("Replace")
        Else
            SaveSongsReplace("Insert")
        End If
        txt_Songs_Search.Text = "<Search Song ID>"
    End Sub

    Private Sub InsertSongMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles InsertSongMenuItem.Click

        If Grp_History.Visible = True Then SongsGrid.Height = 552 : Grp_History.Visible = False

        LoadGenreCategories()
        PopulateSongReplaceDetails()
        btn_Song_Replace.Text = "Insert"
        PanelSongReplace.Visible = True
        
    End Sub

    Private Sub cmb_Genre_Category_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles cmb_Genre_Category.ButtonClick
        If tvw_Genre_Category.Visible = True Then tvw_Genre_Category.Visible = False Else tvw_Genre_Category.Visible = True

    End Sub

    Private Sub tvw_Genre_Category_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvw_Genre_Category.AfterCheck
        'If DynamicChecked Then Exit Sub
        Try
            If e.Node.Text = "(Select All)" Then
                CheckAllNodes(e.Node.Checked)
            ElseIf e.Action <> TreeViewAction.Unknown AndAlso e.Node.Nodes.Count > 0 Then
                CheckAllChildNodes(e.Node, e.Node.Checked)
            End If
            ReadSelection()
            If lbl_Genre_Category.Text <> "" Then LoadSongsForReplace() Else FormatSongsReplaceGrid(True)
        Catch
        End Try
    End Sub
    Private Sub SaveSongsReplace(ByVal ActionType As String)

        Dim Cmd As New SqlCommand
        Cursor = Cursors.WaitCursor
        Dim CurRow As Integer = SongsGrid.CurrentRow.Index
        Dim CurFirstRowIndex As Integer = SongsGrid.FirstDisplayedScrollingRowIndex
        Call OpenDBConnection()

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcReplaceInsertSong"
            .Parameters.AddWithValue("@Playlist_ID", lbl_song_playlist_id.Text).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@AirTime", lbl_song_airtime.Text).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Old_Media_ID", lbl_song_Id.Text).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@New_Media_ID", Grid_Song_Replace.SelectedRows(0).Cells(0).Value).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@ActionType", ActionType).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
        End With
        Cmd.ExecuteNonQuery()

        If Cmd.Parameters("@Result").Value.ToString = "Replacement successful" Then
            'Call FileSyncWhenPromoReplace()
            PanelSongReplace.Visible = False
            Call LoadData(CurRow, CurFirstRowIndex)
        Else
            XtraMessageBox.Show(Cmd.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        Cursor = Cursors.Default
        Call CloseDBConnection()

    End Sub

    Private Sub LoadSongsForReplace()

        Application.DoEvents()
        If lbl_Genre_Category.Text = "" Then Grid_Song_Replace.Rows.Clear()

        Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter
        Call OpenDBConnection()
        
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetSongsListForReplace"
            .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_ID", lbl_song_playlist_id.Text).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Song_ID", lbl_song_Id.Text).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Genre_Categories", lbl_Genre_Category.Text).Direction = ParameterDirection.Input
        End With
        dapSongs.SelectCommand = Cmd
        'MsgBox(Cmd.Parameters("@Playlist_Date").Value.ToString)
        dapSongs.Fill(dsSongs, "PlayList")


        If dsSongs.Tables("PlayList").Rows.Count <> 0 Then
            Grid_Song_Replace.Columns.Clear()
            Grid_Song_Replace.DataSource = dsSongs.Tables("PlayList")
            FormatSongsReplaceGrid(False)

            'For i = 0 To Grid_Song_Replace.Rows.Count - 1
            '    If Grid_Song_Replace.Rows(i).Cells("Playlist_ID").Value.ToString = "" Then Exit For

            '    If Not File.Exists(SongsArchivePath & "\" & Grid_Song_Replace.Rows(i).Cells("FileName").Value) Then
            '        SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
            '    End If

            'Next
            'SongsGrid.Rows(RowIndex).Selected = True
            'SongsGrid.FirstDisplayedScrollingRowIndex = CurFirstRowIndex
            'btn_Save.Enabled = False
        Else 'no records

            FormatSongsReplaceGrid(True)
            'btn_Save.Enabled = False
            'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If

        dsSongs.Dispose()
        dapSongs.Dispose()
        Call CloseDBConnection()
        'If SongsGrid.RowCount <> 0 Then
        'SongsGrid.Sort(SongsGrid.Columns("Playlist_ID"), ListSortDirection.Ascending)
        '    'TmrPlayState.Enabled = True
        '    bPlayMedia = False
        '    Call LoadFile()
        '    MDIInstoreCentral.player.StopPlayback()
        '    PlayMode = "Stopped"
        'End If


    End Sub
    Private Sub WriteGenreSelection(ByVal CategoryCodes As String)

        Dim ptnode, chnode As TreeNode
        Dim v() As String
        v = CategoryCodes.Split(",")

        'DynamicChecked = True
        tvw_Genre_Category.Nodes(0).Checked = False
        For Each ptnode In tvw_Genre_Category.Nodes
            'ptnode.Checked = False
            For Each chnode In ptnode.Nodes
                'chnode.Checked = False
                Dim s As String = chnode.Text
                If Array.IndexOf(v, s) > -1 Then
                    chnode.Checked = True
                End If
            Next chnode
        Next ptnode
        'ReadSelection()

        'DynamicChecked = False
    End Sub
    Private Sub ReadSelection(Optional ByVal RowIndex As Integer = 0)
        Dim ptnode, chnode As TreeNode, str, str1 As String
        For Each ptnode In tvw_Genre_Category.Nodes
            For Each chnode In ptnode.Nodes
                If chnode.Checked = True Then
                    str += "," & chnode.Tag
                    str1 += "," & chnode.Text
                End If
            Next chnode
        Next ptnode
        str = Mid(str, 2)
        str1 = Mid(str1, 2)

        cmb_Genre_Category.Text = str1
        lbl_Genre_Category.Text = str
        'Try
        '    If GridDayparts.Rows.Count <> 0 Then
        '        GridDayparts.Rows(RowIndex).Cells(3).Value = str
        '        GridDayparts.Rows(RowIndex).Cells(4).Value = str1
        '    End If
        'Catch
        'End Try


    End Sub
    Private Sub CheckAllNodes(ByVal nodeChecked As Boolean)
        For Each node In tvw_Genre_Category.Nodes
            If node.Text <> "(Select All)" Then
                node.Checked = nodeChecked
                If node.Nodes.Count > 0 Then
                    ' If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    Me.CheckAllChildNodes(node, nodeChecked)
                End If
            End If
        Next node
    End Sub
    Private Sub CheckAllChildNodes(ByVal treeNode As TreeNode, ByVal nodeChecked As Boolean)

        Dim node As TreeNode
        For Each node In treeNode.Nodes
            node.Checked = nodeChecked
            'If node.Nodes.Count > 0 Then
            '    ' If the current node has child nodes, call the CheckAllChildsNodes method recursively.
            '    Me.CheckAllChildNodes(node, nodeChecked)
            'End If
        Next node
    End Sub


    Private Sub tvw_Genre_Category_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvw_Genre_Category.LostFocus
        tvw_Genre_Category.Visible = False
    End Sub

    Private Sub Grid_Song_Replace_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Song_Replace.CellContentDoubleClick

    End Sub

    Private Sub Grid_Song_Replace_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Song_Replace.CellDoubleClick
        If e.RowIndex <> -1 Then
            btn_Song_Replace.PerformClick()
        End If
    End Sub

    Private Sub Grid_Song_Replace_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Song_Replace.KeyDown
        If e.KeyCode = Keys.Escape Then
            PanelSongReplace.Visible = False
        End If
    End Sub
    
    Private Sub CutMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CutMenuItem.Click
        Try
            insMediaId = CInt(SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_ID").Value.ToString)
            insMediaType = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_Type").Value.ToString
            insAirTime = ""
            insCutAirTime = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Airtime").Value.ToString
            'SongsGrid.Rows(SongsGrid.CurrentRow.Index).DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 0)
        Catch
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub CopyMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CopyMenuItem.Click
        Try
            insMediaId = CInt(SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_ID").Value.ToString)
            insMediaType = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_Type").Value.ToString
            insCutAirTime = ""
            insAirTime = ""
        Catch
        End Try

    End Sub
    Private Sub SaveSongsCutCopyPaste(ByVal ActionType As String)

        Dim Cmd As New SqlCommand
        Cursor = Cursors.WaitCursor
        Dim CurRow As Integer = SongsGrid.CurrentRow.Index
        Dim CurFirstRowIndex As Integer = SongsGrid.FirstDisplayedScrollingRowIndex
        Call OpenDBConnection()

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcCutCopyPasteInPlaylist"
            .Parameters.AddWithValue("@Playlist_ID", insPlaylistId).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@AirTime", SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Airtime").Value.ToString).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Old_Media_ID", CInt(SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_ID").Value.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@New_Media_ID", insMediaId).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Media_Type", insMediaType).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@CutAirTime", insCutAirTime).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@ActionType", ActionType).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
        End With
        Cmd.ExecuteNonQuery()

        If Cmd.Parameters("@Result").Value.ToString = "Modification successful" Then
            'Call FileSyncWhenPromoReplace()
            'PanelSongReplace.Visible = False
            Call LoadData(CurRow, CurFirstRowIndex)
        Else
            XtraMessageBox.Show(Cmd.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        Cursor = Cursors.Default
        Call CloseDBConnection()

    End Sub

    Private Sub PasteMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PasteMenuItem.Click
        If insMediaId = 0 Then Exit Sub
        Try
            insPlaylistId = CInt(SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Playlist_ID").Value.ToString)
            If insCutAirTime <> "" Then 'cutpaste
                SaveSongsCutCopyPaste("CutPaste")
            Else 'copypaste
                SaveSongsCutCopyPaste("CopyPaste")
            End If
            insCutAirTime = ""
        Catch
        End Try

    End Sub

    Private Sub Grid_Song_Replace_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Grid_Song_Replace.ColumnHeaderMouseClick

        Dim newColumn As DataGridViewColumn = _
            Grid_Song_Replace.Columns(e.ColumnIndex)
        Dim oldColumn As DataGridViewColumn = Grid_Song_Replace.SortedColumn
        Dim direction As ListSortDirection

        ' If oldColumn is null, then the DataGridView is not currently sorted. 
        If oldColumn IsNot Nothing Then

            ' Sort the same column again, reversing the SortOrder. 
            If oldColumn Is newColumn AndAlso Grid_Song_Replace.SortOrder = Windows.Forms.SortOrder.Ascending Then
                direction = ListSortDirection.Descending
            Else

                ' Sort a new column and remove the old SortGlyph.
                direction = ListSortDirection.Ascending
                oldColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.None
            End If
        Else
            direction = ListSortDirection.Ascending
        End If

        ' Sort the selected column.
        Grid_Song_Replace.Sort(newColumn, direction)
        If direction = ListSortDirection.Ascending Then
            newColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.Ascending
        Else
            newColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.Descending
        End If
        txt_Songs_Search.Text = "<Search " & Grid_Song_Replace.Columns(e.ColumnIndex).HeaderText & ">"
    End Sub

    Private Sub Grid_Song_Replace_DataBindingComplete(ByVal sender As Object, ByVal e As DataGridViewBindingCompleteEventArgs) Handles Grid_Song_Replace.DataBindingComplete

        ' Put each of the columns into programmatic sort mode. 
        For Each column As DataGridViewColumn In Grid_Song_Replace.Columns
            column.SortMode = DataGridViewColumnSortMode.Programmatic
        Next
    End Sub

    Private Sub HourBar_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles HourBar.ValueChanged
        Dim i As Integer

        If SongsGrid.RowCount = 0 Then Exit Sub

        If HourBar.Value < StoreStTime Then
            HourBar.Value = StoreStTime
            Exit Sub
        ElseIf HourBar.Value > StoreEndTime - 1 Then
            HourBar.Value = StoreEndTime - 1
            Exit Sub
        End If

        For i = 0 To SongsGrid.RowCount - 1
            If SongsGrid.Rows(i).Cells(2).Value.ToString = "H" AndAlso CInt(Strings.Replace(SongsGrid.Rows(i).Cells(6).Value.ToString, "HOUR ", "")) = HourBar.Value Then
                SongsGrid.FirstDisplayedScrollingRowIndex = i
                Exit For
            End If
        Next
    End Sub
    '-------Songs Search part
    'Private Sub Grid_Song_Replace_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Grid_Song_Replace.ColumnHeaderMouseClick
    '    txt_Songs_Search.Text = "<Search " & Grid_Song_Replace.Columns(e.ColumnIndex).HeaderText & ">"
    'End Sub
    Private Sub Grid_Replace_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Grid_Replace.ColumnHeaderMouseClick
        txt_Promos_Search.Text = "<Search " & Grid_Replace.Columns(e.ColumnIndex).HeaderText & ">"
    End Sub
    Private Sub txt_Songs_Search_GotFocus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Songs_Search.GotFocus, txt_Songs_Search.MouseClick
        txt_Songs_Search.SelectAll()
    End Sub
    Private Sub txt_Promos_Search_GotFocus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Promos_Search.GotFocus, txt_Promos_Search.MouseClick
        txt_Promos_Search.SelectAll()
    End Sub
    Private Sub Songs_Search_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_Songs_Search.KeyDown, Grid_Song_Replace.KeyDown
        'Try
        Dim SortColIndex As Integer
        Try
            SortColIndex = Grid_Song_Replace.SortedColumn.Index
        Catch ex As Exception
            SortColIndex = 0
        End Try
        If Grid_Song_Replace.RowCount <> 0 AndAlso StringFound = True AndAlso e.KeyValue = Keys.F3 Then
            iRowIndex += 1
            StringFound = False

            If Not FindString(Grid_Song_Replace, SortColIndex, txt_Songs_Search.Text.Trim) Then
                XtraMessageBox.Show("No more matching '" & Grid_Song_Replace.Columns(SortColIndex).HeaderText & "' with the given search text, ' " & txt_Songs_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
        'Catch
        'End Try
    End Sub
    Private Sub Promos_Search_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_Promos_Search.KeyDown, Grid_Replace.KeyDown
        'Try
        Dim SortColIndex As Integer
        Try
            SortColIndex = Grid_Replace.SortedColumn.Index
        Catch ex As Exception
            SortColIndex = 0
        End Try
        If Grid_Replace.RowCount <> 0 AndAlso StringFound = True AndAlso e.KeyValue = Keys.F3 Then
            iRowIndex += 1
            StringFound = False

            If Not FindString(Grid_Replace, SortColIndex, txt_Promos_Search.Text.Trim) Then
                XtraMessageBox.Show("No more matching '" & Grid_Replace.Columns(SortColIndex).HeaderText & "' with the given search text, ' " & txt_Promos_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
        'Catch
        'End Try
    End Sub
    Private Sub txt_Songs_Search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Songs_Search.KeyPress
        Dim SortColIndex As Integer
        Try
            SortColIndex = Grid_Song_Replace.SortedColumn.Index
        Catch ex As Exception
            SortColIndex = 0
        End Try

        If Grid_Song_Replace.RowCount <> 0 AndAlso txt_Songs_Search.Text.Trim <> "" AndAlso e.KeyChar = Chr(13) Then
            iRowIndex = 0
            StringFound = False

            If Not FindString(Grid_Song_Replace, SortColIndex, txt_Songs_Search.Text.Trim) Then
                XtraMessageBox.Show("No matching '" & SongsGrid.Columns(SortColIndex).HeaderText & "'  with the given search text, ' " & txt_Songs_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
    Private Sub txt_promos_Search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Promos_Search.KeyPress
        Dim SortColIndex As Integer
        Try
            SortColIndex = Grid_Replace.SortedColumn.Index
        Catch ex As Exception
            SortColIndex = 0
        End Try
        If Grid_Replace.RowCount <> 0 AndAlso txt_Promos_Search.Text.Trim <> "" AndAlso e.KeyChar = Chr(13) Then
            iRowIndex = 0
            StringFound = False

            If Not FindString(Grid_Replace, SortColIndex, txt_Promos_Search.Text.Trim) Then
                XtraMessageBox.Show("No matching '" & SongsGrid.Columns(SortColIndex).HeaderText & "'  with the given search text, ' " & txt_Promos_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
    Private Function FindString(ByRef GridSearch As DataGridView, ByVal SeachColumnIndex As Integer, ByVal Searchstring As String) As Boolean

        Dim i As Integer
        Dim TopRow As Integer, LowerRow As Integer
        Dim strpos As Integer = 0

        Dim strFind As String = ""

        With GridSearch
            For i = 0 To GridSearch.Rows.Count - 1
                GridSearch.Rows(i).Selected = False
            Next
            For i = iRowIndex To .Rows.Count - 1
                strFind = LCase(.Rows(i).Cells(SeachColumnIndex).Value.ToString())
                strpos = InStr(strFind, LCase(Searchstring))
                If strpos > 0 Then
                    .Rows(i).Selected = True
                    iRowIndex = i
                    StringFound = True

                    TopRow = .FirstDisplayedScrollingRowIndex
                    LowerRow = TopRow + (.DisplayedRowCount(False) - 1)
                    If i <> -1 Then
                        .Rows(i).Selected = True
                        If i < TopRow Or i > LowerRow Then
                            .FirstDisplayedScrollingRowIndex = i
                        End If
                    End If
                    Exit For
                End If
            Next
        End With

        If StringFound = True Then
            Return True
        Else
            Return False
        End If

    End Function

    '-------Songs Search part ends

    Private Sub Grid_Song_Replace_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Song_Replace.CellContentClick

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub LoadHistory()

        Application.DoEvents()
        If Dt_playlist_date.EditValue = Nothing OrElse cmb_Store.EditValue = Nothing Then Exit Sub
        Dim vMediaId, vOldMediaId, str As String
        vMediaId = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_ID").Value.ToString()
        str = Grp_History.Text
        vOldMediaId = str.Replace("History for : ", "")
        vOldMediaId = vOldMediaId.Split("/")(0).Trim
        If vMediaId = vOldMediaId AndAlso Grp_History.Visible = True Then Exit Sub


        Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter

        Cursor = Cursors.WaitCursor
        Call OpenDBConnection()

        Dim dt As Date = Dt_playlist_date.EditValue.ToString



        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetMediaHistory"
            .Parameters.AddWithValue("@Media_Id", vMediaId).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Media_Type", SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_Type").Value.ToString()).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Playlist_Date", dt.ToString("MM/dd/yyyy").Replace("-", "/")).Direction = ParameterDirection.Input

        End With
        dapSongs.SelectCommand = Cmd
        'MsgBox(Cmd.Parameters("@Playlist_Date").Value.ToString)
        dapSongs.Fill(dsSongs, "MediaHistory")
        Grid_History.Columns.Clear()
        Grid_History.DataSource = dsSongs.Tables("MediaHistory")
        FormatHistoryGrid()

        'If dsSongs.Tables("MediaHistory").Rows.Count <> 0 Then
        '    SongsGrid.Columns.Clear()
        '    SongsGrid.DataSource = dsSongs.Tables("MediaHistory")
        '    FormatSongsGrid(False)

        Grp_History.Text = "History for : " & vMediaId & " / " & SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Title").Value.ToString() & " / " & SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Album").Value.ToString()

        Grp_History.Visible = True
        SongsGrid.Height = 300
        Dim i As Integer
        For i = 0 To Grid_History.Rows.Count - 1
            If Strings.Right(Grid_History.Rows(i).Cells(0).Value.ToString, 8) = DateAdd(DateInterval.Day, -4, dt).ToString("dd/MM/yy").Replace("-", "/") Then
                Grid_History.FirstDisplayedScrollingRowIndex = i
                Exit For
            End If

            Application.DoEvents()
        Next
        Grid_History.Rows(i + 4).selected = True
        '    If SongsGrid.Rows.Count = RowIndex Then RowIndex -= 1

        '    If RowIndex = 0 Then
        '        SongsGrid.Rows(1).Selected = True
        '    Else
        '        SongsGrid.Rows(RowIndex).Selected = True
        '    End If
        '    SongsGrid.SelectedCells(3).Selected = True
        '    SongsGrid.FirstDisplayedScrollingRowIndex = CurFirstRowIndex
        '    btn_Save.Enabled = False
        'Else 'no records

        '    FormatSongsGrid(True)
        '    btn_Save.Enabled = False
        '    'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        'End If

        
        dsSongs.Dispose()
        dapSongs.Dispose()
        Call CloseDBConnection()
        Cursor = Cursors.Default
        'SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Media_Type").Value.ToString = "H"

    End Sub
    Private Sub FormatHistoryGrid()
        Grid_History.Columns(0).Width = 100

        For i = 1 To 48
            Grid_History.Columns(i).Width = 25
            If i Mod 2 = 0 Then Grid_History.Columns(i).Visible = False
            Grid_History.Columns(i).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        Next
        Grid_History.Columns(49).Width = 1500
        Grid_History.Refresh()
    End Sub

    Private Sub Grid_History_CellMouseEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_History.CellMouseEnter
        'If e.ColumnIndex = 11 And e.RowIndex = 4 Then
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 1 Then
            If Grid_History.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString <> "" Then
                Dim c As DataGridViewCell
                c = Grid_History.Rows(e.RowIndex).Cells(e.ColumnIndex)
                c.ToolTipText = Grid_History.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value.ToString
            End If
        End If
    End Sub


    Private Sub Grid_History_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_History.KeyDown
        If e.KeyCode = Keys.F7 Then
            If Grp_History.Visible = True Then Grp_History.Visible = False : SongsGrid.Height = 552 Else LoadHistory()
        End If
    End Sub

    Private Sub btn_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Close.Click
        SongsGrid.Height = 552
        Grp_History.Visible = False
    End Sub

    Private Sub SongsGrid_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SongsGrid.SelectionChanged
        If Grp_History.Visible = True Then LoadHistory()
    End Sub

    Private Sub Grid_History_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_History.CellContentClick

    End Sub
End Class