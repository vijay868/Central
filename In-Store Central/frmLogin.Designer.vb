﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogin))
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.PictureEdit3 = New DevExpress.XtraEditors.PictureEdit()
        Me.lbl_ChangePassword = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
        Me.txt_Password = New DevExpress.XtraEditors.TextEdit()
        Me.txt_LoginID = New DevExpress.XtraEditors.TextEdit()
        Me.lbl_Password = New DevExpress.XtraEditors.LabelControl()
        Me.lbl_LoginId = New DevExpress.XtraEditors.LabelControl()
        Me.btn_Exit = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Login = New DevExpress.XtraEditors.SimpleButton()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Password.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_LoginID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.PictureEdit3)
        Me.PanelControl1.Controls.Add(Me.lbl_ChangePassword)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.PictureEdit1)
        Me.PanelControl1.Controls.Add(Me.txt_Password)
        Me.PanelControl1.Controls.Add(Me.txt_LoginID)
        Me.PanelControl1.Controls.Add(Me.lbl_Password)
        Me.PanelControl1.Controls.Add(Me.lbl_LoginId)
        Me.PanelControl1.Controls.Add(Me.btn_Exit)
        Me.PanelControl1.Controls.Add(Me.btn_Login)
        Me.PanelControl1.Location = New System.Drawing.Point(14, 13)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(338, 214)
        Me.PanelControl1.TabIndex = 34
        '
        'PictureEdit3
        '
        Me.PictureEdit3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PictureEdit3.EditValue = CType(resources.GetObject("PictureEdit3.EditValue"), Object)
        Me.PictureEdit3.Location = New System.Drawing.Point(283, 22)
        Me.PictureEdit3.Name = "PictureEdit3"
        Me.PictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.PictureEdit3.Size = New System.Drawing.Size(189, 27)
        Me.PictureEdit3.TabIndex = 47
        Me.PictureEdit3.Visible = False
        '
        'lbl_ChangePassword
        '
        Me.lbl_ChangePassword.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ChangePassword.Appearance.ForeColor = System.Drawing.Color.Yellow
        Me.lbl_ChangePassword.Location = New System.Drawing.Point(164, 186)
        Me.lbl_ChangePassword.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.lbl_ChangePassword.Name = "lbl_ChangePassword"
        Me.lbl_ChangePassword.Size = New System.Drawing.Size(25, 15)
        Me.lbl_ChangePassword.TabIndex = 45
        Me.lbl_ChangePassword.Text = "here"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(12, 186)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(149, 15)
        Me.LabelControl2.TabIndex = 44
        Me.LabelControl2.Text = "* To change password click"
        '
        'PictureEdit1
        '
        Me.PictureEdit1.EditValue = CType(resources.GetObject("PictureEdit1.EditValue"), Object)
        Me.PictureEdit1.Location = New System.Drawing.Point(12, 8)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.PictureEdit1.Size = New System.Drawing.Size(111, 51)
        Me.PictureEdit1.TabIndex = 41
        '
        'txt_Password
        '
        Me.txt_Password.EditValue = ""
        Me.txt_Password.Location = New System.Drawing.Point(129, 109)
        Me.txt_Password.Name = "txt_Password"
        Me.txt_Password.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Password.Properties.Appearance.Options.UseFont = True
        Me.txt_Password.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_Password.Size = New System.Drawing.Size(166, 22)
        Me.txt_Password.TabIndex = 2
        '
        'txt_LoginID
        '
        Me.txt_LoginID.EditValue = ""
        Me.txt_LoginID.Location = New System.Drawing.Point(129, 68)
        Me.txt_LoginID.Name = "txt_LoginID"
        Me.txt_LoginID.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_LoginID.Properties.Appearance.Options.UseFont = True
        Me.txt_LoginID.Size = New System.Drawing.Size(166, 22)
        Me.txt_LoginID.TabIndex = 1
        '
        'lbl_Password
        '
        Me.lbl_Password.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Password.Location = New System.Drawing.Point(36, 112)
        Me.lbl_Password.Name = "lbl_Password"
        Me.lbl_Password.Size = New System.Drawing.Size(63, 15)
        Me.lbl_Password.TabIndex = 38
        Me.lbl_Password.Text = " Password :"
        '
        'lbl_LoginId
        '
        Me.lbl_LoginId.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_LoginId.Location = New System.Drawing.Point(46, 71)
        Me.lbl_LoginId.Name = "lbl_LoginId"
        Me.lbl_LoginId.Size = New System.Drawing.Size(52, 15)
        Me.lbl_LoginId.TabIndex = 37
        Me.lbl_LoginId.Text = "Login ID :"
        '
        'btn_Exit
        '
        Me.btn_Exit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Exit.Appearance.Options.UseFont = True
        Me.btn_Exit.Location = New System.Drawing.Point(184, 150)
        Me.btn_Exit.Name = "btn_Exit"
        Me.btn_Exit.Size = New System.Drawing.Size(111, 22)
        Me.btn_Exit.TabIndex = 4
        Me.btn_Exit.Text = "Exit"
        '
        'btn_Login
        '
        Me.btn_Login.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Login.Appearance.Options.UseFont = True
        Me.btn_Login.Location = New System.Drawing.Point(36, 150)
        Me.btn_Login.Name = "btn_Login"
        Me.btn_Login.Size = New System.Drawing.Size(111, 22)
        Me.btn_Login.TabIndex = 3
        Me.btn_Login.Text = "Login"
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'frmLogin
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(365, 239)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "In-Store Central"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Password.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_LoginID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txt_LoginID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl_Password As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl_LoginId As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btn_Exit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Login As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Password As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl_ChangePassword As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PictureEdit3 As DevExpress.XtraEditors.PictureEdit
End Class
