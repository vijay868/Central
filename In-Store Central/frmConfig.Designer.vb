﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfig
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node1")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node3")
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node4")
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node5")
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node6")
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Genre", New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2, TreeNode3, TreeNode4, TreeNode5})
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node8")
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node9")
        Dim TreeNode9 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node10")
        Dim TreeNode10 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node11")
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Node7", New System.Windows.Forms.TreeNode() {TreeNode7, TreeNode8, TreeNode9, TreeNode10})
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Config_Tab_Control = New DevExpress.XtraTab.XtraTabControl()
        Me.Genre_Sub_Tab = New DevExpress.XtraTab.XtraTabPage()
        Me.Genre_Panel = New DevExpress.XtraEditors.PanelControl()
        Me.btn_save_edit_Genre = New DevExpress.XtraEditors.SimpleButton()
        Me.Genre_Info_Grid = New System.Windows.Forms.DataGridView()
        Me.Genre_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Genre_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Categories_Sub_Tab = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.cmb_Parent_Category = New DevExpress.XtraEditors.LookUpEdit()
        Me.lbl_Parent_Category = New System.Windows.Forms.Label()
        Me.cmb_Cat_Parent = New System.Windows.Forms.ComboBox()
        Me.Media_Group = New DevExpress.XtraEditors.RadioGroup()
        Me.btn_save_edit_category = New DevExpress.XtraEditors.SimpleButton()
        Me.Category_Info_Grid = New System.Windows.Forms.DataGridView()
        Me.Category_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Category_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Category_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Rotation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Customers_Sub_Tab = New DevExpress.XtraTab.XtraTabPage()
        Me.Customer_Panel = New DevExpress.XtraEditors.PanelControl()
        Me.Customers_Info_Grid = New System.Windows.Forms.DataGridView()
        Me.Customer_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Customer_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_save_edit_customer = New DevExpress.XtraEditors.SimpleButton()
        Me.Store_Sub_Tab = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.tvw_Genre_Category = New System.Windows.Forms.TreeView()
        Me.txt_Promo_Category = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Genre_Category = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.cmb_Daypart_To = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cmb_Daypart_From = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txt_DayPart = New DevExpress.XtraEditors.TextEdit()
        Me.chk_FTPSync = New DevExpress.XtraEditors.CheckEdit()
        Me.btn_StoreSave = New DevExpress.XtraEditors.SimpleButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_MaxSongs = New DevExpress.XtraEditors.TextEdit()
        Me.cmb_To = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmb_From = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_StoreName = New DevExpress.XtraEditors.TextEdit()
        Me.GridRatio = New System.Windows.Forms.DataGridView()
        Me.RowNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Day = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Undefined1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Undefined2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Undefined3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Undefined4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Undefined5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GridDayparts = New System.Windows.Forms.DataGridView()
        Me.Daypart = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DaypartFrom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DaypartTo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Genre_Category_Ids = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Genre_Categories = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_Category_Codes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_Categories = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.StoresGrid = New System.Windows.Forms.DataGridView()
        Me.Store_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StoreName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmb_Genre_Category = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cmb_Promo_Category = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.Audio_Sub_Tab = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.btn_Browse_Converter = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Audio_Converter_Path = New DevExpress.XtraEditors.TextEdit()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btn_Browse_Fileprep_Path = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Fileprep_Path = New DevExpress.XtraEditors.TextEdit()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btn_Audio_Save = New DevExpress.XtraEditors.SimpleButton()
        Me.cmb_Audio_Format = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_Browse_Promos_Path = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Promos_Audio_Path = New DevExpress.XtraEditors.TextEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btn_Browse_Songs_Path = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Songs_Audio_Path = New DevExpress.XtraEditors.TextEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.FTP_Sub_Tab = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.txt_Password = New DevExpress.XtraEditors.TextEdit()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btn_Save_FTP = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_User_Name = New DevExpress.XtraEditors.TextEdit()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_IPAddress = New DevExpress.XtraEditors.TextEdit()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.FolderBrowser = New System.Windows.Forms.FolderBrowserDialog()
        Me.FileBrowser = New System.Windows.Forms.OpenFileDialog()
        CType(Me.Config_Tab_Control, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Config_Tab_Control.SuspendLayout()
        Me.Genre_Sub_Tab.SuspendLayout()
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Genre_Panel.SuspendLayout()
        CType(Me.Genre_Info_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Categories_Sub_Tab.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.cmb_Parent_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Media_Group.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Category_Info_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Customers_Sub_Tab.SuspendLayout()
        CType(Me.Customer_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Customer_Panel.SuspendLayout()
        CType(Me.Customers_Info_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Store_Sub_Tab.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.txt_Promo_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Genre_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Daypart_To.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Daypart_From.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_DayPart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_FTPSync.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_MaxSongs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_To.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_From.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_StoreName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridRatio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridDayparts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StoresGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Genre_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Promo_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Audio_Sub_Tab.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.txt_Audio_Converter_Path.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Fileprep_Path.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Audio_Format.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Promos_Audio_Path.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Songs_Audio_Path.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FTP_Sub_Tab.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.txt_Password.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_User_Name.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IPAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Config_Tab_Control
        '
        Me.Config_Tab_Control.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Config_Tab_Control.Appearance.Options.UseFont = True
        Me.Config_Tab_Control.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Config_Tab_Control.Location = New System.Drawing.Point(0, 0)
        Me.Config_Tab_Control.Name = "Config_Tab_Control"
        Me.Config_Tab_Control.SelectedTabPage = Me.Genre_Sub_Tab
        Me.Config_Tab_Control.Size = New System.Drawing.Size(1149, 742)
        Me.Config_Tab_Control.TabIndex = 5
        Me.Config_Tab_Control.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.Genre_Sub_Tab, Me.Categories_Sub_Tab, Me.Customers_Sub_Tab, Me.Store_Sub_Tab, Me.Audio_Sub_Tab, Me.FTP_Sub_Tab})
        '
        'Genre_Sub_Tab
        '
        Me.Genre_Sub_Tab.Controls.Add(Me.Genre_Panel)
        Me.Genre_Sub_Tab.Name = "Genre_Sub_Tab"
        Me.Genre_Sub_Tab.Size = New System.Drawing.Size(1143, 714)
        Me.Genre_Sub_Tab.Text = "Genre"
        '
        'Genre_Panel
        '
        Me.Genre_Panel.Controls.Add(Me.btn_save_edit_Genre)
        Me.Genre_Panel.Controls.Add(Me.Genre_Info_Grid)
        Me.Genre_Panel.Location = New System.Drawing.Point(2, 5)
        Me.Genre_Panel.Name = "Genre_Panel"
        Me.Genre_Panel.Size = New System.Drawing.Size(299, 421)
        Me.Genre_Panel.TabIndex = 31
        '
        'btn_save_edit_Genre
        '
        Me.btn_save_edit_Genre.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save_edit_Genre.Appearance.Options.UseFont = True
        Me.btn_save_edit_Genre.Enabled = False
        Me.btn_save_edit_Genre.Location = New System.Drawing.Point(190, 7)
        Me.btn_save_edit_Genre.Name = "btn_save_edit_Genre"
        Me.btn_save_edit_Genre.Size = New System.Drawing.Size(104, 23)
        Me.btn_save_edit_Genre.TabIndex = 30
        Me.btn_save_edit_Genre.Text = "Save"
        '
        'Genre_Info_Grid
        '
        Me.Genre_Info_Grid.AllowUserToDeleteRows = False
        Me.Genre_Info_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.Genre_Info_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.Genre_Info_Grid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Genre_Info_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Genre_Info_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Genre_Info_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Genre_Info_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Genre_Info_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.Genre_Info_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Genre_Info_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Genre_ID, Me.Genre_Name})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Genre_Info_Grid.DefaultCellStyle = DataGridViewCellStyle3
        Me.Genre_Info_Grid.EnableHeadersVisualStyles = False
        Me.Genre_Info_Grid.Location = New System.Drawing.Point(7, 36)
        Me.Genre_Info_Grid.Name = "Genre_Info_Grid"
        Me.Genre_Info_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Genre_Info_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.Genre_Info_Grid.RowHeadersVisible = False
        Me.Genre_Info_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Genre_Info_Grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Genre_Info_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Genre_Info_Grid.Size = New System.Drawing.Size(286, 378)
        Me.Genre_Info_Grid.TabIndex = 0
        '
        'Genre_ID
        '
        Me.Genre_ID.HeaderText = "Genre ID"
        Me.Genre_ID.Name = "Genre_ID"
        Me.Genre_ID.Visible = False
        '
        'Genre_Name
        '
        Me.Genre_Name.HeaderText = "Genre Name"
        Me.Genre_Name.Name = "Genre_Name"
        Me.Genre_Name.Width = 285
        '
        'Categories_Sub_Tab
        '
        Me.Categories_Sub_Tab.Controls.Add(Me.PanelControl4)
        Me.Categories_Sub_Tab.Name = "Categories_Sub_Tab"
        Me.Categories_Sub_Tab.Size = New System.Drawing.Size(1143, 714)
        Me.Categories_Sub_Tab.Text = "Categories"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.cmb_Parent_Category)
        Me.PanelControl4.Controls.Add(Me.lbl_Parent_Category)
        Me.PanelControl4.Controls.Add(Me.cmb_Cat_Parent)
        Me.PanelControl4.Controls.Add(Me.Media_Group)
        Me.PanelControl4.Controls.Add(Me.btn_save_edit_category)
        Me.PanelControl4.Controls.Add(Me.Category_Info_Grid)
        Me.PanelControl4.Location = New System.Drawing.Point(2, 5)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(367, 421)
        Me.PanelControl4.TabIndex = 32
        '
        'cmb_Parent_Category
        '
        Me.cmb_Parent_Category.EditValue = ""
        Me.cmb_Parent_Category.Location = New System.Drawing.Point(71, 54)
        Me.cmb_Parent_Category.Name = "cmb_Parent_Category"
        Me.cmb_Parent_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Parent_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Parent_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Parent_Category.Properties.NullText = ""
        Me.cmb_Parent_Category.Size = New System.Drawing.Size(198, 22)
        Me.cmb_Parent_Category.TabIndex = 60
        '
        'lbl_Parent_Category
        '
        Me.lbl_Parent_Category.AutoSize = True
        Me.lbl_Parent_Category.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Parent_Category.Location = New System.Drawing.Point(9, 57)
        Me.lbl_Parent_Category.Name = "lbl_Parent_Category"
        Me.lbl_Parent_Category.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Parent_Category.TabIndex = 61
        Me.lbl_Parent_Category.Text = "Genre:"
        '
        'cmb_Cat_Parent
        '
        Me.cmb_Cat_Parent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_Cat_Parent.FormattingEnabled = True
        Me.cmb_Cat_Parent.Location = New System.Drawing.Point(446, 5)
        Me.cmb_Cat_Parent.Name = "cmb_Cat_Parent"
        Me.cmb_Cat_Parent.Size = New System.Drawing.Size(148, 23)
        Me.cmb_Cat_Parent.TabIndex = 35
        '
        'Media_Group
        '
        Me.Media_Group.EditValue = "Genre"
        Me.Media_Group.Location = New System.Drawing.Point(7, 7)
        Me.Media_Group.Name = "Media_Group"
        Me.Media_Group.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer))
        Me.Media_Group.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.Media_Group.Properties.Appearance.Options.UseBackColor = True
        Me.Media_Group.Properties.Appearance.Options.UseFont = True
        Me.Media_Group.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.Media_Group.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("Genre", "Genre"), New DevExpress.XtraEditors.Controls.RadioGroupItem("Promos", "Promos")})
        Me.Media_Group.Size = New System.Drawing.Size(148, 25)
        Me.Media_Group.TabIndex = 34
        '
        'btn_save_edit_category
        '
        Me.btn_save_edit_category.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save_edit_category.Appearance.Options.UseFont = True
        Me.btn_save_edit_category.Enabled = False
        Me.btn_save_edit_category.Location = New System.Drawing.Point(254, 9)
        Me.btn_save_edit_category.Name = "btn_save_edit_category"
        Me.btn_save_edit_category.Size = New System.Drawing.Size(104, 23)
        Me.btn_save_edit_category.TabIndex = 30
        Me.btn_save_edit_category.Text = "Save"
        '
        'Category_Info_Grid
        '
        Me.Category_Info_Grid.AllowUserToDeleteRows = False
        Me.Category_Info_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        Me.Category_Info_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.Category_Info_Grid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Category_Info_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Category_Info_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Category_Info_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Category_Info_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Category_Info_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.Category_Info_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Category_Info_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Category_Id, Me.Category_Name, Me.Category_Code, Me.Rotation})
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Category_Info_Grid.DefaultCellStyle = DataGridViewCellStyle9
        Me.Category_Info_Grid.EnableHeadersVisualStyles = False
        Me.Category_Info_Grid.Location = New System.Drawing.Point(7, 77)
        Me.Category_Info_Grid.Name = "Category_Info_Grid"
        Me.Category_Info_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Category_Info_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.Category_Info_Grid.RowHeadersVisible = False
        Me.Category_Info_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Category_Info_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Category_Info_Grid.Size = New System.Drawing.Size(353, 337)
        Me.Category_Info_Grid.TabIndex = 0
        '
        'Category_Id
        '
        Me.Category_Id.HeaderText = "Category ID"
        Me.Category_Id.Name = "Category_Id"
        Me.Category_Id.Visible = False
        '
        'Category_Name
        '
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        Me.Category_Name.DefaultCellStyle = DataGridViewCellStyle7
        Me.Category_Name.HeaderText = "Category Name"
        Me.Category_Name.Name = "Category_Name"
        Me.Category_Name.Width = 150
        '
        'Category_Code
        '
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black
        Me.Category_Code.DefaultCellStyle = DataGridViewCellStyle8
        Me.Category_Code.HeaderText = "Category Code"
        Me.Category_Code.Name = "Category_Code"
        Me.Category_Code.Width = 110
        '
        'Rotation
        '
        Me.Rotation.HeaderText = "Rotation"
        Me.Rotation.Name = "Rotation"
        Me.Rotation.Width = 70
        '
        'Customers_Sub_Tab
        '
        Me.Customers_Sub_Tab.Controls.Add(Me.Customer_Panel)
        Me.Customers_Sub_Tab.Name = "Customers_Sub_Tab"
        Me.Customers_Sub_Tab.Size = New System.Drawing.Size(1143, 714)
        Me.Customers_Sub_Tab.Text = "Customers"
        '
        'Customer_Panel
        '
        Me.Customer_Panel.Controls.Add(Me.Customers_Info_Grid)
        Me.Customer_Panel.Controls.Add(Me.btn_save_edit_customer)
        Me.Customer_Panel.Location = New System.Drawing.Point(2, 5)
        Me.Customer_Panel.Name = "Customer_Panel"
        Me.Customer_Panel.Size = New System.Drawing.Size(299, 421)
        Me.Customer_Panel.TabIndex = 32
        '
        'Customers_Info_Grid
        '
        Me.Customers_Info_Grid.AllowUserToDeleteRows = False
        Me.Customers_Info_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black
        Me.Customers_Info_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle11
        Me.Customers_Info_Grid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Customers_Info_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Customers_Info_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Customers_Info_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Customers_Info_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Customers_Info_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.Customers_Info_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Customers_Info_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Customer_ID, Me.Customer_Name})
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Customers_Info_Grid.DefaultCellStyle = DataGridViewCellStyle13
        Me.Customers_Info_Grid.EnableHeadersVisualStyles = False
        Me.Customers_Info_Grid.Location = New System.Drawing.Point(7, 36)
        Me.Customers_Info_Grid.Name = "Customers_Info_Grid"
        Me.Customers_Info_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Customers_Info_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.Customers_Info_Grid.RowHeadersVisible = False
        Me.Customers_Info_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Customers_Info_Grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Customers_Info_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Customers_Info_Grid.Size = New System.Drawing.Size(286, 378)
        Me.Customers_Info_Grid.TabIndex = 31
        '
        'Customer_ID
        '
        Me.Customer_ID.HeaderText = "Customer ID"
        Me.Customer_ID.Name = "Customer_ID"
        Me.Customer_ID.Visible = False
        '
        'Customer_Name
        '
        Me.Customer_Name.HeaderText = "Customer Name"
        Me.Customer_Name.Name = "Customer_Name"
        Me.Customer_Name.Width = 285
        '
        'btn_save_edit_customer
        '
        Me.btn_save_edit_customer.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save_edit_customer.Appearance.Options.UseFont = True
        Me.btn_save_edit_customer.Enabled = False
        Me.btn_save_edit_customer.Location = New System.Drawing.Point(190, 7)
        Me.btn_save_edit_customer.Name = "btn_save_edit_customer"
        Me.btn_save_edit_customer.Size = New System.Drawing.Size(104, 23)
        Me.btn_save_edit_customer.TabIndex = 30
        Me.btn_save_edit_customer.Text = "Save"
        '
        'Store_Sub_Tab
        '
        Me.Store_Sub_Tab.Controls.Add(Me.PanelControl1)
        Me.Store_Sub_Tab.Name = "Store_Sub_Tab"
        Me.Store_Sub_Tab.Size = New System.Drawing.Size(1143, 714)
        Me.Store_Sub_Tab.Text = "Store"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.tvw_Genre_Category)
        Me.PanelControl1.Controls.Add(Me.txt_Promo_Category)
        Me.PanelControl1.Controls.Add(Me.txt_Genre_Category)
        Me.PanelControl1.Controls.Add(Me.SimpleButton1)
        Me.PanelControl1.Controls.Add(Me.cmb_Daypart_To)
        Me.PanelControl1.Controls.Add(Me.cmb_Daypart_From)
        Me.PanelControl1.Controls.Add(Me.txt_DayPart)
        Me.PanelControl1.Controls.Add(Me.chk_FTPSync)
        Me.PanelControl1.Controls.Add(Me.btn_StoreSave)
        Me.PanelControl1.Controls.Add(Me.Label5)
        Me.PanelControl1.Controls.Add(Me.txt_MaxSongs)
        Me.PanelControl1.Controls.Add(Me.cmb_To)
        Me.PanelControl1.Controls.Add(Me.Label3)
        Me.PanelControl1.Controls.Add(Me.cmb_From)
        Me.PanelControl1.Controls.Add(Me.Label2)
        Me.PanelControl1.Controls.Add(Me.Label1)
        Me.PanelControl1.Controls.Add(Me.txt_StoreName)
        Me.PanelControl1.Controls.Add(Me.GridRatio)
        Me.PanelControl1.Controls.Add(Me.GridDayparts)
        Me.PanelControl1.Controls.Add(Me.cmb_Customer)
        Me.PanelControl1.Controls.Add(Me.Label4)
        Me.PanelControl1.Controls.Add(Me.StoresGrid)
        Me.PanelControl1.Controls.Add(Me.cmb_Genre_Category)
        Me.PanelControl1.Controls.Add(Me.cmb_Promo_Category)
        Me.PanelControl1.Location = New System.Drawing.Point(4, 4)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(830, 547)
        Me.PanelControl1.TabIndex = 58
        '
        'tvw_Genre_Category
        '
        Me.tvw_Genre_Category.BackColor = System.Drawing.Color.DimGray
        Me.tvw_Genre_Category.CheckBoxes = True
        Me.tvw_Genre_Category.ForeColor = System.Drawing.Color.White
        Me.tvw_Genre_Category.LineColor = System.Drawing.Color.White
        Me.tvw_Genre_Category.Location = New System.Drawing.Point(517, 174)
        Me.tvw_Genre_Category.Name = "tvw_Genre_Category"
        TreeNode1.Name = "Node1"
        TreeNode1.Tag = "1"
        TreeNode1.Text = "Node1"
        TreeNode2.Name = "Node3"
        TreeNode2.Tag = "3"
        TreeNode2.Text = "Node3"
        TreeNode3.Name = "Node4"
        TreeNode3.Tag = "4"
        TreeNode3.Text = "Node4"
        TreeNode4.Name = "Node5"
        TreeNode4.Tag = "5"
        TreeNode4.Text = "Node5"
        TreeNode5.Name = "Node6"
        TreeNode5.Tag = "6"
        TreeNode5.Text = "Node6"
        TreeNode6.Name = "Genre"
        TreeNode6.Text = "Genre"
        TreeNode7.Name = "Node8"
        TreeNode7.Tag = "8"
        TreeNode7.Text = "Node8"
        TreeNode8.Name = "Node9"
        TreeNode8.Tag = "9"
        TreeNode8.Text = "Node9"
        TreeNode9.Name = "Node10"
        TreeNode9.Tag = "10"
        TreeNode9.Text = "Node10"
        TreeNode10.Name = "Node11"
        TreeNode10.Tag = "11"
        TreeNode10.Text = "Node11"
        TreeNode11.Name = "Node7"
        TreeNode11.Text = "Node7"
        Me.tvw_Genre_Category.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode6, TreeNode11})
        Me.tvw_Genre_Category.Size = New System.Drawing.Size(139, 327)
        Me.tvw_Genre_Category.TabIndex = 92
        Me.tvw_Genre_Category.Visible = False
        '
        'txt_Promo_Category
        '
        Me.txt_Promo_Category.EditValue = ""
        Me.txt_Promo_Category.Location = New System.Drawing.Point(160, 513)
        Me.txt_Promo_Category.Name = "txt_Promo_Category"
        Me.txt_Promo_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Promo_Category.Properties.Appearance.Options.UseFont = True
        Me.txt_Promo_Category.Size = New System.Drawing.Size(137, 22)
        Me.txt_Promo_Category.TabIndex = 80
        Me.txt_Promo_Category.Visible = False
        '
        'txt_Genre_Category
        '
        Me.txt_Genre_Category.EditValue = ""
        Me.txt_Genre_Category.Location = New System.Drawing.Point(20, 513)
        Me.txt_Genre_Category.Name = "txt_Genre_Category"
        Me.txt_Genre_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Genre_Category.Properties.Appearance.Options.UseFont = True
        Me.txt_Genre_Category.Size = New System.Drawing.Size(137, 22)
        Me.txt_Genre_Category.TabIndex = 79
        Me.txt_Genre_Category.Visible = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Enabled = False
        Me.SimpleButton1.Location = New System.Drawing.Point(798, 150)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(20, 23)
        Me.SimpleButton1.TabIndex = 78
        Me.SimpleButton1.Text = "..."
        Me.SimpleButton1.Visible = False
        '
        'cmb_Daypart_To
        '
        Me.cmb_Daypart_To.Location = New System.Drawing.Point(457, 151)
        Me.cmb_Daypart_To.Name = "cmb_Daypart_To"
        Me.cmb_Daypart_To.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Daypart_To.Properties.Appearance.Options.UseFont = True
        Me.cmb_Daypart_To.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Daypart_To.Properties.Items.AddRange(New Object() {"1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM", "12 AM"})
        Me.cmb_Daypart_To.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cmb_Daypart_To.Size = New System.Drawing.Size(59, 22)
        Me.cmb_Daypart_To.TabIndex = 75
        '
        'cmb_Daypart_From
        '
        Me.cmb_Daypart_From.Enabled = False
        Me.cmb_Daypart_From.Location = New System.Drawing.Point(397, 151)
        Me.cmb_Daypart_From.Name = "cmb_Daypart_From"
        Me.cmb_Daypart_From.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Daypart_From.Properties.Appearance.Options.UseFont = True
        Me.cmb_Daypart_From.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Daypart_From.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.cmb_Daypart_From.Properties.AppearanceDisabled.Options.UseFont = True
        Me.cmb_Daypart_From.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.cmb_Daypart_From.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Daypart_From.Properties.Items.AddRange(New Object() {"1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM", "12 AM"})
        Me.cmb_Daypart_From.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cmb_Daypart_From.Size = New System.Drawing.Size(59, 22)
        Me.cmb_Daypart_From.TabIndex = 74
        '
        'txt_DayPart
        '
        Me.txt_DayPart.EditValue = ""
        Me.txt_DayPart.Enabled = False
        Me.txt_DayPart.Location = New System.Drawing.Point(337, 151)
        Me.txt_DayPart.Name = "txt_DayPart"
        Me.txt_DayPart.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DayPart.Properties.Appearance.Options.UseFont = True
        Me.txt_DayPart.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DayPart.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_DayPart.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_DayPart.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_DayPart.Size = New System.Drawing.Size(59, 22)
        Me.txt_DayPart.TabIndex = 73
        '
        'chk_FTPSync
        '
        Me.chk_FTPSync.Location = New System.Drawing.Point(341, 513)
        Me.chk_FTPSync.Name = "chk_FTPSync"
        Me.chk_FTPSync.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_FTPSync.Properties.Appearance.Options.UseFont = True
        Me.chk_FTPSync.Properties.Caption = "FTPSync"
        Me.chk_FTPSync.Size = New System.Drawing.Size(77, 20)
        Me.chk_FTPSync.TabIndex = 72
        '
        'btn_StoreSave
        '
        Me.btn_StoreSave.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_StoreSave.Appearance.Options.UseFont = True
        Me.btn_StoreSave.Enabled = False
        Me.btn_StoreSave.Location = New System.Drawing.Point(714, 508)
        Me.btn_StoreSave.Name = "btn_StoreSave"
        Me.btn_StoreSave.Size = New System.Drawing.Size(104, 23)
        Me.btn_StoreSave.TabIndex = 71
        Me.btn_StoreSave.Text = "Save"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(672, 98)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 15)
        Me.Label5.TabIndex = 70
        Me.Label5.Text = "Max Songs:"
        '
        'txt_MaxSongs
        '
        Me.txt_MaxSongs.EditValue = ""
        Me.txt_MaxSongs.Location = New System.Drawing.Point(745, 95)
        Me.txt_MaxSongs.Name = "txt_MaxSongs"
        Me.txt_MaxSongs.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_MaxSongs.Properties.Appearance.Options.UseFont = True
        Me.txt_MaxSongs.Size = New System.Drawing.Size(72, 22)
        Me.txt_MaxSongs.TabIndex = 69
        '
        'cmb_To
        '
        Me.cmb_To.Location = New System.Drawing.Point(582, 93)
        Me.cmb_To.Name = "cmb_To"
        Me.cmb_To.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_To.Properties.Appearance.Options.UseFont = True
        Me.cmb_To.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_To.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cmb_To.Size = New System.Drawing.Size(72, 22)
        Me.cmb_To.TabIndex = 68
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(557, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(24, 15)
        Me.Label3.TabIndex = 67
        Me.Label3.Text = "To:"
        '
        'cmb_From
        '
        Me.cmb_From.Location = New System.Drawing.Point(457, 93)
        Me.cmb_From.Name = "cmb_From"
        Me.cmb_From.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_From.Properties.Appearance.Options.UseFont = True
        Me.cmb_From.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_From.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cmb_From.Size = New System.Drawing.Size(72, 22)
        Me.cmb_From.TabIndex = 66
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(339, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 15)
        Me.Label2.TabIndex = 65
        Me.Label2.Text = "Store Timing From:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(339, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 15)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "Store:"
        '
        'txt_StoreName
        '
        Me.txt_StoreName.EditValue = ""
        Me.txt_StoreName.Location = New System.Drawing.Point(384, 50)
        Me.txt_StoreName.Name = "txt_StoreName"
        Me.txt_StoreName.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_StoreName.Properties.Appearance.Options.UseFont = True
        Me.txt_StoreName.Size = New System.Drawing.Size(270, 22)
        Me.txt_StoreName.TabIndex = 62
        '
        'GridRatio
        '
        Me.GridRatio.AllowUserToAddRows = False
        Me.GridRatio.AllowUserToResizeRows = False
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black
        Me.GridRatio.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle15
        Me.GridRatio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridRatio.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.GridRatio.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.GridRatio.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.GridRatio.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridRatio.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.GridRatio.ColumnHeadersHeight = 25
        Me.GridRatio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.GridRatio.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RowNumber, Me.Day, Me.Undefined1, Me.Undefined2, Me.Undefined3, Me.Undefined4, Me.Undefined5})
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridRatio.DefaultCellStyle = DataGridViewCellStyle17
        Me.GridRatio.EnableHeadersVisualStyles = False
        Me.GridRatio.Location = New System.Drawing.Point(337, 307)
        Me.GridRatio.Name = "GridRatio"
        Me.GridRatio.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridRatio.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.GridRatio.RowHeadersVisible = False
        Me.GridRatio.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.GridRatio.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.GridRatio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridRatio.Size = New System.Drawing.Size(480, 195)
        Me.GridRatio.TabIndex = 61
        '
        'RowNumber
        '
        Me.RowNumber.HeaderText = "RowNumber"
        Me.RowNumber.Name = "RowNumber"
        Me.RowNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RowNumber.Visible = False
        '
        'Day
        '
        Me.Day.HeaderText = "Day"
        Me.Day.Name = "Day"
        Me.Day.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Day.Width = 80
        '
        'Undefined1
        '
        Me.Undefined1.HeaderText = "<Undefined>"
        Me.Undefined1.Name = "Undefined1"
        Me.Undefined1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Undefined1.Width = 80
        '
        'Undefined2
        '
        Me.Undefined2.HeaderText = "<Undefined>"
        Me.Undefined2.Name = "Undefined2"
        Me.Undefined2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Undefined2.Width = 80
        '
        'Undefined3
        '
        Me.Undefined3.HeaderText = "<Undefined>"
        Me.Undefined3.Name = "Undefined3"
        Me.Undefined3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Undefined3.Width = 80
        '
        'Undefined4
        '
        Me.Undefined4.HeaderText = "<Undefined>"
        Me.Undefined4.Name = "Undefined4"
        Me.Undefined4.Width = 78
        '
        'Undefined5
        '
        Me.Undefined5.HeaderText = "<Undefined>"
        Me.Undefined5.Name = "Undefined5"
        Me.Undefined5.Width = 78
        '
        'GridDayparts
        '
        Me.GridDayparts.AllowUserToAddRows = False
        Me.GridDayparts.AllowUserToDeleteRows = False
        Me.GridDayparts.AllowUserToResizeRows = False
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black
        Me.GridDayparts.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle19
        Me.GridDayparts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridDayparts.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.GridDayparts.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.GridDayparts.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.GridDayparts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridDayparts.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.GridDayparts.ColumnHeadersHeight = 25
        Me.GridDayparts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.GridDayparts.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Daypart, Me.DaypartFrom, Me.DaypartTo, Me.Genre_Category_Ids, Me.Genre_Categories, Me.Promo_Category_Codes, Me.Promo_Categories})
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridDayparts.DefaultCellStyle = DataGridViewCellStyle21
        Me.GridDayparts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.GridDayparts.EnableHeadersVisualStyles = False
        Me.GridDayparts.Location = New System.Drawing.Point(337, 177)
        Me.GridDayparts.Name = "GridDayparts"
        Me.GridDayparts.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridDayparts.RowHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.GridDayparts.RowHeadersVisible = False
        Me.GridDayparts.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.GridDayparts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridDayparts.Size = New System.Drawing.Size(481, 114)
        Me.GridDayparts.TabIndex = 60
        '
        'Daypart
        '
        Me.Daypart.HeaderText = "Daypart"
        Me.Daypart.Name = "Daypart"
        Me.Daypart.ReadOnly = True
        Me.Daypart.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Daypart.Width = 60
        '
        'DaypartFrom
        '
        Me.DaypartFrom.HeaderText = "From"
        Me.DaypartFrom.Name = "DaypartFrom"
        Me.DaypartFrom.ReadOnly = True
        Me.DaypartFrom.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DaypartFrom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DaypartFrom.Width = 60
        '
        'DaypartTo
        '
        Me.DaypartTo.HeaderText = "To"
        Me.DaypartTo.Name = "DaypartTo"
        Me.DaypartTo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DaypartTo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DaypartTo.Width = 60
        '
        'Genre_Category_Ids
        '
        Me.Genre_Category_Ids.HeaderText = "Gen_Cate"
        Me.Genre_Category_Ids.Name = "Genre_Category_Ids"
        Me.Genre_Category_Ids.Visible = False
        '
        'Genre_Categories
        '
        Me.Genre_Categories.HeaderText = "Genre/s"
        Me.Genre_Categories.Name = "Genre_Categories"
        Me.Genre_Categories.Width = 140
        '
        'Promo_Category_Codes
        '
        Me.Promo_Category_Codes.HeaderText = "Promo_Cat"
        Me.Promo_Category_Codes.Name = "Promo_Category_Codes"
        Me.Promo_Category_Codes.Visible = False
        '
        'Promo_Categories
        '
        Me.Promo_Categories.HeaderText = "Promo/s"
        Me.Promo_Categories.Name = "Promo_Categories"
        Me.Promo_Categories.Width = 140
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(79, 16)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.NullText = ""
        Me.cmb_Customer.Size = New System.Drawing.Size(243, 22)
        Me.cmb_Customer.TabIndex = 58
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 15)
        Me.Label4.TabIndex = 59
        Me.Label4.Text = "Customer:"
        '
        'StoresGrid
        '
        Me.StoresGrid.AllowUserToAddRows = False
        Me.StoresGrid.AllowUserToDeleteRows = False
        Me.StoresGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black
        Me.StoresGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle23
        Me.StoresGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.StoresGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.StoresGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.StoresGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.StoresGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.StoresGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.StoresGrid.ColumnHeadersHeight = 25
        Me.StoresGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.StoresGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Store_ID, Me.StoreName})
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.StoresGrid.DefaultCellStyle = DataGridViewCellStyle25
        Me.StoresGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.StoresGrid.EnableHeadersVisualStyles = False
        Me.StoresGrid.Location = New System.Drawing.Point(18, 53)
        Me.StoresGrid.Name = "StoresGrid"
        Me.StoresGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle26.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.StoresGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle26
        Me.StoresGrid.RowHeadersVisible = False
        Me.StoresGrid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.StoresGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.StoresGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.StoresGrid.Size = New System.Drawing.Size(304, 449)
        Me.StoresGrid.TabIndex = 54
        '
        'Store_ID
        '
        Me.Store_ID.HeaderText = "Store ID"
        Me.Store_ID.Name = "Store_ID"
        Me.Store_ID.Visible = False
        Me.Store_ID.Width = 80
        '
        'StoreName
        '
        Me.StoreName.HeaderText = "Store Name"
        Me.StoreName.Name = "StoreName"
        Me.StoreName.Width = 350
        '
        'cmb_Genre_Category
        '
        Me.cmb_Genre_Category.EditValue = ""
        Me.cmb_Genre_Category.Location = New System.Drawing.Point(517, 151)
        Me.cmb_Genre_Category.Name = "cmb_Genre_Category"
        Me.cmb_Genre_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Genre_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Genre_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Genre_Category.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cmb_Genre_Category.Size = New System.Drawing.Size(139, 22)
        Me.cmb_Genre_Category.TabIndex = 76
        '
        'cmb_Promo_Category
        '
        Me.cmb_Promo_Category.EditValue = ""
        Me.cmb_Promo_Category.Location = New System.Drawing.Point(657, 151)
        Me.cmb_Promo_Category.Name = "cmb_Promo_Category"
        Me.cmb_Promo_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Promo_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Promo_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Promo_Category.Properties.DropDownRows = 15
        Me.cmb_Promo_Category.Size = New System.Drawing.Size(140, 22)
        Me.cmb_Promo_Category.TabIndex = 77
        '
        'Audio_Sub_Tab
        '
        Me.Audio_Sub_Tab.Controls.Add(Me.PanelControl2)
        Me.Audio_Sub_Tab.Name = "Audio_Sub_Tab"
        Me.Audio_Sub_Tab.Size = New System.Drawing.Size(1143, 714)
        Me.Audio_Sub_Tab.Text = "Audio"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.btn_Browse_Converter)
        Me.PanelControl2.Controls.Add(Me.txt_Audio_Converter_Path)
        Me.PanelControl2.Controls.Add(Me.Label10)
        Me.PanelControl2.Controls.Add(Me.btn_Browse_Fileprep_Path)
        Me.PanelControl2.Controls.Add(Me.txt_Fileprep_Path)
        Me.PanelControl2.Controls.Add(Me.Label9)
        Me.PanelControl2.Controls.Add(Me.btn_Audio_Save)
        Me.PanelControl2.Controls.Add(Me.cmb_Audio_Format)
        Me.PanelControl2.Controls.Add(Me.Label8)
        Me.PanelControl2.Controls.Add(Me.btn_Browse_Promos_Path)
        Me.PanelControl2.Controls.Add(Me.txt_Promos_Audio_Path)
        Me.PanelControl2.Controls.Add(Me.Label7)
        Me.PanelControl2.Controls.Add(Me.btn_Browse_Songs_Path)
        Me.PanelControl2.Controls.Add(Me.txt_Songs_Audio_Path)
        Me.PanelControl2.Controls.Add(Me.Label6)
        Me.PanelControl2.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(583, 216)
        Me.PanelControl2.TabIndex = 0
        '
        'btn_Browse_Converter
        '
        Me.btn_Browse_Converter.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Browse_Converter.Appearance.Options.UseFont = True
        Me.btn_Browse_Converter.Location = New System.Drawing.Point(536, 158)
        Me.btn_Browse_Converter.Name = "btn_Browse_Converter"
        Me.btn_Browse_Converter.Size = New System.Drawing.Size(36, 22)
        Me.btn_Browse_Converter.TabIndex = 78
        Me.btn_Browse_Converter.Text = "..."
        '
        'txt_Audio_Converter_Path
        '
        Me.txt_Audio_Converter_Path.EditValue = ""
        Me.txt_Audio_Converter_Path.Location = New System.Drawing.Point(120, 159)
        Me.txt_Audio_Converter_Path.Name = "txt_Audio_Converter_Path"
        Me.txt_Audio_Converter_Path.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Audio_Converter_Path.Properties.Appearance.Options.UseFont = True
        Me.txt_Audio_Converter_Path.Size = New System.Drawing.Size(415, 22)
        Me.txt_Audio_Converter_Path.TabIndex = 77
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(17, 162)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 15)
        Me.Label10.TabIndex = 79
        Me.Label10.Text = "Audio Converter:"
        '
        'btn_Browse_Fileprep_Path
        '
        Me.btn_Browse_Fileprep_Path.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Browse_Fileprep_Path.Appearance.Options.UseFont = True
        Me.btn_Browse_Fileprep_Path.Location = New System.Drawing.Point(535, 93)
        Me.btn_Browse_Fileprep_Path.Name = "btn_Browse_Fileprep_Path"
        Me.btn_Browse_Fileprep_Path.Size = New System.Drawing.Size(36, 22)
        Me.btn_Browse_Fileprep_Path.TabIndex = 75
        Me.btn_Browse_Fileprep_Path.Text = "..."
        '
        'txt_Fileprep_Path
        '
        Me.txt_Fileprep_Path.EditValue = ""
        Me.txt_Fileprep_Path.Location = New System.Drawing.Point(119, 94)
        Me.txt_Fileprep_Path.Name = "txt_Fileprep_Path"
        Me.txt_Fileprep_Path.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Fileprep_Path.Properties.Appearance.Options.UseFont = True
        Me.txt_Fileprep_Path.Size = New System.Drawing.Size(415, 22)
        Me.txt_Fileprep_Path.TabIndex = 74
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(32, 96)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(83, 15)
        Me.Label9.TabIndex = 76
        Me.Label9.Text = "Fileprep Path:"
        '
        'btn_Audio_Save
        '
        Me.btn_Audio_Save.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Audio_Save.Appearance.Options.UseFont = True
        Me.btn_Audio_Save.Enabled = False
        Me.btn_Audio_Save.Location = New System.Drawing.Point(467, 187)
        Me.btn_Audio_Save.Name = "btn_Audio_Save"
        Me.btn_Audio_Save.Size = New System.Drawing.Size(104, 23)
        Me.btn_Audio_Save.TabIndex = 73
        Me.btn_Audio_Save.Text = "Save"
        '
        'cmb_Audio_Format
        '
        Me.cmb_Audio_Format.Location = New System.Drawing.Point(119, 127)
        Me.cmb_Audio_Format.Name = "cmb_Audio_Format"
        Me.cmb_Audio_Format.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Audio_Format.Properties.Appearance.Options.UseFont = True
        Me.cmb_Audio_Format.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Audio_Format.Size = New System.Drawing.Size(207, 22)
        Me.cmb_Audio_Format.TabIndex = 72
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(8, 130)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(106, 15)
        Me.Label8.TabIndex = 71
        Me.Label8.Text = "Audio File Format:"
        '
        'btn_Browse_Promos_Path
        '
        Me.btn_Browse_Promos_Path.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Browse_Promos_Path.Appearance.Options.UseFont = True
        Me.btn_Browse_Promos_Path.Location = New System.Drawing.Point(535, 60)
        Me.btn_Browse_Promos_Path.Name = "btn_Browse_Promos_Path"
        Me.btn_Browse_Promos_Path.Size = New System.Drawing.Size(36, 22)
        Me.btn_Browse_Promos_Path.TabIndex = 69
        Me.btn_Browse_Promos_Path.Text = "..."
        '
        'txt_Promos_Audio_Path
        '
        Me.txt_Promos_Audio_Path.EditValue = ""
        Me.txt_Promos_Audio_Path.Location = New System.Drawing.Point(119, 61)
        Me.txt_Promos_Audio_Path.Name = "txt_Promos_Audio_Path"
        Me.txt_Promos_Audio_Path.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Promos_Audio_Path.Properties.Appearance.Options.UseFont = True
        Me.txt_Promos_Audio_Path.Size = New System.Drawing.Size(415, 22)
        Me.txt_Promos_Audio_Path.TabIndex = 68
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 63)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(115, 15)
        Me.Label7.TabIndex = 70
        Me.Label7.Text = "Promos Audio Path:"
        '
        'btn_Browse_Songs_Path
        '
        Me.btn_Browse_Songs_Path.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Browse_Songs_Path.Appearance.Options.UseFont = True
        Me.btn_Browse_Songs_Path.Location = New System.Drawing.Point(535, 20)
        Me.btn_Browse_Songs_Path.Name = "btn_Browse_Songs_Path"
        Me.btn_Browse_Songs_Path.Size = New System.Drawing.Size(36, 22)
        Me.btn_Browse_Songs_Path.TabIndex = 66
        Me.btn_Browse_Songs_Path.Text = "..."
        '
        'txt_Songs_Audio_Path
        '
        Me.txt_Songs_Audio_Path.EditValue = ""
        Me.txt_Songs_Audio_Path.Location = New System.Drawing.Point(119, 21)
        Me.txt_Songs_Audio_Path.Name = "txt_Songs_Audio_Path"
        Me.txt_Songs_Audio_Path.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Songs_Audio_Path.Properties.Appearance.Options.UseFont = True
        Me.txt_Songs_Audio_Path.Size = New System.Drawing.Size(415, 22)
        Me.txt_Songs_Audio_Path.TabIndex = 65
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(11, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(107, 15)
        Me.Label6.TabIndex = 67
        Me.Label6.Text = "Songs Audio Path:"
        '
        'FTP_Sub_Tab
        '
        Me.FTP_Sub_Tab.Controls.Add(Me.PanelControl3)
        Me.FTP_Sub_Tab.Name = "FTP_Sub_Tab"
        Me.FTP_Sub_Tab.Size = New System.Drawing.Size(1143, 714)
        Me.FTP_Sub_Tab.Text = "FTP"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.txt_Password)
        Me.PanelControl3.Controls.Add(Me.Label12)
        Me.PanelControl3.Controls.Add(Me.btn_Save_FTP)
        Me.PanelControl3.Controls.Add(Me.txt_User_Name)
        Me.PanelControl3.Controls.Add(Me.Label14)
        Me.PanelControl3.Controls.Add(Me.txt_IPAddress)
        Me.PanelControl3.Controls.Add(Me.Label15)
        Me.PanelControl3.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(369, 154)
        Me.PanelControl3.TabIndex = 1
        '
        'txt_Password
        '
        Me.txt_Password.EditValue = ""
        Me.txt_Password.Location = New System.Drawing.Point(98, 94)
        Me.txt_Password.Name = "txt_Password"
        Me.txt_Password.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Password.Properties.Appearance.Options.UseFont = True
        Me.txt_Password.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_Password.Size = New System.Drawing.Size(259, 22)
        Me.txt_Password.TabIndex = 74
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(29, 96)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 15)
        Me.Label12.TabIndex = 76
        Me.Label12.Text = "Password:"
        '
        'btn_Save_FTP
        '
        Me.btn_Save_FTP.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Save_FTP.Appearance.Options.UseFont = True
        Me.btn_Save_FTP.Enabled = False
        Me.btn_Save_FTP.Location = New System.Drawing.Point(253, 125)
        Me.btn_Save_FTP.Name = "btn_Save_FTP"
        Me.btn_Save_FTP.Size = New System.Drawing.Size(104, 23)
        Me.btn_Save_FTP.TabIndex = 73
        Me.btn_Save_FTP.Text = "Save"
        '
        'txt_User_Name
        '
        Me.txt_User_Name.EditValue = ""
        Me.txt_User_Name.Location = New System.Drawing.Point(98, 61)
        Me.txt_User_Name.Name = "txt_User_Name"
        Me.txt_User_Name.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_User_Name.Properties.Appearance.Options.UseFont = True
        Me.txt_User_Name.Size = New System.Drawing.Size(259, 22)
        Me.txt_User_Name.TabIndex = 68
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(20, 63)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(73, 15)
        Me.Label14.TabIndex = 70
        Me.Label14.Text = "User Name:"
        '
        'txt_IPAddress
        '
        Me.txt_IPAddress.EditValue = ""
        Me.txt_IPAddress.Location = New System.Drawing.Point(98, 21)
        Me.txt_IPAddress.Name = "txt_IPAddress"
        Me.txt_IPAddress.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IPAddress.Properties.Appearance.Options.UseFont = True
        Me.txt_IPAddress.Size = New System.Drawing.Size(259, 22)
        Me.txt_IPAddress.TabIndex = 65
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(11, 24)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(82, 15)
        Me.Label15.TabIndex = 67
        Me.Label15.Text = "Host Address:"
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToResizeRows = False
        DataGridViewCellStyle27.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.Color.Black
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle27
        Me.DataGridView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle28
        Me.DataGridView1.ColumnHeadersHeight = 25
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle29
        Me.DataGridView1.EnableHeadersVisualStyles = False
        Me.DataGridView1.Location = New System.Drawing.Point(420, 331)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle30.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle30
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(304, 185)
        Me.DataGridView1.TabIndex = 61
        '
        'FileBrowser
        '
        Me.FileBrowser.FileName = "OpenFileDialog1"
        '
        'frmConfig
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1149, 742)
        Me.Controls.Add(Me.Config_Tab_Control)
        Me.Name = "frmConfig"
        Me.Text = "Config"
        CType(Me.Config_Tab_Control, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Config_Tab_Control.ResumeLayout(False)
        Me.Genre_Sub_Tab.ResumeLayout(False)
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Genre_Panel.ResumeLayout(False)
        CType(Me.Genre_Info_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Categories_Sub_Tab.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.PanelControl4.PerformLayout()
        CType(Me.cmb_Parent_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Media_Group.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Category_Info_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Customers_Sub_Tab.ResumeLayout(False)
        CType(Me.Customer_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Customer_Panel.ResumeLayout(False)
        CType(Me.Customers_Info_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Store_Sub_Tab.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.txt_Promo_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Genre_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Daypart_To.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Daypart_From.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_DayPart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_FTPSync.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_MaxSongs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_To.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_From.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_StoreName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridRatio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridDayparts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StoresGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Genre_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Promo_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Audio_Sub_Tab.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.txt_Audio_Converter_Path.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Fileprep_Path.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Audio_Format.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Promos_Audio_Path.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Songs_Audio_Path.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FTP_Sub_Tab.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.txt_Password.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_User_Name.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IPAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Config_Tab_Control As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents Genre_Sub_Tab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Genre_Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_save_edit_Genre As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Genre_Info_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents Customers_Sub_Tab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Customer_Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Customers_Info_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents btn_save_edit_customer As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Store_Sub_Tab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Audio_Sub_Tab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents StoresGrid As System.Windows.Forms.DataGridView
    Friend WithEvents GridDayparts As System.Windows.Forms.DataGridView
    Friend WithEvents GridRatio As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_StoreName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmb_To As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmb_From As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_MaxSongs As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_Browse_Songs_Path As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Songs_Audio_Path As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btn_Browse_Promos_Path As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Promos_Audio_Path As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmb_Audio_Format As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btn_Audio_Save As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_StoreSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btn_Browse_Fileprep_Path As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Fileprep_Path As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents FolderBrowser As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents btn_Browse_Converter As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Audio_Converter_Path As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents FileBrowser As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FTP_Sub_Tab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txt_Password As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btn_Save_FTP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_User_Name As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_IPAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Genre_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Genre_Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Customer_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Customer_Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Store_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StoreName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chk_FTPSync As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Categories_Sub_Tab As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_save_edit_category As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Category_Info_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents Media_Group As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents cmb_Cat_Parent As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_Parent_Category As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lbl_Parent_Category As System.Windows.Forms.Label
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmb_Daypart_To As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cmb_Daypart_From As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txt_DayPart As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Promo_Category As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Genre_Category As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tvw_Genre_Category As System.Windows.Forms.TreeView
    Friend WithEvents cmb_Genre_Category As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents RowNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Day As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Undefined1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Undefined2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Undefined3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Undefined4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Undefined5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmb_Promo_Category As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents Daypart As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DaypartFrom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DaypartTo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Genre_Category_Ids As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Genre_Categories As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_Category_Codes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_Categories As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Category_Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Category_Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Category_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Rotation As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
