﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlayList
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.Grp_History = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Close = New DevExpress.XtraEditors.SimpleButton()
        Me.Grid_History = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PanelPromoReplace = New DevExpress.XtraEditors.PanelControl()
        Me.txt_Promos_Search = New DevExpress.XtraEditors.TextEdit()
        Me.lbl_Airtime = New System.Windows.Forms.Label()
        Me.lbl_Playlist_Id = New System.Windows.Forms.Label()
        Me.lblPromoId = New System.Windows.Forms.Label()
        Me.lbl_Promo_Display = New System.Windows.Forms.Label()
        Me.lbl_Date_Display = New System.Windows.Forms.Label()
        Me.lbl_Promo = New System.Windows.Forms.Label()
        Me.lbl_Date = New System.Windows.Forms.Label()
        Me.btn_Exit = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_ReplacePromo = New DevExpress.XtraEditors.SimpleButton()
        Me.Grid_Replace = New System.Windows.Forms.DataGridView()
        Me.Promo_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PanelSongReplace = New DevExpress.XtraEditors.PanelControl()
        Me.txt_Songs_Search = New DevExpress.XtraEditors.TextEdit()
        Me.lbl_ActionType = New System.Windows.Forms.Label()
        Me.tvw_Genre_Category = New System.Windows.Forms.TreeView()
        Me.cmb_Genre_Category = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.lbl_Parent_Category = New System.Windows.Forms.Label()
        Me.lbl_song_airtime = New System.Windows.Forms.Label()
        Me.lbl_song_playlist_id = New System.Windows.Forms.Label()
        Me.lbl_song_Id = New System.Windows.Forms.Label()
        Me.lbl_Genre_Category = New System.Windows.Forms.Label()
        Me.lbl_Date_Replace = New System.Windows.Forms.Label()
        Me.lbl_CurRow = New System.Windows.Forms.Label()
        Me.btn_Song_Replace_Exit = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Song_Replace = New DevExpress.XtraEditors.SimpleButton()
        Me.Grid_Song_Replace = New System.Windows.Forms.DataGridView()
        Me.Replace_Song_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Replace_Song_Category = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Replace_Song_Title = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Replace_Song_Album = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Replace_Song_Artists = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Replace_Song_FileName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Replace_Song_Duration = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.HourBar = New System.Windows.Forms.TrackBar()
        Me.SongsGrid = New System.Windows.Forms.DataGridView()
        Me.Song_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Category = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Title = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Album = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Artist = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FileName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Duration = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PopUpMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ReplacePromoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReplaceSongMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InsertSongMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Dt_playlist_date = New DevExpress.XtraEditors.DateEdit()
        Me.cmb_Store = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_Save = New DevExpress.XtraEditors.SimpleButton()
        Me.Library_Tab_Control = New DevExpress.XtraTab.XtraTabControl()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.TmrPlayState = New System.Windows.Forms.Timer(Me.components)
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.Grp_History, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Grp_History.SuspendLayout()
        CType(Me.Grid_History, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelPromoReplace, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelPromoReplace.SuspendLayout()
        CType(Me.txt_Promos_Search.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid_Replace, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelSongReplace, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelSongReplace.SuspendLayout()
        CType(Me.txt_Songs_Search.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Genre_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid_Song_Replace, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HourBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SongsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopUpMenu.SuspendLayout()
        CType(Me.Dt_playlist_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dt_playlist_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Library_Tab_Control, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Library_Tab_Control.SuspendLayout()
        Me.SuspendLayout()
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.Grp_History)
        Me.XtraTabPage1.Controls.Add(Me.PanelPromoReplace)
        Me.XtraTabPage1.Controls.Add(Me.PanelSongReplace)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl1)
        Me.XtraTabPage1.Controls.Add(Me.HourBar)
        Me.XtraTabPage1.Controls.Add(Me.SongsGrid)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.Dt_playlist_date)
        Me.XtraTabPage1.Controls.Add(Me.cmb_Store)
        Me.XtraTabPage1.Controls.Add(Me.Label7)
        Me.XtraTabPage1.Controls.Add(Me.cmb_Customer)
        Me.XtraTabPage1.Controls.Add(Me.Label5)
        Me.XtraTabPage1.Controls.Add(Me.btn_Save)
        Me.XtraTabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1362, 691)
        '
        'Grp_History
        '
        Me.Grp_History.AppearanceCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grp_History.AppearanceCaption.Options.UseFont = True
        Me.Grp_History.Controls.Add(Me.btn_Close)
        Me.Grp_History.Controls.Add(Me.Grid_History)
        Me.Grp_History.Location = New System.Drawing.Point(6, 399)
        Me.Grp_History.Name = "Grp_History"
        Me.Grp_History.Size = New System.Drawing.Size(1340, 247)
        Me.Grp_History.TabIndex = 97
        Me.Grp_History.Text = "History for :"
        Me.Grp_History.Visible = False
        '
        'btn_Close
        '
        Me.btn_Close.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Close.Appearance.Options.UseFont = True
        Me.btn_Close.Location = New System.Drawing.Point(1312, 0)
        Me.btn_Close.Name = "btn_Close"
        Me.btn_Close.Size = New System.Drawing.Size(28, 21)
        Me.btn_Close.TabIndex = 98
        Me.btn_Close.Text = "X"
        '
        'Grid_History
        '
        Me.Grid_History.AllowUserToAddRows = False
        Me.Grid_History.AllowUserToDeleteRows = False
        Me.Grid_History.AllowUserToResizeRows = False
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black
        Me.Grid_History.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle17
        Me.Grid_History.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Grid_History.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Grid_History.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Grid_History.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_History.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.Grid_History.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid_History.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid_History.DefaultCellStyle = DataGridViewCellStyle19
        Me.Grid_History.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Grid_History.EnableHeadersVisualStyles = False
        Me.Grid_History.Location = New System.Drawing.Point(3, 28)
        Me.Grid_History.Name = "Grid_History"
        Me.Grid_History.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_History.RowHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.Grid_History.RowHeadersVisible = False
        Me.Grid_History.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Grid_History.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Grid_History.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Grid_History.Size = New System.Drawing.Size(1333, 213)
        Me.Grid_History.TabIndex = 97
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Song ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Category"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Title"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Album"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Artists"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 200
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "FileName"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Duration"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 80
        '
        'PanelPromoReplace
        '
        Me.PanelPromoReplace.Controls.Add(Me.txt_Promos_Search)
        Me.PanelPromoReplace.Controls.Add(Me.lbl_Airtime)
        Me.PanelPromoReplace.Controls.Add(Me.lbl_Playlist_Id)
        Me.PanelPromoReplace.Controls.Add(Me.lblPromoId)
        Me.PanelPromoReplace.Controls.Add(Me.lbl_Promo_Display)
        Me.PanelPromoReplace.Controls.Add(Me.lbl_Date_Display)
        Me.PanelPromoReplace.Controls.Add(Me.lbl_Promo)
        Me.PanelPromoReplace.Controls.Add(Me.lbl_Date)
        Me.PanelPromoReplace.Controls.Add(Me.btn_Exit)
        Me.PanelPromoReplace.Controls.Add(Me.btn_ReplacePromo)
        Me.PanelPromoReplace.Controls.Add(Me.Grid_Replace)
        Me.PanelPromoReplace.Location = New System.Drawing.Point(0, 203)
        Me.PanelPromoReplace.Name = "PanelPromoReplace"
        Me.PanelPromoReplace.Size = New System.Drawing.Size(297, 311)
        Me.PanelPromoReplace.TabIndex = 75
        Me.PanelPromoReplace.Visible = False
        '
        'txt_Promos_Search
        '
        Me.txt_Promos_Search.EditValue = "<Search Promo>"
        Me.txt_Promos_Search.Location = New System.Drawing.Point(5, 257)
        Me.txt_Promos_Search.Name = "txt_Promos_Search"
        Me.txt_Promos_Search.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Promos_Search.Properties.Appearance.Options.UseFont = True
        Me.txt_Promos_Search.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Promos_Search.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_Promos_Search.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_Promos_Search.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_Promos_Search.Size = New System.Drawing.Size(176, 22)
        Me.txt_Promos_Search.TabIndex = 77
        '
        'lbl_Airtime
        '
        Me.lbl_Airtime.AutoSize = True
        Me.lbl_Airtime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Airtime.Location = New System.Drawing.Point(65, 237)
        Me.lbl_Airtime.Name = "lbl_Airtime"
        Me.lbl_Airtime.Size = New System.Drawing.Size(45, 15)
        Me.lbl_Airtime.TabIndex = 76
        Me.lbl_Airtime.Text = "Airtime"
        Me.lbl_Airtime.Visible = False
        '
        'lbl_Playlist_Id
        '
        Me.lbl_Playlist_Id.AutoSize = True
        Me.lbl_Playlist_Id.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Playlist_Id.Location = New System.Drawing.Point(5, 260)
        Me.lbl_Playlist_Id.Name = "lbl_Playlist_Id"
        Me.lbl_Playlist_Id.Size = New System.Drawing.Size(55, 15)
        Me.lbl_Playlist_Id.TabIndex = 75
        Me.lbl_Playlist_Id.Text = "PlaylistId"
        Me.lbl_Playlist_Id.Visible = False
        '
        'lblPromoId
        '
        Me.lblPromoId.AutoSize = True
        Me.lblPromoId.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPromoId.Location = New System.Drawing.Point(5, 237)
        Me.lblPromoId.Name = "lblPromoId"
        Me.lblPromoId.Size = New System.Drawing.Size(54, 15)
        Me.lblPromoId.TabIndex = 74
        Me.lblPromoId.Text = "PromoId"
        Me.lblPromoId.Visible = False
        '
        'lbl_Promo_Display
        '
        Me.lbl_Promo_Display.AutoSize = True
        Me.lbl_Promo_Display.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Promo_Display.Location = New System.Drawing.Point(63, 38)
        Me.lbl_Promo_Display.Name = "lbl_Promo_Display"
        Me.lbl_Promo_Display.Size = New System.Drawing.Size(46, 15)
        Me.lbl_Promo_Display.TabIndex = 73
        Me.lbl_Promo_Display.Text = "Zapper"
        '
        'lbl_Date_Display
        '
        Me.lbl_Date_Display.AutoSize = True
        Me.lbl_Date_Display.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Date_Display.Location = New System.Drawing.Point(63, 15)
        Me.lbl_Date_Display.Name = "lbl_Date_Display"
        Me.lbl_Date_Display.Size = New System.Drawing.Size(69, 15)
        Me.lbl_Date_Display.TabIndex = 72
        Me.lbl_Date_Display.Text = "01/09/2012"
        '
        'lbl_Promo
        '
        Me.lbl_Promo.AutoSize = True
        Me.lbl_Promo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Promo.Location = New System.Drawing.Point(10, 38)
        Me.lbl_Promo.Name = "lbl_Promo"
        Me.lbl_Promo.Size = New System.Drawing.Size(47, 15)
        Me.lbl_Promo.TabIndex = 71
        Me.lbl_Promo.Text = "Promo:"
        '
        'lbl_Date
        '
        Me.lbl_Date.AutoSize = True
        Me.lbl_Date.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Date.Location = New System.Drawing.Point(18, 15)
        Me.lbl_Date.Name = "lbl_Date"
        Me.lbl_Date.Size = New System.Drawing.Size(36, 15)
        Me.lbl_Date.TabIndex = 70
        Me.lbl_Date.Text = "Date:"
        '
        'btn_Exit
        '
        Me.btn_Exit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Exit.Appearance.Options.UseFont = True
        Me.btn_Exit.Location = New System.Drawing.Point(187, 281)
        Me.btn_Exit.Name = "btn_Exit"
        Me.btn_Exit.Size = New System.Drawing.Size(104, 23)
        Me.btn_Exit.TabIndex = 32
        Me.btn_Exit.Text = "Exit"
        '
        'btn_ReplacePromo
        '
        Me.btn_ReplacePromo.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ReplacePromo.Appearance.Options.UseFont = True
        Me.btn_ReplacePromo.Location = New System.Drawing.Point(77, 281)
        Me.btn_ReplacePromo.Name = "btn_ReplacePromo"
        Me.btn_ReplacePromo.Size = New System.Drawing.Size(104, 23)
        Me.btn_ReplacePromo.TabIndex = 31
        Me.btn_ReplacePromo.Text = "Replace"
        '
        'Grid_Replace
        '
        Me.Grid_Replace.AllowUserToAddRows = False
        Me.Grid_Replace.AllowUserToDeleteRows = False
        Me.Grid_Replace.AllowUserToResizeRows = False
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.Color.Black
        Me.Grid_Replace.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle21
        Me.Grid_Replace.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Grid_Replace.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Grid_Replace.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Grid_Replace.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_Replace.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.Grid_Replace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid_Replace.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Promo_ID, Me.Promo_Name})
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid_Replace.DefaultCellStyle = DataGridViewCellStyle23
        Me.Grid_Replace.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Grid_Replace.EnableHeadersVisualStyles = False
        Me.Grid_Replace.Location = New System.Drawing.Point(5, 56)
        Me.Grid_Replace.Name = "Grid_Replace"
        Me.Grid_Replace.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_Replace.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.Grid_Replace.RowHeadersVisible = False
        Me.Grid_Replace.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Grid_Replace.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Grid_Replace.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Grid_Replace.Size = New System.Drawing.Size(286, 201)
        Me.Grid_Replace.TabIndex = 1
        '
        'Promo_ID
        '
        Me.Promo_ID.HeaderText = "Promo ID"
        Me.Promo_ID.Name = "Promo_ID"
        Me.Promo_ID.Visible = False
        '
        'Promo_Name
        '
        Me.Promo_Name.HeaderText = "Promo Name"
        Me.Promo_Name.Name = "Promo_Name"
        Me.Promo_Name.Width = 285
        '
        'PanelSongReplace
        '
        Me.PanelSongReplace.Controls.Add(Me.txt_Songs_Search)
        Me.PanelSongReplace.Controls.Add(Me.lbl_ActionType)
        Me.PanelSongReplace.Controls.Add(Me.tvw_Genre_Category)
        Me.PanelSongReplace.Controls.Add(Me.cmb_Genre_Category)
        Me.PanelSongReplace.Controls.Add(Me.lbl_Parent_Category)
        Me.PanelSongReplace.Controls.Add(Me.lbl_song_airtime)
        Me.PanelSongReplace.Controls.Add(Me.lbl_song_playlist_id)
        Me.PanelSongReplace.Controls.Add(Me.lbl_song_Id)
        Me.PanelSongReplace.Controls.Add(Me.lbl_Genre_Category)
        Me.PanelSongReplace.Controls.Add(Me.lbl_Date_Replace)
        Me.PanelSongReplace.Controls.Add(Me.lbl_CurRow)
        Me.PanelSongReplace.Controls.Add(Me.btn_Song_Replace_Exit)
        Me.PanelSongReplace.Controls.Add(Me.btn_Song_Replace)
        Me.PanelSongReplace.Controls.Add(Me.Grid_Song_Replace)
        Me.PanelSongReplace.Location = New System.Drawing.Point(234, 203)
        Me.PanelSongReplace.Name = "PanelSongReplace"
        Me.PanelSongReplace.Size = New System.Drawing.Size(873, 311)
        Me.PanelSongReplace.TabIndex = 76
        Me.PanelSongReplace.Visible = False
        '
        'txt_Songs_Search
        '
        Me.txt_Songs_Search.EditValue = "<Search Song ID>"
        Me.txt_Songs_Search.Location = New System.Drawing.Point(276, 15)
        Me.txt_Songs_Search.Name = "txt_Songs_Search"
        Me.txt_Songs_Search.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Songs_Search.Properties.Appearance.Options.UseFont = True
        Me.txt_Songs_Search.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Songs_Search.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_Songs_Search.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_Songs_Search.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_Songs_Search.Size = New System.Drawing.Size(180, 22)
        Me.txt_Songs_Search.TabIndex = 96
        '
        'lbl_ActionType
        '
        Me.lbl_ActionType.AutoSize = True
        Me.lbl_ActionType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ActionType.Location = New System.Drawing.Point(531, 289)
        Me.lbl_ActionType.Name = "lbl_ActionType"
        Me.lbl_ActionType.Size = New System.Drawing.Size(36, 15)
        Me.lbl_ActionType.TabIndex = 95
        Me.lbl_ActionType.Text = "Date:"
        Me.lbl_ActionType.Visible = False
        '
        'tvw_Genre_Category
        '
        Me.tvw_Genre_Category.BackColor = System.Drawing.Color.DimGray
        Me.tvw_Genre_Category.CheckBoxes = True
        Me.tvw_Genre_Category.ForeColor = System.Drawing.Color.White
        Me.tvw_Genre_Category.LineColor = System.Drawing.Color.White
        Me.tvw_Genre_Category.Location = New System.Drawing.Point(70, 38)
        Me.tvw_Genre_Category.Name = "tvw_Genre_Category"
        Me.tvw_Genre_Category.Size = New System.Drawing.Size(200, 235)
        Me.tvw_Genre_Category.TabIndex = 94
        Me.tvw_Genre_Category.Visible = False
        '
        'cmb_Genre_Category
        '
        Me.cmb_Genre_Category.EditValue = ""
        Me.cmb_Genre_Category.Location = New System.Drawing.Point(70, 15)
        Me.cmb_Genre_Category.Name = "cmb_Genre_Category"
        Me.cmb_Genre_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Genre_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Genre_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Genre_Category.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cmb_Genre_Category.Size = New System.Drawing.Size(200, 22)
        Me.cmb_Genre_Category.TabIndex = 93
        '
        'lbl_Parent_Category
        '
        Me.lbl_Parent_Category.AutoSize = True
        Me.lbl_Parent_Category.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Parent_Category.Location = New System.Drawing.Point(10, 16)
        Me.lbl_Parent_Category.Name = "lbl_Parent_Category"
        Me.lbl_Parent_Category.Size = New System.Drawing.Size(58, 15)
        Me.lbl_Parent_Category.TabIndex = 78
        Me.lbl_Parent_Category.Text = "Category:"
        '
        'lbl_song_airtime
        '
        Me.lbl_song_airtime.AutoSize = True
        Me.lbl_song_airtime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_song_airtime.Location = New System.Drawing.Point(145, 289)
        Me.lbl_song_airtime.Name = "lbl_song_airtime"
        Me.lbl_song_airtime.Size = New System.Drawing.Size(45, 15)
        Me.lbl_song_airtime.TabIndex = 76
        Me.lbl_song_airtime.Text = "Airtime"
        Me.lbl_song_airtime.Visible = False
        '
        'lbl_song_playlist_id
        '
        Me.lbl_song_playlist_id.AutoSize = True
        Me.lbl_song_playlist_id.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_song_playlist_id.Location = New System.Drawing.Point(5, 289)
        Me.lbl_song_playlist_id.Name = "lbl_song_playlist_id"
        Me.lbl_song_playlist_id.Size = New System.Drawing.Size(55, 15)
        Me.lbl_song_playlist_id.TabIndex = 75
        Me.lbl_song_playlist_id.Text = "PlaylistId"
        Me.lbl_song_playlist_id.Visible = False
        '
        'lbl_song_Id
        '
        Me.lbl_song_Id.AutoSize = True
        Me.lbl_song_Id.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_song_Id.Location = New System.Drawing.Point(73, 289)
        Me.lbl_song_Id.Name = "lbl_song_Id"
        Me.lbl_song_Id.Size = New System.Drawing.Size(54, 15)
        Me.lbl_song_Id.TabIndex = 74
        Me.lbl_song_Id.Text = "PromoId"
        Me.lbl_song_Id.Visible = False
        '
        'lbl_Genre_Category
        '
        Me.lbl_Genre_Category.AutoSize = True
        Me.lbl_Genre_Category.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Genre_Category.Location = New System.Drawing.Point(327, 289)
        Me.lbl_Genre_Category.Name = "lbl_Genre_Category"
        Me.lbl_Genre_Category.Size = New System.Drawing.Size(46, 15)
        Me.lbl_Genre_Category.TabIndex = 73
        Me.lbl_Genre_Category.Text = "Zapper"
        Me.lbl_Genre_Category.Visible = False
        '
        'lbl_Date_Replace
        '
        Me.lbl_Date_Replace.AutoSize = True
        Me.lbl_Date_Replace.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Date_Replace.Location = New System.Drawing.Point(432, 289)
        Me.lbl_Date_Replace.Name = "lbl_Date_Replace"
        Me.lbl_Date_Replace.Size = New System.Drawing.Size(69, 15)
        Me.lbl_Date_Replace.TabIndex = 72
        Me.lbl_Date_Replace.Text = "01/09/2012"
        Me.lbl_Date_Replace.Visible = False
        '
        'lbl_CurRow
        '
        Me.lbl_CurRow.AutoSize = True
        Me.lbl_CurRow.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_CurRow.Location = New System.Drawing.Point(387, 289)
        Me.lbl_CurRow.Name = "lbl_CurRow"
        Me.lbl_CurRow.Size = New System.Drawing.Size(36, 15)
        Me.lbl_CurRow.TabIndex = 70
        Me.lbl_CurRow.Text = "Date:"
        Me.lbl_CurRow.Visible = False
        '
        'btn_Song_Replace_Exit
        '
        Me.btn_Song_Replace_Exit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Song_Replace_Exit.Appearance.Options.UseFont = True
        Me.btn_Song_Replace_Exit.Location = New System.Drawing.Point(761, 281)
        Me.btn_Song_Replace_Exit.Name = "btn_Song_Replace_Exit"
        Me.btn_Song_Replace_Exit.Size = New System.Drawing.Size(104, 23)
        Me.btn_Song_Replace_Exit.TabIndex = 32
        Me.btn_Song_Replace_Exit.Text = "Exit"
        '
        'btn_Song_Replace
        '
        Me.btn_Song_Replace.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Song_Replace.Appearance.Options.UseFont = True
        Me.btn_Song_Replace.Location = New System.Drawing.Point(651, 281)
        Me.btn_Song_Replace.Name = "btn_Song_Replace"
        Me.btn_Song_Replace.Size = New System.Drawing.Size(104, 23)
        Me.btn_Song_Replace.TabIndex = 31
        Me.btn_Song_Replace.Text = "Replace"
        '
        'Grid_Song_Replace
        '
        Me.Grid_Song_Replace.AllowUserToAddRows = False
        Me.Grid_Song_Replace.AllowUserToDeleteRows = False
        Me.Grid_Song_Replace.AllowUserToResizeRows = False
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black
        Me.Grid_Song_Replace.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle25
        Me.Grid_Song_Replace.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Grid_Song_Replace.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Grid_Song_Replace.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Grid_Song_Replace.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle26.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_Song_Replace.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle26
        Me.Grid_Song_Replace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid_Song_Replace.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Replace_Song_Id, Me.Replace_Song_Category, Me.Replace_Song_Title, Me.Replace_Song_Album, Me.Replace_Song_Artists, Me.Replace_Song_FileName, Me.Replace_Song_Duration})
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle27.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid_Song_Replace.DefaultCellStyle = DataGridViewCellStyle27
        Me.Grid_Song_Replace.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Grid_Song_Replace.EnableHeadersVisualStyles = False
        Me.Grid_Song_Replace.Location = New System.Drawing.Point(5, 56)
        Me.Grid_Song_Replace.Name = "Grid_Song_Replace"
        Me.Grid_Song_Replace.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_Song_Replace.RowHeadersDefaultCellStyle = DataGridViewCellStyle28
        Me.Grid_Song_Replace.RowHeadersVisible = False
        Me.Grid_Song_Replace.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Grid_Song_Replace.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Grid_Song_Replace.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Grid_Song_Replace.Size = New System.Drawing.Size(860, 219)
        Me.Grid_Song_Replace.TabIndex = 1
        '
        'Replace_Song_Id
        '
        Me.Replace_Song_Id.HeaderText = "Song ID"
        Me.Replace_Song_Id.Name = "Replace_Song_Id"
        Me.Replace_Song_Id.Width = 80
        '
        'Replace_Song_Category
        '
        Me.Replace_Song_Category.HeaderText = "Category"
        Me.Replace_Song_Category.Name = "Replace_Song_Category"
        Me.Replace_Song_Category.Width = 80
        '
        'Replace_Song_Title
        '
        Me.Replace_Song_Title.HeaderText = "Title"
        Me.Replace_Song_Title.Name = "Replace_Song_Title"
        Me.Replace_Song_Title.Width = 200
        '
        'Replace_Song_Album
        '
        Me.Replace_Song_Album.HeaderText = "Album"
        Me.Replace_Song_Album.Name = "Replace_Song_Album"
        Me.Replace_Song_Album.Width = 200
        '
        'Replace_Song_Artists
        '
        Me.Replace_Song_Artists.HeaderText = "Artists"
        Me.Replace_Song_Artists.Name = "Replace_Song_Artists"
        Me.Replace_Song_Artists.Width = 200
        '
        'Replace_Song_FileName
        '
        Me.Replace_Song_FileName.HeaderText = "FileName"
        Me.Replace_Song_FileName.Name = "Replace_Song_FileName"
        Me.Replace_Song_FileName.Visible = False
        '
        'Replace_Song_Duration
        '
        Me.Replace_Song_Duration.HeaderText = "Duration"
        Me.Replace_Song_Duration.Name = "Replace_Song_Duration"
        Me.Replace_Song_Duration.Width = 80
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelControl1.AutoEllipsis = True
        Me.LabelControl1.Location = New System.Drawing.Point(13, 76)
        Me.LabelControl1.LookAndFeel.SkinName = "Office 2010 Blue"
        Me.LabelControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(750, 13)
        Me.LabelControl1.TabIndex = 95
        Me.LabelControl1.Text = "12 A    1 A    2 A     3 A      4 A     5 A      6 A     7 A     8 A     9 A     " & _
    "10 A    11 A   12 P    1 P      2 P     3 P      4 P     5 P      6 P      7 P  " & _
    "    8 P      9 P    10 P   11 P"
        '
        'HourBar
        '
        Me.HourBar.LargeChange = 1
        Me.HourBar.Location = New System.Drawing.Point(8, 47)
        Me.HourBar.Maximum = 23
        Me.HourBar.Name = "HourBar"
        Me.HourBar.Size = New System.Drawing.Size(754, 45)
        Me.HourBar.TabIndex = 78
        '
        'SongsGrid
        '
        Me.SongsGrid.AllowUserToAddRows = False
        Me.SongsGrid.AllowUserToDeleteRows = False
        Me.SongsGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black
        Me.SongsGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle29
        Me.SongsGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SongsGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.SongsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SongsGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.SongsGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle30.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SongsGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle30
        Me.SongsGrid.ColumnHeadersHeight = 25
        Me.SongsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.SongsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Song_ID, Me.Category, Me.Title, Me.Album, Me.Artist, Me.FileName, Me.Duration})
        Me.SongsGrid.ContextMenuStrip = Me.PopUpMenu
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle31.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SongsGrid.DefaultCellStyle = DataGridViewCellStyle31
        Me.SongsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.SongsGrid.EnableHeadersVisualStyles = False
        Me.SongsGrid.Location = New System.Drawing.Point(7, 94)
        Me.SongsGrid.MultiSelect = False
        Me.SongsGrid.Name = "SongsGrid"
        Me.SongsGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle32.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SongsGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle32
        Me.SongsGrid.RowHeadersVisible = False
        Me.SongsGrid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.SongsGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.SongsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.SongsGrid.Size = New System.Drawing.Size(1339, 552)
        Me.SongsGrid.TabIndex = 52
        '
        'Song_ID
        '
        Me.Song_ID.HeaderText = "Song ID"
        Me.Song_ID.Name = "Song_ID"
        Me.Song_ID.Width = 80
        '
        'Category
        '
        Me.Category.HeaderText = "Category"
        Me.Category.Name = "Category"
        '
        'Title
        '
        Me.Title.HeaderText = "Title"
        Me.Title.Name = "Title"
        Me.Title.Width = 300
        '
        'Album
        '
        Me.Album.HeaderText = "Album"
        Me.Album.Name = "Album"
        Me.Album.Width = 300
        '
        'Artist
        '
        Me.Artist.HeaderText = "Artist"
        Me.Artist.Name = "Artist"
        Me.Artist.Width = 300
        '
        'FileName
        '
        Me.FileName.HeaderText = "File Name"
        Me.FileName.Name = "FileName"
        Me.FileName.Width = 180
        '
        'Duration
        '
        Me.Duration.HeaderText = "Duration"
        Me.Duration.Name = "Duration"
        '
        'PopUpMenu
        '
        Me.PopUpMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReplacePromoMenuItem, Me.ReplaceSongMenuItem, Me.InsertSongMenuItem, Me.ToolStripSeparator1, Me.CutMenuItem, Me.CopyMenuItem, Me.PasteMenuItem})
        Me.PopUpMenu.Name = "PopUpMenu"
        Me.PopUpMenu.Size = New System.Drawing.Size(155, 142)
        '
        'ReplacePromoMenuItem
        '
        Me.ReplacePromoMenuItem.Name = "ReplacePromoMenuItem"
        Me.ReplacePromoMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.ReplacePromoMenuItem.Text = "Replace Promo"
        Me.ReplacePromoMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ReplaceSongMenuItem
        '
        Me.ReplaceSongMenuItem.Name = "ReplaceSongMenuItem"
        Me.ReplaceSongMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.ReplaceSongMenuItem.Text = "Replace Song"
        '
        'InsertSongMenuItem
        '
        Me.InsertSongMenuItem.Name = "InsertSongMenuItem"
        Me.InsertSongMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.InsertSongMenuItem.Text = "Insert Song"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(151, 6)
        '
        'CutMenuItem
        '
        Me.CutMenuItem.Name = "CutMenuItem"
        Me.CutMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.CutMenuItem.Text = "Cut"
        '
        'CopyMenuItem
        '
        Me.CopyMenuItem.Name = "CopyMenuItem"
        Me.CopyMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.CopyMenuItem.Text = "Copy"
        '
        'PasteMenuItem
        '
        Me.PasteMenuItem.Name = "PasteMenuItem"
        Me.PasteMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.PasteMenuItem.Text = "Paste"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(571, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 15)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "Date:"
        '
        'Dt_playlist_date
        '
        Me.Dt_playlist_date.EditValue = Nothing
        Me.Dt_playlist_date.Location = New System.Drawing.Point(616, 22)
        Me.Dt_playlist_date.Name = "Dt_playlist_date"
        Me.Dt_playlist_date.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dt_playlist_date.Properties.Appearance.Options.UseFont = True
        Me.Dt_playlist_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Dt_playlist_date.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.Dt_playlist_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.Dt_playlist_date.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.Dt_playlist_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.Dt_playlist_date.Size = New System.Drawing.Size(146, 22)
        Me.Dt_playlist_date.TabIndex = 71
        '
        'cmb_Store
        '
        Me.cmb_Store.EditValue = ""
        Me.cmb_Store.Location = New System.Drawing.Point(364, 22)
        Me.cmb_Store.Name = "cmb_Store"
        Me.cmb_Store.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Store.Properties.Appearance.Options.UseFont = True
        Me.cmb_Store.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Store.Properties.NullText = ""
        Me.cmb_Store.Size = New System.Drawing.Size(181, 22)
        Me.cmb_Store.TabIndex = 70
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(322, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 15)
        Me.Label7.TabIndex = 69
        Me.Label7.Text = "Store:"
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(70, 22)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.cmb_Customer.Properties.NullText = ""
        Me.cmb_Customer.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Customer.TabIndex = 67
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "Customer:"
        '
        'btn_Save
        '
        Me.btn_Save.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Save.Appearance.Options.UseFont = True
        Me.btn_Save.Location = New System.Drawing.Point(1245, 17)
        Me.btn_Save.Name = "btn_Save"
        Me.btn_Save.Size = New System.Drawing.Size(104, 23)
        Me.btn_Save.TabIndex = 30
        Me.btn_Save.Text = "Save"
        Me.btn_Save.Visible = False
        '
        'Library_Tab_Control
        '
        Me.Library_Tab_Control.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Library_Tab_Control.Appearance.Options.UseFont = True
        Me.Library_Tab_Control.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Library_Tab_Control.Location = New System.Drawing.Point(0, -13)
        Me.Library_Tab_Control.Name = "Library_Tab_Control"
        Me.Library_Tab_Control.SelectedTabPage = Me.XtraTabPage1
        Me.Library_Tab_Control.Size = New System.Drawing.Size(1368, 710)
        Me.Library_Tab_Control.TabIndex = 5
        Me.Library_Tab_Control.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'TmrPlayState
        '
        '
        'frmPlayList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1362, 694)
        Me.Controls.Add(Me.Library_Tab_Control)
        Me.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.Name = "frmPlayList"
        Me.Text = "Playlist Editor"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.Grp_History, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Grp_History.ResumeLayout(False)
        CType(Me.Grid_History, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelPromoReplace, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelPromoReplace.ResumeLayout(False)
        Me.PanelPromoReplace.PerformLayout()
        CType(Me.txt_Promos_Search.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid_Replace, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelSongReplace, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelSongReplace.ResumeLayout(False)
        Me.PanelSongReplace.PerformLayout()
        CType(Me.txt_Songs_Search.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Genre_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid_Song_Replace, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HourBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SongsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopUpMenu.ResumeLayout(False)
        CType(Me.Dt_playlist_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dt_playlist_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Library_Tab_Control, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Library_Tab_Control.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cmb_Store As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents SongsGrid As System.Windows.Forms.DataGridView
    Friend WithEvents btn_Save As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Library_Tab_Control As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Dt_playlist_date As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PopUpMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ReplacePromoMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PanelPromoReplace As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_ReplacePromo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Grid_Replace As System.Windows.Forms.DataGridView
    Friend WithEvents Promo_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn_Exit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblPromoId As System.Windows.Forms.Label
    Friend WithEvents lbl_Promo_Display As System.Windows.Forms.Label
    Friend WithEvents lbl_Date_Display As System.Windows.Forms.Label
    Friend WithEvents lbl_Promo As System.Windows.Forms.Label
    Friend WithEvents lbl_Date As System.Windows.Forms.Label
    Friend WithEvents lbl_Airtime As System.Windows.Forms.Label
    Friend WithEvents lbl_Playlist_Id As System.Windows.Forms.Label
    Friend WithEvents TmrPlayState As System.Windows.Forms.Timer
    Friend WithEvents Song_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Album As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Artist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FileName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Duration As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReplaceSongMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InsertSongMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PanelSongReplace As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lbl_song_airtime As System.Windows.Forms.Label
    Friend WithEvents lbl_song_playlist_id As System.Windows.Forms.Label
    Friend WithEvents lbl_song_Id As System.Windows.Forms.Label
    Friend WithEvents lbl_Genre_Category As System.Windows.Forms.Label
    Friend WithEvents lbl_Date_Replace As System.Windows.Forms.Label
    Friend WithEvents lbl_CurRow As System.Windows.Forms.Label
    Friend WithEvents btn_Song_Replace_Exit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Song_Replace As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Grid_Song_Replace As System.Windows.Forms.DataGridView
    Friend WithEvents lbl_Parent_Category As System.Windows.Forms.Label
    Friend WithEvents tvw_Genre_Category As System.Windows.Forms.TreeView
    Friend WithEvents cmb_Genre_Category As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Replace_Song_Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Replace_Song_Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Replace_Song_Title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Replace_Song_Album As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Replace_Song_Artists As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Replace_Song_FileName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Replace_Song_Duration As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lbl_ActionType As System.Windows.Forms.Label
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HourBar As System.Windows.Forms.TrackBar
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt_Promos_Search As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Songs_Search As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Grp_History As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Grid_History As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn_Close As DevExpress.XtraEditors.SimpleButton
End Class
