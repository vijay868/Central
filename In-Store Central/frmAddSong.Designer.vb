﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddSong
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.BrowseFile = New System.Windows.Forms.OpenFileDialog()
        Me.Genre_Panel = New DevExpress.XtraEditors.PanelControl()
        Me.lbl_Duration = New System.Windows.Forms.Label()
        Me.cmb_Normalization = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmb_Category = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_Artist2 = New DevExpress.XtraEditors.TextEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_Artist1 = New DevExpress.XtraEditors.TextEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_Title = New DevExpress.XtraEditors.TextEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbl_Status = New System.Windows.Forms.Label()
        Me.ConversionBar = New DevExpress.XtraEditors.MarqueeProgressBarControl()
        Me.lbl_form_title = New System.Windows.Forms.Label()
        Me.txt_AudioPath = New DevExpress.XtraEditors.TextEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Album = New DevExpress.XtraEditors.TextEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmb_Genre = New DevExpress.XtraEditors.LookUpEdit()
        Me.btn_Browse = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_SongID = New DevExpress.XtraEditors.TextEdit()
        Me.btn_Quit = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Add = New DevExpress.XtraEditors.SimpleButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Genre_Panel.SuspendLayout()
        CType(Me.cmb_Normalization.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Artist2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Artist1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Title.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConversionBar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_AudioPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Album.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Genre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_SongID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Genre_Panel
        '
        Me.Genre_Panel.Controls.Add(Me.lbl_Duration)
        Me.Genre_Panel.Controls.Add(Me.cmb_Normalization)
        Me.Genre_Panel.Controls.Add(Me.Label5)
        Me.Genre_Panel.Controls.Add(Me.cmb_Category)
        Me.Genre_Panel.Controls.Add(Me.Label9)
        Me.Genre_Panel.Controls.Add(Me.txt_Artist2)
        Me.Genre_Panel.Controls.Add(Me.Label8)
        Me.Genre_Panel.Controls.Add(Me.txt_Artist1)
        Me.Genre_Panel.Controls.Add(Me.Label7)
        Me.Genre_Panel.Controls.Add(Me.txt_Title)
        Me.Genre_Panel.Controls.Add(Me.Label6)
        Me.Genre_Panel.Controls.Add(Me.lbl_Status)
        Me.Genre_Panel.Controls.Add(Me.ConversionBar)
        Me.Genre_Panel.Controls.Add(Me.lbl_form_title)
        Me.Genre_Panel.Controls.Add(Me.txt_AudioPath)
        Me.Genre_Panel.Controls.Add(Me.Label4)
        Me.Genre_Panel.Controls.Add(Me.txt_Album)
        Me.Genre_Panel.Controls.Add(Me.Label3)
        Me.Genre_Panel.Controls.Add(Me.cmb_Genre)
        Me.Genre_Panel.Controls.Add(Me.btn_Browse)
        Me.Genre_Panel.Controls.Add(Me.txt_SongID)
        Me.Genre_Panel.Controls.Add(Me.btn_Quit)
        Me.Genre_Panel.Controls.Add(Me.btn_Add)
        Me.Genre_Panel.Controls.Add(Me.Label2)
        Me.Genre_Panel.Controls.Add(Me.Label1)
        Me.Genre_Panel.Location = New System.Drawing.Point(7, 9)
        Me.Genre_Panel.Name = "Genre_Panel"
        Me.Genre_Panel.Size = New System.Drawing.Size(564, 318)
        Me.Genre_Panel.TabIndex = 58
        '
        'lbl_Duration
        '
        Me.lbl_Duration.AutoSize = True
        Me.lbl_Duration.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Duration.Location = New System.Drawing.Point(383, 141)
        Me.lbl_Duration.Name = "lbl_Duration"
        Me.lbl_Duration.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Duration.TabIndex = 79
        Me.lbl_Duration.Text = "Status:"
        Me.lbl_Duration.Visible = False
        '
        'cmb_Normalization
        '
        Me.cmb_Normalization.EditValue = ""
        Me.cmb_Normalization.Location = New System.Drawing.Point(414, 74)
        Me.cmb_Normalization.Name = "cmb_Normalization"
        Me.cmb_Normalization.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Normalization.Properties.Appearance.Options.UseFont = True
        Me.cmb_Normalization.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Normalization.Properties.NullText = ""
        Me.cmb_Normalization.Size = New System.Drawing.Size(125, 22)
        Me.cmb_Normalization.TabIndex = 77
        Me.cmb_Normalization.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(326, 77)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 15)
        Me.Label5.TabIndex = 78
        Me.Label5.Text = "Normalization:"
        Me.Label5.Visible = False
        '
        'cmb_Category
        '
        Me.cmb_Category.EditValue = ""
        Me.cmb_Category.Location = New System.Drawing.Point(414, 44)
        Me.cmb_Category.Name = "cmb_Category"
        Me.cmb_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Category.Properties.NullText = ""
        Me.cmb_Category.Size = New System.Drawing.Size(125, 22)
        Me.cmb_Category.TabIndex = 75
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(353, 47)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 15)
        Me.Label9.TabIndex = 76
        Me.Label9.Text = "Category:"
        '
        'txt_Artist2
        '
        Me.txt_Artist2.EditValue = ""
        Me.txt_Artist2.Location = New System.Drawing.Point(90, 191)
        Me.txt_Artist2.Name = "txt_Artist2"
        Me.txt_Artist2.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Artist2.Properties.Appearance.Options.UseFont = True
        Me.txt_Artist2.Size = New System.Drawing.Size(232, 22)
        Me.txt_Artist2.TabIndex = 63
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(38, 194)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 15)
        Me.Label8.TabIndex = 74
        Me.Label8.Text = "Artist 2:"
        '
        'txt_Artist1
        '
        Me.txt_Artist1.EditValue = ""
        Me.txt_Artist1.Location = New System.Drawing.Point(90, 162)
        Me.txt_Artist1.Name = "txt_Artist1"
        Me.txt_Artist1.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Artist1.Properties.Appearance.Options.UseFont = True
        Me.txt_Artist1.Size = New System.Drawing.Size(232, 22)
        Me.txt_Artist1.TabIndex = 62
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(38, 165)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 15)
        Me.Label7.TabIndex = 68
        Me.Label7.Text = "Artist 1:"
        '
        'txt_Title
        '
        Me.txt_Title.EditValue = ""
        Me.txt_Title.Location = New System.Drawing.Point(90, 134)
        Me.txt_Title.Name = "txt_Title"
        Me.txt_Title.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Title.Properties.Appearance.Options.UseFont = True
        Me.txt_Title.Size = New System.Drawing.Size(232, 22)
        Me.txt_Title.TabIndex = 61
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(51, 137)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 15)
        Me.Label6.TabIndex = 72
        Me.Label6.Text = "Title:"
        '
        'lbl_Status
        '
        Me.lbl_Status.AutoSize = True
        Me.lbl_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Status.Location = New System.Drawing.Point(40, 287)
        Me.lbl_Status.Name = "lbl_Status"
        Me.lbl_Status.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Status.TabIndex = 66
        Me.lbl_Status.Text = "Status:"
        '
        'ConversionBar
        '
        Me.ConversionBar.EditValue = 0
        Me.ConversionBar.Location = New System.Drawing.Point(319, 287)
        Me.ConversionBar.Name = "ConversionBar"
        Me.ConversionBar.Properties.ProgressAnimationMode = DevExpress.Utils.Drawing.ProgressAnimationMode.Cycle
        Me.ConversionBar.Size = New System.Drawing.Size(224, 21)
        Me.ConversionBar.TabIndex = 70
        Me.ConversionBar.Visible = False
        '
        'lbl_form_title
        '
        Me.lbl_form_title.AutoSize = True
        Me.lbl_form_title.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_form_title.Location = New System.Drawing.Point(222, 13)
        Me.lbl_form_title.Name = "lbl_form_title"
        Me.lbl_form_title.Size = New System.Drawing.Size(100, 15)
        Me.lbl_form_title.TabIndex = 69
        Me.lbl_form_title.Text = "Add New Song"
        '
        'txt_AudioPath
        '
        Me.txt_AudioPath.EditValue = ""
        Me.txt_AudioPath.Location = New System.Drawing.Point(90, 223)
        Me.txt_AudioPath.Name = "txt_AudioPath"
        Me.txt_AudioPath.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_AudioPath.Properties.Appearance.Options.UseFont = True
        Me.txt_AudioPath.Size = New System.Drawing.Size(411, 22)
        Me.txt_AudioPath.TabIndex = 64
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(17, 226)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 15)
        Me.Label4.TabIndex = 68
        Me.Label4.Text = "Select File:"
        '
        'txt_Album
        '
        Me.txt_Album.EditValue = ""
        Me.txt_Album.Location = New System.Drawing.Point(90, 106)
        Me.txt_Album.Name = "txt_Album"
        Me.txt_Album.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Album.Properties.Appearance.Options.UseFont = True
        Me.txt_Album.Size = New System.Drawing.Size(232, 22)
        Me.txt_Album.TabIndex = 60
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(39, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 15)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "Album:"
        '
        'cmb_Genre
        '
        Me.cmb_Genre.EditValue = ""
        Me.cmb_Genre.Location = New System.Drawing.Point(90, 44)
        Me.cmb_Genre.Name = "cmb_Genre"
        Me.cmb_Genre.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Genre.Properties.Appearance.Options.UseFont = True
        Me.cmb_Genre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Genre.Properties.NullText = ""
        Me.cmb_Genre.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Genre.TabIndex = 58
        '
        'btn_Browse
        '
        Me.btn_Browse.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Browse.Appearance.Options.UseFont = True
        Me.btn_Browse.Location = New System.Drawing.Point(503, 222)
        Me.btn_Browse.Name = "btn_Browse"
        Me.btn_Browse.Size = New System.Drawing.Size(36, 22)
        Me.btn_Browse.TabIndex = 65
        Me.btn_Browse.Text = "..."
        '
        'txt_SongID
        '
        Me.txt_SongID.EditValue = ""
        Me.txt_SongID.Location = New System.Drawing.Point(90, 74)
        Me.txt_SongID.Name = "txt_SongID"
        Me.txt_SongID.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_SongID.Properties.Appearance.Options.UseFont = True
        Me.txt_SongID.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_SongID.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_SongID.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_SongID.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_SongID.Size = New System.Drawing.Size(86, 22)
        Me.txt_SongID.TabIndex = 59
        '
        'btn_Quit
        '
        Me.btn_Quit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Quit.Appearance.Options.UseFont = True
        Me.btn_Quit.Location = New System.Drawing.Point(434, 256)
        Me.btn_Quit.Name = "btn_Quit"
        Me.btn_Quit.Size = New System.Drawing.Size(108, 23)
        Me.btn_Quit.TabIndex = 67
        Me.btn_Quit.Text = "Exit"
        '
        'btn_Add
        '
        Me.btn_Add.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Add.Appearance.Options.UseFont = True
        Me.btn_Add.Location = New System.Drawing.Point(319, 255)
        Me.btn_Add.Name = "btn_Add"
        Me.btn_Add.Size = New System.Drawing.Size(108, 23)
        Me.btn_Add.TabIndex = 66
        Me.btn_Add.Text = "Add"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(30, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 15)
        Me.Label2.TabIndex = 64
        Me.Label2.Text = "Song ID:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(40, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 15)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "Genre:"
        '
        'frmAddSong
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(576, 332)
        Me.Controls.Add(Me.Genre_Panel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmAddSong"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Song"
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Genre_Panel.ResumeLayout(False)
        Me.Genre_Panel.PerformLayout()
        CType(Me.cmb_Normalization.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Artist2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Artist1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Title.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConversionBar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_AudioPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Album.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Genre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_SongID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents BrowseFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Genre_Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmb_Genre As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btn_Browse As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_SongID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_Quit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Add As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lbl_form_title As System.Windows.Forms.Label
    Friend WithEvents txt_AudioPath As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_Album As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ConversionBar As DevExpress.XtraEditors.MarqueeProgressBarControl
    Friend WithEvents txt_Artist2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_Artist1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_Title As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmb_Category As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmb_Normalization As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lbl_Duration As System.Windows.Forms.Label
    Friend WithEvents lbl_Status As System.Windows.Forms.Label
End Class
