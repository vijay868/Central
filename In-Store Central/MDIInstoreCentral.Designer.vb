﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MDIInstoreCentral
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDIInstoreCentral))
        Dim ColorPack1 As gTrackBar.ColorPack = New gTrackBar.ColorPack()
        Dim ColorPack2 As gTrackBar.ColorPack = New gTrackBar.ColorPack()
        Dim ColorPack3 As gTrackBar.ColorPack = New gTrackBar.ColorPack()
        Dim ColorLinearGradient1 As gTrackBar.ColorLinearGradient = New gTrackBar.ColorLinearGradient()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.PlayerPanel = New System.Windows.Forms.Panel()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lbl_curtime = New DevExpress.XtraEditors.LabelControl()
        Me.lbl_TotalTime = New DevExpress.XtraEditors.LabelControl()
        Me.MediaTrackBar = New gTrackBar.gTrackBar()
        Me.GlassButton6 = New Glass.GlassButton()
        Me.GlassButton5 = New Glass.GlassButton()
        Me.GlassButton4 = New Glass.GlassButton()
        Me.GlassButton3 = New Glass.GlassButton()
        Me.GlassButton2 = New Glass.GlassButton()
        Me.GlassButton7 = New Glass.GlassButton()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ImgPlayIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.PlayerPanel.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "glossy (8x38)_v2.png")
        Me.ImageList1.Images.SetKeyName(1, "glossy.png")
        '
        'PlayerPanel
        '
        Me.PlayerPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PlayerPanel.BackColor = System.Drawing.Color.Transparent
        Me.PlayerPanel.BackgroundImage = Global.INSCentral.My.Resources.Resources.glossy
        Me.PlayerPanel.Controls.Add(Me.Button6)
        Me.PlayerPanel.Controls.Add(Me.Button5)
        Me.PlayerPanel.Controls.Add(Me.Button4)
        Me.PlayerPanel.Controls.Add(Me.Button3)
        Me.PlayerPanel.Controls.Add(Me.Button2)
        Me.PlayerPanel.Controls.Add(Me.Button1)
        Me.PlayerPanel.Controls.Add(Me.lbl_curtime)
        Me.PlayerPanel.Controls.Add(Me.lbl_TotalTime)
        Me.PlayerPanel.Controls.Add(Me.MediaTrackBar)
        Me.PlayerPanel.Location = New System.Drawing.Point(669, 0)
        Me.PlayerPanel.Name = "PlayerPanel"
        Me.PlayerPanel.Size = New System.Drawing.Size(383, 34)
        Me.PlayerPanel.TabIndex = 28
        Me.PlayerPanel.Visible = False
        '
        'Button6
        '
        Me.Button6.FlatAppearance.BorderSize = 0
        Me.Button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.Button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Image = Global.INSCentral.My.Resources.Resources.Actions_media_playback_stop_icon
        Me.Button6.Location = New System.Drawing.Point(38, 1)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(28, 28)
        Me.Button6.TabIndex = 36
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.FlatAppearance.BorderSize = 0
        Me.Button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.Button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Image = Global.INSCentral.My.Resources.Resources.Actions_media_seek_backward_icon
        Me.Button5.Location = New System.Drawing.Point(18, 9)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(18, 18)
        Me.Button5.TabIndex = 35
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.Button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Image = Global.INSCentral.My.Resources.Resources.Actions_media_skip_backward_icon
        Me.Button4.Location = New System.Drawing.Point(136, 8)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(18, 18)
        Me.Button4.TabIndex = 34
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.Button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Image = Global.INSCentral.My.Resources.Resources.Actions_media_skip_forward_icon
        Me.Button3.Location = New System.Drawing.Point(355, 8)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(18, 18)
        Me.Button3.TabIndex = 33
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Image = Global.INSCentral.My.Resources.Resources.Actions_media_seek_forward_icon
        Me.Button2.Location = New System.Drawing.Point(93, 8)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(18, 18)
        Me.Button2.TabIndex = 32
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Image = Global.INSCentral.My.Resources.Resources.Actions_media_playback_start_icon
        Me.Button1.Location = New System.Drawing.Point(66, 1)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(28, 28)
        Me.Button1.TabIndex = 31
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lbl_curtime
        '
        Me.lbl_curtime.Appearance.ForeColor = System.Drawing.Color.White
        Me.lbl_curtime.Location = New System.Drawing.Point(160, 0)
        Me.lbl_curtime.Name = "lbl_curtime"
        Me.lbl_curtime.Size = New System.Drawing.Size(0, 13)
        Me.lbl_curtime.TabIndex = 30
        '
        'lbl_TotalTime
        '
        Me.lbl_TotalTime.Appearance.ForeColor = System.Drawing.Color.White
        Me.lbl_TotalTime.Location = New System.Drawing.Point(324, 1)
        Me.lbl_TotalTime.Name = "lbl_TotalTime"
        Me.lbl_TotalTime.Size = New System.Drawing.Size(0, 13)
        Me.lbl_TotalTime.TabIndex = 29
        '
        'MediaTrackBar
        '
        Me.MediaTrackBar.BackColor = System.Drawing.Color.Transparent
        ColorPack1.Border = System.Drawing.Color.Black
        ColorPack1.Face = System.Drawing.Color.Gray
        ColorPack1.Highlight = System.Drawing.Color.Gray
        Me.MediaTrackBar.ColorDown = ColorPack1
        ColorPack2.Border = System.Drawing.Color.Black
        ColorPack2.Face = System.Drawing.Color.Gray
        ColorPack2.Highlight = System.Drawing.Color.Gray
        Me.MediaTrackBar.ColorHover = ColorPack2
        ColorPack3.Border = System.Drawing.Color.Black
        ColorPack3.Face = System.Drawing.Color.Gray
        ColorPack3.Highlight = System.Drawing.Color.Gray
        Me.MediaTrackBar.ColorUp = ColorPack3
        Me.MediaTrackBar.Label = Nothing
        Me.MediaTrackBar.Location = New System.Drawing.Point(153, 15)
        Me.MediaTrackBar.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.MediaTrackBar.Name = "MediaTrackBar"
        Me.MediaTrackBar.ShowFocus = False
        Me.MediaTrackBar.Size = New System.Drawing.Size(209, 8)
        ColorLinearGradient1.ColorA = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        ColorLinearGradient1.ColorB = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MediaTrackBar.SliderColorLow = ColorLinearGradient1
        Me.MediaTrackBar.SliderShape = gTrackBar.gTrackBar.eShape.Rectangle
        Me.MediaTrackBar.SliderSize = New System.Drawing.Size(8, 8)
        Me.MediaTrackBar.SliderWidthHigh = 1.0!
        Me.MediaTrackBar.SliderWidthLow = 1.0!
        Me.MediaTrackBar.TabIndex = 29
        Me.MediaTrackBar.TickThickness = 1.0!
        Me.MediaTrackBar.UpDownShow = False
        Me.MediaTrackBar.Value = 0
        Me.MediaTrackBar.ValueAdjusted = 0.0!
        Me.MediaTrackBar.ValueDivisor = gTrackBar.gTrackBar.eValueDivisor.e1
        Me.MediaTrackBar.ValueStrFormat = Nothing
        '
        'GlassButton6
        '
        Me.GlassButton6.BackgroundImage = CType(resources.GetObject("GlassButton6.BackgroundImage"), System.Drawing.Image)
        Me.GlassButton6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GlassButton6.GlowColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GlassButton6.Location = New System.Drawing.Point(484, 0)
        Me.GlassButton6.Name = "GlassButton6"
        Me.GlassButton6.OuterBorderColor = System.Drawing.Color.Transparent
        Me.GlassButton6.Size = New System.Drawing.Size(100, 34)
        Me.GlassButton6.TabIndex = 21
        Me.GlassButton6.Text = "About"
        Me.GlassButton6.UseMnemonic = False
        '
        'GlassButton5
        '
        Me.GlassButton5.BackgroundImage = CType(resources.GetObject("GlassButton5.BackgroundImage"), System.Drawing.Image)
        Me.GlassButton5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GlassButton5.GlowColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GlassButton5.Location = New System.Drawing.Point(387, 0)
        Me.GlassButton5.Name = "GlassButton5"
        Me.GlassButton5.OuterBorderColor = System.Drawing.Color.Transparent
        Me.GlassButton5.Size = New System.Drawing.Size(100, 34)
        Me.GlassButton5.TabIndex = 20
        Me.GlassButton5.Text = "License"
        Me.GlassButton5.UseMnemonic = False
        '
        'GlassButton4
        '
        Me.GlassButton4.BackgroundImage = CType(resources.GetObject("GlassButton4.BackgroundImage"), System.Drawing.Image)
        Me.GlassButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GlassButton4.GlowColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GlassButton4.Location = New System.Drawing.Point(290, 0)
        Me.GlassButton4.Name = "GlassButton4"
        Me.GlassButton4.OuterBorderColor = System.Drawing.Color.Transparent
        Me.GlassButton4.Size = New System.Drawing.Size(100, 34)
        Me.GlassButton4.TabIndex = 19
        Me.GlassButton4.Text = "Config"
        Me.GlassButton4.UseMnemonic = False
        '
        'GlassButton3
        '
        Me.GlassButton3.BackgroundImage = CType(resources.GetObject("GlassButton3.BackgroundImage"), System.Drawing.Image)
        Me.GlassButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GlassButton3.GlowColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GlassButton3.Location = New System.Drawing.Point(193, 0)
        Me.GlassButton3.Name = "GlassButton3"
        Me.GlassButton3.OuterBorderColor = System.Drawing.Color.Transparent
        Me.GlassButton3.Size = New System.Drawing.Size(100, 34)
        Me.GlassButton3.TabIndex = 18
        Me.GlassButton3.Text = "Utilities"
        Me.GlassButton3.UseMnemonic = False
        '
        'GlassButton2
        '
        Me.GlassButton2.BackgroundImage = CType(resources.GetObject("GlassButton2.BackgroundImage"), System.Drawing.Image)
        Me.GlassButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GlassButton2.GlowColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GlassButton2.Location = New System.Drawing.Point(96, 0)
        Me.GlassButton2.Name = "GlassButton2"
        Me.GlassButton2.OuterBorderColor = System.Drawing.Color.Transparent
        Me.GlassButton2.Size = New System.Drawing.Size(100, 34)
        Me.GlassButton2.TabIndex = 17
        Me.GlassButton2.Text = "Playlist"
        Me.GlassButton2.UseMnemonic = False
        '
        'GlassButton7
        '
        Me.GlassButton7.BackgroundImage = Global.INSCentral.My.Resources.Resources.glossy
        Me.GlassButton7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GlassButton7.GlowColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GlassButton7.Location = New System.Drawing.Point(0, 0)
        Me.GlassButton7.Name = "GlassButton7"
        Me.GlassButton7.OuterBorderColor = System.Drawing.Color.Transparent
        Me.GlassButton7.Size = New System.Drawing.Size(98, 34)
        Me.GlassButton7.TabIndex = 14
        Me.GlassButton7.Text = "Library"
        Me.GlassButton7.UseMnemonic = False
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.BackgroundImage = Global.INSCentral.My.Resources.Resources.glossy1
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1151, 35)
        Me.ToolStrip1.TabIndex = 13
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'StatusStrip
        '
        Me.StatusStrip.BackgroundImage = Global.INSCentral.My.Resources.Resources.glossy1
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 592)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(1151, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(0, 17)
        '
        'Timer1
        '
        '
        'ImgPlayIcons
        '
        Me.ImgPlayIcons.ImageStream = CType(resources.GetObject("ImgPlayIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImgPlayIcons.TransparentColor = System.Drawing.Color.Transparent
        Me.ImgPlayIcons.Images.SetKeyName(0, "Actions-media-playback-pause-icon.png")
        Me.ImgPlayIcons.Images.SetKeyName(1, "Actions-media-playback-start-icon.png")
        Me.ImgPlayIcons.Images.SetKeyName(2, "Actions-media-playback-stop-icon.png")
        '
        'MDIInstoreCentral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1151, 614)
        Me.Controls.Add(Me.PlayerPanel)
        Me.Controls.Add(Me.GlassButton6)
        Me.Controls.Add(Me.GlassButton5)
        Me.Controls.Add(Me.GlassButton4)
        Me.Controls.Add(Me.GlassButton3)
        Me.Controls.Add(Me.GlassButton2)
        Me.Controls.Add(Me.GlassButton7)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.StatusStrip)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "MDIInstoreCentral"
        Me.Text = "PlayTM"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.PlayerPanel.ResumeLayout(False)
        Me.PlayerPanel.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents GlassButton7 As Glass.GlassButton
    Friend WithEvents GlassButton6 As Glass.GlassButton
    Friend WithEvents GlassButton5 As Glass.GlassButton
    Friend WithEvents GlassButton4 As Glass.GlassButton
    Friend WithEvents GlassButton3 As Glass.GlassButton
    Friend WithEvents GlassButton2 As Glass.GlassButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents PlayerPanel As System.Windows.Forms.Panel
    Friend WithEvents MediaTrackBar As gTrackBar.gTrackBar
    Friend WithEvents lbl_curtime As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl_TotalTime As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ImgPlayIcons As System.Windows.Forms.ImageList

End Class
