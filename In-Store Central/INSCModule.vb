﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Imports DevExpress.XtraEditors
Imports System.Net.Mail

Module INSCModule
    Public DBConn As SqlConnection
    Public ActiveCustomer As String, ActiveStore As String, ActiveGenre As String, ActiveGenreId As Integer, ActiveCustomerId As Integer
    Public SongsArchivePath, PromosArchivePath, FileprepPath, AudioFileFormat, AudioConverterpath As String
    Public FTPIPAddress, FTPUserName, FTPPassword As String
    Public DBServer, DBUserName, DBPassword As String
    Public FTPRootPath As String, FTPAutoSync As String
    Public PlayMode As String
    Public SmtpPort As Integer, SmtpHost, FromAddress, MailPassword, ToAddress, CCAddress As String

    Private key As String = "&/?@*>:>"

    Public Sub OpenDBConnection()
        DBConn = New SqlConnection
        'DBConn.ConnectionString = ConfigurationSettings.AppSettings("ConnectionString").ToString
        'DBConn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("ConnectionString").ToString

        '"server=MUDRAGADA-PC;Database=INSCentral;UID=storeadmin;PWD=central"

        DBConn.ConnectionString = "server=" & DBServer & ";Database=INSCentral;UID=" & DBUserName & ";PWD=" & DBPassword
        DBConn.Open()

    End Sub
    Public Sub WriteConfig(ByVal AudioConverterpath As String)
        Dim dsConfig As New DataSet
        Dim dtConfig As New DataTable("ConfigurationSettings")

        dtConfig.Columns.Add("DBServer", System.Type.GetType("System.String"))
        dtConfig.Columns.Add("UserName", System.Type.GetType("System.String"))
        dtConfig.Columns.Add("Password", System.Type.GetType("System.String"))
        dtConfig.Columns.Add("AudioConverterPath", System.Type.GetType("System.String"))
        dtConfig.Columns.Add("FTPAutoSync", System.Type.GetType("System.String"))

        dtConfig.Rows.Add(DBServer, DBUserName, DBPassword, AudioConverterpath, FTPAutoSync)
        dsConfig.Tables.Add(dtConfig)

        Dim dtEmail As New DataTable("EmailSettings")

        dtEmail.Columns.Add("Host", System.Type.GetType("System.String"))
        dtEmail.Columns.Add("Port", System.Type.GetType("System.String"))
        dtEmail.Columns.Add("From", System.Type.GetType("System.String"))
        dtEmail.Columns.Add("Password", System.Type.GetType("System.String"))
        dtEmail.Columns.Add("To", System.Type.GetType("System.String"))
        dtEmail.Columns.Add("CC", System.Type.GetType("System.String"))

        dtEmail.Rows.Add(SmtpHost, SmtpPort, FromAddress, MailPassword, ToAddress, CCAddress)
        dsConfig.Tables.Add(dtEmail)

        dsConfig.WriteXml(Application.StartupPath & "\PlayTMConfig.xml")

        dtConfig.Dispose()
        dsConfig.Dispose()
    End Sub
   

    Public Sub CloseDBConnection()
        If Not DBConn Is Nothing Then
            DBConn.Close()
            DBConn = Nothing
        End If
    End Sub
    Public Sub LoadSettings()
        Dim dr As SqlDataReader
        Dim command As New SqlCommand

        Call OpenDBConnection()

        ' cmb_Audio_Format.Properties.Items.Clear()
        Try
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcGetAdminParameters"
            dr = command.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                SongsArchivePath = dr("Songs_Path").ToString
                PromosArchivePath = dr("Promos_Path").ToString
                FileprepPath = dr("Fileprep_Path").ToString
                AudioFileFormat = dr("Audio_Format").ToString
                'AudioConverterpath = dr("AudioConverter_Path").ToString
            End If
            command.CommandText = "ProcGetFTPParameters"
            dr.Close()
            dr = Nothing
            dr = command.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                FTPIPAddress = dr("IP_Address").ToString
                FTPUserName = dr("User_Name").ToString
                FTPPassword = dr("Password").ToString
            End If
            command.Dispose()

            Call CloseDBConnection()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Function ReadConfig() As Boolean
        Dim res As Boolean
        If Not File.Exists(Application.StartupPath & "\PlayTMConfig.xml") Then
            XtraMessageBox.Show("Configuration file missing contact your administrator.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            res = False
        Else
            Dim dsConfig As New DataSet
            dsConfig.ReadXml(Application.StartupPath & "\PlayTMConfig.xml")
            DBServer = dsConfig.Tables("ConfigurationSettings").Rows(0)("DBServer")
            DBUserName = dsConfig.Tables("ConfigurationSettings").Rows(0)("UserName")
            DBPassword = dsConfig.Tables("ConfigurationSettings").Rows(0)("Password")
            AudioConverterpath = dsConfig.Tables("ConfigurationSettings").Rows(0)("AudioConverterPath")
            FTPAutoSync = dsConfig.Tables("ConfigurationSettings").Rows(0)("FTPAutoSync")

            SmtpPort = dsConfig.Tables("EmailSettings").Rows(0)("Port")
            SmtpHost = dsConfig.Tables("EmailSettings").Rows(0)("Host")
            FromAddress = dsConfig.Tables("EmailSettings").Rows(0)("From")
            MailPassword = dsConfig.Tables("EmailSettings").Rows(0)("Password")
            ToAddress = dsConfig.Tables("EmailSettings").Rows(0)("To")
            CCAddress = dsConfig.Tables("EmailSettings").Rows(0)("CC")

            dsConfig.Dispose()
            res = True
        End If
        Return (res)
    End Function
    Public Function encryptString(ByVal strtext As String) As String
        Return Encrypt(strtext, key)
    End Function

    Public Function decryptString(ByVal strtext As String) As String
        Return Decrypt(strtext, key)
    End Function

    'The function used to encrypt the text
    Private Function Encrypt(ByVal strText As String, ByVal strEncrKey _
             As String) As String
        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}

        Try
            byKey = System.Text.Encoding.UTF8.GetBytes(Left(strEncrKey, 8))

            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes(strText)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())

        Catch ex As Exception
            Return ex.Message
        End Try

    End Function

    'The function used to decrypt the text
    Private Function Decrypt(ByVal strText As String, ByVal sDecrKey _
               As String) As String
        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim inputByteArray(strText.Length) As Byte

        Try
            byKey = System.Text.Encoding.UTF8.GetBytes(Left(sDecrKey, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(strText)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)

            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8

            Return encoding.GetString(ms.ToArray())

        Catch ex As Exception
            Return ex.Message
        End Try

    End Function
    
End Module

