﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO
Imports DevExpress.Utils
Imports NAudio
Imports DevExpress.XtraEditors
Imports aTrimmer
Public Class frmAddPromo
    Public CurRow, CurFirstRowIndex As Integer
    'these vars are used while meta data editing
    Private Sub frmAddPromo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dt_start_date.EditValue = Today
        Dt_Stop_Date.EditValue = DateAdd(DateInterval.Year, 10, Today)
        Call LoadCustomers()

    End Sub
    Private Sub LoadCustomers()
        'Lookup Code
        Call OpenDBConnection()
        Dim cmdCustomers As New SqlCommand("ProcGetCustomersList", DBConn)
        cmdCustomers.CommandType = CommandType.StoredProcedure
        Dim dapCustomer As New SqlDataAdapter(cmdCustomers)
        Dim dsCustomers As New DataSet
        dapCustomer.Fill(dsCustomers)

        'Creating Columns for Display in lookup
        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomers.Tables(0)
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        cmb_Customer.Properties.Columns(0).Visible = False
        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False
        cmb_Customer.EditValue = ""

        If dsCustomers.Tables(0).Rows.Count <> 0 Then btn_Add.Enabled = True Else btn_Add.Enabled = False

        dsCustomers.Dispose()
        dapCustomer.Dispose()
        Call LoadNormalizationFactors()
        Call CloseDBConnection()
    End Sub


    Private Sub btn_Browse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Browse.Click
        BrowseFile.Filter = "Wave Files|*.wav"
        BrowseFile.ShowDialog()
        txt_AudioPath.Text = BrowseFile.FileName

    End Sub

    Private Sub btn_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Quit.Click
        'Try
        If btn_Add.Text = "Save" Then
            Me.Close()
            Exit Sub
        End If

        MDIInstoreCentral.PlayerPanel.Visible = True
        MDIInstoreCentral.ResetPlayer()

        'INSCModule.ActiveGenreId = Convert.ToInt32(cmb_Customer.EditValue.ToString)
        INSCModule.ActiveCustomer = cmb_Customer.Text
        Me.Close()

        Dim w As New frmLibrary

        With w
            .MdiParent = MDIInstoreCentral
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
            .Library_Tab_Control.SelectedTabPageIndex = 1
            .cmb_Customer.Text = ActiveCustomer

            '.cmb_Genre.RefreshEditValue()
        End With
        'Catch
        'End Try
    End Sub
    Private Sub LoadNormalizationFactors()
        Dim cmdNormalization As New SqlCommand, adpNormalization As New SqlDataAdapter, dsNormalization As New DataSet

        With cmdNormalization
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdNormalization.CommandText = "ProcGetNormalizationsList"
        adpNormalization.SelectCommand = cmdNormalization
        adpNormalization.Fill(dsNormalization, "NormalizationList")

        cmb_Normalization.Properties.DisplayMember = "Normalization_Factor"
        cmb_Normalization.Properties.ValueMember = "Normalization_ID"
        cmb_Normalization.Properties.DataSource = dsNormalization.Tables("NormalizationList")
        cmb_Normalization.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Normalization.Properties.PopulateColumns()
        'cmb_Normalization.Properties.Columns(0).Visible = False
        cmb_Normalization.Properties.ShowFooter = False
        cmb_Normalization.Properties.ShowHeader = False

        cmb_Normalization.Text = "-6 db"

        dsNormalization.Dispose()
        adpNormalization.Dispose()
    End Sub
    Private Sub btn_Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Add.Click

        If cmb_Customer.EditValue.ToString = "" Then
            XtraMessageBox.Show("Customer can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Customer.Focus()
            Exit Sub
        End If

        If cmb_Category.EditValue.ToString = "" Then
            XtraMessageBox.Show("Category can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Category.Focus()
            Exit Sub
        End If

        If txt_PromoID.Text = "" Then
            XtraMessageBox.Show("Promo ID can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_PromoID.Focus()
            Exit Sub
        End If

        If Not IsNumeric(txt_PromoID.Text) Then
            XtraMessageBox.Show("Promo ID can not numerics only", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_PromoID.Focus()
            Exit Sub
        End If

        If txt_Title.Text = "" Then
            XtraMessageBox.Show("Title of the promo can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_Title.Focus()
            Exit Sub
        End If

        If txt_AudioPath.Text = "" Then
            XtraMessageBox.Show("Promo audio path is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_AudioPath.Focus()
            Exit Sub
        End If

        If Not File.Exists(txt_AudioPath.Text) Then
            XtraMessageBox.Show("Could not find 'Promo audio' in the selected path", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_AudioPath.Focus()
            Exit Sub
        End If

        Dim PromoFileName As String = ""
        Dim PromoAACFileName As String = ""
        Dim FileDuration As String = ""

        Dim PromoTempFile As String = ""



        If File.Exists(PromosArchivePath & "\" & txt_PromoID.Text.Trim & ".aac") Then
            If btn_Add.Text <> "Save" Then
                If XtraMessageBox.Show("This promo is already added. Do you wanted to overwrite?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    File.Delete(PromosArchivePath & "\" & txt_PromoID.Text.Trim & ".aac")
                    Application.DoEvents()
                Else
                    Exit Sub
                End If
            Else
                If PromosArchivePath & "\" & txt_PromoID.Text.Trim & ".aac" <> txt_AudioPath.Text Then
                    File.Delete(PromosArchivePath & "\" & txt_PromoID.Text.Trim & ".aac")
                    Application.DoEvents()
                Else
                    FileDuration = lbl_Duration.Text
                    GoTo SaveToDB
                End If
            End If
        End If

        If Not File.Exists(AudioConverterpath) Then
            XtraMessageBox.Show("Audio converter not found (" & AudioConverterpath & ")", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If


        ConversionBar.Visible = True


        If Not Directory.Exists(Application.StartupPath & "\Temp") = True Then
            Directory.CreateDirectory(Application.StartupPath & "\Temp")
        End If

        PromoFileName = Path.GetFileName(txt_AudioPath.Text)
        PromoTempFile = Application.StartupPath & "\Temp\" & PromoFileName
        PromoAACFileName = txt_PromoID.Text & ".aac" 'PromoFileName.Replace(".wav", ".aac")

        lbl_Status.Text = "Status: Converting promo file"
        Application.DoEvents()

        If ReWriteWavFile(AudioConverterpath, txt_AudioPath.Text, PromoTempFile) Then
            lbl_Status.Text = "Status: Converting promo to .aac file"
            Application.DoEvents()

            If CheckforAACFile(PromoFileName, txt_PromoID.Text) = False Then
                FileDuration = GetWavFileDuration(PromoTempFile)
                If FileDuration <> "Error while reading wav file" Then
                    If Convert_Wav_AACPlus(PromoTempFile, txt_PromoID.Text) Then
                    Else
                        XtraMessageBox.Show("Unable to convert Audio File", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                Else
                    XtraMessageBox.Show("Unable to retrieve audio file duration", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
            Else
                If FileDuration <> "Error while reading wav file" Then
                Else
                    XtraMessageBox.Show("Unable to retrieve audio file duration", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
            End If
        End If
SaveToDB:
        lbl_Status.Text = "Status: Saving to database"
        Application.DoEvents()

        Dim dtStart As Date = Dt_start_date.EditValue.ToString
        Dim dtStop As Date = Dt_Stop_Date.EditValue.ToString

        Dim Cmd As New SqlCommand
        Try
            Call OpenDBConnection()
            With Cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = DBConn
                .CommandTimeout = 500
                .CommandText = "ProcAddPromo"
                .Parameters.AddWithValue("@Customer_ID", Convert.ToInt32(cmb_Customer.EditValue.ToString)).Direction = ParameterDirection.Input
                .Parameters.AddWithValue("@Promo_ID", Val(txt_PromoID.Text)).Direction = ParameterDirection.Input
                .Parameters.AddWithValue("@Title", txt_Title.Text.Trim).Direction = ParameterDirection.Input
                .Parameters.AddWithValue("@FileName", txt_PromoID.Text.Trim & ".aac").Direction = ParameterDirection.Input
                .Parameters.AddWithValue("@Duration", FileDuration).Direction = ParameterDirection.Input
                .Parameters.AddWithValue("@Promo_Cetegory_Id", Convert.ToInt32(cmb_Category.EditValue.ToString)).Direction = ParameterDirection.Input
                '.Parameters.AddWithValue("@Normalization_ID", Convert.ToInt32(cmb_Normalization.EditValue.ToString)).Direction = ParameterDirection.Input
                .Parameters.AddWithValue("@Start_Date", dtStart.ToString("MM/dd/yyyy").Replace("-", "/")).Direction = ParameterDirection.Input
                .Parameters.AddWithValue("@Stop_Date", dtStop.ToString("MM/dd/yyyy").Replace("-", "/")).Direction = ParameterDirection.Input
                .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
                .ExecuteNonQuery()
            End With

            Dim strResult As String
            strResult = Cmd.Parameters("@Result").Value
            If strResult = "Success" Then
                'XtraMessageBox.Show("Promo added successfully", "New Promo", MessageBoxButtons.OK)
                lbl_Status.Text = "Status: Promo added successfully"
                Application.DoEvents()
            Else
                'XtraMessageBox.Show(strResult, "New Promo", MessageBoxButtons.OK)
                lbl_Status.Text = "Status: " & strResult
                Application.DoEvents()
            End If
            ConversionBar.Visible = False
            'lbl_Status.Text = ""
        Catch
        End Try

        If DeleteTempFolder(Application.StartupPath & "\Temp") Then
            'Write Log---Temp folder Delete Sucessfully
        Else
            'Write Log --- Error while deleteing temp folder
        End If
        Call ClearValues()
        Call CloseDBConnection()
        If btn_Add.Text = "Save" And lbl_Status.Text = "Status: Promo added successfully" Then

            Dim f As frmLibrary = TryCast(Me.Owner, frmLibrary)
            f.RefreshPromos(CurRow, CurFirstRowIndex)
            Me.Close()
        End If

    End Sub
    Private Sub ClearValues()
        txt_AudioPath.Text = ""
        txt_PromoID.Text = ""
        txt_Title.Text = ""
    End Sub
    Public Function DeleteTempFolder(ByVal Temppath As String) As Boolean
        Try
            If Directory.Exists(Temppath) = True Then
                Directory.Delete(Temppath, True)
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function CheckforAACFile(ByVal Filename As String, ByVal Media_ID As String) As Boolean
        Dim returnval As Boolean
        Dim aacFilename As String
        aacFilename = Media_ID & "\" & ".aac"

        If File.Exists(PromosArchivePath & "\" & aacFilename) Then
            returnval = True
        Else
            returnval = False
        End If
        Return returnval
    End Function
    Public Function GetWavFileDuration(ByVal FileNameStr As String) As String
        Dim returnval As String
        If File.Exists(FileNameStr) Then
            Try
                Dim wd As NAudio.Wave.WaveFileReader = New Wave.WaveFileReader(FileNameStr)
                returnval = wd.TotalTime.ToString.Split(".")(0)
                wd.Close()
                wd = Nothing
            Catch ex As Exception
                returnval = "Error while reading wav file"
            End Try
        Else
            returnval = "Source File Not Found"
        End If
        Return returnval
    End Function
    Public Function Convert_Wav_AACPlus(ByVal FileNameStr As String, ByVal Media_ID As String) As Boolean
        Dim ConversionProcess As Process
        Dim StrOutput As String
        Dim finfo As FileInfo
        Dim wavfile As String
        Dim aacfile As String
        Dim frmStr As String
        Dim returnval As Boolean


        If File.Exists(FileNameStr) Then
            finfo = New FileInfo(FileNameStr)
            wavfile = finfo.FullName
            aacfile = PromosArchivePath & "\" & Media_ID & ".aac"
            ConversionProcess = New Process
            ConversionProcess.StartInfo.UseShellExecute = False
            ConversionProcess.StartInfo.RedirectStandardError = True
            ConversionProcess.StartInfo.CreateNoWindow = True
            ConversionProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            ConversionProcess.StartInfo.FileName = Application.StartupPath & "\enc_aacPlus.exe"
            ' ConversionProcess.EnableRaisingEvents = True
            frmStr = """" & wavfile & """" & " """ & aacfile & """ --br 128000 --he"
            ConversionProcess.StartInfo.Arguments = frmStr
            ConversionProcess.Start()

            ' StrOutput = ConversionProcess.StandardOutput.ReadToEnd
            Application.DoEvents()
            Do Until ConversionProcess.HasExited
                Application.DoEvents()
            Loop
            'ConversionProcess.WaitForExit()
            If File.Exists(aacfile) Then
                returnval = True
            Else
                returnval = False
            End If
        End If
        Return returnval
    End Function
    Public Function ReWriteWavFile(ByVal ExePath As String, ByVal InFile As String, ByVal OutFile As String) As Boolean

        Dim StrProcessPath As String = String.Empty
        Dim tempOutFile, tempPath As String

        tempPath = Path.GetDirectoryName(OutFile)
        tempOutFile = Path.GetFileNameWithoutExtension(OutFile) & "-trim" & Path.GetExtension(OutFile)

        StrProcessPath = """" & InFile & """ """ & tempPath & "\" & tempOutFile & "" & """ -c wav -sr 44100 -ch Stereo"
        If ShellandWait(ExePath, StrProcessPath) = True Then
            If File.Exists(tempPath & "\" & tempOutFile) = True Then
                Dim wa As New aTrim
                lbl_Status.Text = "Status: Trimming silence"
                Application.DoEvents()
                If (Not wa.StripSilence(tempPath & "\" & tempOutFile, OutFile)) Then
                    lbl_Status.Text = "Status: Trimming failed"
                    Application.DoEvents()
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If

        If File.Exists(tempPath & "\" & tempOutFile) Then
            File.Delete(tempPath & "\" & tempOutFile)
            Application.DoEvents()
        End If


    End Function
    Public Function ShellandWait(ByVal ProcessPath As String, ByVal Arguments As String) As Boolean
        Dim objProcess As System.Diagnostics.Process
        Dim returnval As Boolean
        Try
            objProcess = New System.Diagnostics.Process()
            objProcess.StartInfo.FileName = ProcessPath
            objProcess.StartInfo.Arguments = Arguments
            objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
            objProcess.Start()
            objProcess.WaitForExit()
            objProcess.Close()
            returnval = True
        Catch
            returnval = False
        End Try
        Return returnval
    End Function
    Private Sub LoadCategories(ByVal Customer_Id As Integer)
        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetPromoCategoriesList"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "CategoryList")


        'Creating Columns for Display in lookup
        cmb_Category.Properties.DisplayMember = "Category_Code"
        cmb_Category.Properties.ValueMember = "Promo_Category_ID"
        cmb_Category.Properties.DataSource = dsStores.Tables(0)
        cmb_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Category.Properties.PopulateColumns()
        cmb_Category.Properties.Columns(0).Visible = False
        cmb_Category.Properties.Columns(1).Visible = False
        cmb_Category.Properties.ShowFooter = False
        cmb_Category.Properties.ShowHeader = False
        cmb_Category.EditValue = ""

        'If dsStores.Tables(0).Rows.Count <> 0 Then btn_Save.Enabled = True Else btn_Save.Enabled = False

        dsStores.Dispose()
        dapStores.Dispose()


        Call CloseDBConnection()
    End Sub
    Private Sub cmb_Customer_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If cmb_Customer.EditValue.ToString <> "" Then Call LoadCategories(Convert.ToInt32(cmb_Customer.EditValue.ToString))
    End Sub

    Private Sub Dt_start_date_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Dt_start_date.EditValueChanged
        If Dt_start_date.EditValue = Nothing Then Exit Sub
        Dt_Stop_Date.EditValue = DateAdd(DateInterval.Year, 10, Dt_start_date.EditValue)

    End Sub

    Private Sub Dt_Stop_Date_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dt_Stop_Date.EditValueChanged
        If Dt_Stop_Date.EditValue = Nothing Then Exit Sub
        Dim stDate, endDate As Date
        stDate = Dt_start_date.EditValue
        endDate = Dt_Stop_Date.EditValue
        If endDate < stDate Then
            XtraMessageBox.Show("Invalid 'Stop Date'", "Date Error")
        End If
    End Sub
End Class