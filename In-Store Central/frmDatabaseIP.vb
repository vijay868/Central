﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.XtraEditors
Public Class frmDatabaseIP
    Private Sub frmDatabaseIP_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txt_IPaddress.Focus()
    End Sub

    Private Sub btn_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save.Click
        Call WriteConfig(txt_IPaddress.Text.Trim)
        Call ReadConfig()
        frmLogin.Show()
        Me.Close()
    End Sub

    Private Sub btn_Exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Exit.Click
        End
    End Sub
End Class