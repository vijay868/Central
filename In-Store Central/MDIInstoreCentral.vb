﻿Imports System.Windows.Forms
Imports DevExpress.Utils
Imports DevExpress.XtraEditors
Imports INSCentral.libZPlay
Imports Un4seen.Bass
Imports Un4seen.Bass.AddOn.Aac
Public Class MDIInstoreCentral
    'Bass player
    Private m_MixerSynchProc As Un4seen.Bass.SYNCPROC
    Private m_MixerSyncHandle As Int32 = 0
    Dim CI As New Un4seen.Bass.BASS_CHANNELINFO
    Public iSongStream As Integer
    '---------
    'Public player As ZPlay
    Private LoadMode As Integer
    Private ReverseMode As Boolean
    Private Echo As Boolean
    Private VocalCut As Boolean = False
    Private SideCut As Boolean = False

    Private NextSong As Boolean = False

    Private FadeFinished As Boolean = False

    ' need this for managed stream
    Private fStream As System.IO.FileStream = Nothing
    Private br As System.IO.BinaryReader = Nothing
    Private BufferCounter As Integer

    'Private CallbackFunc As TCallbackFunc

    Private BlockSlideLeft As Boolean = False
    Private BlockSlideRight As Boolean = False

    Dim StreamTotalTime As Long

    ''' <summary>
    ''' Text callback
    ''' </summary>
    ''' <param name="text"></param>
    ''' <remarks></remarks>
    Public Delegate Sub SetTextCallback(ByVal text As String)

    'Private Sub ShowNewForm(ByVal sender As Object, ByVal e As EventArgs)
    '    ' Create a new instance of the child form.
    '    Dim ChildForm As New System.Windows.Forms.Form
    '    ' Make it a child of this MDI form before showing it.
    '    ChildForm.MdiParent = Me

    '    m_ChildFormNumber += 1
    '    ChildForm.Text = "Window " & m_ChildFormNumber

    '    ChildForm.Show()
    'End Sub

    'Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim OpenFileDialog As New OpenFileDialog
    '    OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
    '    OpenFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
    '    If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
    '        Dim FileName As String = OpenFileDialog.FileName
    '        ' TODO: Add code here to open the file.
    '    End If
    'End Sub

    'Private Sub SaveAsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim SaveFileDialog As New SaveFileDialog
    '    SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
    '    SaveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"

    '    If (SaveFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
    '        Dim FileName As String = SaveFileDialog.FileName
    '        ' TODO: Add code here to save the current contents of the form to a file.
    '    End If
    'End Sub


    'Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.Close()
    'End Sub

    'Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    'End Sub

    'Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    'End Sub

    'Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    'Use My.Computer.Clipboard.GetText() or My.Computer.Clipboard.GetData to retrieve information from the clipboard.
    'End Sub

    'Private Sub ToolBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.ToolStrip.Visible = Me.ToolBarToolStripMenuItem.Checked
    'End Sub

    'Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.StatusStrip.Visible = Me.StatusBarToolStripMenuItem.Checked
    'End Sub

    'Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.LayoutMdi(MdiLayout.Cascade)
    'End Sub

    'Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.LayoutMdi(MdiLayout.TileVertical)
    'End Sub

    'Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.LayoutMdi(MdiLayout.TileHorizontal)
    'End Sub

    'Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.LayoutMdi(MdiLayout.ArrangeIcons)
    'End Sub

    'Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    ' Close all child forms of the parent.
    '    For Each ChildForm As Form In Me.MdiChildren
    '        ChildForm.Close()
    '    Next
    'End Sub

    'Private m_ChildFormNumber As Integer

    'Private Sub MDIParent1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    'End Sub
#Region "Player Code"
    'Public Sub showinfo()
    '    Dim StreamInfo As New TStreamInfo()
    '    player.GetStreamInfo(StreamInfo)
    '    StreamTotalTime = 0
    '    StreamTotalTime = StreamInfo.Length.sec
    '    'descr1.Text = StreamInfo.Description & System.Environment.NewLine & System.Convert.ToString(StreamInfo.Length.hms.hour) & " : " & System.Convert.ToString(StreamInfo.Length.hms.minute) & " : " & System.Convert.ToString(StreamInfo.Length.hms.second) & System.Environment.NewLine & System.Convert.ToString(StreamInfo.SamplingRate) & " Hz" & System.Environment.NewLine & System.Convert.ToString(StreamInfo.Bitrate) & " kbps" & System.Environment.NewLine & System.Convert.ToString(StreamInfo.ChannelNumber) & System.Environment.NewLine & System.Convert.ToString(StreamInfo.VBR)
    '    MediaTrackBar.MinValue = 0
    '    MediaTrackBar.MaxValue = CInt(Fix(StreamInfo.Length.sec))
    '    MediaTrackBar.Value = 0
    '    Timer1.Enabled = True
    'End Sub
#End Region
    Public Function FormatSeconds(ByVal nSeconds As Long) As String
        Dim mHours As Long, mMinutes As Long, mSeconds As Long
        mSeconds = nSeconds
        mHours = mSeconds \ 3600
        mMinutes = (mSeconds - (mHours * 3600)) \ 60
        mSeconds = mSeconds - ((mHours * 3600) + (mMinutes * 60))
        Return mMinutes.ToString("00") & ":" & mSeconds.ToString("00")
        '  MsgBox(mHours & ":" & mMinutes & ":" & mSeconds)
    End Function
    Public Sub CloseChild()
        If ActiveMdiChild IsNot Nothing Then
            ActiveMdiChild.Close()
        End If
    End Sub
    Private Sub LoadBackGround()
        Call CloseChild()

        Dim w As New frmBackGround

        With w
            .MdiParent = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
        End With

    End Sub
    Private Sub ClearImage()
        'GlassButton1.BackgroundImage = Application.StartupPath & "\Glossy.png"
        GlassButton2.BackgroundImage = ImageList1.Images(1)
        GlassButton3.BackgroundImage = ImageList1.Images(1)
        GlassButton4.BackgroundImage = ImageList1.Images(1)
        GlassButton5.BackgroundImage = ImageList1.Images(1)
        GlassButton6.BackgroundImage = ImageList1.Images(1)
        GlassButton7.BackgroundImage = ImageList1.Images(1)
        Application.DoEvents()
    End Sub
    Private Sub GlassButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GlassButton7.Click

        ResetPlayer()
        PlayerPanel.Visible = True
        Call ClearImage()

        GlassButton7.BackgroundImage = Nothing 'ImageList1.Images(0)
        ActiveCustomer = ""
        Call CloseChild()

        Dim w As New frmLibrary


        With w
            .MdiParent = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
        End With

        'frmLibrary = Nothing
    End Sub

    Private Sub GlassButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GlassButton4.Click
        PlayerPanel.Visible = False
        ResetPlayer()
        Call CloseChild()

        Call ClearImage()
        GlassButton4.BackgroundImage = Nothing 'ImageList1.Images(0)

        Dim w As New frmConfig
        PlayerPanel.Visible = False
        With w
            .MdiParent = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
        End With
    End Sub


    Private Sub GlassButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GlassButton3.Click
        PlayerPanel.Visible = False
        ResetPlayer()
        Call CloseChild()
        Call ClearImage()
        GlassButton3.BackgroundImage = Nothing 'ImageList1.Images(0)


        Dim w As New frmUtilities
        PlayerPanel.Visible = False
        With w
            .MdiParent = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
        End With
    End Sub

    Private Sub MDIInstoreCentral_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        End
    End Sub


    Private Sub MDIInstoreCentral_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        BassNet.Registration("mhemant@timbremedia.in", "2X22311822182914")
        If False = Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, Me.Handle) Then
            MsgBox("BASS Init Error!")
        End If
        Dim CI As New Un4seen.Bass.BASS_CHANNELINFO
        m_MixerSynchProc = New Un4seen.Bass.SYNCPROC(AddressOf CurrentTrackEnded)
        m_MixerSyncHandle = Bass.BASS_ChannelSetSync(iSongStream, Un4seen.Bass.BASSSync.BASS_SYNC_END, 0, m_MixerSynchProc, 0)

        Call LoadBackGround()
        LoadSettings()
        'player = New libZPlay.ZPlay
        If ReadConfig() Then
            If FTPAutoSync = "True" Then
                Process.Start(Application.StartupPath & "\PlayTM FTPSync", "AppLoad")
            End If
        End If
        'GlassButton1.BackgroundImageLayout = ImageLayout.None
        'GlassButton1.BackgroundImage = ImageList1.Images(0)
    End Sub

    Private Sub GlassButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GlassButton2.Click
        ResetPlayer()
        PlayerPanel.Visible = True
        Call CloseChild()

        Call ClearImage()
        GlassButton2.BackgroundImage = Nothing 'ImageList1.Images(0)

        Dim w As New frmPlayList

        With w
            .MdiParent = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
        End With
    End Sub

    Private Sub GlassButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GlassButton5.Click
        PlayerPanel.Visible = False
        ResetPlayer()
        Call CloseChild()

        Call ClearImage()
        GlassButton5.BackgroundImage = Nothing ' ImageList1.Images(0)

        Dim w As New License
        PlayerPanel.Visible = False
        With w
            .MdiParent = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
        End With
    End Sub

   

    'Private Sub GlassButton1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)  'minimize
    '    Me.WindowState = FormWindowState.Minimized
    'End Sub

    Private Sub MediaTrackBar_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MediaTrackBar.MouseClick
        Dim Len, tLength, newPos As Long
        Len = Bass.BASS_ChannelGetLength(iSongStream)

        tLength = Bass.BASS_ChannelBytes2Seconds(iSongStream, Len)

        newPos = CUInt(e.X * tLength / CDbl((MediaTrackBar.Size.Width)))

        Try
            Dim CurSec As Long
            CurSec = Bass.BASS_ChannelSeconds2Bytes(iSongStream, newPos)
            Bass.BASS_ChannelSetPosition(iSongStream, CurSec, BASSMode.BASS_POS_BYTES)
        Catch
        End Try

        'player.Seek(TTimeFormat.tfSecond, newpos, TSeekMethod.smFromBeginning)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click 'rewind
        Try
            Dim CurSec As Long
            CurSec = Bass.BASS_ChannelSeconds2Bytes(iSongStream, Bass.BASS_ChannelBytes2Seconds(iSongStream, Bass.BASS_ChannelGetPosition(iSongStream, BASSMode.BASS_POS_BYTES)) - 5)
            Bass.BASS_ChannelSetPosition(iSongStream, CurSec, BASSMode.BASS_POS_BYTES)
        Catch
        End Try
        'Dim newpos As New TStreamTime()
        'newpos.sec = 5
        'player.Seek(TTimeFormat.tfSecond, newpos, TSeekMethod.smFromCurrentBackward)
    End Sub

    Private Sub GlassButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GlassButton6.Click
        ResetPlayer()
        PlayerPanel.Visible = False

        Call CloseChild()

        Call ClearImage()
        GlassButton7.BackgroundImage = Nothing 'ImageList1.Images(0)
        Dim w As New frmAbout
        PlayerPanel.Visible = False
        With w
            .MdiParent = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
        End With

    End Sub
    Public Function CheckIfChannelActive() As Boolean
        Dim isActive As Un4seen.Bass.BASSActive
        Dim Res As Boolean = True

        isActive = Bass.BASS_ChannelIsActive(iSongStream)

        If isActive = Un4seen.Bass.BASSActive.BASS_ACTIVE_STOPPED Then
            Res = False
        Else
            Res = True
        End If
        Return Res
    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        Dim tElapsed As Single
        Dim tRemain As Single
        Dim tLength As Single
        Dim pos As Long
        Dim len As Long

        If CheckIfChannelActive() = False Then
            Timer1.Enabled = False
            Exit Sub
        End If


        len = Bass.BASS_ChannelGetLength(iSongStream)
        pos = Bass.BASS_ChannelGetPosition(iSongStream)

        MediaTrackBar.MaxValue = len
        MediaTrackBar.Value = pos

        tLength = Bass.BASS_ChannelBytes2Seconds(iSongStream, len)
        tElapsed = Bass.BASS_ChannelBytes2Seconds(iSongStream, pos)
        tRemain = tLength - tElapsed


        lbl_curtime.Text = Un4seen.Bass.Utils.FixTimespan(tElapsed, "MMSS")
        lbl_TotalTime.Text = Un4seen.Bass.Utils.FixTimespan(tLength, "MMSS")



        'Dim pos As New TStreamTime()
        'player.GetPosition(pos)

        'If MediaTrackBar.MaxValue > CInt(Fix(pos.sec)) Then
        '    MediaTrackBar.Value = CInt(Fix(pos.sec))
        'End If

        'lbl_curtime.Text = Format(pos.hms.minute, "00") & ":" & Format(pos.hms.second, "00")
        ''  position.Text = Format(pos.hms.hour, "00") & " : " & Format(pos.hms.minute, "00") & " : " & Format(pos.hms.second, "00") & " : " & Format(pos.hms.millisecond, "000")
        'Dim Status As New TStreamStatus()

        'player.GetStatus(Status)

        'If Status.fPlay = False Then
        '    Timer1.Enabled = False
        'End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim CurSec As Long
            CurSec = Bass.BASS_ChannelSeconds2Bytes(iSongStream, Bass.BASS_ChannelBytes2Seconds(iSongStream, Bass.BASS_ChannelGetPosition(iSongStream, BASSMode.BASS_POS_BYTES)) + 5)
            Bass.BASS_ChannelSetPosition(iSongStream, CurSec, BASSMode.BASS_POS_BYTES)
        Catch
        End Try
        'Dim newpos As New TStreamTime()
        'newpos.sec = 5
        'player.Seek(TTimeFormat.tfSecond, newpos, TSeekMethod.smFromCurrentForward)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If PlayMode = "Play" Then
            Bass.BASS_ChannelPause(iSongStream)
            PlayMode = "Pause"
            Button1.Image = Global.INSCentral.My.Resources.Resources.Actions_media_playback_pause_icon
            Timer1.Enabled = False
        ElseIf PlayMode = "Pause" Then
            Bass.BASS_ChannelPlay(iSongStream, False)
            PlayMode = "Play"
            Button1.Image = Global.INSCentral.My.Resources.Resources.Actions_media_playback_start_icon
            Timer1.Enabled = True
        ElseIf PlayMode = "Stopped" Then
            Bass.BASS_ChannelPlay(iSongStream, False)
            'player.StartPlayback()
            'StopStream()
            PlayMode = "Play"
            Timer1.Enabled = True
        End If
    End Sub

    Public Sub ResetPlayer()

        StopStream()
        PlayMode = "Stopped"
        Button1.Image = Global.INSCentral.My.Resources.Resources.Actions_media_playback_start_icon 'ImgPlayIcons.Images(1)
        lbl_curtime.Text = ""
        lbl_TotalTime.Text = ""
        MediaTrackBar.Value = 0
    End Sub
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Bass.BASS_ChannelStop(iSongStream)
        PlayMode = "Stopped"
        Button1.Image = Global.INSCentral.My.Resources.Resources.Actions_media_playback_start_icon 'ImgPlayIcons.Images(1)
        lbl_curtime.Text = "00:00"
        'lbl_TotalTime.Text = ""
        MediaTrackBar.Value = 0
        Try
            Dim CurSec As Long
            CurSec = 0 'Bass.BASS_ChannelSeconds2Bytes(iSongStream, 0)
            Bass.BASS_ChannelSetPosition(iSongStream, CurSec, BASSMode.BASS_POS_BYTES)
        Catch
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Increment forward from the gird
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Increment Backward from the gird
    End Sub
    Private Sub CurrentTrackEnded(ByVal MixerHandle As Int32, ByVal Channel As Int32, ByVal Data As Int32, ByVal User As IntPtr)
        StopStream()
    End Sub
    Public Sub PlayAudioFile(ByVal AudioFileName As String)

        iSongStream = BassAac.BASS_AAC_StreamCreateFile(AudioFileName, 0, 0, BASSFlag.BASS_AAC_STEREO)
        If iSongStream = 0 Then
            '   MsgBox("Error!")
            Exit Sub
        End If

        Bass.BASS_ChannelGetInfo(iSongStream, CI)
      
        Bass.BASS_ChannelPlay(iSongStream, False)
        m_MixerSynchProc = New Un4seen.Bass.SYNCPROC(AddressOf CurrentTrackEnded)
        m_MixerSyncHandle = Bass.BASS_ChannelSetSync(iSongStream, Un4seen.Bass.BASSSync.BASS_SYNC_END, 0, m_MixerSynchProc, 0)

        Timer1.Enabled = True

        'player.Close()
        'If Not (player.OpenFile(AudioFileName, TStreamFormat.sfAutodetect)) Then
        '    MessageBox.Show(player.GetError(), String.Empty, MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Exit Sub
        'End If
        'showinfo()
        'player.StartPlayback()
        'lbl_TotalTime.Text = FormatSeconds(StreamTotalTime)

        'player.Close()
        'If Not (player.OpenFile(AudioFileName, TStreamFormat.sfAutodetect)) Then
        '    MessageBox.Show(player.GetError(), String.Empty, MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Exit Sub
        'End If
        'showinfo()
        'player.StartPlayback()
        'lbl_TotalTime.Text = FormatSeconds(StreamTotalTime)
    End Sub
    Public Sub StopStream()
        ' If isActive = Un4seen.Bass.BASSActive.BASS_ACTIVE_PAUSED Or Un4seen.Bass.BASSActive.BASS_ACTIVE_PLAYING Then
        If CheckIfChannelActive() Then
            Bass.BASS_StreamFree(iSongStream)
            iSongStream = 0
            'Bass.BASS_Free()

        End If

    End Sub
End Class
