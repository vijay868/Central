﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActivatePromos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmb_Category = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmb_Store = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.btn_Save = New DevExpress.XtraEditors.SimpleButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_PromosCount = New DevExpress.XtraEditors.TextEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbl_Title = New DevExpress.XtraEditors.LabelControl()
        Me.Promos_Source_Grid = New System.Windows.Forms.DataGridView()
        Me.Source_Promo_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Source_Category = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Source_Title = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Source_FileName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promos_Target_Grid = New System.Windows.Forms.DataGridView()
        Me.Target_Promo_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_Category = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_Title = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_FileName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.cmb_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PromosCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Promos_Source_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Promos_Target_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmb_Category)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.cmb_Store)
        Me.Panel1.Controls.Add(Me.cmb_Customer)
        Me.Panel1.Controls.Add(Me.btn_Save)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_PromosCount)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.lbl_Title)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1295, 84)
        Me.Panel1.TabIndex = 2
        '
        'cmb_Category
        '
        Me.cmb_Category.EditValue = ""
        Me.cmb_Category.Location = New System.Drawing.Point(314, 59)
        Me.cmb_Category.Name = "cmb_Category"
        Me.cmb_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Category.Properties.NullText = ""
        Me.cmb_Category.Size = New System.Drawing.Size(106, 22)
        Me.cmb_Category.TabIndex = 58
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(318, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 15)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "Category"
        '
        'cmb_Store
        '
        Me.cmb_Store.EditValue = ""
        Me.cmb_Store.Location = New System.Drawing.Point(497, 58)
        Me.cmb_Store.Name = "cmb_Store"
        Me.cmb_Store.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Store.Properties.Appearance.Options.UseFont = True
        Me.cmb_Store.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Store.Properties.NullText = ""
        Me.cmb_Store.Size = New System.Drawing.Size(247, 22)
        Me.cmb_Store.TabIndex = 56
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(39, 59)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.NullText = ""
        Me.cmb_Customer.Size = New System.Drawing.Size(270, 22)
        Me.cmb_Customer.TabIndex = 55
        '
        'btn_Save
        '
        Me.btn_Save.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Save.Appearance.Options.UseFont = True
        Me.btn_Save.Location = New System.Drawing.Point(822, 58)
        Me.btn_Save.Name = "btn_Save"
        Me.btn_Save.Size = New System.Drawing.Size(71, 22)
        Me.btn_Save.TabIndex = 45
        Me.btn_Save.Text = "Save"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(744, 42)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 15)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Total"
        '
        'txt_PromosCount
        '
        Me.txt_PromosCount.EditValue = ""
        Me.txt_PromosCount.Enabled = False
        Me.txt_PromosCount.Location = New System.Drawing.Point(747, 58)
        Me.txt_PromosCount.Name = "txt_PromosCount"
        Me.txt_PromosCount.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PromosCount.Properties.Appearance.Options.UseFont = True
        Me.txt_PromosCount.Properties.AppearanceFocused.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PromosCount.Properties.AppearanceFocused.ForeColor = System.Drawing.Color.White
        Me.txt_PromosCount.Properties.AppearanceFocused.Options.UseFont = True
        Me.txt_PromosCount.Properties.AppearanceFocused.Options.UseForeColor = True
        Me.txt_PromosCount.Size = New System.Drawing.Size(69, 22)
        Me.txt_PromosCount.TabIndex = 43
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(494, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 15)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Store"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(42, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 15)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Customer"
        '
        'lbl_Title
        '
        Me.lbl_Title.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Title.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl_Title.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl_Title.Dock = System.Windows.Forms.DockStyle.Top
        Me.lbl_Title.Location = New System.Drawing.Point(0, 0)
        Me.lbl_Title.Name = "lbl_Title"
        Me.lbl_Title.Size = New System.Drawing.Size(1295, 32)
        Me.lbl_Title.TabIndex = 0
        Me.lbl_Title.Text = "Activate/Deactivate Promos"
        '
        'Promos_Source_Grid
        '
        Me.Promos_Source_Grid.AllowDrop = True
        Me.Promos_Source_Grid.AllowUserToAddRows = False
        Me.Promos_Source_Grid.AllowUserToDeleteRows = False
        Me.Promos_Source_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        Me.Promos_Source_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.Promos_Source_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Promos_Source_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Promos_Source_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Promos_Source_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Promos_Source_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.Promos_Source_Grid.ColumnHeadersHeight = 25
        Me.Promos_Source_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.Promos_Source_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Source_Promo_ID, Me.Source_Category, Me.Source_Title, Me.Source_FileName})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Promos_Source_Grid.DefaultCellStyle = DataGridViewCellStyle11
        Me.Promos_Source_Grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Promos_Source_Grid.EnableHeadersVisualStyles = False
        Me.Promos_Source_Grid.Location = New System.Drawing.Point(39, 86)
        Me.Promos_Source_Grid.Name = "Promos_Source_Grid"
        Me.Promos_Source_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Promos_Source_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.Promos_Source_Grid.RowHeadersVisible = False
        Me.Promos_Source_Grid.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Promos_Source_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Promos_Source_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Promos_Source_Grid.Size = New System.Drawing.Size(400, 560)
        Me.Promos_Source_Grid.TabIndex = 3
        '
        'Source_Promo_ID
        '
        Me.Source_Promo_ID.HeaderText = "Promo ID"
        Me.Source_Promo_ID.Name = "Source_Promo_ID"
        Me.Source_Promo_ID.Width = 80
        '
        'Source_Category
        '
        Me.Source_Category.HeaderText = "Category"
        Me.Source_Category.Name = "Source_Category"
        Me.Source_Category.Width = 80
        '
        'Source_Title
        '
        Me.Source_Title.HeaderText = "Title"
        Me.Source_Title.Name = "Source_Title"
        Me.Source_Title.Width = 220
        '
        'Source_FileName
        '
        Me.Source_FileName.HeaderText = "FileName"
        Me.Source_FileName.Name = "Source_FileName"
        Me.Source_FileName.Visible = False
        '
        'Promos_Target_Grid
        '
        Me.Promos_Target_Grid.AllowDrop = True
        Me.Promos_Target_Grid.AllowUserToAddRows = False
        Me.Promos_Target_Grid.AllowUserToDeleteRows = False
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        Me.Promos_Target_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        Me.Promos_Target_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Promos_Target_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Promos_Target_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Promos_Target_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Promos_Target_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.Promos_Target_Grid.ColumnHeadersHeight = 25
        Me.Promos_Target_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.Promos_Target_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Target_Promo_Id, Me.Target_Category, Me.Target_Title, Me.Target_FileName})
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Promos_Target_Grid.DefaultCellStyle = DataGridViewCellStyle15
        Me.Promos_Target_Grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Promos_Target_Grid.EnableHeadersVisualStyles = False
        Me.Promos_Target_Grid.Location = New System.Drawing.Point(497, 86)
        Me.Promos_Target_Grid.Name = "Promos_Target_Grid"
        Me.Promos_Target_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Promos_Target_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.Promos_Target_Grid.RowHeadersVisible = False
        Me.Promos_Target_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Promos_Target_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Promos_Target_Grid.Size = New System.Drawing.Size(400, 560)
        Me.Promos_Target_Grid.TabIndex = 4
        '
        'Target_Promo_Id
        '
        Me.Target_Promo_Id.HeaderText = "Promo ID"
        Me.Target_Promo_Id.Name = "Target_Promo_Id"
        Me.Target_Promo_Id.Width = 80
        '
        'Target_Category
        '
        Me.Target_Category.HeaderText = "Category"
        Me.Target_Category.Name = "Target_Category"
        Me.Target_Category.Width = 80
        '
        'Target_Title
        '
        Me.Target_Title.HeaderText = "Title"
        Me.Target_Title.Name = "Target_Title"
        Me.Target_Title.Width = 220
        '
        'Target_FileName
        '
        Me.Target_FileName.HeaderText = "FileName"
        Me.Target_FileName.Name = "Target_FileName"
        Me.Target_FileName.Visible = False
        '
        'frmActivatePromos
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1295, 742)
        Me.Controls.Add(Me.Promos_Target_Grid)
        Me.Controls.Add(Me.Promos_Source_Grid)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmActivatePromos"
        Me.Text = "Activate Songs"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.cmb_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PromosCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Promos_Source_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Promos_Target_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Promos_Source_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents Promos_Target_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents lbl_Title As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_PromosCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btn_Save As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmb_Store As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Source_Promo_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Source_Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Source_Title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Source_FileName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_Promo_Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_Title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_FileName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmb_Category As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
