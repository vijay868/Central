﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO
Imports DevExpress.Utils
Imports DevExpress.XtraEditors

Public Class frmConfig
    Dim dtTimes As DataTable
    Dim CurStore_Id, CurCustomer_Id As Integer
    Dim CategoryParentList As New List(Of String)
    Dim DynamicChecked As Boolean = False
    Dim CurCategoryText As String = ""
    Dim bDoNotValidate As Boolean = False

    Private Sub frmConfig_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If btn_save_edit_Genre.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                btn_save_edit_Genre.PerformClick()
            End If
        ElseIf btn_save_edit_category.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                btn_save_edit_category.PerformClick()
            End If
        ElseIf btn_save_edit_customer.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                btn_save_edit_customer.PerformClick()
            End If
        ElseIf btn_Audio_Save.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                btn_Audio_Save.PerformClick()
            End If
        ElseIf btn_StoreSave.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                btn_StoreSave.PerformClick()
            End If
        ElseIf btn_Save_FTP.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                'e.Cancel = True
                btn_Save_FTP.PerformClick()
            End If
        End If

    End Sub

    Private Sub frmConfig_Invalidated(ByVal sender As Object, ByVal e As System.Windows.Forms.InvalidateEventArgs) Handles Me.Invalidated

    End Sub

    Private Sub frmConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        bindGenreGrid()
        'bindGenreCategoriesGrid()
        bindCustomersGrid()

        Call CreateSongPromoRatioTable()
        'GridDayparts.Rows.Add(1, "", "", "", "")
        Call LoadHours()
        Call LoadCustomers()

        cmb_Audio_Format.Properties.Items.Add("AACPlus 128 Kbps")
        btn_save_edit_Genre.Enabled = False
        btn_save_edit_category.Enabled = False
        btn_Audio_Save.Enabled = False
        btn_Save_FTP.Enabled = False
        btn_StoreSave.Enabled = False
        btn_save_edit_customer.Enabled = False

    End Sub

    Private Sub bindCategoriesGrid()

        Dim dr As SqlDataReader
        Dim command As New SqlCommand

        Try
            Call OpenDBConnection()
            bDoNotValidate = True 'disabling cell validating while clearing the rows
            Category_Info_Grid.Rows.Clear()
            bDoNotValidate = False
            'Try
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            If Media_Group.SelectedIndex = 0 Then
                command.CommandText = "ProcGetGenreCategoriesList"
                command.Parameters.AddWithValue("@Genre_Id", Convert.ToInt32(cmb_Parent_Category.EditValue.ToString))
            Else
                command.CommandText = "ProcGetPromoCategoriesList"
                command.Parameters.AddWithValue("@Customer_Id", Convert.ToInt32(cmb_Parent_Category.EditValue.ToString))
            End If
            dr = command.ExecuteReader

            Dim i = 0
            If dr.HasRows Then
                While dr.Read
                    If Media_Group.SelectedIndex = 0 Then
                        Category_Info_Grid.Rows.Add(dr(0).ToString, dr(1).ToString, dr(2).ToString, dr(3).ToString)
                    Else
                        Category_Info_Grid.Rows.Add(dr(0).ToString, dr(1).ToString, dr(2).ToString)
                    End If
                    i = i + 1
                End While
            End If
            'Category_Info_Grid.Rows.Add("", "", Strings.Left(cmb_Parent_Category.Text, 3).ToUpper & "'")
            dr.Close()

            Call CloseDBConnection()
            ' Genre_Info_Grid.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Sub
   
    Private Sub bindGenreGrid()

        Dim dr As SqlDataReader
        Dim command As New SqlCommand


        Call OpenDBConnection()

        Genre_Info_Grid.Rows.Clear()
     
        'Try
        command.Connection = DBConn
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "ProcGetGenresList"
        dr = command.ExecuteReader
        If dr.HasRows Then
            While dr.Read
            
                Genre_Info_Grid.Rows.Add(dr(0).ToString, dr(1).ToString)
            End While
        End If
        dr.Close()

        Call CloseDBConnection()
        Genre_Info_Grid.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
        'Catch ex As Exception
        'MsgBox(ex.ToString)
        'End Try
    End Sub
   
    Private Sub LoadCustomers()
        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)
        'cmb_Customer.Properties.Columns(0).Visible = False
        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""

        cmb_Parent_Category.Properties.DisplayMember = "Customer_Name"
        cmb_Parent_Category.Properties.ValueMember = "Customer_ID"
        cmb_Parent_Category.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Parent_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Parent_Category.Properties.ForceInitialize()
        cmb_Parent_Category.Properties.PopulateColumns()
        cmb_Parent_Category.Properties.Columns(0).Visible = False
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)
        '
        cmb_Parent_Category.Properties.ShowFooter = False
        cmb_Parent_Category.Properties.ShowHeader = False

        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub
    Private Sub LoadGenres()
        Dim cmdGenre As New SqlCommand, adpGenre As New SqlDataAdapter, dsGenre As New DataSet

        Call OpenDBConnection()

        With cmdGenre
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdGenre.CommandText = "ProcGetGenresList"
        adpGenre.SelectCommand = cmdGenre
        adpGenre.Fill(dsGenre, "GenreList")


        cmb_Parent_Category.Properties.DisplayMember = "Genre_Name"
        cmb_Parent_Category.Properties.ValueMember = "Genre_ID"
        cmb_Parent_Category.Properties.DataSource = dsGenre.Tables("GenreList")
        cmb_Parent_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Parent_Category.Properties.ForceInitialize()
        cmb_Parent_Category.Properties.PopulateColumns()
        'MsgBox(cmb_Genre.Properties.Columns.VisibleCount)
        cmb_Parent_Category.Properties.Columns(0).Visible = False
        cmb_Parent_Category.Properties.ShowFooter = False
        cmb_Parent_Category.Properties.ShowHeader = False

        'cmb_Parent_Category.EditValue = ""


        Call CloseDBConnection()

        dsGenre.Dispose()
        adpGenre.Dispose()

    End Sub
    Private Sub bindCustomersGrid()

        Dim dr As SqlDataReader
        Dim command As New SqlCommand


        Call OpenDBConnection()
        Customers_Info_Grid.Rows.Clear()
       


        Try

            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcGetCustomersList"
            dr = command.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                  
                    Customers_Info_Grid.Rows.Add(dr(0).ToString, dr(1).ToString)
                End While
            End If
            dr.Close()
           

            Call CloseDBConnection()
            Customers_Info_Grid.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Genre_Info_Grid_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles Genre_Info_Grid.CellBeginEdit
        btn_save_edit_Genre.Enabled = True
    End Sub
    Private Sub Category_Info_Grid_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles Category_Info_Grid.CellBeginEdit
        btn_save_edit_category.Enabled = True
    End Sub
    Private Sub Genre_Info_Grid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Genre_Info_Grid.Click
        Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(0).ReadOnly = True
    End Sub
    'Private Sub Category_Info_Grid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Category_Info_Grid.Click
    '    Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(0).ReadOnly = True
    'End Sub
    Private Sub btn_save_edit_Genre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_edit_Genre.Click
        Dim StrMsg, StrEditMsg As String, GenreName As String
        Dim OutResult As String
        Dim EditOutResult As String
        Cursor = Cursors.WaitCursor
        Call OpenDBConnection()
        For i = 0 To Genre_Info_Grid.Rows.Count - 1
            If Genre_Info_Grid.Rows(i).Cells(0).Value = "" Then
                If Not Genre_Info_Grid.Rows(i).Cells(1).Value = "" Then
                    OutResult = AddNewGenre(Genre_Info_Grid.Rows(i).Cells(1).Value, Genre_Info_Grid.CurrentRow.Index, DBConn)
                    GenreName = Genre_Info_Grid.Rows(i).Cells(1).Value
                    If OutResult <> "Success" Then
                        StrMsg = StrMsg + vbNewLine + Genre_Info_Grid.Rows(i).Cells(1).Value + " : " + OutResult
                    End If
                End If
            Else
                If Not Genre_Info_Grid.Rows(i).Cells(1).Value = "" Then
                    EditOutResult = EditGenre(Genre_Info_Grid.Rows(i).Cells(1).Value, Genre_Info_Grid.Rows(i).Cells(0).Value, i, DBConn)
                    GenreName = Genre_Info_Grid.Rows(i).Cells(1).Value
                    If EditOutResult <> "Success" Then
                        StrEditMsg = StrEditMsg + vbNewLine + Genre_Info_Grid.Rows(i).Cells(1).Value + " : " + EditOutResult
                    End If
                End If
            End If
        Next
        If StrMsg = "" Then
            '  XtraMessageBox.Show("Data saved successfully", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            XtraMessageBox.Show(StrMsg, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        Call CloseDBConnection()
        bindGenreGrid()
        Call SelectRowInGrid(Genre_Info_Grid, GenreName, 1)

        Cursor = Cursors.Default
        btn_save_edit_Genre.Enabled = False

    End Sub
    Private Sub SelectRowInGrid(ByRef GridObj As DataGridView, ByVal ValueToBeSelected As String, ByVal ColNumber As Integer)
        For i As Integer = 0 To GridObj.RowCount - 1
            If GridObj.Rows(i).Cells(ColNumber).Value = ValueToBeSelected Then
                GridObj.CurrentCell = GridObj(ColNumber, i)
                Exit For
            End If
        Next
    End Sub
    Public Function EditGenreCategory(ByVal GenreID As String, ByVal CategoryID As Integer, ByVal CategoryName As String, ByVal CategoryCode As String, ByVal Rotation As Integer) As String
        Dim command As New SqlCommand
        Dim ds As New DataSet
        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcModifyGenreCategory"
            command.Parameters.AddWithValue("@Genre_Category_ID", CategoryID).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Genre_ID", GenreID).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_New_Name", CategoryName).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_New_Code", CategoryCode).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Rotation", Rotation).Direction = ParameterDirection.Input
            '@Category_New_Name varchar(150),@Category_New_Code varchar(50)
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()
            Call CloseDBConnection()
            Return command.Parameters("@Result").Value

        Catch ex As Exception
            Return "Failed"
        End Try
    End Function
    Public Function EditPromoCategory(ByVal CustomerID As Integer, ByVal CategoryID As Integer, ByVal CategoryName As String, ByVal CategoryCode As String) As String
        Dim command As New SqlCommand
        Dim ds As New DataSet
        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcModifyPromoCategory"
            command.Parameters.AddWithValue("@Customer_ID", CustomerID).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Promo_Category_ID", CategoryID).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_New_Name", CategoryName).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_New_Code", CategoryCode).Direction = ParameterDirection.Input
            '@Category_New_Name varchar(150),@Category_New_Code varchar(50)
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()
            Call CloseDBConnection()
            Return command.Parameters("@Result").Value

        Catch ex As Exception
            Return "Failed"
        End Try
    End Function
    Public Function EditGenre(ByVal GenreName As String, ByVal GenreID As Integer, ByVal SelectedRowIndex As Integer, ByRef connection As SqlConnection) As String
        Dim command As New SqlCommand
        Dim ds As New DataSet
        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcModifyGenre"
            command.Parameters.AddWithValue("@Genre_New_Name", GenreName).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Genre_Id", GenreID).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()
            Call CloseDBConnection()
            Return command.Parameters("@Result").Value

        Catch ex As Exception
            Return "Failed"
        End Try
    End Function
    Public Sub DeleteGenre(ByVal GenreName As String, ByVal GenreID As Integer, ByVal SelectedRowIndex As Integer)

        Dim command As New SqlCommand
        Dim ds As New DataSet
        Call OpenDBConnection()
        Try

            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcDeleteGenre"
            command.Parameters.AddWithValue("@Genre_Id", GenreID).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()

            If command.Parameters("@Result").Value.ToString <> "Success" Then
                XtraMessageBox.Show(command.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK)
            Else
                bindGenreGrid()
            End If

            Call CloseDBConnection()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Sub DeleteCategory(ByVal CategoryID As Integer, ByVal SelectedRowIndex As Integer)

        Dim command As New SqlCommand
        Dim ds As New DataSet
        Call OpenDBConnection()
        Try

            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            If Media_Group.SelectedIndex = 0 Then
                command.CommandText = "ProcDeleteGenreCategory"
            Else
                command.CommandText = "ProcDeletePromoCategory"
            End If
            command.Parameters.AddWithValue("@Category_Id", CategoryID).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()

            If command.Parameters("@Result").Value.ToString <> "Success" Then
                XtraMessageBox.Show(command.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK)
            Else
                If Media_Group.SelectedIndex = 0 Then

                    Call bindCategoriesGrid()
                Else
                    Call bindCategoriesGrid()
                End If

            End If

            Call CloseDBConnection()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Function AddNewGenre(ByVal GenreName As String, ByVal SelectedRowIndex As Integer, ByRef connection As SqlConnection) As String
        Dim command As New SqlCommand

        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcAddGenre"
            command.Parameters.AddWithValue("@Genre_Name", GenreName).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()
            Call CloseDBConnection()
            Return command.Parameters("@Result").Value

        Catch ex As Exception
            Return "Failed"
        End Try
    End Function
    Public Function AddNewGenreCategory(ByVal GenreID As Integer, ByVal CategoryName As String, ByVal CategoryCode As String, ByVal Rotation As Integer) As String
        Dim command As New SqlCommand

        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcAddGenreCategory"
            command.Parameters.AddWithValue("@Genre_ID", GenreID).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_Name", CategoryName).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_Code", CategoryCode).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Rotation", Rotation).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()
            Call CloseDBConnection()
            Return command.Parameters("@Result").Value

        Catch ex As Exception
            Return "Failed"
        End Try
    End Function
    Public Function AddNewPromoCategory(ByVal CustomerID As Integer, ByVal CategoryName As String, ByVal CategoryCode As String) As String
        Dim command As New SqlCommand

        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcAddPromoCategory"
            command.Parameters.AddWithValue("@Customer_ID", CustomerID).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_Name", CategoryName).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_Code", CategoryCode).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()
            Call CloseDBConnection()
            Return command.Parameters("@Result").Value

        Catch ex As Exception
            Return "Failed"
        End Try
    End Function
    Private Sub Genre_Info_Grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Genre_Info_Grid.KeyDown
        If e.KeyCode = Keys.Delete AndAlso Genre_Info_Grid.RowCount > 0 Then
            'If XtraMessageBox.Show("Do you want to delete the selected 'Genre'?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            '    DeleteGenre(Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(1).Value, Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(0).Value, Genre_Info_Grid.CurrentRow.Index)
            'End If
            If Not Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(1).Value Is Nothing Then
                DeleteGenre(Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(1).Value, Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(0).Value, Genre_Info_Grid.CurrentRow.Index)
            End If
        End If
    End Sub
    Private Sub Category_Info_Grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Category_Info_Grid.KeyDown
        If e.KeyCode = Keys.Delete AndAlso Category_Info_Grid.RowCount > 0 Then
            'If XtraMessageBox.Show("Do you want to delete the selected 'Genre'?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            '    DeleteGenre(Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(1).Value, Genre_Info_Grid.Rows(Genre_Info_Grid.CurrentRow.Index).Cells(0).Value, Genre_Info_Grid.CurrentRow.Index)
            'End If
            If Not Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells("Category_ID").Value Is Nothing Then
                DeleteCategory(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells("Category_ID").Value, Category_Info_Grid.CurrentRow.Index)
            End If
        End If
    End Sub
    Private Sub btn_save_edit_customer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_edit_customer.Click

        Dim StrMsg, StrEditMsg As String, CustomerName As String = ""
        Dim OutResult As String
        Dim EditOutResult As String
        Cursor = Cursors.WaitCursor
        Call OpenDBConnection()
        For i = 0 To Customers_Info_Grid.Rows.Count - 1
            If Customers_Info_Grid.Rows(i).Cells(0).Value = "" Then
                If Not Customers_Info_Grid.Rows(i).Cells(1).Value = "" Then
                    OutResult = AddNewCustomer(Customers_Info_Grid.Rows(i).Cells(1).Value, Customers_Info_Grid.CurrentRow.Index, DBConn)
                    CustomerName = Customers_Info_Grid.Rows(i).Cells(1).Value
                    If OutResult <> "Success" Then
                        StrMsg = StrMsg + vbNewLine + Customers_Info_Grid.Rows(i).Cells(1).Value + " : " + OutResult
                    End If
                End If
            Else
                If Not Customers_Info_Grid.Rows(i).Cells(1).Value = "" Then
                    EditOutResult = EditCustomer(Customers_Info_Grid.Rows(i).Cells(1).Value, Customers_Info_Grid.Rows(i).Cells(0).Value, i, DBConn)
                    CustomerName = Customers_Info_Grid.Rows(i).Cells(1).Value
                    If EditOutResult <> "Success" Then
                        StrEditMsg = StrEditMsg + vbNewLine + Customers_Info_Grid.Rows(i).Cells(1).Value + " : " + EditOutResult
                    End If
                End If
            End If
        Next
        If StrMsg = "" Then
            'XtraMessageBox.Show("Data saved successfully", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            XtraMessageBox.Show(StrMsg, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        Call CloseDBConnection()
        Cursor = Cursors.Default
        bindCustomersGrid()
        SelectRowInGrid(Customers_Info_Grid, CustomerName, 1)
        btn_save_edit_customer.Enabled = False

    End Sub

    Private Sub Customers_Info_Grid_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles Customers_Info_Grid.CellBeginEdit
        btn_save_edit_customer.Enabled = True

    End Sub


    Private Sub Customers_Info_Grid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Customers_Info_Grid.Click
        Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(0).ReadOnly = True
    End Sub
    Public Function EditCustomer(ByVal CustomerName As String, ByVal CustomerID As Integer, ByVal SelectedRowIndex As Integer, ByRef connection As SqlConnection) As String
        Dim command As New SqlCommand

        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcModifyCustomer"
            command.Parameters.AddWithValue("@Customer_New_Name", CustomerName).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Customer_Id", CustomerID).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()
            Call CloseDBConnection()
            Return command.Parameters("@Result").Value

        Catch ex As Exception
            Return "Failed"
        End Try
    End Function
    Public Sub DeleteCustomer(ByVal CustomerName As String, ByVal CustomerID As Integer, ByVal SelectedRowIndex As Integer)

        Dim command As New SqlCommand
       
        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcDeleteCustomer"
            command.Parameters.AddWithValue("@Customer_Id", CustomerID).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()

            If command.Parameters("@Result").Value <> "Success" Then
                XtraMessageBox.Show(command.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK)
            Else
                bindCustomersGrid()
            End If
            Call CloseDBConnection()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Sub DeleteStore(ByVal StoreName As String, ByVal StoreID As Integer, ByVal SelectedRowIndex As Integer)

        Dim command As New SqlCommand


        Try
            Cursor = Cursors.WaitCursor
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcDeleteStore"
            command.Parameters.AddWithValue("@Store_Id", StoreID).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()

            If command.Parameters("@Result").Value <> "Success" Then
                XtraMessageBox.Show(command.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK)
            Else
                Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString))
            End If
            Call CloseDBConnection()
            Cursor = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Function AddNewCustomer(ByVal CustomerName As String, ByVal SelectedRowIndex As Integer, ByRef connection As SqlConnection) As String
        Dim command As New SqlCommand

        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcAddCustomer"
            command.Parameters.AddWithValue("@Customer_Name", CustomerName).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()
            Call CloseDBConnection()
            Return command.Parameters("@Result").Value
        Catch ex As Exception
            Return "Failed"
        End Try
    End Function

    Private Sub Customers_Info_Grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Customers_Info_Grid.KeyDown
        If e.KeyCode = Keys.Delete AndAlso Customers_Info_Grid.RowCount > 0 Then
            'If XtraMessageBox.Show("Do you want to delete the selected 'Customer'?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            '    DeleteCustomer(Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(1).Value, Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(0).Value, Customers_Info_Grid.CurrentRow.Index)
            'End If
            If Not Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(1).Value Is Nothing Then
                DeleteCustomer(Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(1).Value, Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(0).Value, Customers_Info_Grid.CurrentRow.Index)
            End If
        End If
    End Sub
    'Private Sub Category_Info_Grid_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles Category_Info_Grid.EditingControlShowing
    '    ' Only for a DatagridComboBoxColumn at ColumnIndex 1.
    '    If Category_Info_Grid.CurrentCell.ColumnIndex = 1 Then
    '        Dim combo As System.Windows.Forms.ComboBox = CType(e.Control, System.Windows.Forms.ComboBox)
    '        If (combo IsNot Nothing) Then
    '            ' Remove an existing event-handler, if present, to avoid 
    '            ' adding multiple handlers when the editing control is reused.
    '            RemoveHandler combo.SelectedIndexChanged, New EventHandler(AddressOf CatParentComboBox_SelectedIndexChanged)

    '            ' Add the event handler. 
    '            AddHandler combo.SelectedIndexChanged, New EventHandler(AddressOf CatParentComboBox_SelectedIndexChanged)
    '        End If

    '    End If
    'End Sub
    'Private Sub CatParentComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim combo As System.Windows.Forms.ComboBox = CType(sender, System.Windows.Forms.ComboBox)
    '    Dim ExistingCode As String

    '    cmb_Cat_Parent.SelectedIndex = combo.SelectedIndex
    '    If Not combo.SelectedIndex = -1 Then
    '        Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells("Parent_Cat_ID").Value = cmb_Cat_Parent.SelectedItem.ToString
    '        ExistingCode = "" & Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells("Category_Code").Value

    '        If ExistingCode.Length = 6 AndAlso Strings.Left(ExistingCode, 4) <> Strings.Left(combo.SelectedValue, 3).ToUpper & "-" Then
    '            ExistingCode = Strings.Right(ExistingCode, 2)
    '        End If

    '        If ExistingCode.Length = 6 Then
    '            Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells("Category_Code").Value = ExistingCode
    '        Else
    '            Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells("Category_Code").Value = Strings.Left(combo.SelectedValue, 3).ToUpper & "-" & ExistingCode
    '        End If

    '    End If


    '    'If GridDayparts.CurrentCell.ColumnIndex = 2 Then
    '    '    If combo.SelectedIndex < cmb_To.SelectedIndex Then
    '    '        If (GridDayparts.CurrentRow.Index + 1 = GridDayparts.Rows.Count) Then
    '    '            GridDayparts.Rows.Add(GridDayparts.Rows.Count + 1, combo.SelectedItem, cmb_To.SelectedItem)
    '    '            GridDayparts.Rows(GridDayparts.Rows.Count - 1).Cells(1).ReadOnly = True
    '    '            If GridDayparts.Rows.Count > 3 Then
    '    '                Call AddDaypartColumn("Daypart" & GridDayparts.Rows.Count.ToString)
    '    '            End If
    '    '            'GridRatio.Columns(GridDayparts.Rows.Count - 1).HeaderText = "Daypart" & GridDayparts.Rows.Count.ToString

    '    '            GridRatio.Columns(GridDayparts.Rows.Count + 1).HeaderText = "Daypart" & GridDayparts.Rows.Count.ToString
    '    '            Call AddDefaultRatio(GridDayparts.Rows.Count + 1)
    '    '        Else
    '    '            GridDayparts.Rows(GridDayparts.CurrentRow.Index + 1).Cells(1).Value = combo.SelectedItem
    '    '        End If
    '    '    ElseIf combo.SelectedIndex > cmb_To.SelectedIndex Then
    '    '        combo.SelectedIndex = cmb_To.SelectedIndex
    '    '    ElseIf combo.SelectedIndex = cmb_To.SelectedIndex AndAlso (GridDayparts.CurrentRow.Index + 1 < GridDayparts.Rows.Count) Then
    '    '        For i As Integer = GridDayparts.CurrentRow.Index + 2 To GridDayparts.Rows.Count
    '    '            If i <= 3 Then
    '    '                Call RemoveDaypartValues(i + 1)
    '    '            Else
    '    '                Call RemoveDaypartColumn("Daypart" & i.ToString)
    '    '            End If
    '    '            GridDayparts.Rows.RemoveAt(GridDayparts.Rows.Count - 1)
    '    '            'DataGridView4.Rows.RemoveAt(i - 1)
    '    '        Next
    '    '    End If
    '    'End If
    '    'btn_StoreSave.Enabled = True

    '    'Dim ComboColumn As DataGridViewComboBoxCell = DataGridView4.Rows(0).Cells(0)
    '    'DataGridView4.Rows(0).Cells(0).ReadOnly = True
    'End Sub
    ''Private Sub gridDayparts_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles GridDayparts.EditingControlShowing
    ''    ' Only for a DatagridComboBoxColumn at ColumnIndex 1.
    ''    If GridDayparts.CurrentCell.ColumnIndex = 1 Or GridDayparts.CurrentCell.ColumnIndex = 2 Then
    ''        Dim combo As System.Windows.Forms.ComboBox = CType(e.Control, System.Windows.Forms.ComboBox)
    ''        If (combo IsNot Nothing) Then
    ''            ' Remove an existing event-handler, if present, to avoid 
    ''            ' adding multiple handlers when the editing control is reused.
    ''            RemoveHandler combo.SelectedIndexChanged, New EventHandler(AddressOf ComboBox_SelectedIndexChanged)

    ''            ' Add the event handler. 
    ''            AddHandler combo.SelectedIndexChanged, New EventHandler(AddressOf ComboBox_SelectedIndexChanged)
    ''        End If
    ''    End If
    ''End Sub
    Private Sub AddDefaultRatio(ByVal ColNumber As Integer)
        For i As Integer = 0 To 6
            GridRatio.Rows(i).Cells(ColNumber).Value = "1-3"
        Next
    End Sub
    Private Sub GridDayPartToTimeChanged()
        Dim NewRow As Boolean = False

        If cmb_Daypart_To.SelectedIndex < cmb_To.SelectedIndex Then
            If (GridDayparts.CurrentRow.Index + 1 = GridDayparts.Rows.Count) Then
AddRow:
                If GridDayparts.Rows.Count <> 0 Then GridDayparts.Rows(GridDayparts.CurrentRow.Index).Cells(2).Value = cmb_Daypart_To.SelectedItem

                GridDayparts.Rows.Add(GridDayparts.Rows.Count + 1, IIf(NewRow, cmb_Daypart_From.SelectedItem, cmb_Daypart_To.SelectedItem), cmb_To.SelectedItem, txt_Genre_Category.Text, cmb_Genre_Category.Text, txt_Promo_Category.Text, cmb_Promo_Category.Text)
                GridDayparts.Rows(GridDayparts.Rows.Count - 1).Cells(1).ReadOnly = True
                If GridDayparts.Rows.Count > 4 Then
                    Call AddDaypartColumn("Daypart" & GridDayparts.Rows.Count.ToString)
                End If
                'GridRatio.Columns(GridDayparts.Rows.Count - 1).HeaderText = "Daypart" & GridDayparts.Rows.Count.ToString

                GridRatio.Columns(GridDayparts.Rows.Count + 1).HeaderText = "Daypart" & GridDayparts.Rows.Count.ToString
                Call AddDefaultRatio(GridDayparts.Rows.Count + 1)
                DynamicChecked = True
                tvw_Genre_Category.Nodes(0).Checked = True
                CheckAllNodes(True)
                If GridDayparts.Rows.Count < GridDayparts.CurrentRow.Index + 1 Then
                    ReadSelection(GridDayparts.CurrentRow.Index)
                Else
                    ReadSelection(GridDayparts.CurrentRow.Index + 1)
                End If


                For i As Integer = 1 To cmb_Promo_Category.Properties.Items.Count - 1
                    cmb_Promo_Category.Properties.Items(i).CheckState = CheckState.Checked
                Next
                DynamicChecked = False
            Else
                GridDayparts.Rows(GridDayparts.CurrentRow.Index).Cells(2).Value = cmb_Daypart_To.SelectedItem
                GridDayparts.Rows(GridDayparts.CurrentRow.Index + 1).Cells(1).Value = cmb_Daypart_To.SelectedItem
            End If
        ElseIf cmb_Daypart_To.SelectedIndex > cmb_To.SelectedIndex Then
            cmb_Daypart_To.SelectedIndex = cmb_To.SelectedIndex
            GridDayparts.Rows(GridDayparts.CurrentRow.Index).Cells(2).Value = cmb_Daypart_To.SelectedItem
        ElseIf cmb_Daypart_To.SelectedIndex = cmb_To.SelectedIndex Then
            If GridDayparts.Rows.Count = 0 Then
                NewRow = True
                GoTo AddRow
            ElseIf (GridDayparts.CurrentRow.Index + 1 < GridDayparts.Rows.Count) Then
                GridDayparts.Rows(GridDayparts.CurrentRow.Index).Cells(2).Value = cmb_Daypart_To.SelectedItem
                For i As Integer = GridDayparts.CurrentRow.Index + 2 To GridDayparts.Rows.Count
                    If i <= 5 Then
                        Call RemoveDaypartValues(i + 1)
                    Else
                        Call RemoveDaypartColumn("Daypart" & i.ToString)
                    End If
                    GridDayparts.Rows.RemoveAt(GridDayparts.Rows.Count - 1)
                    'DataGridView4.Rows.RemoveAt(i - 1)
                Next
            End If
        End If
        btn_StoreSave.Enabled = True

    End Sub



    'Private Sub ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim combo As System.Windows.Forms.ComboBox = CType(sender, System.Windows.Forms.ComboBox)

    '    If gridDayparts.CurrentCell.ColumnIndex = 2 Then
    '        If combo.SelectedIndex < cmb_To.SelectedIndex Then
    '            If (GridDayparts.CurrentRow.Index + 1 = GridDayparts.Rows.Count) Then
    '                GridDayparts.Rows.Add(GridDayparts.Rows.Count + 1, combo.SelectedItem, cmb_To.SelectedItem, "", "")
    '                GridDayparts.Rows(GridDayparts.Rows.Count - 1).Cells(1).ReadOnly = True
    '                If GridDayparts.Rows.Count > 5 Then
    '                    Call AddDaypartColumn("Daypart" & GridDayparts.Rows.Count.ToString)
    '                End If
    '                'GridRatio.Columns(GridDayparts.Rows.Count - 1).HeaderText = "Daypart" & GridDayparts.Rows.Count.ToString

    '                GridRatio.Columns(GridDayparts.Rows.Count + 1).HeaderText = "Daypart" & GridDayparts.Rows.Count.ToString
    '                Call AddDefaultRatio(GridDayparts.Rows.Count + 1)
    '            Else
    '                GridDayparts.Rows(GridDayparts.CurrentRow.Index + 1).Cells(1).Value = combo.SelectedItem
    '            End If
    '        ElseIf combo.SelectedIndex > cmb_To.SelectedIndex Then
    '            combo.SelectedIndex = cmb_To.SelectedIndex
    '        ElseIf combo.SelectedIndex = cmb_To.SelectedIndex AndAlso (GridDayparts.CurrentRow.Index + 1 < GridDayparts.Rows.Count) Then
    '            For i As Integer = GridDayparts.CurrentRow.Index + 2 To GridDayparts.Rows.Count
    '                If i <= 5 Then
    '                    Call RemoveDaypartValues(i + 1)
    '                Else
    '                    Call RemoveDaypartColumn("Daypart" & i.ToString)
    '                End If
    '                GridDayparts.Rows.RemoveAt(GridDayparts.Rows.Count - 1)
    '                'DataGridView4.Rows.RemoveAt(i - 1)
    '            Next
    '        End If
    '    End If
    '    btn_StoreSave.Enabled = True

    '    'Dim ComboColumn As DataGridViewComboBoxCell = DataGridView4.Rows(0).Cells(0)
    '    'DataGridView4.Rows(0).Cells(0).ReadOnly = True
    'End Sub
    
    Private Sub RemoveDaypartValues(ByVal ColumnNumber As Integer)
        GridRatio.Columns(ColumnNumber).HeaderText = "<Undefined>"
        For i As Integer = 0 To 6
            GridRatio.Rows(i).Cells(ColumnNumber).Value = ""
        Next
    End Sub
    Private Sub RemoveDaypartColumn(ByVal strColumnName As String)
        'Dim dsTempTable As DataSet = GridRatio.DataSource
        Dim dt As DataTable = GridRatio.DataSource
        dt.Columns.Remove(strColumnName)
        dt.AcceptChanges()
    End Sub
    Private Sub AddDaypartColumn(ByVal strColumnName As String)
        'Dim dsTempTable As DataSet = GridRatio.DataSource
        Dim dt As DataTable = GridRatio.DataSource
        dt.Columns.Add(strColumnName, System.Type.GetType("System.String"))
        dt.AcceptChanges()

        GridRatio.Columns(GridRatio.ColumnCount - 1).SortMode = DataGridViewColumnSortMode.NotSortable

    End Sub

    Private Sub CreateSongPromoRatioTable()

        'with binding
        Dim dtRatio As DataTable
        dtRatio = New DataTable("SongPromoRatio")


        Dim dcColumn As DataColumn

        'RowNumber
        dcColumn = New DataColumn
        dcColumn.ColumnName = "RowNumber"
        dcColumn.DataType = System.Type.GetType("System.Int16")
        dtRatio.Columns.Add(dcColumn)
        dcColumn = Nothing

        'DayName
        dcColumn = New DataColumn
        dcColumn.ColumnName = "Day"
        dcColumn.DataType = System.Type.GetType("System.String")
        dtRatio.Columns.Add(dcColumn)
        dcColumn = Nothing

        'Daypart1
        dcColumn = New DataColumn
        dcColumn.ColumnName = "Daypart1"

        dcColumn.DataType = System.Type.GetType("System.String")
        dtRatio.Columns.Add(dcColumn)
        dcColumn = Nothing

        'Daypart2
        dcColumn = New DataColumn
        dcColumn.ColumnName = "Daypart2"

        dcColumn.DataType = System.Type.GetType("System.String")
        dtRatio.Columns.Add(dcColumn)
        dcColumn = Nothing

        'Daypart3
        dcColumn = New DataColumn
        dcColumn.ColumnName = "Daypart3"

        dcColumn.DataType = System.Type.GetType("System.String")
        dtRatio.Columns.Add(dcColumn)
        dcColumn = Nothing

        'Daypart4
        dcColumn = New DataColumn
        dcColumn.ColumnName = "Daypart4"

        dcColumn.DataType = System.Type.GetType("System.String")
        dtRatio.Columns.Add(dcColumn)
        dcColumn = Nothing

        'Daypart5
        dcColumn = New DataColumn
        dcColumn.ColumnName = "Daypart5"

        dcColumn.DataType = System.Type.GetType("System.String")
        dtRatio.Columns.Add(dcColumn)
        dcColumn = Nothing

        Dim iRow As Integer
        Dim drRow As DataRow
        For iRow = 1 To 7
            drRow = dtRatio.NewRow
            drRow(0) = iRow : drRow(1) = GetDayName(iRow) ': drRow(2) = "" : drRow(3) = "" : drRow(4) = ""
            dtRatio.Rows.Add(drRow)
        Next iRow

        GridRatio.Columns.Clear()

        GridRatio.DataSource = dtRatio
        GridRatio.Columns(0).Visible = False
        GridRatio.Columns(1).ReadOnly = True
        GridRatio.Columns(1).Width = 80
        GridRatio.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
        GridRatio.Columns(2).HeaderText = "<Undefined>"
        GridRatio.Columns(2).Width = 80
        GridRatio.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
        GridRatio.Columns(3).HeaderText = "<Undefined>"
        GridRatio.Columns(3).Width = 80
        GridRatio.Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
        GridRatio.Columns(4).HeaderText = "<Undefined>"
        GridRatio.Columns(4).Width = 80
        GridRatio.Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
        GridRatio.Columns(5).HeaderText = "<Undefined>"
        GridRatio.Columns(5).Width = 78
        GridRatio.Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
        GridRatio.Columns(6).HeaderText = "<Undefined>"
        GridRatio.Columns(6).Width = 78
        GridRatio.Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
        GridRatio.Refresh()
        'For iRow = 1 To 14
        '    GridViewSearch.MakeRowVisible(iRow, False)
        'Next
    End Sub
    Private Function GetDayName(ByVal DayNumber As Integer) As String
        Dim strDay As String = ""
        Select Case DayNumber
            Case 1
                strDay = "Monday"
            Case 2
                strDay = "Tuesday"
            Case 3
                strDay = "Wednesday"
            Case 4
                strDay = "Thursday"
            Case 5
                strDay = "Friday"
            Case 6
                strDay = "Saturday"
            Case 7
                strDay = "Sunday"
        End Select
        Return strDay
    End Function
    Private Sub cmb_From_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_From.Validated
        If cmb_From.SelectedItem = Nothing Then Exit Sub
        cmb_Daypart_From.Text = cmb_From.SelectedItem.ToString
        If GridDayparts.Rows.Count <> 0 Then GridDayparts.Rows(0).Cells(1).Value = cmb_From.SelectedItem.ToString
        'GridDayparts.Rows(0).Cells(1).ReadOnly = True
        btn_StoreSave.Enabled = True

    End Sub

    Private Sub cmb_From_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmb_From.Validating
        If cmb_From.SelectedIndex >= cmb_To.SelectedIndex AndAlso cmb_To.SelectedIndex <> -1 Then
            MessageBox.Show("From time can not later than 'To' time", "Alert", MessageBoxButtons.OK)
            e.Cancel = True
        End If
    End Sub
    Private Sub cmb_To_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmb_To.Validating
        If cmb_From.SelectedIndex >= cmb_To.SelectedIndex AndAlso cmb_From.SelectedIndex <> -1 Then
            MessageBox.Show("To time can not earlier than 'From' time", "Alert", MessageBoxButtons.OK)
            e.Cancel = True
        End If
    End Sub
    Private Sub cmb_To_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_To.Validated
        If cmb_To.SelectedItem = Nothing Then Exit Sub

        If GridDayparts.RowCount = 1 Then
            cmb_Daypart_To.Text = cmb_To.SelectedItem.ToString
            GridDayparts.Rows(0).Cells(2).Value = cmb_To.SelectedItem.ToString
            GridRatio.Columns(2).HeaderText = "Daypart1"
            Call AddDefaultRatio(2)
            btn_StoreSave.Enabled = True
        Else
            GridDayparts.Rows(GridDayparts.RowCount - 1).Cells(2).Value = cmb_To.SelectedItem.ToString

        End If
    End Sub
    Private Function GetTimeValue(Optional ByVal Tovalue As Boolean = False, Optional ByVal TimeString As String = "") As String
        Dim strTime As String

        strTime = "1900-01-01 "
        If TimeString.Contains("12 AM") Then
            If Tovalue Then strTime = "1900-01-01 "
            TimeString = "00"
        ElseIf TimeString.Contains(" AM") Then
            TimeString = Convert.ToInt32(TimeString.Replace(" AM", "")).ToString("00")
        ElseIf TimeString = "12 PM" Then
            TimeString = "12"
        ElseIf TimeString.Contains(" PM") Then
            TimeString = Convert.ToInt32(TimeString.Replace(" PM", "") + 12).ToString("00")
        End If
        strTime += TimeString + ":00:00"
        Return strTime
    End Function
    Private Sub LoadHours()
        Dim hr As Integer, hrAdd As String

        hrAdd = "12 AM"
        cmb_From.Properties.Items.Add(hrAdd)

        For hr = 1 To 23


            If hr >= 1 And hr <= 11 Then
                hrAdd = hr & " AM"

            ElseIf hr = 12 Then
                hrAdd = "12 PM"

            Else
                hrAdd = hr - 12 & " PM"

            End If
            cmb_From.Properties.Items.Add(hrAdd)
            cmb_To.Properties.Items.Add(hrAdd)


        Next
        hrAdd = "12 AM"
        'cmb_From.Properties.Items.Add(hrAdd)
        cmb_To.Properties.Items.Add(hrAdd)
    End Sub

    Private Sub GridRatio_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles GridRatio.CellBeginEdit
        GridRatio.Rows(e.RowIndex).ErrorText = String.Empty
        btn_StoreSave.Enabled = True

    End Sub

    Private Function CheckRatioFormat(ByVal ValueToCheck As String) As Boolean

        If Not ValueToCheck.Contains("-") Then Return False

        Dim Vals() As String = ValueToCheck.Split("-")
        If Vals.Length <> 2 Then Return False

        If Not IsNumeric(Vals(0)) OrElse Not IsNumeric(Vals(1)) Then
            Return False
        Else
            Return True
        End If

    End Function
    Private Sub GridRatio_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles GridRatio.CellValidating
        If e.ColumnIndex = 2 Or e.ColumnIndex = 3 Then
            'If Not IsNumeric(e.FormattedValue) Then

            If e.FormattedValue <> "" AndAlso Not CheckRatioFormat(e.FormattedValue) Then
                'GridRatio.Rows(e.RowIndex).Cells(e.ColumnIndex).ErrorText = "must be a numeric value."
                GridRatio.Rows(e.RowIndex).ErrorText = "must be a numeric value."
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub cmb_To_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_To.SelectedIndexChanged
        'If cmb_From.SelectedIndex >= cmb_To.SelectedIndex AndAlso cmb_From.SelectedIndex <> -1 Then
        '    MessageBox.Show("To time can not earlier than 'From' time", "Alert", MessageBoxButtons.OK)
        '    cmb_To.Focus()
        'End If
    End Sub

    Private Sub cmb_From_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_From.SelectedIndexChanged
        'If cmb_From.SelectedIndex >= cmb_To.SelectedIndex AndAlso cmb_From.SelectedIndex <> -1 Then
        '    MessageBox.Show("To time can not earlier than 'From' time", "Alert", MessageBoxButtons.OK)
        '    cmb_From.Focus()
        'End If
    End Sub

    Private Sub cmb_Customer_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If Not cmb_Customer.EditValue = Nothing Then
            Cursor = Cursors.WaitCursor
            'btn_StoreSave.Enabled = True
            CurCustomer_Id = Convert.ToInt32(cmb_Customer.EditValue.ToString)
            Call LoadStores(CurCustomer_Id)
            Cursor = Cursors.Default
        End If
    End Sub

    Private Sub LoadStores(ByVal Customer_Id As Integer)
        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")

        Dim drRow As DataRow
        drRow = dsStores.Tables(0).NewRow
        drRow(0) = 0
        drRow(1) = "<Add New>"
        dsStores.Tables(0).Rows.Add(drRow)

        StoresGrid.Columns.Clear()

        StoresGrid.DataSource = dsStores.Tables("StoreList")
        StoresGrid.Columns(0).HeaderText = "Store Id"
        StoresGrid.Columns(1).HeaderText = "Store Name"
        StoresGrid.Columns(0).Width = 80
        StoresGrid.Columns(0).Visible = False
        StoresGrid.Columns(1).Width = 350

        StoresGrid.Refresh()
        StoresGrid.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable

        dsStores.Dispose()
        dapStores.Dispose()
        'If StoresGrid.Rows.Count = 1 Then CurStore_Id = 0 : ClearValues()
        Call CloseDBConnection()
    End Sub
    Private Function ValidateAudioPaths() As Boolean

        If txt_Songs_Audio_Path.Text.Trim = "" Then
            XtraMessageBox.Show("Songs audio path is not configured", "Alert", MessageBoxButtons.OK)
            Return False
        End If
        If txt_Promos_Audio_Path.Text.Trim = "" Then
            XtraMessageBox.Show("Promos audio path is not configured", "Alert", MessageBoxButtons.OK)
            Return False
        End If
        If txt_Fileprep_Path.Text.Trim = "" Then
            XtraMessageBox.Show("Fileprep path is not configured", "Alert", MessageBoxButtons.OK)
            Return False
        End If
        If cmb_Audio_Format.Text.Trim = "" Then
            XtraMessageBox.Show("Audio format is not configured", "Alert", MessageBoxButtons.OK)
            Return False
        End If
        If txt_Audio_Converter_Path.Text.Trim = "" Then
            XtraMessageBox.Show("Audio converter path is not configured", "Alert", MessageBoxButtons.OK)
            Return False
        End If

        If Not Directory.Exists(txt_Songs_Audio_Path.Text.Trim) Then
            XtraMessageBox.Show("Selected songs audio path doesn't exist", "Alert", MessageBoxButtons.OK)
            Return False
        End If

        If Not Directory.Exists(txt_Promos_Audio_Path.Text.Trim) Then
            XtraMessageBox.Show("Selected promos audio path doesn't exist", "Alert", MessageBoxButtons.OK)
            Return False
        End If

        If Not Directory.Exists(txt_Fileprep_Path.Text.Trim) Then
            XtraMessageBox.Show("Selected fileprep path doesn't exist", "Alert", MessageBoxButtons.OK)
            Return False
        End If

        If Not File.Exists(txt_Audio_Converter_Path.Text.Trim) Then
            XtraMessageBox.Show("Audio converter is not found in the selected path", "Alert", MessageBoxButtons.OK)
            Return False
        End If
        Return True
    End Function
    Private Sub btn_Audio_Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Audio_Save.Click

        If Not ValidateAudioPaths() Then Exit Sub


        Cursor = Cursors.WaitCursor
        Dim command As New SqlCommand
        Dim ds As New DataSet

        Call OpenDBConnection()
        command.Connection = DBConn
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "ProcAddModifyAdminParameters"
        command.Parameters.AddWithValue("@Songs_Path", txt_Songs_Audio_Path.Text).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@Promos_Path", txt_Promos_Audio_Path.Text).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@Audio_Format", cmb_Audio_Format.Text).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@Fileprep_Path", txt_Fileprep_Path.Text).Direction = ParameterDirection.Input
        'command.Parameters.AddWithValue("@AudioConverter_Path", txt_Audio_Converter_Path.Text).Direction = ParameterDirection.Input
        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
        command.ExecuteNonQuery()

        Call CloseDBConnection()
        Call WriteConfig(txt_Audio_Converter_Path.Text)

        SongsArchivePath = txt_Songs_Audio_Path.Text
        PromosArchivePath = txt_Promos_Audio_Path.Text
        FileprepPath = txt_Fileprep_Path.Text
        AudioFileFormat = cmb_Audio_Format.Text
        AudioConverterpath = txt_Audio_Converter_Path.Text


        Cursor = Cursors.Default
        'XtraMessageBox.Show("Audio configuration settings are saved.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
        txt_Songs_Audio_Path.Focus()
        btn_Audio_Save.Enabled = False

    End Sub

    Private Sub Config_Tab_Control_SelectedPageChanged(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles Config_Tab_Control.SelectedPageChanged

        If e.Page.Text = "Store" Then
            btn_StoreSave.Enabled = False

            Call LoadCustomers()
            cmb_Customer.Properties.PopulateColumns()
            cmb_Customer.Properties.Columns(0).Visible = False
        ElseIf e.Page.Text = "Audio" Then
            txt_Songs_Audio_Path.Text = SongsArchivePath
            txt_Promos_Audio_Path.Text = PromosArchivePath
            txt_Fileprep_Path.Text = FileprepPath
            cmb_Audio_Format.Text = AudioFileFormat
            txt_Audio_Converter_Path.Text = AudioConverterpath
            btn_Audio_Save.Enabled = False

        ElseIf e.Page.Text = "FTP" Then

            txt_IPAddress.Text = FTPIPAddress
            txt_Password.Text = FTPPassword
            txt_User_Name.Text = FTPUserName

            btn_Save_FTP.Enabled = False
        ElseIf e.Page.Text = "Customer" Then

            bindCustomersGrid()
            btn_save_edit_customer.Enabled = False

        ElseIf e.Page.Text = "Genre" Then
            bindGenreGrid()
            btn_save_edit_Genre.Enabled = False
        ElseIf e.Page.Text = "Categories" Then
            'bindGenreCategoriesGrid()
            Media_Group.SelectedIndex = 0
            Call LoadGenres()
            btn_save_edit_category.Enabled = False

        End If
    End Sub

    Private Sub btn_Browse_Songs_Path_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Browse_Songs_Path.Click
        FolderBrowser.ShowDialog()
        txt_Songs_Audio_Path.Text = FolderBrowser.SelectedPath
    End Sub

    Private Sub btn_Browse_Promos_Path_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Browse_Promos_Path.Click
        FolderBrowser.ShowDialog()
        txt_Promos_Audio_Path.Text = FolderBrowser.SelectedPath
    End Sub

    Private Sub btn_Browse_Fileprep_Path_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Browse_Fileprep_Path.Click
        FolderBrowser.ShowDialog()
        txt_Fileprep_Path.Text = FolderBrowser.SelectedPath
    End Sub

    Private Sub ShowValues(ByVal Customer_Id As Integer, ByVal Store_Id As Integer)
        Dim cmdStore As New SqlCommand, dapStore As New SqlDataAdapter, dsStore As New DataSet

        Call OpenDBConnection()
        With cmdStore
            .Connection = DBConn
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 300
            .CommandText = "ProcGetStoreDetails"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Store_Id", Store_Id).Direction = ParameterDirection.Input
        End With
        dapStore.SelectCommand = cmdStore
        dapStore.Fill(dsStore)
        tvw_Genre_Category.Nodes.Clear()
        cmb_Promo_Category.Properties.Items.Clear()
        CreateSongPromoRatioTable()

        If dsStore.Tables(0).Rows.Count <> 0 Then
            txt_StoreName.Text = dsStore.Tables(0).Rows(0).Item("Store_Name").ToString
            cmb_From.Text = dsStore.Tables(0).Rows(0).Item("Store_Opens_At").ToString
            cmb_To.Text = dsStore.Tables(0).Rows(0).Item("Store_Closed_At").ToString
            txt_MaxSongs.Text = dsStore.Tables(0).Rows(0).Item("Max_Songs").ToString

            chk_FTPSync.Checked = dsStore.Tables(0).Rows(0).Item("FTP_Sync").ToString
            GridDayparts.Rows.Clear()

            'bypass for a new store in a new customer
            If Store_Id <> 0 Then

                For i As Integer = 0 To dsStore.Tables(1).Rows.Count - 1
                    GridDayparts.Rows.Add()
                    'MsgBox(dsStore.Tables(1).Rows(i).Item("Genre_Categories").ToString)
                    'MsgBox(dsStore.Tables(1).Rows(i).Item("Genre_Category_Codes").ToString)
                    GridDayparts.Rows(i).Cells(0).Value = i + 1
                    GridDayparts.Rows(i).Cells(1).Value = dsStore.Tables(1).Rows(i).Item("daypart_from").ToString
                    GridDayparts.Rows(i).Cells(2).Value = dsStore.Tables(1).Rows(i).Item("daypart_to").ToString
                    GridDayparts.Rows(i).Cells(3).Value = dsStore.Tables(1).Rows(i).Item("Genre_Categories").ToString
                    GridDayparts.Rows(i).Cells(4).Value = dsStore.Tables(1).Rows(i).Item("Genre_Category_Codes").ToString
                    GridDayparts.Rows(i).Cells(5).Value = dsStore.Tables(1).Rows(i).Item("Promo_Categories").ToString
                    GridDayparts.Rows(i).Cells(6).Value = dsStore.Tables(1).Rows(i).Item("Promo_Category_Codes").ToString
                Next
        
                Dim rno, cno As Integer
                For i As Integer = 0 To dsStore.Tables(2).Rows.Count - 1
                    rno = Convert.ToInt32(dsStore.Tables(2).Rows(i).Item("day_number").ToString) - 1
                    cno = Convert.ToInt32(dsStore.Tables(2).Rows(i).Item("daypart_id").ToString) + 1

                    If cno > GridRatio.ColumnCount - 1 Then AddDaypartColumn("Daypart" & (cno - 1).ToString)
                    GridRatio.Columns(cno).HeaderText = "Daypart" & (cno - 1).ToString
                    GridRatio.Rows(rno).Cells(cno).Value = dsStore.Tables(2).Rows(i).Item("Ratio").ToString
                Next
            Else
                GridDayparts.Rows.Add()
            End If

            'loading genre categories of activate songs of this store
            Dim CurGenre As String = ""
            Dim NodeCount As Integer = 0

            tvw_Genre_Category.Nodes.Add("(Select All)")
            For i As Integer = 0 To dsStore.Tables(3).Rows.Count - 1
                If CurGenre <> dsStore.Tables(3).Rows(i).Item("genre_name").ToString Then
                    NodeCount += 1
                    CurGenre = dsStore.Tables(3).Rows(i).Item("genre_name").ToString
                    tvw_Genre_Category.Nodes.Add(CurGenre)
                End If
                Dim ChildNode As New TreeNode
                ChildNode.Tag = dsStore.Tables(3).Rows(i).Item("Genre_Category_Id").ToString
                ChildNode.Text = dsStore.Tables(3).Rows(i).Item("Category_Code").ToString
                tvw_Genre_Category.Nodes(NodeCount).Nodes.Add(ChildNode)
                ChildNode = Nothing
            Next
            'promo categories of activated promos of this store
            For i As Integer = 0 To dsStore.Tables(4).Rows.Count - 1
                cmb_Promo_Category.Properties.Items.Add(dsStore.Tables(4).Rows(i).Item("Promo_Category_Id").ToString, dsStore.Tables(4).Rows(i).Item("Category_Code").ToString)
            Next
        End If
        cmb_Promo_Category.Properties.ShowButtons = False
        cmb_Promo_Category.Properties.ShowPopupCloseButton = False
        cmb_Promo_Category.Properties.PopupSizeable = False
        'cmb_Promo_Category.CheckAll()
        'cmb_Promo_Category.Properties.PopupFormMinSize
        If Store_Id <> 0 Then
            Call ReadGridRow(0)
        Else
            ClearValues()
        End If
        Call CloseDBConnection()
    End Sub
    'Private Sub checkedComboBoxEdit1_CustomDisplayText(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs)
    '    Dim edit As CheckedComboBoxEdit = TryCast(sender, CheckedComboBoxEdit)
    '    For Each item As CheckedListBoxItem In edit.Properties.Items
    '        If item.CheckState <> CheckState.Checked Then
    '            Return
    '        End If
    '    Next item
    '    e.DisplayText = "Text"
    'End Sub
    Private Sub ClearValues()
        GridDayparts.Rows.Clear()
        'GridDayparts.Rows.Add(1, "", "", "", "")
        Call CreateSongPromoRatioTable()
        txt_StoreName.Text = "<Enter store name>"
        txt_MaxSongs.Text = 100
        'DynamicChecked = False
        'cmb_From.SelectedIndex = -1
        'cmb_To.SelectedIndex = -1
        cmb_From.SelectedIndex = 9
        cmb_To.SelectedIndex = 20
        cmb_Daypart_From.SelectedIndex = 8
        If cmb_Daypart_To.SelectedIndex = 20 Then
            cmb_Daypart_From.SelectedIndex = 8
            cmb_Daypart_From.SelectedIndex = 9
            Call GridDayPartToTimeChanged()
        Else
            cmb_Daypart_To.SelectedIndex = 20
        End If

        txt_DayPart.Text = 1

        'If GridRatio.Columns(2).HeaderText <> "Daypart1" Then
        ' Call GridDayPartToTimeChanged()
        'If GridDayparts.Rows.Count = 0 Then GridDayparts.Rows.Add()
        'cmb_Daypart_To.Text = cmb_To.SelectedItem.ToString
        'GridDayparts.Rows(0).Cells(2).Value = cmb_To.SelectedItem.ToString
        'GridRatio.Columns(2).HeaderText = "Daypart1"
        'Call AddDefaultRatio(2)
        'End If

        tvw_Genre_Category.Nodes(0).Checked = True
        'cmb_Promo_Category.CheckAll()
        'cmb_Promo_Category.EditValue = ""
        For i As Integer = 0 To cmb_Promo_Category.Properties.Items.Count - 1
            cmb_Promo_Category.Properties.Items(i).CheckState = CheckState.Checked
        Next

    End Sub

    Private Sub StoresGrid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles StoresGrid.KeyDown
        If e.KeyCode = Keys.Delete AndAlso StoresGrid.RowCount > 0 Then
            'If XtraMessageBox.Show("Do you want to delete the selected 'Customer'?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            '    DeleteCustomer(Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(1).Value, Customers_Info_Grid.Rows(Customers_Info_Grid.CurrentRow.Index).Cells(0).Value, Customers_Info_Grid.CurrentRow.Index)
            'End If
            If Not StoresGrid.Rows(StoresGrid.CurrentRow.Index).Cells(1).Value Is Nothing Then
                DeleteStore(StoresGrid.Rows(StoresGrid.CurrentRow.Index).Cells(1).Value, StoresGrid.Rows(StoresGrid.CurrentRow.Index).Cells(0).Value, StoresGrid.CurrentRow.Index)
            End If
        End If

    End Sub
    Private Sub StoresGrid_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StoresGrid.SelectionChanged

        If StoresGrid.SelectedRows.Count = 0 Then Exit Sub
        If StoresGrid.SelectedRows(0).Cells(0).Value.ToString = 0 Then
            CurStore_Id = 0
            'Call ClearValues()
        Else
            CurStore_Id = StoresGrid.SelectedRows(0).Cells(0).Value.ToString
        End If
        Call ShowValues(CurCustomer_Id, CurStore_Id)
        btn_StoreSave.Enabled = False

    End Sub
    Private Function PromosDefined(ByVal DayPartNo As Integer) As Boolean
        Dim P As Boolean = False
        Dim i As Integer
        For r As Integer = 0 To 6
            i = GridRatio.Rows(r).Cells(1).Value.ToString.Split("-")(0)
            If i <> 0 Then
                P = True
                Exit For
            End If
        Next

        Return P
    End Function
   
    Private Sub btn_StoreSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_StoreSave.Click
        If txt_StoreName.Text = "<Enter store name>" OrElse txt_StoreName.Text.Trim = "" Then
            XtraMessageBox.Show("New store name is not entered", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_StoreName.Focus()
            Exit Sub
        End If

        If cmb_From.Text = "" Then
            XtraMessageBox.Show("Store open time is not entered", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_From.Focus()
            Exit Sub
        End If

        If cmb_To.Text = "" Then
            XtraMessageBox.Show("Store closing time is not entered", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_To.Focus()
            Exit Sub
        End If

        If Val(txt_MaxSongs.Text) = 0 Then
            XtraMessageBox.Show("Maximum songs is not entered", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_MaxSongs.Focus()
            Exit Sub
        End If

        For r As Integer = 0 To GridDayparts.Rows.Count - 1
            If GridDayparts.Rows(r).Cells(4).Value.ToString = "" Then
                XtraMessageBox.Show("Genre categories can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmb_Genre_Category.Focus()
                Exit Sub
            ElseIf GridDayparts.Rows(r).Cells(6).Value.ToString = "" AndAlso PromosDefined(r + 2) Then
                XtraMessageBox.Show("Promo categories for Daypart" & (r + 1).ToString & " can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmb_Promo_Category.Focus()
                Exit Sub
            End If
        Next

        Cursor = Cursors.WaitCursor
        Dim cmdStore As New SqlCommand, NewStoreId As Integer
        Dim StoreName As String = ""
        Call OpenDBConnection()
        With cmdStore
            .Connection = DBConn
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 300

            If CurStore_Id = 0 Then ' new store
                .CommandText = "ProcAddStore"
                .Parameters.AddWithValue("@Customer_Id", Convert.ToInt32(cmb_Customer.EditValue.ToString)).Direction = ParameterDirection.Input
                .Parameters.Add("@Store_Id", SqlDbType.BigInt).Direction = ParameterDirection.Output
            Else
                .CommandText = "ProcModifyStore"
                .Parameters.AddWithValue("@Customer_Id", Convert.ToInt32(cmb_Customer.EditValue.ToString)).Direction = ParameterDirection.Input
                .Parameters.AddWithValue("@Store_Id", CurStore_Id).Direction = ParameterDirection.Input
            End If
            .Parameters.AddWithValue("@Store_Name", txt_StoreName.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Store_Opens_At", GetTimeValue(False, cmb_From.Text)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Store_Closed_At", GetTimeValue(True, cmb_To.Text)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Max_Songs", txt_MaxSongs.Text).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FTP_Sync", chk_FTPSync.EditValue).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .ExecuteNonQuery()
            StoreName = txt_StoreName.Text.Trim
        End With

        If cmdStore.Parameters("@Result").Value.ToString = "Success" Then
            If CurStore_Id = 0 Then NewStoreId = cmdStore.Parameters("@Store_Id").Value Else NewStoreId = CurStore_Id
            Dim cmdDayParts As New SqlCommand
            cmdDayParts.Connection = DBConn
            cmdDayParts.CommandTimeout = 300
            cmdDayParts.CommandType = CommandType.StoredProcedure
            cmdDayParts.CommandText = "ProcAddStoreDayparts"
            cmdDayParts.Parameters.AddWithValue("@Store_Id", NewStoreId).Direction = ParameterDirection.Input
            cmdDayParts.Parameters.Add("@Daypart_Id", SqlDbType.Int).Direction = ParameterDirection.Input
            cmdDayParts.Parameters.Add("@Daypart_From", SqlDbType.DateTime).Direction = ParameterDirection.Input
            cmdDayParts.Parameters.Add("@Daypart_To", SqlDbType.DateTime).Direction = ParameterDirection.Input
            cmdDayParts.Parameters.Add("@Genre_Categories", SqlDbType.VarChar, 150).Direction = ParameterDirection.Input
            cmdDayParts.Parameters.Add("@Promo_Categories", SqlDbType.VarChar, 50).Direction = ParameterDirection.Input
            cmdDayParts.Parameters.Add("@Mode", SqlDbType.VarChar, 50).Direction = ParameterDirection.Input

            If CurStore_Id = 0 Then cmdDayParts.Parameters("@Mode").Value = "New" Else cmdDayParts.Parameters("@Mode").Value = "Update"

            cmdDayParts.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            For i As Integer = 0 To GridDayparts.Rows.Count - 1
                cmdDayParts.Parameters("@Daypart_Id").Value = GridDayparts.Rows(i).Cells(0).Value
                cmdDayParts.Parameters("@Daypart_From").Value = GetTimeValue(False, GridDayparts.Rows(i).Cells(1).Value)
                cmdDayParts.Parameters("@Daypart_To").Value = GetTimeValue(True, GridDayparts.Rows(i).Cells(2).Value)
                cmdDayParts.Parameters("@Genre_Categories").Value = GridDayparts.Rows(i).Cells(3).Value
                If GridDayparts.Rows(i).Cells(5).Value = Nothing Then
                    cmdDayParts.Parameters("@Promo_Categories").Value = ""
                Else
                    cmdDayParts.Parameters("@Promo_Categories").Value = GridDayparts.Rows(i).Cells(5).Value
                End If

                cmdDayParts.ExecuteNonQuery()
            Next

            Dim cmdSongPromoRatio As New SqlCommand
            cmdSongPromoRatio.Connection = DBConn
            cmdSongPromoRatio.CommandType = CommandType.StoredProcedure
            cmdSongPromoRatio.CommandTimeout = 300
            cmdSongPromoRatio.CommandText = "ProcAddStoreSongPromoRatio"
            cmdSongPromoRatio.Parameters.AddWithValue("@Store_Id", NewStoreId).Direction = ParameterDirection.Input
            cmdSongPromoRatio.Parameters.Add("@Daypart_Id", SqlDbType.Int).Direction = ParameterDirection.Input
            cmdSongPromoRatio.Parameters.Add("@Day_name", SqlDbType.VarChar, 50).Direction = ParameterDirection.Input
            cmdSongPromoRatio.Parameters.Add("@Ratio", SqlDbType.VarChar, 50).Direction = ParameterDirection.Input
            cmdSongPromoRatio.Parameters.Add("@Mode", SqlDbType.VarChar, 50).Direction = ParameterDirection.Input

            If CurStore_Id = 0 Then cmdSongPromoRatio.Parameters("@Mode").Value = "New" Else cmdSongPromoRatio.Parameters("@Mode").Value = "Update"
            cmdSongPromoRatio.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            For j As Integer = 2 To GridRatio.Columns.Count - 1
                If GridRatio.Columns(j).HeaderText = "<Undefined>" Then Exit For
                For r As Integer = 0 To 6
                    cmdSongPromoRatio.Parameters("@Daypart_Id").Value = j - 1
                    cmdSongPromoRatio.Parameters("@Day_name").Value = GridRatio.Rows(r).Cells(1).Value
                    cmdSongPromoRatio.Parameters("@Ratio").Value = GridRatio.Rows(r).Cells(j).Value
                    cmdSongPromoRatio.ExecuteNonQuery()
                Next
            Next
            cmdDayParts.Dispose()
            cmdSongPromoRatio.Dispose()

        Else
            Cursor = Cursors.Default
            XtraMessageBox.Show(cmdStore.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
        cmdStore.Dispose()
        Call CloseDBConnection()
        Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString))
        If StoresGrid.Rows.Count > 1 Then
            Call SelectRowInGrid(StoresGrid, StoreName, 1)
        End If
        Cursor = Cursors.Default
        btn_save_edit_Genre.Enabled = False
    End Sub

    Private Sub btn_Browse_Converter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Browse_Converter.Click
        FileBrowser.Filter = "Audio Converter EXE|*.exe"
        FileBrowser.ShowDialog()
        txt_Audio_Converter_Path.Text = FileBrowser.FileName
    End Sub

    Private Sub Config_Tab_Control_SelectedPageChanging(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangingEventArgs) Handles Config_Tab_Control.SelectedPageChanging
        If Not e.PrevPage Is Nothing AndAlso e.PrevPage.Name = "Genre_Sub_Tab" AndAlso btn_save_edit_Genre.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                'e.Cancel = True
                btn_save_edit_Genre.PerformClick()
            Else
                btn_save_edit_Genre.Enabled = False

            End If
        ElseIf Not e.PrevPage Is Nothing AndAlso e.PrevPage.Name = "Categories_Sub_Tab" AndAlso btn_save_edit_category.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                'e.Cancel = True
                btn_save_edit_category.PerformClick()
            Else
                btn_save_edit_category.Enabled = False

            End If
        ElseIf Not e.PrevPage Is Nothing AndAlso e.PrevPage.Name = "Audio_Sub_Tab" AndAlso btn_Audio_Save.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                'e.Cancel = True
                btn_Audio_Save.PerformClick()
                If btn_Audio_Save.Enabled = True Then
                    e.Cancel = True
                End If
            Else
                btn_Audio_Save.Enabled = False

            End If
        ElseIf Not e.PrevPage Is Nothing AndAlso e.PrevPage.Name = "Customers_Sub_Tab" AndAlso btn_save_edit_customer.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                'e.Cancel = True
                btn_save_edit_customer.PerformClick()
            Else
                btn_save_edit_customer.Enabled = False

            End If
        ElseIf Not e.PrevPage Is Nothing AndAlso e.PrevPage.Name = "Store_Sub_Tab" AndAlso btn_StoreSave.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                'e.Cancel = True
                btn_StoreSave.PerformClick()
            Else
                btn_StoreSave.Enabled = False

            End If

        ElseIf Not e.PrevPage Is Nothing AndAlso e.PrevPage.Name = "FTP_Sub_Tab" AndAlso btn_Save_FTP.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                'e.Cancel = True
                btn_Save_FTP.PerformClick()
            Else
                btn_Save_FTP.Enabled = False

            End If
        End If
    End Sub

    Private Sub txt_Audio_Converter_Path_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Audio_Converter_Path.EditValueChanged
        btn_Audio_Save.Enabled = True

    End Sub

    Private Sub txt_Fileprep_Path_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Fileprep_Path.EditValueChanged
        btn_Audio_Save.Enabled = True

    End Sub

    Private Sub txt_Promos_Audio_Path_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Promos_Audio_Path.EditValueChanged
        btn_Audio_Save.Enabled = True

    End Sub

    Private Sub cmb_Audio_Format_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Audio_Format.EditValueChanged
        btn_Audio_Save.Enabled = True

    End Sub

    Private Sub txt_StoreName_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_StoreName.EditValueChanged
        btn_StoreSave.Enabled = True

    End Sub

    Private Sub txt_MaxSongs_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_MaxSongs.EditValueChanged
        btn_StoreSave.Enabled = True

    End Sub


    Private Sub txt_Songs_Audio_Path_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_Songs_Audio_Path.EditValueChanged
        btn_Audio_Save.Enabled = True

    End Sub

    Private Sub btn_Save_FTP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Save_FTP.Click

        If txt_IPAddress.Text = "" OrElse txt_User_Name.Text = "" OrElse txt_Password.Text = "" Then
            XtraMessageBox.Show("All the fields are mandatory", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            btn_Save_FTP.Enabled = False
            Exit Sub
        End If

        Cursor = Cursors.WaitCursor
        Dim command As New SqlCommand
        Dim ds As New DataSet

        Call OpenDBConnection()
        command.Connection = DBConn
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "ProcAddModifyFTPParameters"
        command.Parameters.AddWithValue("@IP_Address", txt_IPAddress.Text).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@User_Name", txt_User_Name.Text).Direction = ParameterDirection.Input
        command.Parameters.AddWithValue("@Password", txt_Password.Text).Direction = ParameterDirection.Input
       
        command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
        command.ExecuteNonQuery()

        Call CloseDBConnection()

        FTPIPAddress = txt_IPAddress.Text
        FTPPassword = txt_Password.Text
        FTPUserName = txt_User_Name.Text

        Cursor = Cursors.Default
        'XtraMessageBox.Show("Audio configuration settings are saved.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
        btn_Save_FTP.Enabled = False
        txt_IPAddress.Focus()
    End Sub

    Private Sub txt_IPAddress_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_IPAddress.EditValueChanged
        If txt_IPAddress.Text.Trim <> "" Then btn_Save_FTP.Enabled = True
    End Sub

    Private Sub txt_Password_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Password.EditValueChanged
        If txt_Password.Text.Trim <> "" Then btn_Save_FTP.Enabled = True
    End Sub

    Private Sub txt_User_Name_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_User_Name.EditValueChanged
        If txt_User_Name.Text.Trim <> "" Then btn_Save_FTP.Enabled = True
    End Sub

    Private Sub chk_FTPSync_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_FTPSync.CheckedChanged
        btn_StoreSave.Enabled = True
    End Sub

    Private Sub Media_Group_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Media_Group.SelectedIndexChanged
        Category_Info_Grid.Rows.Clear()
        If Media_Group.SelectedIndex = 0 Then
            lbl_Parent_Category.Text = "Genre:"

            PanelControl4.Width = 367
            Category_Info_Grid.Width = 353
            Category_Info_Grid.Columns("Rotation").Visible = True
            btn_save_edit_category.Left = 254

            Call LoadGenres()
            'Call bindGenreCategoriesGrid()
        Else
            PanelControl4.Width = 298
            Category_Info_Grid.Width = 286
            Category_Info_Grid.Columns("Rotation").Visible = False
            btn_save_edit_category.Left = 189
            Call LoadCustomers()
            lbl_Parent_Category.Text = "Customer:"
            'Call bindPromoCategoriesGrid()
        End If
        cmb_Parent_Category.Properties.Columns(0).Visible = False
        cmb_Parent_Category.EditValue = ""
    End Sub
    Private Sub SaveEditGenreCategory()
        Dim StrMsg, StrEditMsg As String, CategoryCode As String = ""
        Dim OutResult As String
        Dim EditOutResult As String
        Dim IfFailed As Boolean = False, DuplicateFound As Boolean = False, DuplicateFlag As Boolean = False

        Cursor = Cursors.WaitCursor
        Call OpenDBConnection()
        For i = 0 To Category_Info_Grid.Rows.Count - 1
            If Not CheckNulls(i) Then IfFailed = True : GoTo nxtrow
            If Category_Info_Grid.Rows(i).Cells("Category_Id").Value = "" Then
                If Category_Info_Grid.Rows(i).Cells("Category_Name").Value <> "" Then
                    DuplicateFound = CheckDuplicates(i)
                    If DuplicateFound Then
                        If DuplicateFlag = False Then DuplicateFlag = True
                    Else
                        OutResult = AddNewGenreCategory(Convert.ToInt32(cmb_Parent_Category.EditValue.ToString), Category_Info_Grid.Rows(i).Cells("Category_Name").Value, Category_Info_Grid.Rows(i).Cells("Category_Code").Value, Category_Info_Grid.Rows(i).Cells("Rotation").Value)
                        CategoryCode = Category_Info_Grid.Rows(i).Cells("Category_Code").Value
                        If OutResult <> "Success" Then
                            StrMsg = StrMsg + vbNewLine + Category_Info_Grid.Rows(i).Cells("Category_Name").Value + " : " + OutResult
                        End If
                    End If

                End If
            Else
                If Category_Info_Grid.Rows(i).Cells("Category_Name").Value <> "" Then
                    DuplicateFound = CheckDuplicates(i)
                    If DuplicateFound Then
                        If DuplicateFlag = False Then DuplicateFlag = True
                    Else
                        EditOutResult = EditGenreCategory(Convert.ToInt32(cmb_Parent_Category.EditValue.ToString), Category_Info_Grid.Rows(i).Cells("Category_Id").Value, Category_Info_Grid.Rows(i).Cells("Category_Name").Value, Category_Info_Grid.Rows(i).Cells("Category_Code").Value, Category_Info_Grid.Rows(i).Cells("Rotation").Value)
                        CategoryCode = Category_Info_Grid.Rows(i).Cells("Category_Code").Value
                        If EditOutResult <> "Success" Then
                            StrEditMsg = StrEditMsg + vbNewLine + Category_Info_Grid.Rows(i).Cells("Category_Id").Value + " : " + EditOutResult
                        End If
                    End If
                End If
                End If
nxtrow:
        Next
        If StrMsg = "" Then
            '  XtraMessageBox.Show("Data saved successfully", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            XtraMessageBox.Show(StrMsg, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        Call CloseDBConnection()

        Cursor = Cursors.Default
        If Not IfFailed AndAlso Not Duplicateflag Then
            bindCategoriesGrid()
            Call SelectRowInGrid(Category_Info_Grid, CategoryCode, 2)
            btn_save_edit_category.Enabled = False
        End If
    End Sub
    Private Sub SaveEditPromoCategory()
        Dim StrMsg, StrEditMsg As String, CategoryCode As String = ""
        Dim OutResult As String
        Dim EditOutResult As String
        Dim IfFailed As Boolean = False, DuplicateFound As Boolean = False, DuplicateFlag As Boolean = False

        Cursor = Cursors.WaitCursor
        Call OpenDBConnection()
        For i = 0 To Category_Info_Grid.Rows.Count - 1
            If Not CheckNulls(i) Then IfFailed = True : GoTo nxtrow
            If Category_Info_Grid.Rows(i).Cells("Category_Id").Value = "" Then
                If Category_Info_Grid.Rows(i).Cells("Category_Name").Value <> "" Then
                    DuplicateFound = CheckDuplicates(i)
                    If DuplicateFound Then
                        If DuplicateFlag = False Then DuplicateFlag = True
                    Else
                        OutResult = AddNewPromoCategory(Convert.ToInt32(cmb_Parent_Category.EditValue.ToString), Category_Info_Grid.Rows(i).Cells("Category_Name").Value, Category_Info_Grid.Rows(i).Cells("Category_Code").Value)
                        CategoryCode = Category_Info_Grid.Rows(i).Cells("Category_Code").Value
                        If OutResult <> "Success" Then
                            StrMsg = StrMsg + vbNewLine + Category_Info_Grid.Rows(i).Cells("Category_Id").Value + " : " + OutResult
                        End If
                    End If
                End If
            Else
                If Category_Info_Grid.Rows(i).Cells("Category_Name").Value <> "" Then
                    DuplicateFound = CheckDuplicates(i)
                    If DuplicateFound Then
                        If DuplicateFlag = False Then DuplicateFlag = True
                    Else
                        EditOutResult = EditPromoCategory(Convert.ToInt32(cmb_Parent_Category.EditValue.ToString), Category_Info_Grid.Rows(i).Cells("Category_Id").Value, Category_Info_Grid.Rows(i).Cells("Category_Name").Value, Category_Info_Grid.Rows(i).Cells("Category_Code").Value)
                        CategoryCode = Category_Info_Grid.Rows(i).Cells("Category_Code").Value
                        If EditOutResult <> "Success" Then
                            StrEditMsg = StrEditMsg + vbNewLine + Category_Info_Grid.Rows(i).Cells("Category_Id").Value + " : " + EditOutResult
                        End If
                    End If
                End If
            End If
nxtrow:
        Next
        If StrMsg = "" Then
            '  XtraMessageBox.Show("Data saved successfully", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            XtraMessageBox.Show(StrMsg, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        Call CloseDBConnection()
        Cursor = Cursors.Default
        If Not IfFailed AndAlso Not DuplicateFlag Then
            bindGenreGrid()
            Call SelectRowInGrid(Category_Info_Grid, CategoryCode, 2)


            btn_save_edit_category.Enabled = False
        End If
    End Sub
    Private Function CheckDuplicates(ByVal RowIndex As Integer) As Boolean
        Dim res As Boolean = False
        If Category_Info_Grid.Rows(RowIndex).Cells("Category_Name").Value <> "" Then
            'failed, then check for duplicates
            For i = 0 To Category_Info_Grid.Rows.Count - 1 'category name
                If Category_Info_Grid.Rows(i).Cells("Category_Name").Value <> "" And RowIndex <> i Then
                    If Category_Info_Grid.Rows(RowIndex).Cells("Category_Name").Value.ToString.ToUpper = Category_Info_Grid.Rows(i).Cells("Category_Name").Value.ToString.ToUpper Then
                        XtraMessageBox.Show(Category_Info_Grid.Rows(RowIndex).Cells("Category_Name").Value & " category is already existing", "PlayTM", MessageBoxButtons.OK)
                        res = True
                        GoTo REsult
                    End If
                End If
            Next


            For i = 0 To Category_Info_Grid.Rows.Count - 1 'category code
                If Category_Info_Grid.Rows(i).Cells("Category_Code").Value <> "" And RowIndex <> i Then
                    If Category_Info_Grid.Rows(RowIndex).Cells("Category_Code").Value.ToString.ToUpper = Category_Info_Grid.Rows(i).Cells("Category_Code").Value.ToString.ToUpper Then
                        XtraMessageBox.Show(Category_Info_Grid.Rows(RowIndex).Cells("Category_Code").Value & " category code is already existing", "PlayTM", MessageBoxButtons.OK)
                        res = True
                        GoTo REsult

                    End If
                End If
            Next
        End If
result:
        Return res
    End Function

    Private Function CheckNulls(ByVal RowIndex As Integer) As Boolean
        Dim res As Boolean = True

        If Category_Info_Grid.Rows(RowIndex).Cells("Category_Name").Value <> "" Then
            If Category_Info_Grid.Rows(RowIndex).Cells(0).Value = "" And Category_Info_Grid.Rows(RowIndex).Cells("Category_Code").Value = "" Then
                XtraMessageBox.Show("All the values are mandatory", "PlayTM", MessageBoxButtons.OK)
                res = False
                GoTo REsult
            ElseIf Strings.Replace(Category_Info_Grid.Rows(RowIndex).Cells("Category_Code").Value, Strings.Left(cmb_Parent_Category.Text, 3) & "-", "").Trim = "" Then
                XtraMessageBox.Show("Category code is not entered", "PlayTM", MessageBoxButtons.OK)

                res = False
                GoTo REsult
            End If

        End If

        
Result:
        Return res
    End Function
    Private Sub btn_save_edit_category_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_edit_category.Click

        If Media_Group.SelectedIndex = 0 Then
            Call SaveEditGenreCategory()
        Else
            Call SaveEditPromoCategory()
        End If
    End Sub

    Private Sub Category_Info_Grid_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles Category_Info_Grid.CellValidating

        If bDoNotValidate Then Exit Sub
        If CurCategoryText <> cmb_Parent_Category.Text Then Exit Sub
        If Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(1).Value = "" Then Exit Sub
        'Dim Category_Code As String

        If e.ColumnIndex = 2 Then
            Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(2).Value = Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(2).EditedFormattedValue
            'MsgBox(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(1).Value.ToString)
            'MsgBox(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(4).getv)
            'MsgBox(Strings.Left(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(2).EditedFormattedValue, 3) & "-")
            'If InStr(

            If Strings.Left(cmb_Parent_Category.Text, 3).ToUpper & "-" <> Strings.Left(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(2).Value, 3) & "-" Then
                'MsgBox(Strings.Left(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(1).EditedFormattedValue, 3) & Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(4).EditedFormattedValue)
                Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(2).Value = Strings.Left(cmb_Parent_Category.Text, 3).ToUpper & "-" & Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(2).Value
                Category_Info_Grid.RefreshEdit()
            End If

            'MsgBox(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(4).Value)
            ' MsgBox(Strings.Left(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(1).EditedFormattedValue, 3).ToUpper & Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(4).EditedFormattedValue)
            'MsgBox(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(4).EditedFormattedValue)
            If Strings.Len(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(2).Value) <> 6 Then
                XtraMessageBox.Show("Category code should be six characters. Eg: ENG-12, 'ENG' being first three letters of the 'Genre/Customer' selected")
                e.Cancel = True
            ElseIf Strings.Mid(Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(2).Value, 4, 1) <> "-" Then
                XtraMessageBox.Show("Category code should be six characters with '-' at fourth position. Eg: ENG-12, 'ENG' being first three letters of the 'Genre/Customer' selected")
                e.Cancel = True
            End If
        ElseIf e.ColumnIndex = 3 Then
            Dim CellValue As Integer
            Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(3).Value = Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(3).EditedFormattedValue
            CellValue = Category_Info_Grid.Rows(Category_Info_Grid.CurrentRow.Index).Cells(3).Value
            If CellValue < 1 OrElse CellValue > 20 Then
                XtraMessageBox.Show("Rotation value can be between 1 and 20 only")
                e.Cancel = True
            End If
        End If

    End Sub


    Private Sub cmb_Parent_Category_ButtonPressed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles cmb_Parent_Category.ButtonPressed
        cmb_Parent_Category.Properties.Columns(0).Visible = False
    End Sub


    Private Sub cmb_Parent_Category_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Parent_Category.EditValueChanged
        If Not cmb_Parent_Category.EditValue = Nothing Then
            CurCategoryText = cmb_Parent_Category.Text
            Cursor = Cursors.WaitCursor
            'btn_StoreSave.Enabled = True
            Call bindCategoriesGrid()
            Cursor = Cursors.Default
        End If
    End Sub
    Private Sub CheckAllNodes(ByVal nodeChecked As Boolean)
        For Each node In tvw_Genre_Category.Nodes
            If node.Text <> "(Select All)" Then
                node.Checked = nodeChecked
                If node.Nodes.Count > 0 Then
                    ' If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    Me.CheckAllChildNodes(node, nodeChecked)
                End If
            End If

        Next node
    End Sub
    Private Sub CheckAllChildNodes(ByVal treeNode As TreeNode, ByVal nodeChecked As Boolean)

        Dim node As TreeNode
        For Each node In treeNode.Nodes
            node.Checked = nodeChecked
            If node.Nodes.Count > 0 Then
                ' If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                Me.CheckAllChildNodes(node, nodeChecked)
            End If
        Next node




    End Sub
    Private Sub ReadSelection(Optional ByVal RowIndex As Integer = 0)
        Dim ptnode, chnode As TreeNode, str, str1 As String
        For Each ptnode In tvw_Genre_Category.Nodes
            For Each chnode In ptnode.Nodes
                If chnode.Checked = True Then
                    str += "," & chnode.Tag
                    str1 += "," & chnode.Text
                End If
            Next chnode
        Next ptnode
        str = Mid(str, 2)
        str1 = Mid(str1, 2)

        cmb_Genre_Category.Text = str1
        txt_Genre_Category.Text = str
        Try
            If GridDayparts.Rows.Count <> 0 Then
                GridDayparts.Rows(RowIndex).Cells(3).Value = str
                GridDayparts.Rows(RowIndex).Cells(4).Value = str1
            End If
        Catch
        End Try


    End Sub
    Private Sub WriteGenreSelection(ByVal CategoryCodes As String, ByVal RowIndex As Integer)

        Dim ptnode, chnode As TreeNode
        Dim v() As String
        v = CategoryCodes.Split(",")

        'DynamicChecked = True

        For Each ptnode In tvw_Genre_Category.Nodes
            ptnode.Checked = False
            For Each chnode In ptnode.Nodes
                chnode.Checked = False
                Dim s As String = chnode.Text
                If Array.IndexOf(v, s) > -1 Then
                    chnode.Checked = True
                End If
            Next chnode
        Next ptnode
        ReadSelection(RowIndex)

        'DynamicChecked = False
    End Sub
    Private Sub WritePromoSelection(ByVal CategoryCodes As String)

        Dim PromoCat As Object
        Dim v() As String
        v = CategoryCodes.Split(",")

        'DynamicChecked = True

        For Each promocat In cmb_Promo_Category.Properties.Items
            PromoCat.checkstate = 0
            If Array.IndexOf(v, PromoCat.description) > -1 Then
                PromoCat.checkstate = 1
            End If
        Next PromoCat
        'Call ReadSelection()

        'DynamicChecked = False
    End Sub
    Private Sub tvw_Genre_Category_AfterCheck(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvw_Genre_Category.AfterCheck
        If DynamicChecked Then Exit Sub
        If e.Node.Text = "(Select All)" Then
            CheckAllNodes(e.Node.Checked)
        ElseIf e.Action <> TreeViewAction.Unknown AndAlso e.Node.Nodes.Count > 0 Then
            CheckAllChildNodes(e.Node, e.Node.Checked)
        End If
        If GridDayparts.Rows.Count <> 0 Then
            Call ReadSelection(GridDayparts.SelectedRows(0).Index)
        End If
    End Sub
  
    Private Sub tvw_Genre_Category_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvw_Genre_Category.LostFocus
        tvw_Genre_Category.Visible = False
    End Sub
    '
    Private Sub cmb_Genre_Category_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles cmb_Genre_Category.ButtonClick
        If tvw_Genre_Category.Visible = True Then tvw_Genre_Category.Visible = False Else tvw_Genre_Category.Visible = True
        btn_StoreSave.Enabled = True
    End Sub

    Private Sub cmb_Promo_Category_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Promo_Category.EditValueChanged
        Dim str, str1 As String
        If Not cmb_Promo_Category.EditValue = Nothing Then
            For Each s As Object In cmb_Promo_Category.Properties.Items
                If s.checkstate = 1 Then
                    'MsgBox(s.description)
                    ' MsgBox(s.value.ToString)
                    str += "," & s.value.ToString
                    str1 += "," & s.description.ToString
                End If
            Next
            If str <> "" Then
                str = Mid(str, 2)
                str1 = Mid(str1, 2)
                txt_Promo_Category.Text = str
                'MsgBox(GridDayparts.SelectedRows(0).Index)
                'If DynamicChecked AndAlso GridDayparts.Rows.Count > GridDayparts.CurrentRow.Index + 1 Then
                '    GridDayparts.Rows(GridDayparts.CurrentRow.Index + 1).Cells(5).Value = str
                '    GridDayparts.Rows(GridDayparts.CurrentRow.Index + 1).Cells(6).Value = str1
                'Else
                If GridDayparts.SelectedRows.Count = 0 Then
                    If GridDayparts.Rows.Count = 0 Then GridDayparts.Rows.Add()
                    GridDayparts.Rows(0).Cells(5).Value = str
                    GridDayparts.Rows(0).Cells(6).Value = str1
                Else
                    GridDayparts.Rows(GridDayparts.SelectedRows(0).Index).Cells(5).Value = str
                    GridDayparts.Rows(GridDayparts.SelectedRows(0).Index).Cells(6).Value = str1
                End If
               
                'End If
            Else
                GridDayparts.Rows(GridDayparts.SelectedRows(0).Index).Cells(5).Value = ""
                GridDayparts.Rows(GridDayparts.SelectedRows(0).Index).Cells(6).Value = ""
            End If
        Else
            Try
                GridDayparts.Rows(GridDayparts.SelectedRows(0).Index).Cells(5).Value = ""
                GridDayparts.Rows(GridDayparts.SelectedRows(0).Index).Cells(6).Value = ""
            Catch
            End Try
        End If
        btn_StoreSave.Enabled = True
    End Sub

    Private Sub GridDayparts_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridDayparts.SelectionChanged
        'If e.RowIndex = -1 Then Exit Sub
        'MsgBox(e.RowIndex)
        ' MsgBox(GridDayparts.SelectedRows(0).Index)
        If GridDayparts.Rows.Count <> 0 Then
            If GridDayparts.SelectedRows.Count = 0 Then
                Call ReadGridRow(0)
            ElseIf Not GridDayparts.SelectedRows(0).Cells(0).Value = Nothing Then
                Call ReadGridRow(GridDayparts.SelectedRows(0).Index)
            End If
        End If
    End Sub
    Private Sub ReadGridRow(ByVal RowIndex)
        If GridDayparts.Rows.Count = 0 Then Exit Sub
        With GridDayparts.Rows(RowIndex)
            If GridDayparts.Rows.Count <> 0 AndAlso .Cells(0).Value <> Nothing AndAlso .Cells(1).Value <> Nothing Then
                DynamicChecked = True
                txt_DayPart.Text = "" & .Cells(0).Value.ToString
                cmb_Daypart_From.Text = "" & .Cells(1).Value.ToString
                cmb_Daypart_To.Text = "" & .Cells(2).Value.ToString
                'cmb_Genre_Category.Text = .Cells(4).Value.ToString
                WriteGenreSelection(.Cells(6).Value.ToString, RowIndex)
                'cmb_Promo_Category.Text = .Cells(6).Value.ToString
                'Dim temp = .Cells(4).Value.ToString
                'WritePromoSelection(.Cells(4).Value.ToString)
                DynamicChecked = False
            End If
        End With
    End Sub

    Private Sub cmb_Daypart_To_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Daypart_To.SelectedIndexChanged
        If Not DynamicChecked Then
            Call GridDayPartToTimeChanged()
            btn_StoreSave.Enabled = True
        End If
    End Sub

    Private Sub cmb_Parent_Category_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles cmb_Parent_Category.EditValueChanging
        If btn_save_edit_category.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                btn_save_edit_category.PerformClick()
            End If
        End If
    End Sub

    Private Sub GridDayparts_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridDayparts.CellContentClick

    End Sub

    Private Sub Category_Info_Grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Category_Info_Grid.CellContentClick

    End Sub

    Private Sub StoresGrid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles StoresGrid.CellContentClick

    End Sub

    Private Sub Store_Sub_Tab_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Store_Sub_Tab.Paint

    End Sub
End Class
