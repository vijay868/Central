﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddPromo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.BrowseFile = New System.Windows.Forms.OpenFileDialog()
        Me.Genre_Panel = New DevExpress.XtraEditors.PanelControl()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Dt_Stop_Date = New DevExpress.XtraEditors.DateEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Dt_start_date = New DevExpress.XtraEditors.DateEdit()
        Me.lbl_Duration = New System.Windows.Forms.Label()
        Me.cmb_Normalization = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmb_Category = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbl_Status = New System.Windows.Forms.Label()
        Me.ConversionBar = New DevExpress.XtraEditors.MarqueeProgressBarControl()
        Me.lbl_form_title = New System.Windows.Forms.Label()
        Me.txt_AudioPath = New DevExpress.XtraEditors.TextEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Title = New DevExpress.XtraEditors.TextEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.btn_Browse = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_PromoID = New DevExpress.XtraEditors.TextEdit()
        Me.btn_Quit = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Add = New DevExpress.XtraEditors.SimpleButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Genre_Panel.SuspendLayout()
        CType(Me.Dt_Stop_Date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dt_Stop_Date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dt_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dt_start_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Normalization.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConversionBar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_AudioPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Title.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_PromoID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Genre_Panel
        '
        Me.Genre_Panel.Controls.Add(Me.Label8)
        Me.Genre_Panel.Controls.Add(Me.Dt_Stop_Date)
        Me.Genre_Panel.Controls.Add(Me.Label5)
        Me.Genre_Panel.Controls.Add(Me.Dt_start_date)
        Me.Genre_Panel.Controls.Add(Me.lbl_Duration)
        Me.Genre_Panel.Controls.Add(Me.cmb_Normalization)
        Me.Genre_Panel.Controls.Add(Me.Label7)
        Me.Genre_Panel.Controls.Add(Me.cmb_Category)
        Me.Genre_Panel.Controls.Add(Me.Label6)
        Me.Genre_Panel.Controls.Add(Me.lbl_Status)
        Me.Genre_Panel.Controls.Add(Me.ConversionBar)
        Me.Genre_Panel.Controls.Add(Me.lbl_form_title)
        Me.Genre_Panel.Controls.Add(Me.txt_AudioPath)
        Me.Genre_Panel.Controls.Add(Me.Label4)
        Me.Genre_Panel.Controls.Add(Me.txt_Title)
        Me.Genre_Panel.Controls.Add(Me.Label3)
        Me.Genre_Panel.Controls.Add(Me.cmb_Customer)
        Me.Genre_Panel.Controls.Add(Me.btn_Browse)
        Me.Genre_Panel.Controls.Add(Me.txt_PromoID)
        Me.Genre_Panel.Controls.Add(Me.btn_Quit)
        Me.Genre_Panel.Controls.Add(Me.btn_Add)
        Me.Genre_Panel.Controls.Add(Me.Label2)
        Me.Genre_Panel.Controls.Add(Me.Label1)
        Me.Genre_Panel.Location = New System.Drawing.Point(7, 9)
        Me.Genre_Panel.Name = "Genre_Panel"
        Me.Genre_Panel.Size = New System.Drawing.Size(564, 277)
        Me.Genre_Panel.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(346, 138)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(64, 15)
        Me.Label8.TabIndex = 78
        Me.Label8.Text = "Stop Date:"
        '
        'Dt_Stop_Date
        '
        Me.Dt_Stop_Date.EditValue = Nothing
        Me.Dt_Stop_Date.Location = New System.Drawing.Point(413, 135)
        Me.Dt_Stop_Date.Name = "Dt_Stop_Date"
        Me.Dt_Stop_Date.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dt_Stop_Date.Properties.Appearance.Options.UseFont = True
        Me.Dt_Stop_Date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Dt_Stop_Date.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.Dt_Stop_Date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.Dt_Stop_Date.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.Dt_Stop_Date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.Dt_Stop_Date.Size = New System.Drawing.Size(126, 22)
        Me.Dt_Stop_Date.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(346, 101)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 15)
        Me.Label5.TabIndex = 76
        Me.Label5.Text = "Start Date:"
        '
        'Dt_start_date
        '
        Me.Dt_start_date.EditValue = Nothing
        Me.Dt_start_date.Location = New System.Drawing.Point(413, 96)
        Me.Dt_start_date.Name = "Dt_start_date"
        Me.Dt_start_date.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dt_start_date.Properties.Appearance.Options.UseFont = True
        Me.Dt_start_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Dt_start_date.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.Dt_start_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.Dt_start_date.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.Dt_start_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.Dt_start_date.Size = New System.Drawing.Size(126, 22)
        Me.Dt_start_date.TabIndex = 5
        '
        'lbl_Duration
        '
        Me.lbl_Duration.AutoSize = True
        Me.lbl_Duration.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Duration.Location = New System.Drawing.Point(520, 198)
        Me.lbl_Duration.Name = "lbl_Duration"
        Me.lbl_Duration.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Duration.TabIndex = 74
        Me.lbl_Duration.Text = "Status:"
        Me.lbl_Duration.Visible = False
        '
        'cmb_Normalization
        '
        Me.cmb_Normalization.EditValue = ""
        Me.cmb_Normalization.Location = New System.Drawing.Point(257, 96)
        Me.cmb_Normalization.Name = "cmb_Normalization"
        Me.cmb_Normalization.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Normalization.Properties.Appearance.Options.UseFont = True
        Me.cmb_Normalization.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Normalization.Properties.NullText = ""
        Me.cmb_Normalization.Size = New System.Drawing.Size(65, 22)
        Me.cmb_Normalization.TabIndex = 2
        Me.cmb_Normalization.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(171, 101)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 15)
        Me.Label7.TabIndex = 73
        Me.Label7.Text = "Normalization:"
        Me.Label7.Visible = False
        '
        'cmb_Category
        '
        Me.cmb_Category.EditValue = ""
        Me.cmb_Category.Location = New System.Drawing.Point(414, 54)
        Me.cmb_Category.Name = "cmb_Category"
        Me.cmb_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Category.Properties.NullText = ""
        Me.cmb_Category.Size = New System.Drawing.Size(125, 22)
        Me.cmb_Category.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(353, 57)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 15)
        Me.Label6.TabIndex = 71
        Me.Label6.Text = "Category:"
        '
        'lbl_Status
        '
        Me.lbl_Status.AutoSize = True
        Me.lbl_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Status.Location = New System.Drawing.Point(40, 245)
        Me.lbl_Status.Name = "lbl_Status"
        Me.lbl_Status.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Status.TabIndex = 66
        Me.lbl_Status.Text = "Status:"
        '
        'ConversionBar
        '
        Me.ConversionBar.EditValue = 0
        Me.ConversionBar.Location = New System.Drawing.Point(319, 245)
        Me.ConversionBar.Name = "ConversionBar"
        Me.ConversionBar.Properties.ProgressAnimationMode = DevExpress.Utils.Drawing.ProgressAnimationMode.Cycle
        Me.ConversionBar.Size = New System.Drawing.Size(224, 21)
        Me.ConversionBar.TabIndex = 7
        Me.ConversionBar.Visible = False
        '
        'lbl_form_title
        '
        Me.lbl_form_title.AutoSize = True
        Me.lbl_form_title.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_form_title.Location = New System.Drawing.Point(222, 13)
        Me.lbl_form_title.Name = "lbl_form_title"
        Me.lbl_form_title.Size = New System.Drawing.Size(109, 15)
        Me.lbl_form_title.TabIndex = 69
        Me.lbl_form_title.Text = "Add New Promo"
        '
        'txt_AudioPath
        '
        Me.txt_AudioPath.EditValue = ""
        Me.txt_AudioPath.Location = New System.Drawing.Point(90, 174)
        Me.txt_AudioPath.Name = "txt_AudioPath"
        Me.txt_AudioPath.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_AudioPath.Properties.Appearance.Options.UseFont = True
        Me.txt_AudioPath.Size = New System.Drawing.Size(411, 22)
        Me.txt_AudioPath.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(17, 177)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 15)
        Me.Label4.TabIndex = 68
        Me.Label4.Text = "Select File:"
        '
        'txt_Title
        '
        Me.txt_Title.EditValue = ""
        Me.txt_Title.Location = New System.Drawing.Point(90, 135)
        Me.txt_Title.Name = "txt_Title"
        Me.txt_Title.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Title.Properties.Appearance.Options.UseFont = True
        Me.txt_Title.Size = New System.Drawing.Size(232, 22)
        Me.txt_Title.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(51, 138)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 15)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "Title:"
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(90, 54)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.NullText = ""
        Me.cmb_Customer.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Customer.TabIndex = 0
        '
        'btn_Browse
        '
        Me.btn_Browse.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Browse.Appearance.Options.UseFont = True
        Me.btn_Browse.Location = New System.Drawing.Point(503, 173)
        Me.btn_Browse.Name = "btn_Browse"
        Me.btn_Browse.Size = New System.Drawing.Size(36, 22)
        Me.btn_Browse.TabIndex = 8
        Me.btn_Browse.Text = "..."
        '
        'txt_PromoID
        '
        Me.txt_PromoID.EditValue = ""
        Me.txt_PromoID.Location = New System.Drawing.Point(90, 96)
        Me.txt_PromoID.Name = "txt_PromoID"
        Me.txt_PromoID.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PromoID.Properties.Appearance.Options.UseFont = True
        Me.txt_PromoID.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PromoID.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_PromoID.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_PromoID.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_PromoID.Size = New System.Drawing.Size(76, 22)
        Me.txt_PromoID.TabIndex = 1
        '
        'btn_Quit
        '
        Me.btn_Quit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Quit.Appearance.Options.UseFont = True
        Me.btn_Quit.Location = New System.Drawing.Point(434, 214)
        Me.btn_Quit.Name = "btn_Quit"
        Me.btn_Quit.Size = New System.Drawing.Size(108, 23)
        Me.btn_Quit.TabIndex = 10
        Me.btn_Quit.Text = "Exit"
        '
        'btn_Add
        '
        Me.btn_Add.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Add.Appearance.Options.UseFont = True
        Me.btn_Add.Location = New System.Drawing.Point(319, 213)
        Me.btn_Add.Name = "btn_Add"
        Me.btn_Add.Size = New System.Drawing.Size(108, 23)
        Me.btn_Add.TabIndex = 9
        Me.btn_Add.Text = "Add"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 101)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 15)
        Me.Label2.TabIndex = 64
        Me.Label2.Text = "Promo ID:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(21, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 15)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Customer:"
        '
        'frmAddPromo
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(576, 291)
        Me.Controls.Add(Me.Genre_Panel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmAddPromo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Promo"
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Genre_Panel.ResumeLayout(False)
        Me.Genre_Panel.PerformLayout()
        CType(Me.Dt_Stop_Date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dt_Stop_Date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dt_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dt_start_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Normalization.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConversionBar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_AudioPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Title.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_PromoID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents BrowseFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Genre_Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btn_Browse As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_PromoID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_Quit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Add As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lbl_form_title As System.Windows.Forms.Label
    Friend WithEvents txt_AudioPath As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_Title As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ConversionBar As DevExpress.XtraEditors.MarqueeProgressBarControl
    Friend WithEvents lbl_Status As System.Windows.Forms.Label
    Friend WithEvents cmb_Category As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmb_Normalization As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lbl_Duration As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Dt_start_date As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Dt_Stop_Date As DevExpress.XtraEditors.DateEdit
End Class
