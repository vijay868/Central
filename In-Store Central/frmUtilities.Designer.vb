﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUtilities
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.BrowseFile = New System.Windows.Forms.OpenFileDialog()
        Me.Genre_Panel = New DevExpress.XtraEditors.PanelControl()
        Me.cmb_Utilities = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_Quit = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Run = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Genre_Panel.SuspendLayout()
        CType(Me.cmb_Utilities.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Genre_Panel
        '
        Me.Genre_Panel.Controls.Add(Me.cmb_Utilities)
        Me.Genre_Panel.Controls.Add(Me.Label8)
        Me.Genre_Panel.Controls.Add(Me.btn_Quit)
        Me.Genre_Panel.Controls.Add(Me.btn_Run)
        Me.Genre_Panel.Location = New System.Drawing.Point(0, 1)
        Me.Genre_Panel.Name = "Genre_Panel"
        Me.Genre_Panel.Size = New System.Drawing.Size(437, 116)
        Me.Genre_Panel.TabIndex = 58
        '
        'cmb_Utilities
        '
        Me.cmb_Utilities.Location = New System.Drawing.Point(76, 32)
        Me.cmb_Utilities.Name = "cmb_Utilities"
        Me.cmb_Utilities.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Utilities.Properties.Appearance.Options.UseFont = True
        Me.cmb_Utilities.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Utilities.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cmb_Utilities.Size = New System.Drawing.Size(207, 22)
        Me.cmb_Utilities.TabIndex = 74
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(31, 35)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 15)
        Me.Label8.TabIndex = 73
        Me.Label8.Text = "Utility:"
        '
        'btn_Quit
        '
        Me.btn_Quit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Quit.Appearance.Options.UseFont = True
        Me.btn_Quit.Location = New System.Drawing.Point(312, 74)
        Me.btn_Quit.Name = "btn_Quit"
        Me.btn_Quit.Size = New System.Drawing.Size(108, 23)
        Me.btn_Quit.TabIndex = 62
        Me.btn_Quit.Text = "Exit"
        '
        'btn_Run
        '
        Me.btn_Run.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Run.Appearance.Options.UseFont = True
        Me.btn_Run.Location = New System.Drawing.Point(312, 31)
        Me.btn_Run.Name = "btn_Run"
        Me.btn_Run.Size = New System.Drawing.Size(108, 23)
        Me.btn_Run.TabIndex = 61
        Me.btn_Run.Text = "Run"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(1, -13)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(1313, 948)
        Me.XtraTabControl1.TabIndex = 59
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage2})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.Genre_Panel)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1307, 929)
        '
        'frmUtilities
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1370, 750)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Name = "frmUtilities"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Utilities"
        CType(Me.Genre_Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Genre_Panel.ResumeLayout(False)
        Me.Genre_Panel.PerformLayout()
        CType(Me.cmb_Utilities.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents BrowseFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Genre_Panel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_Quit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Run As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cmb_Utilities As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
End Class
