﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO
Imports DevExpress.Utils

Imports DevExpress.XtraEditors
Public Class License

    Private Sub License_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        EncpassPhrase = "The best w@y to destroy @n enemy is to make him a friend."
        EncsaltValue = "PPO998JJutyBVesZXsafpoklEWEQuyhbSEfghuvASPOOjg"

        DecpassPhrase = "The only way to h@ve @ friend is to be one."
        EncsaltValue = "PPO998JJutyBVesZXsafpoklEWEQuyhbSEfghuvASPOOjg"
        InitializeStore()
        Call LoadCustomers()
    End Sub
    Private Sub LoadCustomers()


        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        cmb_Customer.Properties.Columns(0).Visible = False
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)

        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""
        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub
    Private Sub LoadStores(ByVal Customer_Id As Integer)

        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")
        'Creating Columns for Display in lookup
        cmb_Store.Properties.DisplayMember = "Store_Name"
        cmb_Store.Properties.ValueMember = "Store_ID"
        cmb_Store.Properties.DataSource = dsStores.Tables(0)
        cmb_Store.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Store.Properties.PopulateColumns()
        cmb_Store.Properties.Columns(0).Visible = False
        cmb_Store.Properties.ShowFooter = False
        cmb_Store.Properties.ShowHeader = False
        cmb_Store.EditValue = ""

        dsStores.Dispose()
        dapStores.Dispose()
        Call CloseDBConnection()
    End Sub
    Private Sub cmb_Customer_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If Not cmb_Customer.EditValue = Nothing Then
            Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString))
            ActiveCustomer = Convert.ToInt32(cmb_Customer.EditValue.ToString)
            txt_licensecode.Text = ""
            txt_seedcode.Text = ""
        End If

    End Sub

    'Private Sub cmb_Store_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Store.EditValueChanged
    '    If Not cmb_Store.EditValue = Nothing Then

    '    End If
    'End Sub
    Public Function InsertSeedCode(ByVal Customer_Name As String, ByVal Store_Name As String, ByVal system_name As String, ByVal MacID As String, ByVal ExpiryDate As Date, ByVal LicenseCode As String, ByVal StoreID As String) As Boolean
        Dim returnval As Boolean
        Dim Cmd As New SqlCommand

        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcAddLicenseInfo"
            .Parameters.AddWithValue("@Customer_Name", Customer_Name).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Store_Name", Store_Name).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@System_Name", system_name).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@MAC_Address", MacID).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Expiry_Date", ExpiryDate).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@License_Code", LicenseCode).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .ExecuteNonQuery()
        End With

        Dim strResult As String
        strResult = Cmd.Parameters("@Result").Value
        If strResult = "Success" Then
            returnval = True
        Else
            XtraMessageBox.Show("Selected store not found", "Alert", MessageBoxButtons.OK)
            returnval = False
        End If
        Call CloseDBConnection()
        Return returnval
    End Function
    Private Sub InitializeStore()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Store.Properties.DataSource = dt
        dt.Dispose()
    End Sub
    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_generate.Click
        If txt_seedcode.Text = "" Then
            Exit Sub
        End If

        Dim SplitDesc As String()
        Dim DsystemName As String = System.String.Empty
        Dim DMotherBoardID As String = System.String.Empty
        Dim DMacID As String = System.String.Empty
        Dim DProcessorID As String = System.String.Empty
        Dim DCustomerName As String = System.String.Empty
        Dim DStoreName As String = System.String.Empty
        Dim DExpiryDate As Date

        Try
            SplitDesc = Split(txt_seedcode.Text, "*^")
            DsystemName = AESDecrypt(SplitDesc(0), EncpassPhrase, EncsaltValue)
            DMotherBoardID = AESDecrypt(SplitDesc(1), EncpassPhrase, EncsaltValue)
            DMacID = AESDecrypt(SplitDesc(2), EncpassPhrase, EncsaltValue)
            DProcessorID = AESDecrypt(SplitDesc(3), EncpassPhrase, EncsaltValue)
            DCustomerName = AESDecrypt(SplitDesc(5), EncpassPhrase, EncsaltValue)
            DStoreName = AESDecrypt(SplitDesc(4), EncpassPhrase, EncsaltValue)
            DExpiryDate = Dt_expiry_date.EditValue
        Catch ex As Exception
            XtraMessageBox.Show("Invalid Seed Code", "Alert", MessageBoxButtons.OK)
            Exit Sub
        End Try


        If Not cmb_Customer.Text = DCustomerName Then
            XtraMessageBox.Show("Invalid Seed Code... Customer Name is not matching", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If Not cmb_Store.Text = DStoreName Then
            XtraMessageBox.Show("Invalid Seed Code... Store Name is not matching", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If txt_seedcode.Text = "" Then
            XtraMessageBox.Show("Seed Code is empty", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If Dt_expiry_date.Text = "" Then
            XtraMessageBox.Show("Expiry date is empty", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Try
            If InsertSeedCode(DCustomerName, DStoreName, DsystemName, DMacID, DExpiryDate, txt_seedcode.Text, "") = True Then
                Dim ClientName As String = System.String.Empty
                Dim ClientStoreName As String = System.String.Empty
                Dim ClientSystemName As String = System.String.Empty
                Dim ClientMotherBoardID As String = System.String.Empty
                Dim ClientMacID As String = System.String.Empty
                Dim ClientProcessorID As String = System.String.Empty
                Dim SoftwareExpiryDate As String = System.String.Empty
                Dim FinalKeyStr As String = System.String.Empty

                ClientName = AESEncrypt(DCustomerName, DecpassPhrase, EncsaltValue)
                ClientStoreName = AESEncrypt(DStoreName, DecpassPhrase, EncsaltValue)
                ClientSystemName = AESEncrypt(DsystemName, DecpassPhrase, EncsaltValue)
                ClientMotherBoardID = AESEncrypt(DMotherBoardID, DecpassPhrase, EncsaltValue)
                ClientMacID = AESEncrypt(DMacID, DecpassPhrase, EncsaltValue)
                ClientProcessorID = AESEncrypt(DProcessorID, DecpassPhrase, EncsaltValue)
                SoftwareExpiryDate = AESEncrypt(DExpiryDate.ToString("MM/dd/yyyy"), DecpassPhrase, EncsaltValue)
                FinalKeyStr = ClientName & "*^" & ClientStoreName & "*^" & ClientSystemName & "*^" & ClientMotherBoardID & "*^" & ClientMacID & "*^" & ClientProcessorID & "*^" & SoftwareExpiryDate
                txt_licensecode.Text = FinalKeyStr
                btn_copy.Enabled = True
                btn_sendmail.Enabled = True
            Else
                txt_licensecode.Text = ""
            End If
        Catch ex As Exception
            XtraMessageBox.Show(ex.Message.ToString, "Alert", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub btn_copy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_copy.Click
        Try
            My.Computer.Clipboard.SetText(txt_licensecode.Text)
        Catch
        End Try
    End Sub

    Private Sub btn_sendmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_sendmail.Click
        Dim command As String
        Dim SendText As String
        'MsgBox(AESDecrypt(SplitDesc(2), EncpassPhrase, EncsaltValue))
        SendText = txt_licensecode.Text
        command = "mailto:?body=" & SendText
        Process.Start(command)
    End Sub

    Private Sub cmb_Store_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Store.EditValueChanged
        
    End Sub
End Class