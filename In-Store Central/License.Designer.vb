﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class License
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmb_Store = New DevExpress.XtraEditors.LookUpEdit()
        Me.Dt_expiry_date = New DevExpress.XtraEditors.DateEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_seedcode = New DevExpress.XtraEditors.MemoEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.btn_copy = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_sendmail = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_generate = New DevExpress.XtraEditors.SimpleButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_licensecode = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dt_expiry_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dt_expiry_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_seedcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.txt_licensecode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'PanelControl1
        '
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(502, 365)
        Me.PanelControl1.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(32, 15)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 74
        Me.Label5.Text = "Customer:"
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(96, 12)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.cmb_Customer.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Customer.TabIndex = 73
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(54, 43)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 15)
        Me.Label7.TabIndex = 75
        Me.Label7.Text = "Store:"
        '
        'cmb_Store
        '
        Me.cmb_Store.EditValue = ""
        Me.cmb_Store.Location = New System.Drawing.Point(96, 40)
        Me.cmb_Store.Name = "cmb_Store"
        Me.cmb_Store.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Store.Properties.Appearance.Options.UseFont = True
        Me.cmb_Store.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Store.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.cmb_Store.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Store.TabIndex = 76
        '
        'Dt_expiry_date
        '
        Me.Dt_expiry_date.EditValue = Nothing
        Me.Dt_expiry_date.Location = New System.Drawing.Point(96, 174)
        Me.Dt_expiry_date.Name = "Dt_expiry_date"
        Me.Dt_expiry_date.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dt_expiry_date.Properties.Appearance.Options.UseFont = True
        Me.Dt_expiry_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Dt_expiry_date.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.Dt_expiry_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.Dt_expiry_date.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.Dt_expiry_date.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.Dt_expiry_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.Dt_expiry_date.Size = New System.Drawing.Size(146, 22)
        Me.Dt_expiry_date.TabIndex = 77
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 177)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 15)
        Me.Label1.TabIndex = 78
        Me.Label1.Text = "Date of Expiry:"
        '
        'txt_seedcode
        '
        Me.txt_seedcode.EditValue = ""
        Me.txt_seedcode.Location = New System.Drawing.Point(96, 70)
        Me.txt_seedcode.Name = "txt_seedcode"
        Me.txt_seedcode.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.txt_seedcode.Size = New System.Drawing.Size(390, 96)
        Me.txt_seedcode.TabIndex = 2
        Me.txt_seedcode.ToolTip = " "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 15)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Seed Code:"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.btn_copy)
        Me.PanelControl2.Controls.Add(Me.btn_sendmail)
        Me.PanelControl2.Controls.Add(Me.btn_generate)
        Me.PanelControl2.Controls.Add(Me.Label3)
        Me.PanelControl2.Controls.Add(Me.txt_licensecode)
        Me.PanelControl2.Controls.Add(Me.Label2)
        Me.PanelControl2.Controls.Add(Me.txt_seedcode)
        Me.PanelControl2.Controls.Add(Me.Label1)
        Me.PanelControl2.Controls.Add(Me.Dt_expiry_date)
        Me.PanelControl2.Controls.Add(Me.cmb_Store)
        Me.PanelControl2.Controls.Add(Me.Label7)
        Me.PanelControl2.Controls.Add(Me.cmb_Customer)
        Me.PanelControl2.Controls.Add(Me.Label5)
        Me.PanelControl2.Location = New System.Drawing.Point(4, 4)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(496, 357)
        Me.PanelControl2.TabIndex = 1
        '
        'btn_copy
        '
        Me.btn_copy.Enabled = False
        Me.btn_copy.Location = New System.Drawing.Point(330, 306)
        Me.btn_copy.Name = "btn_copy"
        Me.btn_copy.Size = New System.Drawing.Size(75, 23)
        Me.btn_copy.TabIndex = 83
        Me.btn_copy.Text = "Copy"
        '
        'btn_sendmail
        '
        Me.btn_sendmail.Enabled = False
        Me.btn_sendmail.Location = New System.Drawing.Point(411, 306)
        Me.btn_sendmail.Name = "btn_sendmail"
        Me.btn_sendmail.Size = New System.Drawing.Size(75, 23)
        Me.btn_sendmail.TabIndex = 82
        Me.btn_sendmail.Text = "Send Mail"
        '
        'btn_generate
        '
        Me.btn_generate.Location = New System.Drawing.Point(96, 306)
        Me.btn_generate.Name = "btn_generate"
        Me.btn_generate.Size = New System.Drawing.Size(75, 23)
        Me.btn_generate.TabIndex = 2
        Me.btn_generate.Text = "Generate"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 205)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 15)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "License Code:"
        '
        'txt_licensecode
        '
        Me.txt_licensecode.Location = New System.Drawing.Point(96, 204)
        Me.txt_licensecode.Name = "txt_licensecode"
        Me.txt_licensecode.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.txt_licensecode.Size = New System.Drawing.Size(390, 96)
        Me.txt_licensecode.TabIndex = 80
        '
        'License
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(502, 365)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "License"
        Me.Text = "License"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dt_expiry_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dt_expiry_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_seedcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.txt_licensecode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmb_Store As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Dt_expiry_date As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_seedcode As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btn_generate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_licensecode As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btn_copy As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_sendmail As DevExpress.XtraEditors.SimpleButton
End Class
