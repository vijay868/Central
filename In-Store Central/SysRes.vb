﻿Imports System.Management
Module SysRes
    Public Function get_ProcessorID() As String

        Dim StrProcessorId As String = ""
        Dim query As New SelectQuery("Win32_processor")
        Dim search As New ManagementObjectSearcher(query)
        Dim info As ManagementObject

        For Each info In search.Get()
            StrProcessorId = info("processorid").ToString
        Next
        Return StrProcessorId

    End Function
    Public Function get_DriveSerialID() As String
        Dim StrDriveSerialId As String = ""
        Dim query As New SelectQuery("Win32_Win32_DiskDrive")
        Dim search As New ManagementObjectSearcher(query)
        Dim info As ManagementObject

        For Each info In search.Get()
            If info("SerialNumber") = "" Then
                StrDriveSerialId = "None"
            Else
                StrDriveSerialId = info("SerialNumber").ToString
            End If
        Next
        Return StrDriveSerialId
    End Function
    Public Function get_MotherboardID() As String
        Dim StrMotherboardId As String = ""
        Dim query As New SelectQuery("Win32_BaseBoard")
        Dim search As New ManagementObjectSearcher(query)
        Dim info As ManagementObject

        For Each info In search.Get()
            StrMotherboardId = info("SerialNumber").ToString
        Next
        Return StrMotherboardId
    End Function
    Public Function get_MacAddressID() As String
        Dim StrMacAddressId As String = ""
        Dim mc As System.Management.ManagementClass
        Dim mo As ManagementObject
        mc = New ManagementClass("Win32_NetworkAdapterConfiguration")
        Dim moc As ManagementObjectCollection = mc.GetInstances()
        For Each mo In moc
            If mo.Item("IPEnabled") = True Then
                StrMacAddressId = mo.Item("MacAddress").ToString()
            End If
        Next
        Return StrMacAddressId
    End Function
End Module
