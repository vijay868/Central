﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.XtraEditors
Public Class frmChangePassword
    Private Sub btn_Apply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Apply.Click

        If txt_OldPassword.Text.Trim = "" Then
            xtraMessageBox.Show("Enter old password", "Password Change Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_OldPassword.Focus()
            Exit Sub
        End If

        If txt_NewPassword.Text.Trim = "" Then
            xtraMessageBox.Show("Enter new password", "Password Change Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_NewPassword.Focus()
            Exit Sub
        End If

        If txt_ConfirmPassword.Text.Trim = "" Then
            xtraMessageBox.Show("Enter confirm password", "Password Change Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_ConfirmPassword.Focus()
            Exit Sub
        End If

        If txt_NewPassword.Text.Trim <> txt_ConfirmPassword.Text.Trim Then
            xtraMessageBox.Show("New password and confirm password doesn't match", "Password Change Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_ConfirmPassword.Focus()
            Exit Sub
        End If

        Call OpenDBConnection()
        Dim cmdChangePassword As New SqlCommand

        With cmdChangePassword
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcModifyPassword"
            .Parameters.AddWithValue("@User_Name", "Administrator").Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Old_Password", txt_OldPassword.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@New_Password", txt_NewPassword.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Confirm_Password", txt_ConfirmPassword.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .ExecuteNonQuery()

            Dim strResult As String
            strResult = .Parameters("@Result").Value
            If strResult = "Password changed successfully." Then
                xtraMessageBox.Show(strResult, "Password changed", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
                frmLogin.Show()
                frmLogin.txt_LoginID.Focus()
            Else
                xtraMessageBox.Show(strResult, "Password change Alert", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txt_OldPassword.Focus()
            End If
        End With

        cmdChangePassword = Nothing
        Call CloseDBConnection()


    End Sub

    Private Sub btn_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click
        Me.Close()
        frmLogin.Show()
    End Sub

    Private Sub txt_ConfirmPassword_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_ConfirmPassword.KeyDown
        If e.KeyData = Keys.Enter Then
            Call btn_Apply.PerformClick()
        End If
    End Sub
End Class