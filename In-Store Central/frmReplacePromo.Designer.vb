﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmreplacepromos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelReplace = New DevExpress.XtraEditors.PanelControl()
        Me.btn_Save = New DevExpress.XtraEditors.SimpleButton()
        Me.Genre_Info_Grid = New System.Windows.Forms.DataGridView()
        Me.Promo_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.PanelReplace, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelReplace.SuspendLayout()
        CType(Me.Genre_Info_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(1, -13)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(1313, 948)
        Me.XtraTabControl1.TabIndex = 59
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage2})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.PanelReplace)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1307, 929)
        '
        'PanelReplace
        '
        Me.PanelReplace.Controls.Add(Me.btn_Save)
        Me.PanelReplace.Controls.Add(Me.Genre_Info_Grid)
        Me.PanelReplace.Location = New System.Drawing.Point(3, 3)
        Me.PanelReplace.Name = "PanelReplace"
        Me.PanelReplace.Size = New System.Drawing.Size(297, 197)
        Me.PanelReplace.TabIndex = 74
        '
        'btn_Save
        '
        Me.btn_Save.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Save.Appearance.Options.UseFont = True
        Me.btn_Save.Location = New System.Drawing.Point(188, 169)
        Me.btn_Save.Name = "btn_Save"
        Me.btn_Save.Size = New System.Drawing.Size(104, 23)
        Me.btn_Save.TabIndex = 31
        Me.btn_Save.Text = "Save"
        '
        'Genre_Info_Grid
        '
        Me.Genre_Info_Grid.AllowUserToDeleteRows = False
        Me.Genre_Info_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.Genre_Info_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.Genre_Info_Grid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Genre_Info_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Genre_Info_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Genre_Info_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Genre_Info_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Genre_Info_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.Genre_Info_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Genre_Info_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Promo_ID, Me.Promo_Name})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Genre_Info_Grid.DefaultCellStyle = DataGridViewCellStyle3
        Me.Genre_Info_Grid.EnableHeadersVisualStyles = False
        Me.Genre_Info_Grid.Location = New System.Drawing.Point(5, 5)
        Me.Genre_Info_Grid.Name = "Genre_Info_Grid"
        Me.Genre_Info_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Genre_Info_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.Genre_Info_Grid.RowHeadersVisible = False
        Me.Genre_Info_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Genre_Info_Grid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Genre_Info_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Genre_Info_Grid.Size = New System.Drawing.Size(286, 163)
        Me.Genre_Info_Grid.TabIndex = 1
        '
        'Promo_ID
        '
        Me.Promo_ID.HeaderText = "Promo ID"
        Me.Promo_ID.Name = "Promo_ID"
        Me.Promo_ID.Visible = False
        '
        'Promo_Name
        '
        Me.Promo_Name.HeaderText = "Promo Name"
        Me.Promo_Name.Name = "Promo_Name"
        Me.Promo_Name.Width = 285
        '
        'frmreplacepromos
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(308, 207)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmreplacepromos"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Replace Promo"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.PanelReplace, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelReplace.ResumeLayout(False)
        CType(Me.Genre_Info_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelReplace As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Genre_Info_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents Promo_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn_Save As DevExpress.XtraEditors.SimpleButton
End Class
