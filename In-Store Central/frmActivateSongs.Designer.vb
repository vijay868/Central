﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActivateSongs
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmb_Category = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmb_Store = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmb_Genre = New DevExpress.XtraEditors.LookUpEdit()
        Me.btn_Save = New DevExpress.XtraEditors.SimpleButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_SongsCount = New DevExpress.XtraEditors.TextEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbl_Title = New DevExpress.XtraEditors.LabelControl()
        Me.Songs_Target_Grid = New System.Windows.Forms.DataGridView()
        Me.Target_Song_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_Category = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_Title = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_Album = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_Artist = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_FileName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Target_Duration = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Songs_Source_Grid = New System.Windows.Forms.DataGridView()
        Me.sid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.c = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.t = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.A = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.F = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.g = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.cmb_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Genre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_SongsCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Songs_Target_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Songs_Source_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmb_Category)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.cmb_Store)
        Me.Panel1.Controls.Add(Me.cmb_Customer)
        Me.Panel1.Controls.Add(Me.cmb_Genre)
        Me.Panel1.Controls.Add(Me.btn_Save)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_SongsCount)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.lbl_Title)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1341, 84)
        Me.Panel1.TabIndex = 2
        '
        'cmb_Category
        '
        Me.cmb_Category.EditValue = ""
        Me.cmb_Category.Location = New System.Drawing.Point(400, 60)
        Me.cmb_Category.Name = "cmb_Category"
        Me.cmb_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Category.Properties.NullText = ""
        Me.cmb_Category.Size = New System.Drawing.Size(129, 22)
        Me.cmb_Category.TabIndex = 58
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(343, 62)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 15)
        Me.Label5.TabIndex = 57
        Me.Label5.Text = "Category"
        '
        'cmb_Store
        '
        Me.cmb_Store.EditValue = ""
        Me.cmb_Store.Location = New System.Drawing.Point(868, 60)
        Me.cmb_Store.Name = "cmb_Store"
        Me.cmb_Store.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Store.Properties.Appearance.Options.UseFont = True
        Me.cmb_Store.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Store.Properties.NullText = ""
        Me.cmb_Store.Size = New System.Drawing.Size(258, 22)
        Me.cmb_Store.TabIndex = 56
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(868, 32)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.NullText = ""
        Me.cmb_Customer.Size = New System.Drawing.Size(258, 22)
        Me.cmb_Customer.TabIndex = 55
        '
        'cmb_Genre
        '
        Me.cmb_Genre.EditValue = ""
        Me.cmb_Genre.Location = New System.Drawing.Point(81, 60)
        Me.cmb_Genre.Name = "cmb_Genre"
        Me.cmb_Genre.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Genre.Properties.Appearance.Options.UseFont = True
        Me.cmb_Genre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Genre.Properties.NullText = ""
        Me.cmb_Genre.Size = New System.Drawing.Size(229, 22)
        Me.cmb_Genre.TabIndex = 49
        '
        'btn_Save
        '
        Me.btn_Save.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Save.Appearance.Options.UseFont = True
        Me.btn_Save.Enabled = False
        Me.btn_Save.Location = New System.Drawing.Point(1206, 60)
        Me.btn_Save.Name = "btn_Save"
        Me.btn_Save.Size = New System.Drawing.Size(93, 22)
        Me.btn_Save.TabIndex = 45
        Me.btn_Save.Text = "Save"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(1163, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 15)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Total"
        '
        'txt_SongsCount
        '
        Me.txt_SongsCount.EditValue = ""
        Me.txt_SongsCount.Enabled = False
        Me.txt_SongsCount.Location = New System.Drawing.Point(1206, 32)
        Me.txt_SongsCount.Name = "txt_SongsCount"
        Me.txt_SongsCount.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_SongsCount.Properties.Appearance.Options.UseFont = True
        Me.txt_SongsCount.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_SongsCount.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_SongsCount.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_SongsCount.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_SongsCount.Size = New System.Drawing.Size(93, 22)
        Me.txt_SongsCount.TabIndex = 43
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(826, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 15)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Store"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(802, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 15)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Customer"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(39, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 15)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Genre"
        '
        'lbl_Title
        '
        Me.lbl_Title.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Title.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbl_Title.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbl_Title.Dock = System.Windows.Forms.DockStyle.Top
        Me.lbl_Title.Location = New System.Drawing.Point(0, 0)
        Me.lbl_Title.Name = "lbl_Title"
        Me.lbl_Title.Size = New System.Drawing.Size(1341, 32)
        Me.lbl_Title.TabIndex = 0
        Me.lbl_Title.Text = "Activate/Deactivate Songs"
        '
        'Songs_Target_Grid
        '
        Me.Songs_Target_Grid.AllowDrop = True
        Me.Songs_Target_Grid.AllowUserToAddRows = False
        Me.Songs_Target_Grid.AllowUserToDeleteRows = False
        Me.Songs_Target_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        Me.Songs_Target_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.Songs_Target_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Songs_Target_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Songs_Target_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Songs_Target_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Songs_Target_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.Songs_Target_Grid.ColumnHeadersHeight = 25
        Me.Songs_Target_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.Songs_Target_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Target_Song_ID, Me.Target_Category, Me.Target_Title, Me.Target_Album, Me.Target_Artist, Me.Target_FileName, Me.Target_Duration})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Songs_Target_Grid.DefaultCellStyle = DataGridViewCellStyle11
        Me.Songs_Target_Grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Songs_Target_Grid.EnableHeadersVisualStyles = False
        Me.Songs_Target_Grid.Location = New System.Drawing.Point(803, 85)
        Me.Songs_Target_Grid.Name = "Songs_Target_Grid"
        Me.Songs_Target_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Songs_Target_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.Songs_Target_Grid.RowHeadersVisible = False
        Me.Songs_Target_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Songs_Target_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Songs_Target_Grid.Size = New System.Drawing.Size(499, 560)
        Me.Songs_Target_Grid.TabIndex = 4
        '
        'Target_Song_ID
        '
        Me.Target_Song_ID.HeaderText = "Song ID"
        Me.Target_Song_ID.Name = "Target_Song_ID"
        Me.Target_Song_ID.Width = 80
        '
        'Target_Category
        '
        Me.Target_Category.HeaderText = "Category"
        Me.Target_Category.Name = "Target_Category"
        Me.Target_Category.Width = 80
        '
        'Target_Title
        '
        Me.Target_Title.HeaderText = "Title"
        Me.Target_Title.Name = "Target_Title"
        Me.Target_Title.Width = 200
        '
        'Target_Album
        '
        Me.Target_Album.HeaderText = "Album"
        Me.Target_Album.Name = "Target_Album"
        Me.Target_Album.Width = 200
        '
        'Target_Artist
        '
        Me.Target_Artist.HeaderText = "Artist"
        Me.Target_Artist.Name = "Target_Artist"
        Me.Target_Artist.Visible = False
        '
        'Target_FileName
        '
        Me.Target_FileName.HeaderText = "FileName"
        Me.Target_FileName.Name = "Target_FileName"
        Me.Target_FileName.Visible = False
        '
        'Target_Duration
        '
        Me.Target_Duration.HeaderText = "Duration"
        Me.Target_Duration.Name = "Target_Duration"
        Me.Target_Duration.Visible = False
        '
        'Songs_Source_Grid
        '
        Me.Songs_Source_Grid.AllowDrop = True
        Me.Songs_Source_Grid.AllowUserToAddRows = False
        Me.Songs_Source_Grid.AllowUserToDeleteRows = False
        Me.Songs_Source_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        Me.Songs_Source_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        Me.Songs_Source_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Songs_Source_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Songs_Source_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Songs_Source_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Songs_Source_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.Songs_Source_Grid.ColumnHeadersHeight = 25
        Me.Songs_Source_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.Songs_Source_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.sid, Me.c, Me.t, Me.A, Me.Ar, Me.F, Me.d, Me.g})
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Songs_Source_Grid.DefaultCellStyle = DataGridViewCellStyle15
        Me.Songs_Source_Grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Songs_Source_Grid.EnableHeadersVisualStyles = False
        Me.Songs_Source_Grid.Location = New System.Drawing.Point(39, 86)
        Me.Songs_Source_Grid.Name = "Songs_Source_Grid"
        Me.Songs_Source_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Songs_Source_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.Songs_Source_Grid.RowHeadersVisible = False
        Me.Songs_Source_Grid.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Songs_Source_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Songs_Source_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Songs_Source_Grid.Size = New System.Drawing.Size(700, 560)
        Me.Songs_Source_Grid.TabIndex = 5
        '
        'sid
        '
        Me.sid.HeaderText = "Song ID"
        Me.sid.Name = "sid"
        Me.sid.Width = 80
        '
        'c
        '
        Me.c.HeaderText = "Category"
        Me.c.Name = "c"
        Me.c.Width = 80
        '
        't
        '
        Me.t.HeaderText = "Title"
        Me.t.Name = "t"
        Me.t.Width = 200
        '
        'A
        '
        Me.A.HeaderText = "Album"
        Me.A.Name = "A"
        Me.A.Width = 200
        '
        'Ar
        '
        Me.Ar.HeaderText = "Artist"
        Me.Ar.Name = "Ar"
        Me.Ar.Width = 200
        '
        'F
        '
        Me.F.HeaderText = "FileName"
        Me.F.Name = "F"
        '
        'd
        '
        Me.d.HeaderText = "Duration"
        Me.d.Name = "d"
        '
        'g
        '
        Me.g.HeaderText = "Genre Id"
        Me.g.Name = "g"
        '
        'frmActivateSongs
        '
        Me.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1341, 742)
        Me.Controls.Add(Me.Songs_Source_Grid)
        Me.Controls.Add(Me.Songs_Target_Grid)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmActivateSongs"
        Me.Text = "Activate Songs"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.cmb_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Store.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Genre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_SongsCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Songs_Target_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Songs_Source_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Songs_Target_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents lbl_Title As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_SongsCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btn_Save As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmb_Genre As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmb_Store As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Target_Song_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_Title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_Album As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_Artist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_FileName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Target_Duration As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmb_Category As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Songs_Source_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents sid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents c As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents t As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents A As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents F As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents d As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents g As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
