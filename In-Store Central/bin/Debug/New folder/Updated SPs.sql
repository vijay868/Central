-- =============================================
-- Create procedure basic template
-- =============================================
-- creating the store procedure
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'ProcCheckStoreConfiguration' 
	   AND 	  type = 'P')
    DROP PROCEDURE ProcCheckStoreConfiguration
GO

CREATE PROCEDURE ProcCheckStoreConfiguration(@Store_Id bigint,@Result varchar(75) output) 
as
Begin

set nocount on

Declare @Scount int;
Declare @Pcount int;

select @Scount=count(*) from viewactivatedsongs where store_id=@store_id
select @PCount=count(*) from viewactivatedpromos where store_id=@store_id


if @scount=0
	Begin
		set @result='Songs are not activated for this store'
		return
	End
if @pcount=0
	Begin
		select @pcount=count(*) from song_promo_ratio where store_id=@store_id and ratio not like '%-0'
		if @pcount<>0
			Begin
				set @result='Promos are not activated for this store'
				return
			end
	End

set @Result='Success';

set nocount off
End
	
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'ProcDeletePromo' 
	   AND 	  type = 'P')
    DROP PROCEDURE ProcDeletePromo
GO
CREATE PROCEDURE ProcDeletePromo (@Promo_Id BigInt, @FileName varchar(75) output,@Result varchar(5000) output)
AS
Begin
	Declare @If_Activated bigint;
	Select @If_Activated=count(*) from ViewActivatedPromos where promo_id=@promo_Id;
	
	If @If_Activated=0 --not activated to any store
		Begin
			select @FileName=[FileName] from Promos_Master where promo_id=@promo_id;
			Delete from Promos_Master where promo_id=@promo_id;
			set @result='Success'
		End 
	Else --song activated
		Begin
			Declare @Store_Names varchar(5000);
			select @Store_Names=coalesce(@Store_Names+', ', '','''')  + [Store_Name] from ViewActivatedPromos where
			promo_id=@promo_Id ;
			set @result='This promo is assigned to ' + @store_names + ' stores';
		End
End

Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'ProcDeleteSong' 
	   AND 	  type = 'P')
    DROP PROCEDURE ProcDeleteSong
GO
CREATE PROCEDURE ProcDeleteSong (@Song_Id BigInt, @FileName varchar(75) output, @Result varchar(5000) output)
AS
Begin
	Declare @If_Activated bigint;
	
	Select @If_Activated=count(*) from ViewActivatedSongs where song_id=@Song_Id;
	
	If @If_Activated=0 --not activated to any store
		Begin
			select @FileName=[filename] from  Songs_Master where song_id=@song_id;
			Delete from Songs_Master where song_id=@song_id;
			set @result='Success'
			
		End 
	Else --song activated
		Begin
			Declare @Store_Names varchar(5000);
			select @Store_Names=coalesce( @Store_Names+', ', '','''')  + [Store_Name] from ViewActivatedSongs where song_id=@Song_Id ;
			set @result='This song is assigned to ' + @store_names + ' stores';
		End
End




Go