﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangePassword
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.txt_ConfirmPassword = New DevExpress.XtraEditors.TextEdit()
        Me.lbl_ConfirmPassword = New DevExpress.XtraEditors.LabelControl()
        Me.txt_NewPassword = New DevExpress.XtraEditors.TextEdit()
        Me.txt_OldPassword = New DevExpress.XtraEditors.TextEdit()
        Me.lbl_NewPassword = New DevExpress.XtraEditors.LabelControl()
        Me.lbl_OldPassword = New DevExpress.XtraEditors.LabelControl()
        Me.btn_Cancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_Apply = New DevExpress.XtraEditors.SimpleButton()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.txt_ConfirmPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_NewPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_OldPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.txt_ConfirmPassword)
        Me.PanelControl1.Controls.Add(Me.lbl_ConfirmPassword)
        Me.PanelControl1.Controls.Add(Me.txt_NewPassword)
        Me.PanelControl1.Controls.Add(Me.txt_OldPassword)
        Me.PanelControl1.Controls.Add(Me.lbl_NewPassword)
        Me.PanelControl1.Controls.Add(Me.lbl_OldPassword)
        Me.PanelControl1.Controls.Add(Me.btn_Cancel)
        Me.PanelControl1.Controls.Add(Me.btn_Apply)
        Me.PanelControl1.Location = New System.Drawing.Point(14, 13)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(338, 214)
        Me.PanelControl1.TabIndex = 34
        '
        'txt_ConfirmPassword
        '
        Me.txt_ConfirmPassword.EditValue = ""
        Me.txt_ConfirmPassword.Location = New System.Drawing.Point(129, 108)
        Me.txt_ConfirmPassword.Name = "txt_ConfirmPassword"
        Me.txt_ConfirmPassword.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ConfirmPassword.Properties.Appearance.Options.UseFont = True
        Me.txt_ConfirmPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_ConfirmPassword.Size = New System.Drawing.Size(166, 20)
        Me.txt_ConfirmPassword.TabIndex = 3
        '
        'lbl_ConfirmPassword
        '
        Me.lbl_ConfirmPassword.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ConfirmPassword.Location = New System.Drawing.Point(20, 111)
        Me.lbl_ConfirmPassword.Name = "lbl_ConfirmPassword"
        Me.lbl_ConfirmPassword.Size = New System.Drawing.Size(104, 14)
        Me.lbl_ConfirmPassword.TabIndex = 41
        Me.lbl_ConfirmPassword.Text = "Confirm Password :"
        '
        'txt_NewPassword
        '
        Me.txt_NewPassword.EditValue = ""
        Me.txt_NewPassword.Location = New System.Drawing.Point(129, 67)
        Me.txt_NewPassword.Name = "txt_NewPassword"
        Me.txt_NewPassword.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NewPassword.Properties.Appearance.Options.UseFont = True
        Me.txt_NewPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_NewPassword.Size = New System.Drawing.Size(166, 20)
        Me.txt_NewPassword.TabIndex = 2
        '
        'txt_OldPassword
        '
        Me.txt_OldPassword.EditValue = ""
        Me.txt_OldPassword.Location = New System.Drawing.Point(129, 26)
        Me.txt_OldPassword.Name = "txt_OldPassword"
        Me.txt_OldPassword.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_OldPassword.Properties.Appearance.Options.UseFont = True
        Me.txt_OldPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_OldPassword.Size = New System.Drawing.Size(166, 20)
        Me.txt_OldPassword.TabIndex = 1
        '
        'lbl_NewPassword
        '
        Me.lbl_NewPassword.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NewPassword.Location = New System.Drawing.Point(36, 70)
        Me.lbl_NewPassword.Name = "lbl_NewPassword"
        Me.lbl_NewPassword.Size = New System.Drawing.Size(88, 14)
        Me.lbl_NewPassword.TabIndex = 38
        Me.lbl_NewPassword.Text = "New Password :"
        '
        'lbl_OldPassword
        '
        Me.lbl_OldPassword.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_OldPassword.Location = New System.Drawing.Point(43, 29)
        Me.lbl_OldPassword.Name = "lbl_OldPassword"
        Me.lbl_OldPassword.Size = New System.Drawing.Size(81, 14)
        Me.lbl_OldPassword.TabIndex = 37
        Me.lbl_OldPassword.Text = "Old Password :"
        '
        'btn_Cancel
        '
        Me.btn_Cancel.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Cancel.Appearance.Options.UseFont = True
        Me.btn_Cancel.Location = New System.Drawing.Point(191, 163)
        Me.btn_Cancel.Name = "btn_Cancel"
        Me.btn_Cancel.Size = New System.Drawing.Size(104, 22)
        Me.btn_Cancel.TabIndex = 5
        Me.btn_Cancel.Text = "Cancel"
        '
        'btn_Apply
        '
        Me.btn_Apply.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Apply.Appearance.Options.UseFont = True
        Me.btn_Apply.Location = New System.Drawing.Point(35, 163)
        Me.btn_Apply.Name = "btn_Apply"
        Me.btn_Apply.Size = New System.Drawing.Size(104, 22)
        Me.btn_Apply.TabIndex = 4
        Me.btn_Apply.Text = "Apply"
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'frmChangePassword
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(365, 239)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChangePassword"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Change Password"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.txt_ConfirmPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_NewPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_OldPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txt_OldPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl_NewPassword As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl_OldPassword As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btn_Cancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_Apply As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_NewPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_ConfirmPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbl_ConfirmPassword As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
End Class
