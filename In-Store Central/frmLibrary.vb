﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO
Imports DevExpress.Utils

Imports DevExpress.XtraEditors

Public Class frmLibrary
    Dim CurFileRowIndex As Integer = 0
    Dim bPlayMedia As Boolean
    Dim iRowIndex As Integer = 0
    Dim Searchstring As String = ""
    Dim StringFound As Boolean = False, bActivatedCount As Integer = 0
    Dim CurGenreId As Integer

    Private Sub LoadGenres()
        Dim cmdGenre As New SqlCommand, adpGenre As New SqlDataAdapter, dsGenre As New DataSet

        Call OpenDBConnection()

        With cmdGenre
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdGenre.CommandText = "ProcGetGenresList"
        adpGenre.SelectCommand = cmdGenre
        adpGenre.Fill(dsGenre, "GenreList")

        cmb_Genre.Properties.DisplayMember = "Genre_Name"
        cmb_Genre.Properties.ValueMember = "Genre_ID"
        cmb_Genre.Properties.DataSource = dsGenre.Tables("GenreList")
        cmb_Genre.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Genre.Properties.PopulateColumns()
        cmb_Genre.Properties.Columns(0).Visible = False
        cmb_Genre.Properties.ShowFooter = False
        cmb_Genre.Properties.ShowHeader = False


        cmb_Modify_Category.Properties.DisplayMember = "Genre_Name"
        cmb_Modify_Category.Properties.ValueMember = "Genre_ID"
        cmb_Modify_Category.Properties.DataSource = dsGenre.Tables("GenreList")
        cmb_Modify_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Modify_Category.Properties.ForceInitialize()
        cmb_Modify_Category.Properties.PopulateColumns()
        'MsgBox(cmb_Genre.Properties.Columns.VisibleCount)
        cmb_Modify_Category.Properties.Columns(0).Visible = False
        cmb_Modify_Category.Properties.ShowFooter = False
        cmb_Modify_Category.Properties.ShowHeader = False
        cmb_Genre.EditValue = ""


        If dsGenre.Tables("GenreList").Rows.Count <> 0 Then btn_SongActivate.Enabled = True Else btn_SongActivate.Enabled = False

        Call CloseDBConnection()

        dsGenre.Dispose()
        adpGenre.Dispose()



    End Sub
    Private Sub bindCategoriesGrid()

        Dim dr As SqlDataReader
        Dim command As New SqlCommand

        Try
            Call OpenDBConnection()

            Category_Modify_Grid.Rows.Clear()

            'Try
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure

            If Library_Tab_Control.SelectedTabPage.Text = "Songs" Then
                command.CommandText = "ProcGetGenreCategoriesList"
                command.Parameters.AddWithValue("@Genre_Id", Convert.ToInt32(cmb_Modify_Category.EditValue.ToString))
            Else
                command.CommandText = "ProcGetPromoCategoriesList"
                command.Parameters.AddWithValue("@Customer_Id", Convert.ToInt32(cmb_Modify_Category.EditValue.ToString))
            End If
            dr = command.ExecuteReader

            Dim i = 0
            If dr.HasRows Then
                While dr.Read
                    Category_Modify_Grid.Rows.Add(dr(0).ToString, dr(1).ToString, dr(2).ToString)
                    i = i + 1
                End While
            End If
            'Category_Info_Grid.Rows.Add("", "", Strings.Left(cmb_Parent_Category.Text, 3).ToUpper & "'")
            dr.Close()

            Call CloseDBConnection()
            ' Genre_Info_Grid.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub cmb_Parent_Category_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Modify_Category.EditValueChanged

        If Not cmb_Modify_Category.EditValue = Nothing Then

            Cursor = Cursors.WaitCursor
            'btn_StoreSave.Enabled = True
            Call bindCategoriesGrid()
            Cursor = Cursors.Default
        End If
    End Sub
    Private Sub LoadCustomers()


        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        'cmb_Customer.Properties.Columns(0).Visible = False
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)

        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""

        cmb_Customer_Storetab.Properties.DisplayMember = "Customer_Name"
        cmb_Customer_Storetab.Properties.ValueMember = "Customer_ID"
        cmb_Customer_Storetab.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer_Storetab.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer_Storetab.Properties.PopulateColumns()
        'MsgBox(cmb_Customer_Storetab.Properties.Columns.VisibleCount)
        'cmb_Customer_Storetab.Properties.Columns(0).Visible = False
        cmb_Customer_Storetab.Properties.ShowFooter = False
        cmb_Customer_Storetab.Properties.ShowHeader = False


        cmb_Modify_Category.Properties.DisplayMember = "Customer_Name"
        cmb_Modify_Category.Properties.ValueMember = "Customer_ID"
        cmb_Modify_Category.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Modify_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Modify_Category.Properties.ForceInitialize()
        cmb_Modify_Category.Properties.PopulateColumns()
        'MsgBox(cmb_Customer_Storetab.Properties.Columns.VisibleCount)
        cmb_Modify_Category.Properties.Columns(0).Visible = False
        cmb_Modify_Category.Properties.ShowFooter = False
        cmb_Modify_Category.Properties.ShowHeader = False
        cmb_Modify_Category.EditValue = ""

        cmb_Customer_Storetab.EditValue = ""

        If dsCustomer.Tables("CustomerList").Rows.Count <> 0 Then btn_ActivatePromo.Enabled = True Else btn_ActivatePromo.Enabled = False

        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub
    Private Sub Library_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call FormatSongsGrid(True)
        Call FormatPromosGrid(True)
        Call FormatStoreGrid("Songs", True)
        Call LoadGenres()
        '  Call LoadCustomers()
        Call InitializeStore()

    End Sub

    Private Sub cmb_Genre_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Genre.EditValueChanged
        If Not cmb_Genre.EditValue = Nothing Then
            Cursor = Cursors.WaitCursor
            If Grp_Songs_History.Visible = True Then Grp_Songs_History.Visible = False : SongsGrid.Height = 570
            CurGenreId = Convert.ToInt32(cmb_Genre.EditValue.ToString)
            Call LoadSongs(CurGenreId, 0, 0)
            txt_Songs_Search.Text = "<Search Song Id>"
            'If SongsGrid.RowCount <> 0 Then bPlayMedia = False : Call LoadMedia("Song")
            Cursor = Cursors.Default
            If SongsGrid.RowCount <> 0 Then SongsGrid.Sort(SongsGrid.Columns(0), ListSortDirection.Ascending)

            'Check if audio file exists
            For i As Integer = 0 To SongsGrid.Rows.Count - 1
                If SongsGrid.Rows(i).Cells("FileName").Value.ToString <> "" Then
                    If Not File.Exists(SongsArchivePath & "\" & SongsGrid.Rows(i).Cells("FileName").Value.ToString) Then
                        SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    End If
                End If
            Next

        End If
    End Sub
    Private Sub LoadMedia(ByVal MediaType As String)
        If MediaType = "Song" AndAlso SongsGrid.RowCount <> 0 Then
            MDIInstoreCentral.PlayAudioFile(SongsArchivePath & "\" & SongsGrid.Rows(0).Cells("FileName").Value)
            'MDIInstoreCentral.player.StopPlayback()
        ElseIf MediaType = "Promo" AndAlso PromosGrid.RowCount <> 0 Then
            MDIInstoreCentral.PlayAudioFile(PromosArchivePath & "\" & PromosGrid.Rows(0).Cells(2).Value)
            'MDIInstoreCentral.player.StopPlayback()
        ElseIf MediaType = "StoreSong" AndAlso StoresGrid.RowCount <> 0 Then
            MDIInstoreCentral.PlayAudioFile(SongsArchivePath & "\" & StoresGrid.Rows(0).Cells(4).Value.ToString)
            'MDIInstoreCentral.player.StopPlayback()
        ElseIf MediaType = "StorePromo" AndAlso StoresGrid.RowCount <> 0 Then
            MDIInstoreCentral.PlayAudioFile(PromosArchivePath & "\" & StoresGrid.Rows(0).Cells(3).Value.ToString)
            'MDIInstoreCentral.player.StopPlayback()
        End If
        PlayMode = "Pause"
    End Sub
    Private Function UpdateSongsCaterogy(ByVal CategoryName As String, ByVal CategoryID As Integer, ByVal MediaID As Integer) As Boolean
        Dim returval As Boolean

        Cursor = Cursors.WaitCursor
        Dim dr As SqlDataReader
        Dim command As New SqlCommand
        Dim ds As New DataSet

        Call OpenDBConnection()

        Try
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcModifyCategory"

            command.Parameters.AddWithValue("@Category_Type", CategoryName).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Category_ID", CategoryID).Direction = ParameterDirection.Input
            command.Parameters.AddWithValue("@Media_ID", MediaID).Direction = ParameterDirection.Input
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output

            command.ExecuteNonQuery()

            If command.Parameters("@Result").Value.ToString = "Replacement successful" Then
                returval = True
            Else
                ' XtraMessageBox.Show(command.Parameters("@Result").Value.ToString, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                returval = True
            End If
            Call CloseDBConnection()
            Cursor = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Return returval
    End Function
    Private Sub FormatSongsGrid(ByVal ResetStructure As Boolean)

        If ResetStructure Then
            Dim gridData As New DataTable
            gridData.Columns.Add("Song_ID")
            gridData.Columns.Add("Category")
            gridData.Columns.Add("Title")
            gridData.Columns.Add("Album")
            gridData.Columns.Add("Artist")
            gridData.Columns.Add("FileName")
            gridData.Columns.Add("Duration")
            gridData.Columns.Add("Normalization")
            gridData.Columns.Add("Genre_Id")
            'For i As Integer = 1 To 30
            '    ' PromosGrid.Rows.Add("", "", "", "")
            '    gridData.Rows.Add("", "", "", "", "", "")
            'Next

            SongsGrid.Columns.Clear()
            SongsGrid.DataSource = gridData
            SongsGrid.Columns(0).HeaderText = "Song ID"
        End If

        SongsGrid.Columns(0).Width = 80
        SongsGrid.Columns(1).Width = 80 '"Category"
        SongsGrid.Columns(2).Width = 310 '"Title"
        SongsGrid.Columns(3).Width = 310 '"Album"
        SongsGrid.Columns(4).Width = 310 '"Artist"
        SongsGrid.Columns(5).Width = 140 '"FileName"
        SongsGrid.Columns(6).Width = 75 '"Duration"
        SongsGrid.Columns(7).Width = 100 'normalization
        SongsGrid.Columns(7).Visible = False 'normalization was removed at later stages and hence was hidden without removing
        SongsGrid.Columns(8).Visible = False '"Genre_Id"
        SongsGrid.Refresh()
    End Sub
    Private Sub FormatStoreGrid(ByVal SelType As String, ByVal ResetStructure As Boolean)
        If SelType = "Songs" Then
            StoresGrid.Width = 1326
            If ResetStructure Then
                Dim gridData As New DataTable
                gridData.Columns.Add("Song_ID")
                gridData.Columns.Add("Category")
                gridData.Columns.Add("Title")
                gridData.Columns.Add("Album")
                gridData.Columns.Add("Artists")
                gridData.Columns.Add("FileName")
                gridData.Columns.Add("Duration")
                gridData.Columns.Add("GenreId")
                'For i As Integer = 1 To 30
                '    ' StoresGrid.Rows.Add("", "", "", "")
                '    gridData.Rows.Add("", "", "", "", "", "")
                'Next

                StoresGrid.Columns.Clear()
                StoresGrid.DataSource = gridData
                StoresGrid.Columns(0).HeaderText = "Song ID"
            End If

            StoresGrid.Columns(0).Width = 80
            StoresGrid.Columns(1).Width = 80
            StoresGrid.Columns(2).Width = 310
            StoresGrid.Columns(3).Width = 310
            StoresGrid.Columns(4).Width = 310
            StoresGrid.Columns(5).Width = 140
            StoresGrid.Columns(6).Width = 75
            StoresGrid.Columns(7).Visible = False '"Genre_Id"
            StoresGrid.Refresh()
        Else
            StoresGrid.Width = 1140 '940
            If ResetStructure Then
                Dim gridData As New DataTable
                gridData.Columns.Add("Promo_ID")
                gridData.Columns.Add("Category")
                gridData.Columns.Add("Title")
                gridData.Columns.Add("FileName")
                gridData.Columns.Add("Duration")
                gridData.Columns.Add("Start_Date")
                gridData.Columns.Add("Stop_Date")
                'For i As Integer = 1 To 30
                '    ' StoresGrid.Rows.Add("", "", "", "")
                '    gridData.Rows.Add("", "", "", "", "", "", "")
                'Next

                StoresGrid.Columns.Clear()
                StoresGrid.DataSource = gridData
                StoresGrid.Columns(0).HeaderText = "Promo ID"
                StoresGrid.Columns(5).HeaderText = "Start Date"
                StoresGrid.Columns(6).HeaderText = "Stop Date"
                StoresGrid.Columns(5).Width = 100
                StoresGrid.Columns(6).Width = 100
            End If

            StoresGrid.Columns(0).Width = 80
            StoresGrid.Columns(1).Width = 80
            StoresGrid.Columns(2).Width = 320
            StoresGrid.Columns(3).Width = 160
            StoresGrid.Columns(4).Width = 75
           
            StoresGrid.Refresh()
        End If

    End Sub
    Private Sub FormatPromosGrid(ByVal ResetStructure As Boolean)

        If ResetStructure Then
            Dim gridData As New DataTable
            gridData.Columns.Add("Promo_ID")
            gridData.Columns.Add("Category")
            gridData.Columns.Add("Title")
            gridData.Columns.Add("FileName")
            gridData.Columns.Add("Duration")
            gridData.Columns.Add("Normalization")
            gridData.Columns.Add("Start_Date")
            gridData.Columns.Add("Stop_Date")

            'For i As Integer = 1 To 30
            '    ' PromosGrid.Rows.Add("", "", "", "")
            '    gridData.Rows.Add("", "", "", "")
            'Next

            PromosGrid.Columns.Clear()
            PromosGrid.DataSource = gridData
            PromosGrid.Columns(0).HeaderText = "Promo ID"
            PromosGrid.Columns(6).HeaderText = "Start Date"
            PromosGrid.Columns(7).HeaderText = "Stop Date"
        End If

        PromosGrid.Columns(0).Width = 80
        PromosGrid.Columns(1).Width = 80
        PromosGrid.Columns(2).Width = 320
        PromosGrid.Columns(3).Width = 160
        PromosGrid.Columns(4).Width = 75
        PromosGrid.Columns(5).Width = 100
        PromosGrid.Columns(5).Visible = False
        PromosGrid.Columns(6).Width = 100
        PromosGrid.Columns(7).Width = 100
        PromosGrid.Refresh()
    End Sub

    Private Sub SongsGrid_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles SongsGrid.CellDoubleClick
        CurFileRowIndex = e.RowIndex
        If CurFileRowIndex = -1 Then Exit Sub

        MDIInstoreCentral.StopStream()
        MDIInstoreCentral.lbl_TotalTime.Text = ""
        MDIInstoreCentral.lbl_curtime.Text = ""
        PlayMode = "Play"

        If File.Exists(SongsArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells("FileName").Value) Then
            MDIInstoreCentral.PlayAudioFile(SongsArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells("FileName").Value)
        End If

    End Sub
    Private Sub LoadDummy()
        MDIInstoreCentral.StopStream()

        MDIInstoreCentral.lbl_TotalTime.Text = ""
        MDIInstoreCentral.lbl_curtime.Text = ""
        PlayMode = "Play"

        If File.Exists(Application.StartupPath & "\t.aac") Then
            MDIInstoreCentral.PlayAudioFile(Application.StartupPath & "\t.aac")
            'MDIInstoreCentral.ResetPlayer()
        End If
    End Sub


    Private Sub PromosGrid_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles PromosGrid.CellDoubleClick
        CurFileRowIndex = e.RowIndex
        If CurFileRowIndex = -1 Then Exit Sub

        MDIInstoreCentral.StopStream()
        MDIInstoreCentral.lbl_TotalTime.Text = ""
        MDIInstoreCentral.lbl_curtime.Text = ""
        PlayMode = "Play"

        If File.Exists(PromosArchivePath & "\" & PromosGrid.Rows(CurFileRowIndex).Cells(3).Value) Then
            MDIInstoreCentral.PlayAudioFile(PromosArchivePath & "\" & PromosGrid.Rows(CurFileRowIndex).Cells(3).Value)
        End If
    End Sub

    Public Sub Deletepromo(ByVal PromoID As Integer)

        Dim command As New SqlCommand
        Dim ds As New DataSet, strResult, strFileName As String



        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcDeletePromo"
            command.Parameters.AddWithValue("@Promo_Id", PromoID).Direction = ParameterDirection.Input
            command.Parameters.Add("@FileName", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()

            strResult = command.Parameters("@Result").Value
            Call CloseDBConnection()

            If strResult <> "Success" Then
                bActivatedCount += 1
                'XtraMessageBox.Show(strResult, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                strFileName = command.Parameters("@FileName").Value
                If File.Exists(PromosArchivePath & "\" & strFileName) Then
                    Try
                        File.Delete(PromosArchivePath & "\" & strFileName)
                        Application.DoEvents()
                    Catch ex As Exception
                        XtraMessageBox.Show(ex.Message.ToString)
                    End Try
                End If
                'Call LoadPromos()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Sub DeleteSong(ByVal SongID As Integer)

        Dim command As New SqlCommand
        Dim ds As New DataSet, strResult, strFileName As String

        Try
            Call OpenDBConnection()
            command.Connection = DBConn
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "ProcDeleteSong"
            command.Parameters.AddWithValue("@Song_Id", SongID).Direction = ParameterDirection.Input
            command.Parameters.Add("@FileName", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            command.ExecuteNonQuery()


            strResult = command.Parameters("@Result").Value
            Call CloseDBConnection()

            If strResult <> "Success" Then
                bActivatedCount += 1
                'XtraMessageBox.Show(strResult, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                strFileName = command.Parameters("@FileName").Value
                If File.Exists(SongsArchivePath & "\" & strFileName) Then
                    Try
                        File.Delete(SongsArchivePath & "\" & strFileName)
                        Application.DoEvents()
                    Catch ex As Exception

                    End Try
                End If
                'Call LoadSongs()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub LoadSongs(ByVal GenreId As Integer, Optional ByVal CurRow As Integer = 0, Optional ByVal CurFirstRowIndex As Integer = 0)

        If GenreId = 0 Then Exit Sub

        Dim Cmd As New SqlCommand, Dadp As New SqlDataAdapter, Ds As New DataSet
        Call OpenDBConnection()

        Dim FilesCount As Integer, i As Integer

        Cmd.Parameters.Clear()
        Cmd.Connection = DBConn
        Cmd.CommandType = CommandType.StoredProcedure
        Cmd.CommandText = "ProcGetSongsList"
        Cmd.Parameters.Add("@Genre_ID", SqlDbType.BigInt).Direction = ParameterDirection.Input
        'Cmd.Parameters.AddWithValue("@Genre_Id", 3).Direction = ParameterDirection.Input
        Cmd.Parameters("@Genre_ID").Value = GenreId 'Convert.ToInt32(cmb_Genre.EditValue.ToString)
        Dadp.SelectCommand = Cmd
        Dadp.Fill(Ds, "SongsList")

        If Ds.Tables(0).Rows.Count > 0 Then
            SongsGrid.Columns.Clear()
            SongsGrid.DataSource = Ds.Tables(0)
            FormatSongsGrid(False)
            txt_SongsCount.Text = SongsGrid.Rows.Count
            SongsGrid.Columns(0).HeaderText = "Song ID"
            Application.DoEvents()
            'Check if audio file exists
            For i = 0 To SongsGrid.Rows.Count - 1
                If SongsGrid.Rows(i).Cells("FileName").Value.ToString <> "" Then
                    If Not File.Exists(SongsArchivePath & "\" & SongsGrid.Rows(i).Cells("FileName").Value.ToString) Then
                        SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    End If
                End If
            Next
            Try

                SongsGrid.FirstDisplayedScrollingRowIndex = CurFirstRowIndex
                SongsGrid.Rows(CurRow).Selected = True
            Catch
            End Try
        Else
            'If SongsGrid.Rows.Count = 0 Then
            txt_SongsCount.Text = 0
            FormatSongsGrid(True)
            'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If

        CloseDBConnection()
    End Sub

    Private Sub btn_SongActivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_SongActivate.Click
        Dim CurGenre As String

        CurGenre = cmb_Genre.Text

        If ActiveMdiChild IsNot Nothing Then
            ActiveMdiChild.Close()
        End If


        Dim w As New frmActivateSongs

        With w
            .MdiParent = MDIInstoreCentral
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
            w.cmb_Genre.Text = CurGenre
        End With

    End Sub

    Private Sub btn_ActivatePromo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ActivatePromo.Click
        Dim CurCustomer As String

        CurCustomer = cmb_Customer.Text

        If ActiveMdiChild IsNot Nothing Then
            ActiveMdiChild.Close()
        End If

        Dim w As New frmActivatePromos

        With w
            .MdiParent = MDIInstoreCentral
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
            .cmb_Customer.Text = CurCustomer
        End With

    End Sub
    Public Sub RefreshSongs(Optional ByVal CurRow As Integer = 0, Optional ByVal CurFirstRowIndex As Integer = 0)
        If ActiveGenreId <> 0 Then
            Cursor = Cursors.WaitCursor
            'CurGenreId = Convert.ToInt32(cmb_Genre.EditValue.ToString)
            Call LoadSongs(ActiveGenreId, CurRow, CurFirstRowIndex)
            txt_Songs_Search.Text = "<Search Song Id>"
            'If SongsGrid.RowCount <> 0 Then bPlayMedia = False : Call LoadMedia("Song")
            Cursor = Cursors.Default
            If SongsGrid.RowCount <> 0 Then SongsGrid.Sort(SongsGrid.Columns(0), ListSortDirection.Ascending)

            'Check if audio file exists
            For i As Integer = 0 To SongsGrid.Rows.Count - 1
                If SongsGrid.Rows(i).Cells("FileName").Value.ToString <> "" Then
                    If Not File.Exists(SongsArchivePath & "\" & SongsGrid.Rows(i).Cells("FileName").Value.ToString) Then
                        SongsGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    End If
                End If
            Next
            Try
                SongsGrid.Rows(0).Selected = False
                SongsGrid.FirstDisplayedScrollingRowIndex = CurFirstRowIndex
                SongsGrid.Rows(CurRow).Selected = True
            Catch
            End Try
        End If
    End Sub
    Public Sub RefreshPromos(Optional ByVal CurRow As Integer = 0, Optional ByVal CurFirstRowIndex As Integer = 0)

        If ActiveCustomerId <> 0 Then
            Cursor = Cursors.WaitCursor
            '
            Call LoadPromos(ActiveCustomerId, CurRow, CurFirstRowIndex)

            If PromosGrid.RowCount <> 0 Then PromosGrid.Sort(PromosGrid.Columns(0), ListSortDirection.Ascending)
            Cursor = Cursors.Default
            ''Check if promos file exists
            For i As Integer = 0 To PromosGrid.Rows.Count - 1
                If PromosGrid.Rows(i).Cells(2).Value.ToString <> "" Then
                    If Not File.Exists(PromosArchivePath & "\" & PromosGrid.Rows(i).Cells(3).Value.ToString) Then
                        PromosGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    End If
                End If
            Next
            Try
                PromosGrid.Rows(0).Selected = False
                PromosGrid.FirstDisplayedScrollingRowIndex = CurFirstRowIndex
                PromosGrid.Rows(CurRow).Selected = True
            Catch
            End Try
        End If
    End Sub
    Private Sub cmb_Customer_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If Not cmb_Customer.EditValue = Nothing Then
            'CurCustomerId = Convert.ToInt32(cmb_Customer.EditValue.ToString)
            If cmb_Customer.Text.ToString <> "" Then
                Cursor = Cursors.WaitCursor
                If Grp_Promos_History.Visible = True Then Grp_Promos_History.Visible = False : PromosGrid.Height = 570
                Call LoadPromos(Convert.ToInt32(cmb_Customer.EditValue.ToString), 0, 0)
                If PromosGrid.RowCount <> 0 Then PromosGrid.Sort(PromosGrid.Columns(0), ListSortDirection.Ascending)
                Cursor = Cursors.Default
                ''Check if promos file exists
                For i As Integer = 0 To PromosGrid.Rows.Count - 1
                    If PromosGrid.Rows(i).Cells(2).Value.ToString <> "" Then
                        If Not File.Exists(PromosArchivePath & "\" & PromosGrid.Rows(i).Cells(3).Value.ToString) Then
                            PromosGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                        End If
                    End If
                Next
            End If
        End If
    End Sub
    Private Sub LoadPromos(ByVal CustomerId As Integer, Optional ByVal CurRow As Integer = 0, Optional ByVal CurFirstRowIndex As Integer = 0)

        If CustomerId = 0 Then Exit Sub

        Dim Cmd As New SqlCommand, Dadp As New SqlDataAdapter, Ds As New DataSet
        Dim FilesCount As Integer, i As Integer

        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With


        Cmd.Parameters.Clear()
        Cmd.CommandText = "ProcGetPromosListForCustomer"
        Cmd.Parameters.AddWithValue("@Customer_ID", CustomerId).Direction = ParameterDirection.Input


        Dadp.SelectCommand = Cmd
        Dadp.Fill(Ds, "PromosList")

        If Ds.Tables(0).Rows.Count > 0 Then
            PromosGrid.Columns.Clear()
          
            PromosGrid.DataSource = Ds.Tables(0)
            FormatPromosGrid(False)
            txt_PromosCount.Text = PromosGrid.Rows.Count
            ''Check if promos file exists
            For i = 0 To PromosGrid.Rows.Count - 1
                If PromosGrid.Rows(i).Cells(3).Value.ToString <> "" Then
                    If Not File.Exists(PromosArchivePath & "\" & PromosGrid.Rows(i).Cells(3).Value.ToString) Then
                        PromosGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    End If
                End If
            Next
            Try

                PromosGrid.FirstDisplayedScrollingRowIndex = CurFirstRowIndex
                PromosGrid.Rows(CurRow).Selected = True
            Catch
            End Try
        Else
            txt_PromosCount.Text = 0
            FormatPromosGrid(True)
            ' XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If

        CloseDBConnection()

    End Sub

    Private Sub btn_AddPromo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_AddPromo.Click

        MDIInstoreCentral.PlayerPanel.Visible = False
        MDIInstoreCentral.ResetPlayer()

        If PromosArchivePath = "" Then
            XtraMessageBox.Show("Audio path for Promos is not configured", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        ActiveCustomer = cmb_Customer.Text
        Me.Close()
        'If ActiveMdiChild IsNot Nothing Then
        '    ActiveMdiChild.Close()
        'End If


        Dim w As New frmAddPromo

        With w
            .MdiParent = MDIInstoreCentral
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
            .cmb_Customer.Text = ActiveCustomer
        End With
    End Sub

    'Private Sub Library_Tab_Control_Selected(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageEventArgs) Handles Library_Tab_Control.TabIndexChanged
    '    If Library_Tab_Control.TabIndex = 1 Then
    '        cmb_Customer.Properties.Columns(0).Visible = False
    '    End If
    'End Sub

    Private Sub Library_Tab_Control_SelectedPageChanged(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles Library_Tab_Control.SelectedPageChanged
        CurFileRowIndex = 0
        'MDIInstoreCentral.player.Close()

        MDIInstoreCentral.lbl_TotalTime.Text = ""
        MDIInstoreCentral.lbl_curtime.Text = ""
        bPlayMedia = False

        MDIInstoreCentral.ResetPlayer()

        If Library_Tab_Control.SelectedTabPageIndex = 1 Then
            If ActiveCustomer <> "" Then cmb_Customer.Text = ActiveCustomer : Call LoadPromos(0, 0, 0)
            LoadCustomers()
            cmb_Customer.Properties.PopulateColumns()
            cmb_Customer.Properties.Columns(0).Visible = False
            'Call LoadCustomers()
        ElseIf Library_Tab_Control.SelectedTabPageIndex = 0 Then
            LoadGenres()
        ElseIf Library_Tab_Control.SelectedTabPageIndex = 2 Then
            LoadCustomers()
            cmb_Customer_Storetab.Properties.ForceInitialize()
            cmb_Customer_Storetab.Properties.PopulateColumns()
            If cmb_Customer_Storetab.Properties.Columns.Count <> 0 Then cmb_Customer_Storetab.Properties.Columns(0).Visible = False
        End If
    End Sub
    Private Sub InitializeStore()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Store_Storetab.Properties.DataSource = dt
        dt.Dispose()
    End Sub
    Private Sub btn_Go_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Go.Click
        Application.DoEvents()
        If cmb_Store_Storetab.EditValue = Nothing Then Exit Sub
        Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter
        Dim ifChecked As Boolean = False
        Call OpenDBConnection()

        MDIInstoreCentral.ResetPlayer()

        bPlayMedia = False

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoreSongPromoList"
            .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Store_Storetab.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Filter_Criteria", cmb_Type_Storetab.Text).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 0).Direction = ParameterDirection.Input

        End With
        dapSongs.SelectCommand = Cmd
        dapSongs.Fill(dsSongs, "List")

        If dsSongs.Tables("List").Rows.Count <> 0 Then
            StoresGrid.Columns.Clear()
            StoresGrid.DataSource = dsSongs.Tables("List")
            FormatStoreGrid(cmb_Type_Storetab.Text, False)
            'Check if audio file exists
CheckAudioExists:
            For i = 0 To StoresGrid.Rows.Count - 1
                If StoresGrid.Rows(i).Cells(0).Value.ToString = "" Then Exit For
                If cmb_Type_Storetab.Text = "Songs" Then
                    If Not File.Exists(SongsArchivePath & "\" & StoresGrid.Rows(i).Cells("FileName").Value.ToString) Then
                        StoresGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    End If
                ElseIf cmb_Type_Storetab.Text = "Promos" Then
                    If Not File.Exists(PromosArchivePath & "\" & StoresGrid.Rows(i).Cells("FileName").Value.ToString) Then
                        StoresGrid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    End If
                End If
            Next
            txt_Filecount_Storetab.Text = StoresGrid.Rows.Count
        Else 'no records
            txt_Filecount_Storetab.Text = 0
            FormatStoreGrid(cmb_Type_Storetab.Text, True)
            'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If

        dsSongs.Dispose()
        dapSongs.Dispose()
        Call CloseDBConnection()

        If StoresGrid.RowCount <> 0 Then
            StoresGrid.Sort(StoresGrid.Columns(0), ListSortDirection.Ascending)
            If Not ifChecked Then
                ifChecked -= True
                GoTo CheckAudioExists
            End If
            '    If cmb_Type_Storetab.Text = "Songs" Then
            '        Call LoadMedia("StoreSong")
            '    ElseIf cmb_Type_Storetab.Text = "Promo" Then
            '        Call LoadMedia("StorePromo")
            '    End If
        End If

    End Sub

    Private Sub cmb_Customer_Storetab_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Customer_Storetab.EditValueChanged
        If Not cmb_Customer_Storetab.EditValue = Nothing Then
            Call LoadStores(Convert.ToInt32(cmb_Customer_Storetab.EditValue.ToString))
            If StoresGrid.RowCount <> 0 Then StoresGrid.Sort(StoresGrid.Columns(0), ListSortDirection.Ascending)
        End If

    End Sub

    Private Sub LoadStores(ByVal Customer_Id As Integer)

        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")


        'Creating Columns for Display in lookup
        cmb_Store_Storetab.Properties.DisplayMember = "Store_Name"
        cmb_Store_Storetab.Properties.ValueMember = "Store_ID"
        cmb_Store_Storetab.Properties.DataSource = dsStores.Tables(0)
        cmb_Store_Storetab.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Store_Storetab.Properties.PopulateColumns()
        cmb_Store_Storetab.Properties.Columns(0).Visible = False
        cmb_Store_Storetab.Properties.ShowFooter = False
        cmb_Store_Storetab.Properties.ShowHeader = False
        cmb_Store_Storetab.EditValue = ""

        If dsStores.Tables(0).Rows.Count <> 0 Then btn_Go.Enabled = True Else btn_Go.Enabled = False

        dsStores.Dispose()
        dapStores.Dispose()


        Call CloseDBConnection()
    End Sub
    'Private Sub SongsPlayer_PlayStateChange(ByVal sender As Object, ByVal e As AxWMPLib._WMPOCXEvents_PlayStateChangeEvent) Handles SongsPlayer.PlayStateChange

    '    Static Dim PlayMe As Boolean = True

    '    Select Case SongsPlayer.playState
    '        Case 3
    '            If Not bPlayMedia Then SongsPlayer.Ctlcontrols.pause() : bPlayMedia = True
    '        Case 10
    '            If PlayMe Then
    '                SongsPlayer.Ctlcontrols.play()
    '            End If
    '        Case 8
    '            PlayMe = False

    '            'If PlaySong() Then

    '            SongsPlayer.Ctlcontrols.play()
    '            PlayMe = True

    '            'End If
    '    End Select
    'End Sub
    '    Private Function PlaySong() As Boolean
    '        Dim res As Boolean = True
    'NxtFile:
    '        If CurFileRowIndex < SongsGrid.RowCount - 1 Then
    '            CurFileRowIndex += 1
    '            If File.Exists(SongsArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells(4).Value) Then

    '                SongsPlayer.URL = SongsArchivePath & "\" & SongsGrid.Rows(CurFileRowIndex).Cells(4).Value
    '                'If Not bPlayMedia Then SongsPlayer.Ctlcontrols.pause()
    '                res = True
    '            Else
    '                CurFileRowIndex += 1
    '                'GoTo nxtfile
    '            End If
    '        Else
    '            res = False
    '        End If
    '        Return res
    '    End Function
    'Private Sub PromosPlayer_PlayStateChange(ByVal sender As Object, ByVal e As AxWMPLib._WMPOCXEvents_PlayStateChangeEvent) Handles PromosPlayer.PlayStateChange
    '    Static Dim PlayMe As Boolean = True
    '    Select Case PromosPlayer.playState
    '        Case 3
    '            If Not bPlayMedia Then PromosPlayer.Ctlcontrols.pause() : bPlayMedia = True
    '        Case 10
    '            If PlayMe Then
    '                PromosPlayer.Ctlcontrols.play()
    '            End If
    '        Case 8
    '            PlayMe = False

    '            'If PlayPromo() Then
    '            PromosPlayer.Ctlcontrols.play()
    '            PlayMe = True
    '            'End If
    '    End Select
    'End Sub
    '    Private Function PlayPromo() As Boolean
    '        Dim res As Boolean = True
    'NxtFile:
    '        If CurFileRowIndex < SongsGrid.RowCount - 1 Then
    '            CurFileRowIndex += 1
    '            If File.Exists(PromosArchivePath & "\" & PromosGrid.Rows(CurFileRowIndex).Cells(2).Value) Then
    '                PromosPlayer.URL = PromosArchivePath & "\" & PromosGrid.Rows(CurFileRowIndex).Cells(2).Value
    '                res = True
    '            Else
    '                CurFileRowIndex += 1
    '                'GoTo nxtfile
    '            End If
    '        Else
    '            res = False
    '        End If
    '        Return res
    '    End Function
    'Private Sub StorePlayer_PlayStateChange(ByVal sender As Object, ByVal e As AxWMPLib._WMPOCXEvents_PlayStateChangeEvent) Handles StorePlayer.PlayStateChange
    '    Static Dim PlayMe As Boolean = True
    '    Select Case StorePlayer.playState
    '        Case 3
    '            If Not bPlayMedia Then StorePlayer.Ctlcontrols.pause() : bPlayMedia = True
    '        Case 10
    '            If PlayMe Then
    '                StorePlayer.Ctlcontrols.play()
    '            End If
    '        Case 8
    '            PlayMe = False

    '            'If PlayStore() Then
    '            StorePlayer.Ctlcontrols.play()
    '            PlayMe = True
    '            'End If
    '    End Select
    'End Sub
    '    Private Function PlayStore() As Boolean
    '        Dim res As Boolean = True
    'NxtFile:
    '        If CurFileRowIndex < StoresGrid.RowCount - 1 Then
    '            CurFileRowIndex += 1
    '            If cmb_Type_Storetab.Text = "Songs" AndAlso File.Exists(SongsArchivePath & "\" & StoresGrid.Rows(CurFileRowIndex).Cells(4).Value.ToString) Then
    '                StorePlayer.URL = SongsArchivePath & "\" & StoresGrid.Rows(CurFileRowIndex).Cells(4).Value.ToString
    '                res = True
    '            ElseIf cmb_Type_Storetab.Text = "Promos" AndAlso File.Exists(PromosArchivePath & "\" & StoresGrid.Rows(CurFileRowIndex).Cells(2).Value.ToString) Then
    '                StorePlayer.URL = SongsArchivePath & "\" & StoresGrid.Rows(CurFileRowIndex).Cells(4).Value.ToString
    '                res = True
    '            Else
    '                CurFileRowIndex += 1
    '                'GoTo nxtfile
    '            End If

    '        Else
    '            res = False
    '        End If
    '        Return res
    '    End Function

    Private Sub Grid_Songs_History_CellMouseEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Songs_History.CellMouseEnter
        'If e.ColumnIndex = 11 And e.RowIndex = 4 Then
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 1 Then
            If Grid_Songs_History.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString <> "" Then
                Dim c As DataGridViewCell
                c = Grid_Songs_History.Rows(e.RowIndex).Cells(e.ColumnIndex)
                c.ToolTipText = Grid_Songs_History.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value.ToString
            End If
        End If
    End Sub
    Private Sub Grid_Promos_History_CellMouseEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Promos_History.CellMouseEnter
        'If e.ColumnIndex = 11 And e.RowIndex = 4 Then
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 1 Then
            If Grid_Promos_History.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString <> "" Then
                Dim c As DataGridViewCell
                c = Grid_Promos_History.Rows(e.RowIndex).Cells(e.ColumnIndex)
                c.ToolTipText = Grid_Promos_History.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value.ToString
            End If
        End If
    End Sub
    Private Sub StoresGrid_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles StoresGrid.CellDoubleClick
        CurFileRowIndex = e.RowIndex
        If CurFileRowIndex = -1 Then Exit Sub

        MDIInstoreCentral.StopStream()
        MDIInstoreCentral.lbl_TotalTime.Text = ""
        MDIInstoreCentral.lbl_curtime.Text = ""
        PlayMode = "Play"

        If cmb_Type_Storetab.Text = "Songs" AndAlso File.Exists(SongsArchivePath & "\" & StoresGrid.Rows(CurFileRowIndex).Cells("FileName").Value.ToString) Then
            MDIInstoreCentral.PlayAudioFile(SongsArchivePath & "\" & StoresGrid.Rows(CurFileRowIndex).Cells("FileName").Value.ToString)
        ElseIf cmb_Type_Storetab.Text = "Promos" AndAlso File.Exists(PromosArchivePath & "\" & StoresGrid.Rows(CurFileRowIndex).Cells("FileName").Value.ToString) Then
            MDIInstoreCentral.PlayAudioFile(PromosArchivePath & "\" & StoresGrid.Rows(CurFileRowIndex).Cells("FileName").Value.ToString)
        End If


    End Sub
    '-------Songs Search part
    Private Sub SongsGrid_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles SongsGrid.ColumnHeaderMouseClick
        txt_Songs_Search.Text = "<Search " & SongsGrid.Columns(e.ColumnIndex).HeaderText & ">"
    End Sub
    Private Sub txt_Songs_Search_GotFocus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Songs_Search.GotFocus, txt_Songs_Search.MouseClick
        txt_Songs_Search.SelectAll()
    End Sub
    Private Sub Songs_Search_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_Songs_Search.KeyDown, SongsGrid.KeyDown
        'Try
        If SongsGrid.RowCount <> 0 AndAlso StringFound = True AndAlso e.KeyValue = Keys.F3 Then
            iRowIndex += 1
            StringFound = False
            If Not FindString(SongsGrid, SongsGrid.SortedColumn.Index, txt_Songs_Search.Text.Trim) Then
                XtraMessageBox.Show("No more matching '" & SongsGrid.Columns(SongsGrid.SortedColumn.Index).HeaderText & "' with the given search text, ' " & txt_Songs_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        ElseIf TypeOf sender Is DataGridView AndAlso e.KeyCode = Keys.Delete AndAlso SongsGrid.RowCount > 0 Then
            Dim C As Integer = SongsGrid.SelectedRows.Count
            bActivatedCount = 0
            'If XtraMessageBox.Show("Do you want to delete the selected 'Song'?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            If C <> 0 Then
                If XtraMessageBox.Show("Do you want to delete the selected songs?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                    Dim CurRow, CurFirstRowIndex As Integer
                    CurRow = SongsGrid.CurrentRow.Index
                    CurFirstRowIndex = SongsGrid.FirstDisplayedScrollingRowIndex
                    For i = 0 To C - 1
                        DeleteSong(SongsGrid.Rows(SongsGrid.SelectedRows(i).Index).Cells(0).Value)
                    Next
                    LoadSongs(Convert.ToInt32(cmb_Genre.EditValue.ToString), CurRow, CurFirstRowIndex)
                    If bActivatedCount <> 0 Then
                        XtraMessageBox.Show("Of the selected songs, " & bActivatedCount.ToString & " song(s) were not deleted as they are activated to stores", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                    bActivatedCount = 0
                End If
            End If
            'End If
        ElseIf e.KeyCode = Keys.F7 Then
            If Grp_Songs_History.Visible = True Then Grp_Songs_History.Visible = False : SongsGrid.Height = 570 Else LoadHistory("Songs")
        End If
        'Catch
        'End Try
    End Sub
    Private Sub txt_Songs_Search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Songs_Search.KeyPress
        If SongsGrid.RowCount <> 0 AndAlso txt_Songs_Search.Text.Trim <> "" AndAlso e.KeyChar = Chr(13) Then
            iRowIndex = 0
            StringFound = False
            If Not FindString(SongsGrid, SongsGrid.SortedColumn.Index, txt_Songs_Search.Text.Trim) Then
                XtraMessageBox.Show("No matching '" & SongsGrid.Columns(SongsGrid.SortedColumn.Index).HeaderText & "'  with the given search text, ' " & txt_Songs_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
    '-------Songs Search part ends

    Private Sub PromosGrid_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles PromosGrid.ColumnHeaderMouseClick
        txt_Promos_Search.Text = "<Search " & PromosGrid.Columns(e.ColumnIndex).HeaderText & ">"
    End Sub

    Private Sub txt_Promos_Search_GotFocus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Promos_Search.GotFocus, txt_Promos_Search.MouseClick
        txt_Promos_Search.SelectAll()
    End Sub

    Private Sub Promos_Search_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles PromosGrid.KeyDown
        'Try
        If PromosGrid.RowCount <> 0 AndAlso StringFound = True AndAlso e.KeyValue = Keys.F3 Then
            iRowIndex += 1
            StringFound = False
            If Not FindString(PromosGrid, PromosGrid.SortedColumn.Index, txt_Promos_Search.Text.Trim) Then
                XtraMessageBox.Show("No more matching '" & PromosGrid.Columns(PromosGrid.SortedColumn.Index).HeaderText & "' with the given search text, ' " & txt_Promos_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        ElseIf TypeOf sender Is DataGridView AndAlso e.KeyCode = Keys.Delete AndAlso PromosGrid.RowCount > 0 Then
            Dim c As Integer = PromosGrid.SelectedRows.Count
            If c <> 0 Then
                Dim CurRow, CurFirstRowIndex As Integer
                CurRow = PromosGrid.CurrentRow.Index
                CurFirstRowIndex = PromosGrid.FirstDisplayedScrollingRowIndex
                bActivatedCount = 0
                If XtraMessageBox.Show("Do you want to delete the selected promos?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                    Call LoadDummy()
                    For i As Integer = 0 To c - 1
                        Deletepromo(PromosGrid.Rows(PromosGrid.SelectedRows(i).Index).Cells(0).Value)
                    Next
                    'Deletepromo(PromosGrid.Rows(PromosGrid.CurrentRow.Index).Cells(0).Value)
                    LoadPromos(Convert.ToInt32(cmb_Customer.EditValue.ToString), CurRow, CurFirstRowIndex)
                    If bActivatedCount <> 0 Then
                        XtraMessageBox.Show("Of the selected promos, " & bActivatedCount.ToString & " promo(s) were not deleted as they are activated to stores", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                    bActivatedCount = 0
                End If
            End If
            'If XtraMessageBox.Show("Do you want to delete the selected 'Promo'?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

            'End If
        ElseIf e.KeyCode = Keys.F7 Then
            If Grp_Promos_History.Visible = True Then Grp_Promos_History.Visible = False : PromosGrid.Height = 570 Else LoadHistory("Promos")
        End If
        'Catch
        'End Try
    End Sub
    Private Sub Grid_Songs_History_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Songs_History.KeyDown
        If e.KeyCode = Keys.F7 Then
            If Grp_Songs_History.Visible = True Then Grp_Songs_History.Visible = False : SongsGrid.Height = 570 Else LoadHistory("Songs")
        End If
    End Sub
    Private Sub Grid_Promos_History_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Promos_History.KeyDown
        If e.KeyCode = Keys.F7 Then
            If Grp_Promos_History.Visible = True Then Grp_Promos_History.Visible = False : PromosGrid.Height = 570 Else LoadHistory("Promos")
        End If
    End Sub
    Private Sub btn_Songs_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Songs_Close.Click
        SongsGrid.Height = 570
        Grp_Songs_History.Visible = False
    End Sub
    Private Sub btn_promos_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Promos_Close.Click
        SongsGrid.Height = 570
        Grp_Promos_History.Visible = False
    End Sub
    Private Sub SongsGrid_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SongsGrid.SelectionChanged
        If Grp_Songs_History.Visible = True Then LoadHistory("Songs")
    End Sub
    Private Sub PromosGrid_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PromosGrid.SelectionChanged
        If Grp_Promos_History.Visible = True Then LoadHistory("Promos")
    End Sub
    Private Sub FormatHistoryGrid(ByRef HistoryGrid As DataGridView)
        HistoryGrid.Columns(0).Width = 100

        For i = 1 To 48
            HistoryGrid.Columns(i).Width = 25
            If i Mod 2 = 0 Then HistoryGrid.Columns(i).Visible = False
            HistoryGrid.Columns(i).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        Next
        HistoryGrid.Columns(49).Width = 1500
        HistoryGrid.Refresh()
    End Sub
    Private Sub LoadHistory(ByVal MediaType As String)

        Application.DoEvents()

        Dim vMediaId, vOldMediaId, str As String

        Dim objHist As GroupControl
        Dim MediaGrid As DataGridView

        If MediaType = "Songs" Then
            objHist = Grp_Songs_History
            MediaGrid = Grid_Songs_History
            vMediaId = SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells(0).Value.ToString()
        Else
            MediaGrid = Grid_Promos_History
            objHist = Grp_Promos_History
            vMediaId = PromosGrid.Rows(PromosGrid.CurrentRow.Index).Cells(0).Value.ToString()
        End If

        'str = objHist.Text
        'vOldMediaId = str.Replace("History for : ", "")
        'vOldMediaId = vOldMediaId.Split("/")(0).Trim
        'If vMediaId = vOldMediaId AndAlso objHist.Visible = True Then Exit Sub


        Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter

        Cursor = Cursors.WaitCursor
        Call OpenDBConnection()

        Dim dt As Date = Today

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetMediaHistory"
            .Parameters.AddWithValue("@Media_Id", vMediaId).Direction = ParameterDirection.Input
            If MediaType = "Songs" Then
                .Parameters.AddWithValue("@Media_Type", "S").Direction = ParameterDirection.Input
            Else
                .Parameters.AddWithValue("@Media_Type", "P").Direction = ParameterDirection.Input
            End If


        End With
        dapSongs.SelectCommand = Cmd
        'MsgBox(Cmd.Parameters("@Playlist_Date").Value.ToString)
        dapSongs.Fill(dsSongs, "MediaHistory")
        MediaGrid.Columns.Clear()
        MediaGrid.DataSource = dsSongs.Tables("MediaHistory")
        FormatHistoryGrid(MediaGrid)

        If MediaType = "Songs" Then
            Grp_Songs_History.Text = "History for : " & vMediaId & " / " & SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Title").Value.ToString() & " / " & SongsGrid.Rows(SongsGrid.CurrentRow.Index).Cells("Album").Value.ToString()
            Grp_Songs_History.Visible = True
            SongsGrid.Height = 349
        Else
            Grp_Promos_History.Text = "History for : " & vMediaId & " / " & PromosGrid.Rows(PromosGrid.CurrentRow.Index).Cells("Category").Value.ToString() & " / " & PromosGrid.Rows(PromosGrid.CurrentRow.Index).Cells("Title").Value.ToString()
            Grp_Promos_History.Visible = True
            PromosGrid.Height = 349
        End If



        For i = 0 To MediaGrid.Rows.Count - 1
            If Strings.Right(MediaGrid.Rows(i).Cells(0).Value.ToString, 8) = dt.ToString("dd/MM/yy").Replace("-", "/") Then
                MediaGrid.FirstDisplayedScrollingRowIndex = i
                Exit For
            End If

            Application.DoEvents()
        Next


        dsSongs.Dispose()
        dapSongs.Dispose()
        Call CloseDBConnection()
        Cursor = Cursors.Default

    End Sub

    Private Sub txt_Promos_Search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Promos_Search.KeyPress
        If PromosGrid.RowCount <> 0 AndAlso txt_Promos_Search.Text.Trim <> "" AndAlso e.KeyChar = Chr(13) Then
            iRowIndex = 0
            StringFound = False
            If Not FindString(PromosGrid, PromosGrid.SortedColumn.Index, txt_Promos_Search.Text.Trim) Then
                XtraMessageBox.Show("No matching '" & PromosGrid.Columns(PromosGrid.SortedColumn.Index).HeaderText & "'  with the given search text, ' " & txt_Promos_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
    Private Sub StoresGrid_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles StoresGrid.ColumnHeaderMouseClick
        txt_Stores_Search.Text = "<Search " & StoresGrid.Columns(e.ColumnIndex).HeaderText & ">"
    End Sub

    Private Sub txt_Stores_Search_GotFocus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Stores_Search.GotFocus, txt_Stores_Search.MouseClick
        txt_Stores_Search.SelectAll()
    End Sub

    Private Sub Stores_Search_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_Stores_Search.KeyDown, StoresGrid.KeyDown
        If StoresGrid.RowCount <> 0 AndAlso StringFound = True AndAlso e.KeyValue = Keys.F3 Then
            iRowIndex += 1
            StringFound = False
            If Not FindString(StoresGrid, StoresGrid.SortedColumn.Index, txt_Stores_Search.Text.Trim) Then
                XtraMessageBox.Show("No more matching '" & StoresGrid.Columns(StoresGrid.SortedColumn.Index).HeaderText & "' with the given search text, ' " & txt_Stores_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub txt_Stores_Search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Stores_Search.KeyPress
        If StoresGrid.RowCount <> 0 AndAlso txt_Stores_Search.Text.Trim <> "" AndAlso e.KeyChar = Chr(13) Then
            iRowIndex = 0
            StringFound = False
            If Not FindString(StoresGrid, StoresGrid.SortedColumn.Index, txt_Stores_Search.Text.Trim) Then
                XtraMessageBox.Show("No matching '" & StoresGrid.Columns(StoresGrid.SortedColumn.Index).HeaderText & "'  with the given search text, ' " & txt_Stores_Search.Text.Trim & " '", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
    Private Function FindString(ByRef GridSearch As DataGridView, ByVal SeachColumnIndex As Integer, ByVal Searchstring As String) As Boolean

        Dim i As Integer
        Dim TopRow As Integer, LowerRow As Integer
        Dim strpos As Integer = 0

        Dim strFind As String = ""

        'For i = 0 To dgData.Rows.Count - 2
        '    dgData.Rows(i).Selected = False
        'Next

        With GridSearch
            For i = iRowIndex To .Rows.Count - 1
                strFind = LCase(.Rows(i).Cells(SeachColumnIndex).Value.ToString())
                strpos = InStr(strFind, LCase(Searchstring))
                If strpos > 0 Then
                    .Rows(i).Selected = True
                    iRowIndex = i
                    StringFound = True

                    TopRow = .FirstDisplayedScrollingRowIndex
                    LowerRow = TopRow + (.DisplayedRowCount(False) - 1)
                    If i <> -1 Then
                        .Rows(i).Selected = True
                        If i < TopRow Or i > LowerRow Then
                            .FirstDisplayedScrollingRowIndex = i
                        End If
                    End If
                    Exit For
                End If
            Next
        End With

        If StringFound = True Then
            Return True
        Else
            Return False
        End If

    End Function


    Private Sub btn_Add_Song_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Add_Song.Click
        MDIInstoreCentral.PlayerPanel.Visible = False
        MDIInstoreCentral.ResetPlayer()

        If SongsArchivePath = "" Then
            XtraMessageBox.Show("Audio path for Songs is not configured", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        ActiveGenre = cmb_Genre.Text
        Me.Close()

        'If ActiveMdiChild IsNot Nothing Then
        '    ActiveMdiChild.Close()
        'End If

        Dim w As New frmAddSong

        With w
            .MdiParent = MDIInstoreCentral
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
            .cmb_Genre.Text = ActiveGenre
        End With

    End Sub

    Private Sub TStrip_btn_Genre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TStrip_btn_Genre.Click
        lbl_Modify_Category.Text = "Genre:"
        If Grp_Songs_History.Visible = True Then Grp_Songs_History.Visible = False : SongsGrid.Height = 570
        PanelModify_Category.Visible = True
        ' Category_Info_Grid.Rows.Clear()
    End Sub

    Private Sub btn_Exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Modify_Exit.Click
        PanelModify_Category.Visible = False
    End Sub

    Private Sub PopUpMenu_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles PopUpMenu.Opening
        If SongsGrid.SelectedRows.Count > 0 Then
            e.Cancel = False
            cmb_Modify_Category.EditValue = cmb_Genre.EditValue
            bindCategoriesGrid()
        Else
            e.Cancel = True
        End If

    End Sub

    Private Sub btn_ReplacePromo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ModifyCategory.Click
        If cmb_Modify_Category.Text = "" Then
            XtraMessageBox.Show("Please select 'Genre' and proceed further", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Dim CurRow As Integer = 0
        Dim CurFirstRowIndex As Integer = 0
        If Library_Tab_Control.SelectedTabPage.Text = "Promos" Then
            If PromosGrid.SelectedRows.Count > 0 Then
                CurRow = PromosGrid.CurrentRow.Index
                CurFirstRowIndex = PromosGrid.FirstDisplayedScrollingRowIndex
                If Category_Modify_Grid.SelectedRows.Count > 0 Then
                    Dim Dvgrow As DataGridViewRow
                    For Each Dvgrow In PromosGrid.SelectedRows
                        UpdateSongsCaterogy("Promo", CInt(Category_Modify_Grid.Rows(Category_Modify_Grid.CurrentRow.Index).Cells(0).Value), Dvgrow.Cells(0).Value)
                    Next
                Else
                    XtraMessageBox.Show("Please select 'Category' and proceed further", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                LoadPromos(Convert.ToInt32(cmb_Customer.EditValue.ToString), CurRow, CurFirstRowIndex)
            End If
        ElseIf Library_Tab_Control.SelectedTabPage.Text = "Songs" Then

            If SongsGrid.SelectedRows.Count > 0 Then
                CurRow = SongsGrid.CurrentRow.Index
                CurFirstRowIndex = SongsGrid.FirstDisplayedScrollingRowIndex
                If Category_Modify_Grid.SelectedRows.Count > 0 Then
                    Dim Dvgrow As DataGridViewRow
                    For Each Dvgrow In SongsGrid.SelectedRows
                        UpdateSongsCaterogy("Genre", CInt(Category_Modify_Grid.Rows(Category_Modify_Grid.CurrentRow.Index).Cells(0).Value), Dvgrow.Cells(0).Value)
                    Next
                Else
                    XtraMessageBox.Show("Please select 'Category' and proceed further", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                LoadSongs(Convert.ToInt32(cmb_Genre.EditValue.ToString), CurRow, CurFirstRowIndex)
            End If
        End If
        PanelModify_Category.Visible = False
    End Sub


    Private Sub pop_Genre_Promo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pop_Genre_Promo.Click
        lbl_Modify_Category.Text = "Customer:"
        If Grp_Promos_History.Visible = True Then Grp_Promos_History.Visible = False : PromosGrid.Height = 570
        PanelModify_Category.Visible = True
        ' Category_Info_Grid.Rows.Clear()
    End Sub

    Private Sub popupMenuPromo_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles popupMenuPromo.Opening
        If PromosGrid.SelectedRows.Count > 0 Then
            e.Cancel = False
            cmb_Modify_Category.EditValue = cmb_Customer.EditValue
            bindCategoriesGrid()
        Else
            e.Cancel = True
        End If
        ' bindCategoriesGrid()
    End Sub

   
    Private Sub SongsGrid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles SongsGrid.CellContentClick

    End Sub

    Private Sub TStrip_btn_Modify_Metadata_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TStrip_btn_Modify_Metadata.Click
        Dim CurRow, CurFirstRowIndex As Integer

        Dim vSongId, vGenre, vAlbum, vNormalization, vTitle, vFileName, vArtist(1), vCategory, vDuration As String
        If Grp_Songs_History.Visible = True Then Grp_Songs_History.Visible = False : SongsGrid.Height = 570

        If SongsGrid.SelectedRows.Count > 0 Then
            CurRow = SongsGrid.CurrentRow.Index
            CurFirstRowIndex = SongsGrid.FirstDisplayedScrollingRowIndex
            Dim Dvgrow As DataGridViewRow
            Dvgrow = SongsGrid.SelectedRows(0)
            vSongId = Dvgrow.Cells(0).Value.ToString
            vGenre = cmb_Genre.Text
            vCategory = Dvgrow.Cells("Category").Value.ToString
            vTitle = Dvgrow.Cells("Title").Value.ToString
            vFileName = Dvgrow.Cells("FileName").Value.ToString
            vNormalization = Dvgrow.Cells("Normalization").Value.ToString
            vArtist = Dvgrow.Cells("Artists").Value.ToString.Split(",")
            vAlbum = Dvgrow.Cells("Album").Value.ToString
            vDuration = Dvgrow.Cells("Duration").Value.ToString
            'MsgBox(Dvgrow.Cells(0).Value.ToString)
            '    UpdateSongsCaterogy("Promo", CInt(Category_Modify_Grid.Rows(Category_Modify_Grid.CurrentRow.Index).Cells(0).Value), Dvgrow.Cells(0).Value)

        Else
            XtraMessageBox.Show("Please select a row to modify", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim w As New frmAddSong

        With w
            '.MdiParent = Me
            .Owner = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog 'Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
            .btn_Add.Text = "Save"
            .lbl_form_title.Text = "Edit Metadata"
            .cmb_Genre.Text = vGenre
            .txt_Album.Text = vAlbum
            If vArtist.Length > 0 Then .txt_Artist1.Text = vArtist(0)
            If vArtist.Length > 1 Then .txt_Artist2.Text = vArtist(1)
            .txt_AudioPath.Text = SongsArchivePath & "\" & vFileName
            .txt_SongID.Text = vSongId
            .txt_Title.Text = vTitle
            .cmb_Category.Text = vCategory

            .cmb_Normalization.Text = vNormalization
            .lbl_Duration.Text = "00:" & vDuration
            .txt_SongID.Enabled = False
            .currow = CurRow
            .CurFirstRowIndex = CurFirstRowIndex
        End With
    End Sub

    Private Sub pop_Promo_Metadata_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pop_Promo_Metadata.Click
        Dim CurRow, CurFirstRowIndex As Integer

        If Grp_Promos_History.Visible = True Then Grp_Promos_History.Visible = False : PromosGrid.Height = 570

        Dim vCustomer, vCategory, vPromoId, vNormalization, vTitle, vFileName, vDuration, vstdate(2), vspdate(2) As String
        Dim vStartDate, vStopDate As Date
        If PromosGrid.SelectedRows.Count > 0 Then
            ActiveCustomerId = Convert.ToInt32(cmb_Customer.EditValue.ToString)
            CurRow = PromosGrid.CurrentRow.Index
            CurFirstRowIndex = PromosGrid.FirstDisplayedScrollingRowIndex
            Dim Dvgrow As DataGridViewRow
            Dvgrow = PromosGrid.SelectedRows(0)
            vPromoId = Dvgrow.Cells(0).Value.ToString
            vCustomer = cmb_Customer.Text
            vCategory = Dvgrow.Cells("Category").Value.ToString
            vTitle = Dvgrow.Cells("Title").Value.ToString
            vFileName = Dvgrow.Cells("FileName").Value.ToString
            vNormalization = Dvgrow.Cells("Normalization").Value.ToString
            vDuration = "00:" & Dvgrow.Cells("Duration").Value.ToString
            If Dvgrow.Cells("Start Date").Value.ToString <> "" Then vstdate = Dvgrow.Cells("Start Date").Value.ToString.Split("/")
            If Dvgrow.Cells("Stop Date").Value.ToString <> "" Then vspdate = Dvgrow.Cells("Stop Date").Value.ToString.Split("/")
            If vstdate(0) <> "" Then
                vStartDate = DateSerial(vstdate(2), vstdate(1), vstdate(0))
            End If
            If vspdate(0) <> "" Then
                vStopDate = DateSerial(vspdate(2), vspdate(1), vspdate(0))
            End If

            'MsgBox(Dvgrow.Cells(0).Value.ToString)
            '    UpdateSongsCaterogy("Promo", CInt(Category_Modify_Grid.Rows(Category_Modify_Grid.CurrentRow.Index).Cells(0).Value), Dvgrow.Cells(0).Value)

        Else
            XtraMessageBox.Show("Please select a row to modify", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim w As New frmAddPromo

        With w
            '.MdiParent = Me
            .Owner = Me
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog 'Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
            .btn_Add.Text = "Save"
            .lbl_form_title.Text = "Edit Metadata"
            .cmb_Customer.Text = vCustomer
            .cmb_Customer.RefreshEditValue()
            .cmb_Normalization.Text = vNormalization
            .txt_PromoID.Text = vPromoId
            .txt_Title.Text = vTitle
            .txt_AudioPath.Text = PromosArchivePath & "\" & vFileName
            .cmb_Category.Text = vCategory
            .lbl_Duration.Text = vDuration
            .Dt_start_date.EditValue = vStartDate
            .Dt_Stop_Date.EditValue = vStopDate
            .txt_PromoID.Enabled = False
            .CurRow = CurRow
            .CurFirstRowIndex = CurFirstRowIndex
        End With
    End Sub

End Class
    