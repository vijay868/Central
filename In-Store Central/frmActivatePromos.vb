﻿Imports DevExpress.XtraEditors
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Repository

Imports System.Collections.Generic

Imports System.Drawing
Imports System.Text

Imports DevExpress.Utils

Public Class frmActivatePromos
    Dim dtCustomerPromos As DataTable
    Dim dtStorePromos As DataTable
    Dim StorePromoArray() As Integer
    Private Sub frmActivatePromos_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If btn_Save.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                btn_Save.PerformClick()
            End If
        End If

    End Sub
    Private Sub frmActivatePromos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call FormatGrid(Promos_Source_Grid, True)
        Call FormatGrid(Promos_Target_Grid, True)

        Call LoadCustomers()
        Call InitializeStore()
        cmb_Store.EditValue = ""
        btn_Save.Enabled = False
    End Sub
    Private Sub LoadCustomers()

        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()

        cmb_Customer.Properties.Columns(0).Visible = False
        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""

        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub

    Private Sub LoadStores(ByVal Customer_Id As Integer)
        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")


        'Creating Columns for Display in lookup
        cmb_Store.Properties.DisplayMember = "Store_Name"
        cmb_Store.Properties.ValueMember = "Store_ID"
        cmb_Store.Properties.DataSource = dsStores.Tables(0)
        cmb_Store.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Store.Properties.PopulateColumns()
        cmb_Store.Properties.Columns(0).Visible = False
        cmb_Store.Properties.ShowFooter = False
        cmb_Store.Properties.ShowHeader = False

        cmb_Store.ItemIndex = -1

        'cmb_Store.EditValue = ""
        'If dsStores.Tables(0).Rows.Count <> 0 Then btn_Save.Enabled = True Else btn_Save.Enabled = False
        dsStores.Dispose()
        dapStores.Dispose()


        Call CloseDBConnection()
    End Sub
    Private Sub LoadCategories(ByVal Customer_Id As Integer)
        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetPromoCategoriesList"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        cmb_Category.Properties.DataSource = Nothing
        cmb_Category.EditValue = ""
        cmb_Category.RefreshEditValue()


        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "CategoryList")
        dsStores.Tables(0).Rows.Add(0, "", "ALL")
        dsStores.Tables(0).DefaultView.Sort = "Promo_Category_ID asc"

        'Creating Columns for Display in lookup
        cmb_Category.Properties.DisplayMember = "Category_Code"
        cmb_Category.Properties.ValueMember = "Promo_Category_ID"
        cmb_Category.Properties.DataSource = dsStores.Tables(0)
        cmb_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Category.Properties.PopulateColumns()
        cmb_Category.Properties.Columns(0).Visible = False
        cmb_Category.Properties.Columns(1).Visible = False
        cmb_Category.Properties.ShowFooter = False
        cmb_Category.Properties.ShowHeader = False
        cmb_Category.EditValue = ""

        'If dsStores.Tables(0).Rows.Count <> 0 Then btn_Save.Enabled = True Else btn_Save.Enabled = False
        cmb_Category.ItemIndex = 0
        dsStores.Dispose()
        dapStores.Dispose()
        Call CloseDBConnection()
    End Sub
    Private Sub cmb_Customer_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If cmb_Customer.EditValue.ToString <> "" Then
            Call LoadCategories(Convert.ToInt32(cmb_Customer.EditValue.ToString))
        End If

    End Sub
    Private Sub LoadCustomerPromos()

        Application.DoEvents()

        Dim Cmd As New SqlCommand, dsPromos As New DataSet, dapPromos As New SqlDataAdapter
        Dim i As Integer

        dtCustomerPromos = Nothing
        dtCustomerPromos = New DataTable


        Call OpenDBConnection()

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetPromosListForCustomer"
            .Parameters.AddWithValue("@Customer_ID", Convert.ToInt32(cmb_Customer.EditValue.ToString)).Direction = ParameterDirection.Input
            If cmb_Category.EditValue.ToString <> "" AndAlso cmb_Category.EditValue.ToString <> 0 Then
                .Parameters.AddWithValue("@Promo_Category_Id", Convert.ToInt32(cmb_Category.EditValue.ToString)).Direction = ParameterDirection.Input
            End If

        End With
        dapPromos.SelectCommand = Cmd
        dapPromos.Fill(dsPromos, "PromosList")

        dtCustomerPromos = dsPromos.Tables("PromosList")
        FormatGrid(Promos_Source_Grid, True)
        If dsPromos.Tables("PromosList").Rows.Count <> 0 Then

            'Promos_Source_Grid.Rows.Clear()

            'Check if audio file exists

            For Each dr As DataRow In dsPromos.Tables("PromosList").Rows
                If File.Exists(PromosArchivePath & "\" & dr("FileName").ToString) Then
                    Promos_Source_Grid.Rows.Add(dr("Media Id"), dr("Category"), dr("Title"), dr("FileName"))
                    Application.DoEvents()
                End If
                'If Not File.Exists(PromosArchivePath & "\" & Promos_Source_Grid.Rows(i).Cells(3).Value.ToString) Then
                '    Promos_Source_Grid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                'End If
                'i += 1
            Next
            'FormatGrid(Promos_Source_Grid, False)
        Else 'no records
            'FormatGrid(Promos_Source_Grid, True)
            'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If
        'FormatGrid(Promos_Target_Grid, True)
        Call CloseDBConnection()

    End Sub
    Private Sub RefreshCustomerPromos() 'Exected when store is changed in the same customer
        Application.DoEvents()
        Dim i As Integer
        'dtCustomerPromos.Select("[Media Id]<>12002", "[Media ID] asc", DataViewRowState.CurrentRows)

        If dtCustomerPromos.Rows.Count <> 0 Then
            dtCustomerPromos.DefaultView.Sort = "[Media Id] asc"
            Promos_Source_Grid.Rows.Clear()

            'Check if audio file exists

            For Each dr As DataRow In dtCustomerPromos.Rows
                If Not StorePromoArray.Contains(dr("Media Id")) Then
                    
                    If File.Exists(PromosArchivePath & "\" & dr("FileName").ToString) Then
                        Promos_Source_Grid.Rows.Add(dr("Media Id"), dr("Category"), dr("Title"), dr("FileName"))
                        Application.DoEvents()
                        i += 1
                    End If

                    'Promos_Source_Grid.Rows.Add(dr("Media Id"), dr("Category"), dr("Title"), dr("FileName"))
                    'Application.DoEvents()
                    'If Not File.Exists(PromosArchivePath & "\" & Promos_Source_Grid.Rows(i).Cells(3).Value.ToString) Then
                    '    Promos_Source_Grid.Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(255, 100, 100)
                    'End If
                    'i += 1
                End If
            Next

            FormatGrid(Promos_Source_Grid, False)
      
        End If


    End Sub
  
    Private Sub cmb_Store_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Store.EditValueChanged

        Application.DoEvents()
        If cmb_Store.EditValue.ToString = "" Then Exit Sub
        Dim Cmd As New SqlCommand, dsPromos As New DataSet, dapPromos As New SqlDataAdapter
        Call OpenDBConnection()

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoreSongPromoList"
            .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Filter_Criteria", "Promos").Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 0).Direction = ParameterDirection.Input
        End With
        dapPromos.SelectCommand = Cmd
        dapPromos.Fill(dsPromos, "PromosList")
        dtStorePromos = Nothing
        dtStorePromos = New DataTable
        dtStorePromos = dsPromos.Tables("PromosList")
        Call LoadStorePromoData()
        
        dsPromos.Dispose()
        dapPromos.Dispose()
        Call CloseDBConnection()
        Call RefreshCustomerPromos()
    End Sub
    Private Sub LoadStorePromoData()
        Dim r As Integer
        If dtStorePromos.Rows.Count <> 0 Then
            Promos_Target_Grid.Rows.Clear()
            ReDim StorePromoArray(dtStorePromos.Rows.Count - 1)
            ''reading the table
            For Each dr As DataRow In dtStorePromos.Rows
                StorePromoArray(r) = dr("MediaId")
                r += 1
                Promos_Target_Grid.Rows.Add(dr("MediaId"), dr("Category"), dr("Title"), dr("FileName"))
                Application.DoEvents()
                ''Check if audio file exists
                'For i = 0 To Promos_Target_Grid.Rows.Count - 1
                '    If Promos_Target_Grid.Rows(i).Cells(0).Value.ToString = "" Then Exit For
                '    If Not File.Exists("D:\Works\Files\" & Promos_Target_Grid.Rows(i).Cells(4).Value.ToString) Then
                '        Promos_Target_Grid.Rows(i).DefaultCellStyle.BackColor = Color.Red
                '    End If
                'Next
            Next
            FormatGrid(Promos_Target_Grid, False)
            txt_PromosCount.Text = r

        Else 'no records
            ReDim StorePromoArray(0)
            txt_PromosCount.Text = 0
            Promos_Target_Grid.Rows.Clear()
            FormatGrid(Promos_Target_Grid, True)
            'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If
    End Sub

    Private Sub DataGridViews_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Promos_Source_Grid.MouseDown, Promos_Target_Grid.MouseDown
        Dim handler = CType(sender, DataGridView)
        Dim hti As DataGridView.HitTestInfo = handler.HitTest(e.X, e.Y)
        If hti.Type <> DataGrid.HitTestType.ColumnHeader Then
            If e.Button <> Windows.Forms.MouseButtons.Left OrElse handler.SelectedRows.Count = 0 Then Return
            handler.DoDragDrop(handler, DragDropEffects.Move)
        End If
    End Sub

    Private Sub DataGridViews_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Promos_Source_Grid.DragEnter, Promos_Target_Grid.DragEnter
        e.Effect = DragDropEffects.Move
    End Sub

    Private Sub DataGridViews_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Promos_Source_Grid.DragDrop, Promos_Target_Grid.DragDrop

        Dim handler = CType(sender, DataGridView)
        Dim source = CType(e.Data.GetData(GetType(DataGridView)), DataGridView)
        Dim iRowAdded As Boolean
        If source.Name = handler.Name Then Exit Sub

        If cmb_Customer.Text = "" Or cmb_Store.Text = "" Then Exit Sub

        Dim dtRow As DataRow, dtRow1() As DataRow
        For Each row As DataGridViewRow In source.SelectedRows
            'If CStr(row.Cells(3).Value) = "Y" Then
            If row.Cells(0).Value.ToString <> "" Then
                If handler.Name = "Promos_Target_Grid" AndAlso File.Exists(PromosArchivePath & "\" & row.Cells(3).Value) Then
                    dtRow = dtStorePromos.NewRow
                    dtRow(0) = row.Cells(0).Value : dtRow(1) = row.Cells(1).Value : dtRow(2) = row.Cells(2).Value : dtRow(3) = row.Cells(3).Value
                    dtStorePromos.Rows.Add(dtRow)
                    iRowAdded = True
                ElseIf handler.Name = "Promos_Source_Grid" Then
                    dtRow1 = dtStorePromos.Select("MediaId=" & row.Cells(0).Value)
                    dtStorePromos.Rows.Remove(dtRow1(0))
                    iRowAdded = True
                End If
            End If
        Next

        If iRowAdded = True Then
            btn_Save.Enabled = True
            Call LoadStorePromoData()
            Call RefreshCustomerPromos()
        End If
        'Dim handler = CType(sender, DataGridView)
        'Dim source = CType(e.Data.GetData(GetType(DataGridView)), DataGridView)

        'If source.Name = handler.Name Then Exit Sub

        'If cmb_Customer.Text = "" Or cmb_Store.Text = "" Then Exit Sub

        'For Each row As DataGridViewRow In source.SelectedRows
        '    'If CStr(row.Cells(3).Value) = "Y" Then
        '    If handler.Name = "Promos_Target_Grid" Then
        '        If row.Cells(0).Value.ToString <> "" AndAlso Not FindPromo(handler, row.Cells(0).Value) Then
        '            source.Rows.Remove(row)
        '            handler.Rows.Add(row)
        '        End If
        '    Else
        '        If row.Cells(0).Value.ToString <> "" Then
        '            source.Rows.Remove(row)
        '            If Not FindPromo(handler, row.Cells(0).Value) Then handler.Rows.Add(row)
        '        End If
        '    End If
        'Next

        ''handler.Sort(handler.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
        ''If handler.Name = "Promos_Target_Grid" Then
        'txt_PromosCount.Text = Promos_Target_Grid.Rows.Count
        ''End If
    End Sub

    Private Sub InitializeStore()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Store.Properties.DataSource = dt
        dt.Dispose()
    End Sub

    Private Sub FormatGrid(ByRef Grid As DataGridView, ByVal ResetStructure As Boolean)

        If ResetStructure Then
            Grid.Columns.Clear()
            Grid.Columns.Add("Promo_ID", "Promo ID")
            Grid.Columns.Add("Category", "Category")
            Grid.Columns.Add("Title", "Title")
            Grid.Columns.Add("FileName", "FileName")
            Grid.Columns.Add("Duration", "Duration")
            'For i As Integer = 1 To 30
            '    Grid.Rows.Add("", "", "")
            'Next
            txt_PromosCount.Text = 0
        ElseIf Grid.Rows.Count < 30 Then
            'For i As Integer = Grid.Rows.Count + 1 To 30
            '    Grid.Rows.Add("", "", "")
            'Next
        End If

        Grid.Columns(0).Width = 80
        Grid.Columns(0).SortMode = DataGridViewColumnSortMode.Automatic
        Grid.Columns(1).Width = 80
        Grid.Columns(2).Width = 220
        Grid.Columns(2).SortMode = DataGridViewColumnSortMode.Automatic
        Grid.Columns(3).Visible = False
        Grid.Columns(4).Visible = False
        Grid.Refresh()

    End Sub
    Private Function FindPromo(ByRef GridEffected As DataGridView, ByVal Promo_Id As Integer) As Boolean
        Dim Found As Boolean = False
        For i As Integer = 0 To GridEffected.Rows.Count - 1

            If GridEffected.Rows(i).Cells(0).Value.ToString = "" Then
                Exit For
            ElseIf Promo_Id = GridEffected.Rows(i).Cells(0).Value.ToString Then
                Found = True
                Exit For
            End If
        Next
        Return Found
    End Function

    Private Sub btn_Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Save.Click


        If cmb_Store.EditValue = Nothing Then Exit Sub

        Dim Promo_Ids As String = ""
        For i As Integer = 0 To Promos_Target_Grid.Rows.Count - 1
            If Promos_Target_Grid.Rows(i).Cells(0).Value.ToString = "" Then Exit For
            Promo_Ids += Promos_Target_Grid.Rows(i).Cells(0).Value.ToString + ","
        Next
        If Promo_Ids <> "" Then Promo_Ids = Promo_Ids.Substring(0, Promo_Ids.Length - 1)


        Dim CmdPromos As New SqlCommand
        Call OpenDBConnection()
        With CmdPromos
            .Connection = DBConn
            .CommandText = "ProcActivatePromos"
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Promo_Ids", Promo_Ids).Direction = ParameterDirection.Input
            .ExecuteNonQuery()
        End With

        Call CloseDBConnection()
        CmdPromos.Dispose()
        CmdPromos = Nothing
        btn_Save.Enabled = False
        'XtraMessageBox.Show("Activated promos saved", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    'Private Sub Promos_Source_Grid_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Promos_Source_Grid.ColumnHeaderMouseClick
    '    Dim newColumn As DataGridViewColumn = _
    '  Promos_Source_Grid.Columns(e.ColumnIndex)
    '    Dim oldColumn As DataGridViewColumn = Promos_Source_Grid.SortedColumn
    '    Dim direction As ListSortDirection


    '    ' If oldColumn is null, then the DataGridView is not currently sorted. 
    '    If oldColumn IsNot Nothing Then

    '        ' Sort the same column again, reversing the SortOrder. 
    '        If oldColumn Is newColumn AndAlso Promos_Source_Grid.SortOrder = Windows.Forms.SortOrder.Ascending Then
    '            direction = ListSortDirection.Descending
    '        Else

    '            ' Sort a new column and remove the old SortGlyph.
    '            direction = ListSortDirection.Ascending
    '            oldColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.None
    '        End If
    '    Else
    '        direction = ListSortDirection.Ascending
    '    End If

    '    ' Sort the selected column.
    '    Promos_Source_Grid.Sort(newColumn, direction)
    '    If direction = ListSortDirection.Ascending Then
    '        newColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.Ascending
    '    Else
    '        newColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.Descending
    '    End If
    'End Sub
    'Private Sub GridColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles Promos_Source_Grid.ColumnHeaderMouseClick, Promos_Target_Grid.ColumnHeaderMouseClick

    '    Dim Grid = CType(sender, DataGridView)

    '    MsgBox(Grid.Name.ToString)

    '    Dim newColumn As DataGridViewColumn = _
    '   Grid.Columns(e.ColumnIndex)
    '    Dim oldColumn As DataGridViewColumn = Grid.SortedColumn
    '    Dim direction As ListSortDirection


    '    ' If oldColumn is null, then the DataGridView is not currently sorted. 
    '    If oldColumn IsNot Nothing Then

    '        ' Sort the same column again, reversing the SortOrder. 
    '        If oldColumn Is newColumn AndAlso Grid.SortOrder = Windows.Forms.SortOrder.Ascending Then
    '            direction = ListSortDirection.Descending
    '        Else

    '            ' Sort a new column and remove the old SortGlyph.
    '            direction = ListSortDirection.Ascending
    '            oldColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.None
    '        End If
    '    Else
    '        direction = ListSortDirection.Ascending
    '    End If

    '    ' Sort the selected column.
    '    Grid.Sort(newColumn, direction)
    '    If direction = ListSortDirection.Ascending Then
    '        newColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.Ascending
    '    Else
    '        newColumn.HeaderCell.SortGlyphDirection = Windows.Forms.SortOrder.Descending
    '    End If
    'End Sub

    'Private Sub SongsGrid_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles Promos_Source_Grid.DataBindingComplete, Promos_Target_Grid.DataBindingComplete
    '    Dim Grid = CType(sender, DataGridView)
    '    For Each column As DataGridViewColumn In Grid.Columns
    '        column.SortMode = DataGridViewColumnSortMode.Programmatic
    '    Next
    'End Sub

    'Private Sub Promos_Source_Grid_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles Promos_Source_Grid.DataBindingComplete
    '    For Each column As DataGridViewColumn In Promos_Source_Grid.Columns
    '        column.SortMode = DataGridViewColumnSortMode.Programmatic
    '    Next
    'End Sub

    Private Sub cmb_Category_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Category.EditValueChanged
        If cmb_Category.EditValue.ToString <> "" Then
            Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString))
            Call LoadCustomerPromos()
        End If
        
    End Sub
End Class