﻿Imports DevExpress.XtraEditors
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.ComponentModel

Public Class frmActivateSongs
    Dim StoreMaxCount As Integer
    Dim dtGenreSongs As DataTable
    Dim dtStoreSongs As DataTable
    Dim StoreSongArray() As Integer

    Private Sub frmActivateSongs_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If btn_Save.Enabled = True Then
            If XtraMessageBox.Show("You have not saved the modifications done, do you want to save now?", "Saving", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                btn_Save.PerformClick()
            End If
        End If

    End Sub
    Private Sub frmActivateSongs_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call FormatGrid(Songs_Source_Grid, True)
        Call FormatGrid(Songs_Target_Grid, True)

        Call LoadGenres()
        Call LoadCustomers()
        Call InitializeStore()
        cmb_Store.EditValue = ""
        ReDim StoreSongArray(0)
        btn_Save.Enabled = True
    End Sub
    Private Sub LoadGenres()

        Dim cmdGenre As New SqlCommand, adpGenre As New SqlDataAdapter, dsGenre As New DataSet

        Call OpenDBConnection()

        With cmdGenre
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdGenre.CommandText = "ProcGetGenresList"
        adpGenre.SelectCommand = cmdGenre
        adpGenre.Fill(dsGenre, "GenreList")

        cmb_Genre.Properties.DisplayMember = "Genre_Name"
        cmb_Genre.Properties.ValueMember = "Genre_ID"
        cmb_Genre.Properties.DataSource = dsGenre.Tables("GenreList")
        cmb_Genre.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Genre.Properties.PopulateColumns()
        cmb_Genre.Properties.Columns(0).Visible = False
        cmb_Genre.Properties.ShowFooter = False
        cmb_Genre.Properties.ShowHeader = False

        cmb_Genre.EditValue = ""

        Call CloseDBConnection()

        dsGenre.Dispose()
        adpGenre.Dispose()

    End Sub
    Private Sub LoadCustomers()

        Dim cmdCustomer As New SqlCommand, adpCustomer As New SqlDataAdapter, dsCustomer As New DataSet

        Call OpenDBConnection()

        With cmdCustomer
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdCustomer.CommandText = "ProcGetCustomersList"
        adpCustomer.SelectCommand = cmdCustomer
        adpCustomer.Fill(dsCustomer, "CustomerList")

        cmb_Customer.Properties.DisplayMember = "Customer_Name"
        cmb_Customer.Properties.ValueMember = "Customer_ID"
        cmb_Customer.Properties.DataSource = dsCustomer.Tables("CustomerList")
        cmb_Customer.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Customer.Properties.PopulateColumns()
        'MsgBox(cmb_Customer.Properties.Columns.VisibleCount)
        cmb_Customer.Properties.Columns(0).Visible = False
        cmb_Customer.Properties.ShowFooter = False
        cmb_Customer.Properties.ShowHeader = False

        cmb_Customer.EditValue = ""

        Call CloseDBConnection()

        dsCustomer.Dispose()
        adpCustomer.Dispose()

    End Sub

    Private Sub LoadStores(ByVal Customer_Id As Integer)
        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoresListOfCustomer"
            .Parameters.AddWithValue("@Customer_Id", Customer_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "StoreList")


        'Creating Columns for Display in lookup
        cmb_Store.Properties.DisplayMember = "Store_Name"
        cmb_Store.Properties.ValueMember = "Store_ID"
        cmb_Store.Properties.DataSource = dsStores.Tables(0)
        cmb_Store.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Store.Properties.PopulateColumns()
        cmb_Store.Properties.Columns(0).Visible = False
        cmb_Store.Properties.ShowFooter = False
        cmb_Store.Properties.ShowHeader = False
        cmb_Store.EditValue = ""

        'If dsStores.Tables(0).Rows.Count <> 0 Then btn_Save.Enabled = True Else btn_Save.Enabled = False

        dsStores.Dispose()
        dapStores.Dispose()


        Call CloseDBConnection()
    End Sub

    Private Sub cmb_Customer_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Customer.EditValueChanged
        If cmb_Customer.EditValue.ToString <> "" Then Call LoadStores(Convert.ToInt32(cmb_Customer.EditValue.ToString))
    End Sub
    Private Sub cmb_Genre_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Genre.EditValueChanged
        Application.DoEvents()
        If cmb_Genre.EditValue.ToString = "" Then Exit Sub
        LoadCategories(Convert.ToInt32(cmb_Genre.EditValue.ToString))

    End Sub
    Private Sub LoadCategories(ByVal Genre_Id As Integer)
        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetGenreCategoriesList"
            .Parameters.AddWithValue("@Genre_Id", Genre_Id).Direction = ParameterDirection.Input
        End With

        cmb_Category.Properties.DataSource = Nothing
        cmb_Category.EditValue = ""
        cmb_Category.RefreshEditValue()

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "CategoryList")

        dsStores.Tables(0).Rows.Add(0, "", "ALL")
        dsStores.Tables(0).DefaultView.Sort = " Genre_Category_ID asc"
        'Creating Columns for Display in lookup
        cmb_Category.Properties.DisplayMember = "Category_Code"
        cmb_Category.Properties.ValueMember = "Genre_Category_ID"
        cmb_Category.Properties.DataSource = dsStores.Tables(0)
        cmb_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Category.Properties.PopulateColumns()
        cmb_Category.Properties.Columns(0).Visible = False
        cmb_Category.Properties.Columns(1).Visible = False
        cmb_Category.Properties.ShowFooter = False
        cmb_Category.Properties.ShowHeader = False
        cmb_Category.EditValue = ""

        'If dsStores.Tables(0).Rows.Count <> 0 Then btn_Save.Enabled = True Else btn_Save.Enabled = False
        cmb_Category.ItemIndex = 0
        dsStores.Dispose()
        dapStores.Dispose()

        Call CloseDBConnection()

    End Sub
    Private Sub LoadSongs()
        Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter
        Dim i As Integer

        Call OpenDBConnection()

        dtGenreSongs = Nothing
        dtGenreSongs = New DataTable

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetSongsList"
            .Parameters.AddWithValue("@Genre_ID", Convert.ToInt32(cmb_Genre.EditValue.ToString)).Direction = ParameterDirection.Input
            If cmb_Category.EditValue.ToString <> "" AndAlso cmb_Category.EditValue.ToString <> 0 Then
                .Parameters.AddWithValue("@Genre_Category_Id", Convert.ToInt32(cmb_Category.EditValue.ToString)).Direction = ParameterDirection.Input
            End If
        End With
        dapSongs.SelectCommand = Cmd
        dapSongs.Fill(dsSongs, "SongsList")

        dtGenreSongs = dsSongs.Tables("SongsList")

        If dtGenreSongs.Rows.Count <> 0 Then
            Songs_Source_Grid.Rows.Clear()
            'Check if audio file exists
            For Each dr As DataRow In dtGenreSongs.Rows
                If StoreSongArray(0) <> 0 Then
                    If Not StoreSongArray.Contains(dr("Media Id")) Then
                        If File.Exists(SongsArchivePath & "\" & dr("FileName")) Then
                            Songs_Source_Grid.Rows.Add(dr("Media Id"), dr("Category"), dr("Title"), dr("Album"), dr("Artists"), dr("FileName"), dr("Duration"), dr("Genre_Id"))

                            Application.DoEvents()
                        End If
                        i += 1
                    End If
                Else
                    If File.Exists(SongsArchivePath & "\" & dr("FileName")) Then
                        Songs_Source_Grid.Rows.Add(dr("Media Id"), dr("Category"), dr("Title"), dr("Album"), dr("Artists"), dr("FileName"), dr("Duration"), dr("Genre_Id"))

                        Application.DoEvents()
                    End If
                    i += 1
                    End If
            Next
            FormatGrid(Songs_Source_Grid, False)
        Else 'no records
            FormatGrid(Songs_Source_Grid, True)
            'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If
        Call CloseDBConnection()

        'Application.DoEvents()
        'If cmb_Genre.EditValue.ToString = "" Then Exit Sub
        'Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter
        'Dim i As Integer

        'Call OpenDBConnection()

        'With Cmd
        '    .CommandType = CommandType.StoredProcedure
        '    .Connection = DBConn
        '    .CommandTimeout = 500
        '    .CommandText = "ProcGetSongsList"
        '    .Parameters.AddWithValue("@Genre_ID", Convert.ToInt32(cmb_Genre.EditValue.ToString)).Direction = ParameterDirection.Input
        'End With
        'dapSongs.SelectCommand = Cmd
        'dapSongs.Fill(dsSongs, "SongsList")

        'If dsSongs.Tables("SongsList").Rows.Count <> 0 Then
        '    songs_source_grid.Rows.Clear()
        '    'songs_source_grid.DataSource = dsSongs.Tables("SongsList")
        '    'FormatGrid(songs_source_grid, False)
        '    'Check if audio file exists

        '    For Each dr As DataRow In dsSongs.Tables("SongsList").Rows
        '        songs_source_grid.Rows.Add(dr("Media Id"), dr("Title"), dr("Album"), dr("Artists"), dr("FileName"), dr("Duration"))
        '        Application.DoEvents()
        '        'If Not File.Exists("D:\Works\Files\" & songs_source_grid.Rows(i).Cells(4).Value.ToString) Then
        '        '    songs_source_grid.Rows(i).DefaultCellStyle.BackColor = Color.Red
        '        'End If
        '        i += 1
        '    Next

        'Else 'no records

        '    FormatGrid(songs_source_grid, True)
        '    XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        'End If
        'Call CloseDBConnection()

    End Sub
    Private Sub RefreshGenreSongs() 'Exected when store is changed in the same customer
        Application.DoEvents()
        Dim i As Integer
        'dtCustomerPromos.Select("[Media Id]<>12002", "[Media ID] asc", DataViewRowState.CurrentRows)
        Try
            If dtGenreSongs.Rows.Count <> 0 Then
                dtGenreSongs.DefaultView.Sort = "[Media Id] asc"
                Songs_Source_Grid.Rows.Clear()

                'Check if audio file exists

                For Each dr As DataRow In dtGenreSongs.Rows
                    If Not StoreSongArray.Contains(dr("Media Id")) Then

                        If File.Exists(SongsArchivePath & "\" & dr("FileName")) Then
                            Songs_Source_Grid.Rows.Add(dr("Media Id"), dr("Category"), dr("Title"), dr("Album"), dr("Artists"), dr("FileName"), dr("Duration"), dr("Genre_Id"))
                            Application.DoEvents()
                            i += 1
                        End If

                    End If
                Next

                FormatGrid(Songs_Source_Grid, False)

            End If
        Catch
        End Try

    End Sub
    'Private Sub cmb_Genre_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Genre.EditValueChanged
    '    Application.DoEvents()
    '    If cmb_Genre.EditValue.ToString = "" Then Exit Sub
    '    Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter
    '    Call OpenDBConnection()

    '    With Cmd
    '        .CommandType = CommandType.StoredProcedure
    '        .Connection = DBConn
    '        .CommandTimeout = 500
    '        .CommandText = "ProcGetSongsList"
    '        .Parameters.AddWithValue("@Genre_ID", Convert.ToInt32(cmb_Genre.EditValue.ToString)).Direction = ParameterDirection.Input

    '    End With
    '    dapSongs.SelectCommand = Cmd
    '    dapSongs.Fill(dsSongs, "SongsList")

    '    If dsSongs.Tables("SongsList").Rows.Count <> 0 Then
    '        songs_source_grid.Columns.Clear()
    '        songs_source_grid.DataSource = dsSongs.Tables("SongsList")
    '        FormatGrid(songs_source_grid, False)
    '        'Check if audio file exists
    '        For i = 0 To songs_source_grid.Rows.Count - 1
    '            If songs_source_grid.Rows(i).Cells(0).Value.ToString = "" Then Exit For
    '            If Not File.Exists("D:\Works\Files\" & songs_source_grid.Rows(i).Cells(4).Value.ToString) Then
    '                songs_source_grid.Rows(i).DefaultCellStyle.BackColor = Color.Red
    '            End If
    '        Next
    '    Else 'no records

    '        FormatGrid(songs_source_grid, True)
    '        XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
    '    End If
    '    Call CloseDBConnection()

    'End Sub


    Private Sub cmb_Store_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Store.EditValueChanged
        Application.DoEvents()
        If cmb_Store.EditValue = Nothing Then Exit Sub

        Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter
        Call OpenDBConnection()

        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetStoreSongPromoList"
            .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Filter_Criteria", "Songs").Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FilePrep", 0).Direction = ParameterDirection.Input
        End With
        dapSongs.SelectCommand = Cmd
        dapSongs.Fill(dsSongs, "SongsList")
        dtStoreSongs = Nothing
        dtStoreSongs = dsSongs.Tables("SongsList")

        StoreMaxCount = dsSongs.Tables(1).Rows(0).Item(0)

        Call LoadStoreSongData()

        dsSongs.Dispose()
        dapSongs.Dispose()
        Call CloseDBConnection()

        Call RefreshGenreSongs()

        txt_SongsCount.Text = Songs_Target_Grid.RowCount

        'Application.DoEvents()
        'If cmb_Store.EditValue.ToString = "" Then Exit Sub
        'Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter
        'Call OpenDBConnection()

        'With Cmd
        '    .CommandType = CommandType.StoredProcedure
        '    .Connection = DBConn
        '    .CommandTimeout = 500
        '    .CommandText = "ProcGetStoreSongPromoList"
        '    .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        '    .Parameters.AddWithValue("@Filter_Criteria", "Songs").Direction = ParameterDirection.Input
        'End With
        'dapSongs.SelectCommand = Cmd
        'dapSongs.Fill(dsSongs, "SongsList")
        'StoreMaxCount = dsSongs.Tables(1).Rows(0).Item(0)
        'If dsSongs.Tables("SongsList").Rows.Count <> 0 Then
        '    Songs_Target_Grid.Rows.Clear()

        '    ''reading the table
        '    For Each dr As DataRow In dsSongs.Tables("SongsList").Rows
        '        Songs_Target_Grid.Rows.Add(dr("MediaId"), dr("Title"), dr("Album"), dr("Artists"), dr("FileName"), dr("Duration"))
        '        Application.DoEvents()
        '        ''Check if audio file exists
        '        'For i = 0 To Songs_Target_Grid.Rows.Count - 1
        '        '    If Songs_Target_Grid.Rows(i).Cells(0).Value.ToString = "" Then Exit For
        '        '    If Not File.Exists("D:\Works\Files\" & Songs_Target_Grid.Rows(i).Cells(4).Value.ToString) Then
        '        '        Songs_Target_Grid.Rows(i).DefaultCellStyle.BackColor = Color.Red
        '        '    End If
        '        'Next
        '    Next
        '    FormatGrid(Songs_Target_Grid, False)
        '    txt_SongsCount.Text = Songs_Target_Grid.Rows.Count

        'Else 'no records
        '    txt_SongsCount.Text = 0
        '    Songs_Target_Grid.Rows.Clear()
        '    'FormatGrid(Songs_Target_Grid, True)
        '    XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        'End If
        'dsSongs.Dispose()
        'dapSongs.Dispose()
        'Call CloseDBConnection()


        'Application.DoEvents()
        'If cmb_Store.EditValue.ToString = "" Then Exit Sub
        'Dim Cmd As New SqlCommand, dsSongs As New DataSet, dapSongs As New SqlDataAdapter
        'Call OpenDBConnection()

        'With Cmd
        '    .CommandType = CommandType.StoredProcedure
        '    .Connection = DBConn
        '    .CommandTimeout = 500
        '    .CommandText = "ProcGetStoreSongPromoList"
        '    .Parameters.AddWithValue("@Store_ID", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
        '    .Parameters.AddWithValue("@Filter_Criteria", "Songs").Direction = ParameterDirection.Input

        'End With
        'dapSongs.SelectCommand = Cmd
        'dapSongs.Fill(dsSongs, "SongsList")

        'If dsSongs.Tables("SongsList").Rows.Count <> 0 Then
        '    Songs_Target_Grid.Columns.Clear()
        '    Songs_Target_Grid.DataSource = dsSongs.Tables("SongsList")
        '    FormatGrid(Songs_Target_Grid, False)
        '    'Check if audio file exists
        '    For i = 0 To Songs_Target_Grid.Rows.Count - 1
        '        If Songs_Target_Grid.Rows(i).Cells(0).Value.ToString = "" Then Exit For
        '        If Not File.Exists("D:\Works\Files\" & Songs_Target_Grid.Rows(i).Cells(4).Value.ToString) Then
        '            Songs_Target_Grid.Rows(i).DefaultCellStyle.BackColor = Color.Red
        '        End If
        '    Next
        'Else 'no records

        '    FormatGrid(Songs_Target_Grid, True)
        '    XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        'End If
        'dsSongs.Dispose()
        'dapSongs.Dispose()
        'Call CloseDBConnection()

    End Sub
    Private Sub LoadStoreSongData()
        Dim r As Integer

        If dtStoreSongs.Rows.Count <> 0 Then
            ReDim StoreSongArray(dtStoreSongs.Rows.Count - 1)
            Songs_Target_Grid.Rows.Clear()

            ''reading the table
            For Each dr As DataRow In dtStoreSongs.Rows
                StoreSongArray(r) = (dr("MediaId"))
                r += 1
                Songs_Target_Grid.Rows.Add(dr("MediaId"), dr("Category"), dr("Title"), dr("Album"), dr("Artists"), dr("FileName"), dr("Duration"), dr("Genre_Id"))
                Application.DoEvents()
                ''Check if audio file exists
                'For i = 0 To Songs_Target_Grid.Rows.Count - 1
                '    If Songs_Target_Grid.Rows(i).Cells(0).Value.ToString = "" Then Exit For
                '    If Not File.Exists("D:\Works\Files\" & Songs_Target_Grid.Rows(i).Cells(4).Value.ToString) Then
                '        Songs_Target_Grid.Rows(i).DefaultCellStyle.BackColor = Color.Red
                '    End If
                'Next
            Next
            FormatGrid(Songs_Target_Grid, False)
            txt_SongsCount.Text = r

        Else 'no records
            ReDim StoreSongArray(0)
            txt_SongsCount.Text = 0
            Songs_Target_Grid.Rows.Clear()
            FormatGrid(Songs_Target_Grid, True)
            'XtraMessageBox.Show("No data found", "Alert", MessageBoxButtons.OK)
        End If
    End Sub

    '    Public Function HitTest( _
    ' ByVal x As Integer, _
    ' ByVal y As Integer _
    ') As DataGridView.HitTestInfo
    Private Sub DataGridViews_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Songs_Source_Grid.MouseDown, Songs_Target_Grid.MouseDown
        Dim handler = CType(sender, DataGridView)
        Dim hti As DataGridView.HitTestInfo = handler.HitTest(e.X, e.Y)
        If hti.Type <> DataGrid.HitTestType.ColumnHeader Then
            If e.Button <> Windows.Forms.MouseButtons.Left OrElse handler.SelectedRows.Count = 0 Then Return
            handler.DoDragDrop(handler, DragDropEffects.Move)
        End If
       
    End Sub

    Private Sub DataGridViews_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Songs_Source_Grid.DragEnter, Songs_Target_Grid.DragEnter
        e.Effect = DragDropEffects.Move
    End Sub

    Private Sub DataGridViews_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Songs_Source_Grid.DragDrop, Songs_Target_Grid.DragDrop

        Dim handler = CType(sender, DataGridView)
        Dim source = CType(e.Data.GetData(GetType(DataGridView)), DataGridView)

        Dim iRowAdded As Boolean

        If source.Name = handler.Name Then Exit Sub
        If cmb_Customer.Text = "" Or cmb_Store.Text = "" Or cmb_Genre.Text = "" Then Exit Sub

        Dim dtRow As DataRow, dtRow1() As DataRow

        'Finding the number of rows in the grid
        Dim sCount As Integer
        For sCount = 0 To Songs_Target_Grid.Rows.Count - 1
            If Songs_Target_Grid.Rows(sCount).Cells(0).Value.ToString = "" Then Exit For
        Next

        For Each row As DataGridViewRow In source.SelectedRows
            If row.Cells(0).Value.ToString <> "" Then
                'If CStr(row.Cells(3).Value) = "Y" Then
                If handler.Name = "Songs_Target_Grid" AndAlso File.Exists(SongsArchivePath & "\" & row.Cells(5).Value) Then 'Not FindSong(handler, row.Cells(0).Value) Then
                    If StoreMaxCount > sCount Then
                        dtRow = dtStoreSongs.NewRow
                        'dr("MediaId"), dr("Title"), dr("Album"), dr("Artists"), dr("FileName"), dr("Duration")
                        dtRow(0) = row.Cells(0).Value : dtRow(1) = row.Cells(1).Value : dtRow(2) = row.Cells(2).Value : dtRow(3) = row.Cells(3).Value : dtRow(4) = row.Cells(4).Value : dtRow(5) = row.Cells(5).Value : dtRow(6) = row.Cells(6).Value : dtRow(7) = row.Cells(7).Value
                        dtStoreSongs.Rows.Add(dtRow)
                        sCount += 1
                        iRowAdded = True
                    Else
                        XtraMessageBox.Show("Reached maximum Number of Songs allowed for the store", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit For
                    End If
                ElseIf handler.Name = "Song_Source_Grid" And row.Cells(7).Value = Convert.ToInt32(cmb_Genre.EditValue.ToString) Then
                    sCount -= 1
                    dtRow1 = dtStoreSongs.Select("MediaId=" & row.Cells(0).Value)
                    dtStoreSongs.Rows.Remove(dtRow1(0))
                    iRowAdded = True
                End If

            End If
            'End If
        Next

        If iRowAdded Then
            Call LoadStoreSongData()
            Call RefreshGenreSongs()
            btn_Save.Enabled = True
        End If
        'Dim handler = CType(sender, DataGridView)
        'Dim source = CType(e.Data.GetData(GetType(DataGridView)), DataGridView)

        'If source.Name = handler.Name Then Exit Sub

        'For Each row As DataGridViewRow In source.SelectedRows
        '    'If CStr(row.Cells(3).Value) = "Y" Then
        '    If handler.Name = "Songs_Target_Grid" Then
        '        If Not FindSong(handler, row.Cells(0).Value) Then
        '            If StoreMaxCount > Songs_Target_Grid.Rows.Count Then
        '                source.Rows.Remove(row)
        '                handler.Rows.Add(row)
        '            Else
        '                XtraMessageBox.Show("Reached maximum Number of Songs allowed for the store", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '                Exit For
        '            End If
        '        End If
        '    Else
        '        source.Rows.Remove(row)
        '        If Not FindSong(handler, row.Cells(0).Value) Then handler.Rows.Add(row)
        '    End If

        '    'End If
        'Next

        'handler.Sort(handler.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
        ''If handler.Name = "Songs_Target_Grid" Then
        'txt_SongsCount.Text = Songs_Target_Grid.Rows.Count
        ''End If
    End Sub
    Private Sub InitializeStore()
        Dim dt As New DataTable
        dt.Columns.Add("Unknown")
        cmb_Store.Properties.DataSource = dt
        dt.Dispose()
    End Sub
    'Private Sub RemoveRow(ByRef  ByVal drRow As DataRow)
    '    'Dim dsTempTable As DataSet = GridRatio.DataSource
    '    Dim dt As DataTable = GridRatio.DataSource
    '    dt.Columns.Remove(strColumnName)
    '    dt.AcceptChanges()
    'End Sub
    'Private Sub AddRow(ByVal strColumnName As String)
    '    'Dim dsTempTable As DataSet = GridRatio.DataSource
    '    Dim dt As DataTable = GridRatio.DataSource
    '    dt.Columns.Add(strColumnName, System.Type.GetType("System.String"))
    '    dt.AcceptChanges()

    'End Sub

    'Private Sub FormatGrid(ByRef Grid As DataGridView, ByVal ResetStructure As Boolean)

    '    If ResetStructure Then
    '        Dim gridData As New DataTable
    '        gridData.Columns.Add("Song_ID")
    '        gridData.Columns.Add("Title")
    '        gridData.Columns.Add("Album")
    '        gridData.Columns.Add("Artists")
    '        gridData.Columns.Add("FileName")
    '        gridData.Columns.Add("Duration")
    '        For i As Integer = 1 To 30
    '            gridData.Rows.Add("", "", "", "", "", "")
    '        Next

    '        Grid.Columns.Clear()
    '        Grid.DataSource = gridData
    '        Grid.Columns(0).HeaderText = "Song ID"
    '        gridData.Dispose()
    '    End If

    '    Grid.Columns(0).Width = 80
    '    Grid.Columns(1).Width = 200
    '    Grid.Columns(2).Width = 200

    '    If Grid.Name = "songs_source_grid" Then
    '        Grid.Columns(3).Width = 200
    '    Else
    '        Grid.Columns(3).Width = 0
    '    End If
    '    Grid.Columns(4).Width = 0
    '    Grid.Columns(5).Width = 0

    '    Grid.Refresh()

    'End Sub
    Private Sub FormatGrid(ByRef Grid As DataGridView, ByVal ResetStructure As Boolean)

        If ResetStructure Then
            Grid.Columns.Clear()
            Grid.Columns.Add("Song_ID", "Song ID")
            Grid.Columns.Add("Category", "Category")
            Grid.Columns.Add("Title", "Title")
            Grid.Columns.Add("Album", "Album")
            Grid.Columns.Add("Artists", "Artists")
            Grid.Columns.Add("FileName", "FileName")
            Grid.Columns.Add("Duration", "Duration")
            Grid.Columns.Add("Genre_Id", "Genre ID")

            'For i As Integer = 1 To 30
            '    Grid.Rows.Add("", "", "", "", "", "")
            'Next
            'txt_SongsCount.Text = 0
        ElseIf Grid.Rows.Count < 30 Then
            'For i As Integer = Grid.Rows.Count + 1 To 30
            '    Grid.Rows.Add("", "", "", "", "", "")
            'Next
        End If

        Grid.Columns(0).Width = 80
        'Grid.Columns(0).SortMode = DataGridViewColumnSortMode.Automatic
        Grid.Columns(1).Width = 80
        'Grid.Columns(1).SortMode = DataGridViewColumnSortMode.Automatic
        Grid.Columns(2).Width = 200
        'Grid.Columns(2).SortMode = DataGridViewColumnSortMode.Automatic
        Grid.Columns(3).Width = 200
        'Grid.Columns(3).SortMode = DataGridViewColumnSortMode.Automatic
        Grid.Columns(4).Width = 200
        'Grid.Columns(4).SortMode = DataGridViewColumnSortMode.Automatic
        If Grid.Name = "Songs_Target_Grid" Then
            Grid.Columns(4).Visible = False
        End If
        Grid.Columns(5).Visible = False
        Grid.Columns(6).Visible = False
        Grid.Columns(7).Visible = False
        Grid.Refresh()

    End Sub
    Private Function FindSong(ByRef GridEffected As DataGridView, ByVal Song_Id As Integer) As Boolean
        Dim Found As Boolean = False
        For i As Integer = 0 To GridEffected.Rows.Count - 1
            If GridEffected.Rows(i).Cells(0).Value.ToString = "" Then
                Exit For
            ElseIf Song_Id = GridEffected.Rows(i).Cells(0).Value.ToString Then
                Found = True
                Exit For
            End If
        Next
        Return Found
    End Function

    Private Sub btn_Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Save.Click

        If cmb_Store.EditValue = Nothing Then Exit Sub

        Cursor = Cursors.WaitCursor
        Dim Song_Ids As String

        If Song_Ids <> "" Then Song_Ids = Song_Ids.Substring(0, Song_Ids.Length - 1)


        Dim CmdSongs As New SqlCommand, CmdResetSongs As New SqlCommand

        Call OpenDBConnection()
        With CmdResetSongs
            .Connection = DBConn
            .CommandText = "ProcResetActivatedSongs"
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .ExecuteNonQuery()
        End With

        With CmdSongs
            .Connection = DBConn
            .CommandText = "ProcActivateSongs"
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .Parameters.AddWithValue("@Store_Id", Convert.ToInt32(cmb_Store.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Song_Ids", Song_Ids).Direction = ParameterDirection.Input
        End With

        Call CloseDBConnection()

        CmdResetSongs.Dispose()
        CmdResetSongs = Nothing
        CmdSongs.Dispose()
        CmdSongs = Nothing
        Cursor = Cursors.Default
        btn_Save.Enabled = False
        ' XtraMessageBox.Show("Activated songs saved", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub cmb_Category_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmb_Category.EditValueChanged
        If cmb_Category.EditValue.ToString <> "" Then Call LoadSongs()
    End Sub


   
    Private Sub lbl_Title_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbl_Title.Click

    End Sub
End Class
