﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibrary
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Library_Tab_Control = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.Grp_Songs_History = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Songs_Close = New DevExpress.XtraEditors.SimpleButton()
        Me.Grid_Songs_History = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_Add_Song = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Songs_Search = New DevExpress.XtraEditors.TextEdit()
        Me.SongsGrid = New System.Windows.Forms.DataGridView()
        Me.Song_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Title = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Album = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Artist = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FileName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Duration = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Genre_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PopUpMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TStrip_btn_Genre = New System.Windows.Forms.ToolStripMenuItem()
        Me.TStrip_btn_Modify_Metadata = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_SongsCount = New DevExpress.XtraEditors.TextEdit()
        Me.cmb_Genre = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_SongActivate = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.Grp_Promos_History = New DevExpress.XtraEditors.GroupControl()
        Me.btn_Promos_Close = New DevExpress.XtraEditors.SimpleButton()
        Me.Grid_Promos_History = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txt_Promos_Search = New DevExpress.XtraEditors.TextEdit()
        Me.btn_AddPromo = New DevExpress.XtraEditors.SimpleButton()
        Me.PromosGrid = New System.Windows.Forms.DataGridView()
        Me.Promo_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_Category = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_Title = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_FileName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_Duration = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Promo_Normalization = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Start_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Stop_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.popupMenuPromo = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.pop_Genre_Promo = New System.Windows.Forms.ToolStripMenuItem()
        Me.pop_Promo_Metadata = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_PromosCount = New DevExpress.XtraEditors.TextEdit()
        Me.cmb_Customer = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_ActivatePromo = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.txt_Stores_Search = New DevExpress.XtraEditors.TextEdit()
        Me.cmb_Type_Storetab = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.StoresGrid = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GenreId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmb_Store_Storetab = New DevExpress.XtraEditors.LookUpEdit()
        Me.btn_Go = New DevExpress.XtraEditors.SimpleButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_Filecount_Storetab = New DevExpress.XtraEditors.TextEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmb_Customer_Storetab = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.PanelModify_Category = New DevExpress.XtraEditors.PanelControl()
        Me.cmb_Modify_Category = New DevExpress.XtraEditors.LookUpEdit()
        Me.lbl_Modify_Category = New System.Windows.Forms.Label()
        Me.Category_Modify_Grid = New System.Windows.Forms.DataGridView()
        Me.Category_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Category_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Category_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_Modify_Exit = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_ModifyCategory = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.Library_Tab_Control, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Library_Tab_Control.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.Grp_Songs_History, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Grp_Songs_History.SuspendLayout()
        CType(Me.Grid_Songs_History, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Songs_Search.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SongsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopUpMenu.SuspendLayout()
        CType(Me.txt_SongsCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Genre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.Grp_Promos_History, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Grp_Promos_History.SuspendLayout()
        CType(Me.Grid_Promos_History, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Promos_Search.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PromosGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.popupMenuPromo.SuspendLayout()
        CType(Me.txt_PromosCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.txt_Stores_Search.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Type_Storetab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StoresGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Store_Storetab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Filecount_Storetab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_Customer_Storetab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelModify_Category, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelModify_Category.SuspendLayout()
        CType(Me.cmb_Modify_Category.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Category_Modify_Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Library_Tab_Control
        '
        Me.Library_Tab_Control.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Library_Tab_Control.Appearance.Options.UseFont = True
        Me.Library_Tab_Control.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Library_Tab_Control.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Library_Tab_Control.Location = New System.Drawing.Point(0, 0)
        Me.Library_Tab_Control.Name = "Library_Tab_Control"
        Me.Library_Tab_Control.SelectedTabPage = Me.XtraTabPage1
        Me.Library_Tab_Control.Size = New System.Drawing.Size(1343, 651)
        Me.Library_Tab_Control.TabIndex = 4
        Me.Library_Tab_Control.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.Grp_Songs_History)
        Me.XtraTabPage1.Controls.Add(Me.btn_Add_Song)
        Me.XtraTabPage1.Controls.Add(Me.txt_Songs_Search)
        Me.XtraTabPage1.Controls.Add(Me.SongsGrid)
        Me.XtraTabPage1.Controls.Add(Me.Label2)
        Me.XtraTabPage1.Controls.Add(Me.txt_SongsCount)
        Me.XtraTabPage1.Controls.Add(Me.cmb_Genre)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.btn_SongActivate)
        Me.XtraTabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1337, 623)
        Me.XtraTabPage1.Text = "Songs"
        '
        'Grp_Songs_History
        '
        Me.Grp_Songs_History.AppearanceCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grp_Songs_History.AppearanceCaption.Options.UseFont = True
        Me.Grp_Songs_History.Controls.Add(Me.btn_Songs_Close)
        Me.Grp_Songs_History.Controls.Add(Me.Grid_Songs_History)
        Me.Grp_Songs_History.Location = New System.Drawing.Point(6, 400)
        Me.Grp_Songs_History.Name = "Grp_Songs_History"
        Me.Grp_Songs_History.Size = New System.Drawing.Size(1327, 216)
        Me.Grp_Songs_History.TabIndex = 98
        Me.Grp_Songs_History.Text = "History for :"
        Me.Grp_Songs_History.Visible = False
        '
        'btn_Songs_Close
        '
        Me.btn_Songs_Close.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Songs_Close.Appearance.Options.UseFont = True
        Me.btn_Songs_Close.Location = New System.Drawing.Point(1298, 0)
        Me.btn_Songs_Close.Name = "btn_Songs_Close"
        Me.btn_Songs_Close.Size = New System.Drawing.Size(28, 21)
        Me.btn_Songs_Close.TabIndex = 98
        Me.btn_Songs_Close.Text = "X"
        '
        'Grid_Songs_History
        '
        Me.Grid_Songs_History.AllowUserToAddRows = False
        Me.Grid_Songs_History.AllowUserToDeleteRows = False
        Me.Grid_Songs_History.AllowUserToResizeRows = False
        DataGridViewCellStyle27.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.Color.Black
        Me.Grid_Songs_History.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle27
        Me.Grid_Songs_History.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Grid_Songs_History.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Grid_Songs_History.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Grid_Songs_History.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_Songs_History.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle28
        Me.Grid_Songs_History.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid_Songs_History.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13})
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid_Songs_History.DefaultCellStyle = DataGridViewCellStyle29
        Me.Grid_Songs_History.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Grid_Songs_History.EnableHeadersVisualStyles = False
        Me.Grid_Songs_History.Location = New System.Drawing.Point(3, 34)
        Me.Grid_Songs_History.Name = "Grid_Songs_History"
        Me.Grid_Songs_History.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle30.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_Songs_History.RowHeadersDefaultCellStyle = DataGridViewCellStyle30
        Me.Grid_Songs_History.RowHeadersVisible = False
        Me.Grid_Songs_History.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Grid_Songs_History.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Grid_Songs_History.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Grid_Songs_History.Size = New System.Drawing.Size(1319, 177)
        Me.Grid_Songs_History.TabIndex = 97
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Song ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Category"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Title"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 200
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Album"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 200
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Artists"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Width = 200
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "FileName"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Duration"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Width = 80
        '
        'btn_Add_Song
        '
        Me.btn_Add_Song.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Add_Song.Appearance.Options.UseFont = True
        Me.btn_Add_Song.Location = New System.Drawing.Point(462, 16)
        Me.btn_Add_Song.Name = "btn_Add_Song"
        Me.btn_Add_Song.Size = New System.Drawing.Size(119, 25)
        Me.btn_Add_Song.TabIndex = 67
        Me.btn_Add_Song.Text = "Add Song"
        '
        'txt_Songs_Search
        '
        Me.txt_Songs_Search.EditValue = "<Search Text>"
        Me.txt_Songs_Search.Location = New System.Drawing.Point(587, 18)
        Me.txt_Songs_Search.Name = "txt_Songs_Search"
        Me.txt_Songs_Search.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Songs_Search.Properties.Appearance.Options.UseFont = True
        Me.txt_Songs_Search.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Songs_Search.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_Songs_Search.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_Songs_Search.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_Songs_Search.Size = New System.Drawing.Size(180, 22)
        Me.txt_Songs_Search.TabIndex = 53
        '
        'SongsGrid
        '
        Me.SongsGrid.AllowUserToAddRows = False
        Me.SongsGrid.AllowUserToDeleteRows = False
        Me.SongsGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle31.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black
        Me.SongsGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle31
        Me.SongsGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SongsGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.SongsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SongsGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.SongsGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle32.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SongsGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle32
        Me.SongsGrid.ColumnHeadersHeight = 25
        Me.SongsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.SongsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Song_ID, Me.Title, Me.Album, Me.Artist, Me.FileName, Me.Duration, Me.Genre_Id})
        Me.SongsGrid.ContextMenuStrip = Me.PopUpMenu
        DataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle33.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle33.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SongsGrid.DefaultCellStyle = DataGridViewCellStyle33
        Me.SongsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.SongsGrid.EnableHeadersVisualStyles = False
        Me.SongsGrid.Location = New System.Drawing.Point(7, 46)
        Me.SongsGrid.Name = "SongsGrid"
        Me.SongsGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle34.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SongsGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle34
        Me.SongsGrid.RowHeadersVisible = False
        Me.SongsGrid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.SongsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.SongsGrid.ShowCellErrors = False
        Me.SongsGrid.ShowCellToolTips = False
        Me.SongsGrid.ShowEditingIcon = False
        Me.SongsGrid.ShowRowErrors = False
        Me.SongsGrid.Size = New System.Drawing.Size(1326, 570)
        Me.SongsGrid.TabIndex = 52
        '
        'Song_ID
        '
        Me.Song_ID.HeaderText = "Song ID"
        Me.Song_ID.Name = "Song_ID"
        Me.Song_ID.Width = 80
        '
        'Title
        '
        Me.Title.HeaderText = "Title"
        Me.Title.Name = "Title"
        Me.Title.Width = 300
        '
        'Album
        '
        Me.Album.HeaderText = "Album"
        Me.Album.Name = "Album"
        Me.Album.Width = 300
        '
        'Artist
        '
        Me.Artist.HeaderText = "Artist"
        Me.Artist.Name = "Artist"
        Me.Artist.Width = 300
        '
        'FileName
        '
        Me.FileName.HeaderText = "File Name"
        Me.FileName.Name = "FileName"
        Me.FileName.Width = 180
        '
        'Duration
        '
        Me.Duration.HeaderText = "Duration"
        Me.Duration.Name = "Duration"
        '
        'Genre_Id
        '
        Me.Genre_Id.HeaderText = "Genre ID"
        Me.Genre_Id.Name = "Genre_Id"
        Me.Genre_Id.Visible = False
        '
        'PopUpMenu
        '
        Me.PopUpMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TStrip_btn_Genre, Me.TStrip_btn_Modify_Metadata})
        Me.PopUpMenu.Name = "PopUpMenu"
        Me.PopUpMenu.Size = New System.Drawing.Size(164, 48)
        '
        'TStrip_btn_Genre
        '
        Me.TStrip_btn_Genre.Name = "TStrip_btn_Genre"
        Me.TStrip_btn_Genre.Size = New System.Drawing.Size(163, 22)
        Me.TStrip_btn_Genre.Text = "Modify Category"
        '
        'TStrip_btn_Modify_Metadata
        '
        Me.TStrip_btn_Modify_Metadata.Name = "TStrip_btn_Modify_Metadata"
        Me.TStrip_btn_Modify_Metadata.Size = New System.Drawing.Size(163, 22)
        Me.TStrip_btn_Modify_Metadata.Text = "Edit Song"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(1147, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 15)
        Me.Label2.TabIndex = 51
        Me.Label2.Text = "Total:"
        '
        'txt_SongsCount
        '
        Me.txt_SongsCount.EditValue = ""
        Me.txt_SongsCount.Enabled = False
        Me.txt_SongsCount.Location = New System.Drawing.Point(1197, 19)
        Me.txt_SongsCount.Name = "txt_SongsCount"
        Me.txt_SongsCount.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_SongsCount.Properties.Appearance.Options.UseFont = True
        Me.txt_SongsCount.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_SongsCount.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_SongsCount.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_SongsCount.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_SongsCount.Size = New System.Drawing.Size(98, 22)
        Me.txt_SongsCount.TabIndex = 50
        '
        'cmb_Genre
        '
        Me.cmb_Genre.EditValue = ""
        Me.cmb_Genre.Location = New System.Drawing.Point(57, 18)
        Me.cmb_Genre.Name = "cmb_Genre"
        Me.cmb_Genre.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Genre.Properties.Appearance.Options.UseFont = True
        Me.cmb_Genre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Genre.Properties.NullText = ""
        Me.cmb_Genre.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Genre.TabIndex = 48
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 15)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = "Genre:"
        '
        'btn_SongActivate
        '
        Me.btn_SongActivate.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_SongActivate.Appearance.Options.UseFont = True
        Me.btn_SongActivate.Enabled = False
        Me.btn_SongActivate.Location = New System.Drawing.Point(295, 16)
        Me.btn_SongActivate.Name = "btn_SongActivate"
        Me.btn_SongActivate.Size = New System.Drawing.Size(161, 25)
        Me.btn_SongActivate.TabIndex = 30
        Me.btn_SongActivate.Text = "Activate/Deactivate to Store"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.Grp_Promos_History)
        Me.XtraTabPage2.Controls.Add(Me.txt_Promos_Search)
        Me.XtraTabPage2.Controls.Add(Me.btn_AddPromo)
        Me.XtraTabPage2.Controls.Add(Me.PromosGrid)
        Me.XtraTabPage2.Controls.Add(Me.Label3)
        Me.XtraTabPage2.Controls.Add(Me.txt_PromosCount)
        Me.XtraTabPage2.Controls.Add(Me.cmb_Customer)
        Me.XtraTabPage2.Controls.Add(Me.Label4)
        Me.XtraTabPage2.Controls.Add(Me.btn_ActivatePromo)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1337, 623)
        Me.XtraTabPage2.Text = "Promos"
        '
        'Grp_Promos_History
        '
        Me.Grp_Promos_History.AppearanceCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grp_Promos_History.AppearanceCaption.Options.UseFont = True
        Me.Grp_Promos_History.Controls.Add(Me.btn_Promos_Close)
        Me.Grp_Promos_History.Controls.Add(Me.Grid_Promos_History)
        Me.Grp_Promos_History.Location = New System.Drawing.Point(10, 395)
        Me.Grp_Promos_History.Name = "Grp_Promos_History"
        Me.Grp_Promos_History.Size = New System.Drawing.Size(940, 216)
        Me.Grp_Promos_History.TabIndex = 99
        Me.Grp_Promos_History.Text = "History for :"
        Me.Grp_Promos_History.Visible = False
        '
        'btn_Promos_Close
        '
        Me.btn_Promos_Close.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Promos_Close.Appearance.Options.UseFont = True
        Me.btn_Promos_Close.Location = New System.Drawing.Point(911, 0)
        Me.btn_Promos_Close.Name = "btn_Promos_Close"
        Me.btn_Promos_Close.Size = New System.Drawing.Size(28, 21)
        Me.btn_Promos_Close.TabIndex = 98
        Me.btn_Promos_Close.Text = "X"
        '
        'Grid_Promos_History
        '
        Me.Grid_Promos_History.AllowUserToAddRows = False
        Me.Grid_Promos_History.AllowUserToDeleteRows = False
        Me.Grid_Promos_History.AllowUserToResizeRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        Me.Grid_Promos_History.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.Grid_Promos_History.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Grid_Promos_History.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Grid_Promos_History.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Grid_Promos_History.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_Promos_History.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.Grid_Promos_History.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid_Promos_History.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid_Promos_History.DefaultCellStyle = DataGridViewCellStyle7
        Me.Grid_Promos_History.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Grid_Promos_History.EnableHeadersVisualStyles = False
        Me.Grid_Promos_History.Location = New System.Drawing.Point(3, 34)
        Me.Grid_Promos_History.Name = "Grid_Promos_History"
        Me.Grid_Promos_History.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid_Promos_History.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.Grid_Promos_History.RowHeadersVisible = False
        Me.Grid_Promos_History.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Grid_Promos_History.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Grid_Promos_History.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Grid_Promos_History.Size = New System.Drawing.Size(932, 177)
        Me.Grid_Promos_History.TabIndex = 97
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Song ID"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Width = 80
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Category"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Width = 80
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Title"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Width = 200
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Album"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Width = 200
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Artists"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Width = 200
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "FileName"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "Duration"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.Width = 80
        '
        'txt_Promos_Search
        '
        Me.txt_Promos_Search.EditValue = "<Search Text>"
        Me.txt_Promos_Search.Location = New System.Drawing.Point(615, 14)
        Me.txt_Promos_Search.Name = "txt_Promos_Search"
        Me.txt_Promos_Search.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Promos_Search.Properties.Appearance.Options.UseFont = True
        Me.txt_Promos_Search.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Promos_Search.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_Promos_Search.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_Promos_Search.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_Promos_Search.Size = New System.Drawing.Size(146, 22)
        Me.txt_Promos_Search.TabIndex = 75
        '
        'btn_AddPromo
        '
        Me.btn_AddPromo.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_AddPromo.Appearance.Options.UseFont = True
        Me.btn_AddPromo.Location = New System.Drawing.Point(490, 12)
        Me.btn_AddPromo.Name = "btn_AddPromo"
        Me.btn_AddPromo.Size = New System.Drawing.Size(119, 25)
        Me.btn_AddPromo.TabIndex = 74
        Me.btn_AddPromo.Text = "Add Promo"
        '
        'PromosGrid
        '
        Me.PromosGrid.AllowUserToAddRows = False
        Me.PromosGrid.AllowUserToDeleteRows = False
        Me.PromosGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle35.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black
        Me.PromosGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle35
        Me.PromosGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PromosGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.PromosGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.PromosGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.PromosGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle36.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle36.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PromosGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle36
        Me.PromosGrid.ColumnHeadersHeight = 25
        Me.PromosGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.PromosGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Promo_ID, Me.Promo_Category, Me.Promo_Title, Me.Promo_FileName, Me.Promo_Duration, Me.Promo_Normalization, Me.Start_Date, Me.Stop_Date})
        Me.PromosGrid.ContextMenuStrip = Me.popupMenuPromo
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle37.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle37.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.PromosGrid.DefaultCellStyle = DataGridViewCellStyle37
        Me.PromosGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.PromosGrid.EnableHeadersVisualStyles = False
        Me.PromosGrid.Location = New System.Drawing.Point(10, 41)
        Me.PromosGrid.Name = "PromosGrid"
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle38.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PromosGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle38
        Me.PromosGrid.RowHeadersVisible = False
        Me.PromosGrid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.PromosGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.PromosGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.PromosGrid.Size = New System.Drawing.Size(939, 570)
        Me.PromosGrid.TabIndex = 73
        '
        'Promo_ID
        '
        Me.Promo_ID.HeaderText = "Promo ID"
        Me.Promo_ID.Name = "Promo_ID"
        Me.Promo_ID.Width = 80
        '
        'Promo_Category
        '
        Me.Promo_Category.HeaderText = "Category"
        Me.Promo_Category.Name = "Promo_Category"
        Me.Promo_Category.Width = 80
        '
        'Promo_Title
        '
        Me.Promo_Title.HeaderText = "Title"
        Me.Promo_Title.Name = "Promo_Title"
        Me.Promo_Title.Width = 320
        '
        'Promo_FileName
        '
        Me.Promo_FileName.HeaderText = "File Name"
        Me.Promo_FileName.Name = "Promo_FileName"
        Me.Promo_FileName.Width = 160
        '
        'Promo_Duration
        '
        Me.Promo_Duration.HeaderText = "Duration"
        Me.Promo_Duration.Name = "Promo_Duration"
        Me.Promo_Duration.Width = 75
        '
        'Promo_Normalization
        '
        Me.Promo_Normalization.HeaderText = "Normalization"
        Me.Promo_Normalization.Name = "Promo_Normalization"
        Me.Promo_Normalization.Visible = False
        '
        'Start_Date
        '
        Me.Start_Date.HeaderText = "Start Date"
        Me.Start_Date.Name = "Start_Date"
        '
        'Stop_Date
        '
        Me.Stop_Date.HeaderText = "Stop Date"
        Me.Stop_Date.Name = "Stop_Date"
        '
        'popupMenuPromo
        '
        Me.popupMenuPromo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.pop_Genre_Promo, Me.pop_Promo_Metadata})
        Me.popupMenuPromo.Name = "PopUpMenu"
        Me.popupMenuPromo.Size = New System.Drawing.Size(164, 48)
        '
        'pop_Genre_Promo
        '
        Me.pop_Genre_Promo.Name = "pop_Genre_Promo"
        Me.pop_Genre_Promo.Size = New System.Drawing.Size(163, 22)
        Me.pop_Genre_Promo.Text = "Modify Category"
        '
        'pop_Promo_Metadata
        '
        Me.pop_Promo_Metadata.Name = "pop_Promo_Metadata"
        Me.pop_Promo_Metadata.Size = New System.Drawing.Size(163, 22)
        Me.pop_Promo_Metadata.Text = "Edit Promo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(767, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 15)
        Me.Label3.TabIndex = 72
        Me.Label3.Text = "Total:"
        '
        'txt_PromosCount
        '
        Me.txt_PromosCount.EditValue = ""
        Me.txt_PromosCount.Enabled = False
        Me.txt_PromosCount.Location = New System.Drawing.Point(810, 14)
        Me.txt_PromosCount.Name = "txt_PromosCount"
        Me.txt_PromosCount.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PromosCount.Properties.Appearance.Options.UseFont = True
        Me.txt_PromosCount.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_PromosCount.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_PromosCount.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_PromosCount.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_PromosCount.Size = New System.Drawing.Size(62, 22)
        Me.txt_PromosCount.TabIndex = 71
        '
        'cmb_Customer
        '
        Me.cmb_Customer.EditValue = ""
        Me.cmb_Customer.Location = New System.Drawing.Point(79, 14)
        Me.cmb_Customer.Name = "cmb_Customer"
        Me.cmb_Customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer.Properties.NullText = ""
        Me.cmb_Customer.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Customer.TabIndex = 69
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 15)
        Me.Label4.TabIndex = 70
        Me.Label4.Text = "Customer:"
        '
        'btn_ActivatePromo
        '
        Me.btn_ActivatePromo.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ActivatePromo.Appearance.Options.UseFont = True
        Me.btn_ActivatePromo.Enabled = False
        Me.btn_ActivatePromo.Location = New System.Drawing.Point(323, 12)
        Me.btn_ActivatePromo.Name = "btn_ActivatePromo"
        Me.btn_ActivatePromo.Size = New System.Drawing.Size(161, 25)
        Me.btn_ActivatePromo.TabIndex = 68
        Me.btn_ActivatePromo.Text = "Activate/Deactivate to Store"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.txt_Stores_Search)
        Me.XtraTabPage3.Controls.Add(Me.cmb_Type_Storetab)
        Me.XtraTabPage3.Controls.Add(Me.Label8)
        Me.XtraTabPage3.Controls.Add(Me.StoresGrid)
        Me.XtraTabPage3.Controls.Add(Me.cmb_Store_Storetab)
        Me.XtraTabPage3.Controls.Add(Me.btn_Go)
        Me.XtraTabPage3.Controls.Add(Me.Label6)
        Me.XtraTabPage3.Controls.Add(Me.txt_Filecount_Storetab)
        Me.XtraTabPage3.Controls.Add(Me.Label7)
        Me.XtraTabPage3.Controls.Add(Me.cmb_Customer_Storetab)
        Me.XtraTabPage3.Controls.Add(Me.Label5)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(1337, 623)
        Me.XtraTabPage3.Text = "Store"
        '
        'txt_Stores_Search
        '
        Me.txt_Stores_Search.EditValue = "<Search Text>"
        Me.txt_Stores_Search.Location = New System.Drawing.Point(770, 14)
        Me.txt_Stores_Search.Name = "txt_Stores_Search"
        Me.txt_Stores_Search.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Stores_Search.Properties.Appearance.Options.UseFont = True
        Me.txt_Stores_Search.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Stores_Search.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_Stores_Search.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_Stores_Search.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_Stores_Search.Size = New System.Drawing.Size(146, 22)
        Me.txt_Stores_Search.TabIndex = 70
        '
        'cmb_Type_Storetab
        '
        Me.cmb_Type_Storetab.EditValue = "Songs"
        Me.cmb_Type_Storetab.Location = New System.Drawing.Point(602, 14)
        Me.cmb_Type_Storetab.Name = "cmb_Type_Storetab"
        Me.cmb_Type_Storetab.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Type_Storetab.Properties.Appearance.Options.UseFont = True
        Me.cmb_Type_Storetab.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Type_Storetab.Properties.Items.AddRange(New Object() {"Songs", "Promos"})
        Me.cmb_Type_Storetab.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cmb_Type_Storetab.Size = New System.Drawing.Size(89, 22)
        Me.cmb_Type_Storetab.TabIndex = 69
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(564, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(33, 15)
        Me.Label8.TabIndex = 68
        Me.Label8.Text = "Type"
        '
        'StoresGrid
        '
        Me.StoresGrid.AllowUserToAddRows = False
        Me.StoresGrid.AllowUserToDeleteRows = False
        Me.StoresGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        Me.StoresGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        Me.StoresGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.StoresGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.StoresGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.StoresGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.StoresGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.StoresGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.StoresGrid.ColumnHeadersHeight = 25
        Me.StoresGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.StoresGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.GenreId})
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.StoresGrid.DefaultCellStyle = DataGridViewCellStyle15
        Me.StoresGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.StoresGrid.EnableHeadersVisualStyles = False
        Me.StoresGrid.Location = New System.Drawing.Point(7, 46)
        Me.StoresGrid.MultiSelect = False
        Me.StoresGrid.Name = "StoresGrid"
        Me.StoresGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.StoresGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.StoresGrid.RowHeadersVisible = False
        Me.StoresGrid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.StoresGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.StoresGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.StoresGrid.Size = New System.Drawing.Size(1326, 570)
        Me.StoresGrid.TabIndex = 67
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Song ID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Title"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Album"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Artist"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 200
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "File Name"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Duration"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'GenreId
        '
        Me.GenreId.HeaderText = "Category"
        Me.GenreId.Name = "GenreId"
        Me.GenreId.Visible = False
        '
        'cmb_Store_Storetab
        '
        Me.cmb_Store_Storetab.EditValue = ""
        Me.cmb_Store_Storetab.Location = New System.Drawing.Point(366, 14)
        Me.cmb_Store_Storetab.Name = "cmb_Store_Storetab"
        Me.cmb_Store_Storetab.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Store_Storetab.Properties.Appearance.Options.UseFont = True
        Me.cmb_Store_Storetab.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Store_Storetab.Properties.NullText = ""
        Me.cmb_Store_Storetab.Size = New System.Drawing.Size(181, 22)
        Me.cmb_Store_Storetab.TabIndex = 66
        '
        'btn_Go
        '
        Me.btn_Go.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Go.Appearance.Options.UseFont = True
        Me.btn_Go.Enabled = False
        Me.btn_Go.Location = New System.Drawing.Point(694, 14)
        Me.btn_Go.Name = "btn_Go"
        Me.btn_Go.Size = New System.Drawing.Size(45, 22)
        Me.btn_Go.TabIndex = 65
        Me.btn_Go.Text = "Go"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(942, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 15)
        Me.Label6.TabIndex = 64
        Me.Label6.Text = "Total"
        '
        'txt_Filecount_Storetab
        '
        Me.txt_Filecount_Storetab.EditValue = ""
        Me.txt_Filecount_Storetab.Enabled = False
        Me.txt_Filecount_Storetab.Location = New System.Drawing.Point(984, 14)
        Me.txt_Filecount_Storetab.Name = "txt_Filecount_Storetab"
        Me.txt_Filecount_Storetab.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Filecount_Storetab.Properties.Appearance.Options.UseFont = True
        Me.txt_Filecount_Storetab.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Filecount_Storetab.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.White
        Me.txt_Filecount_Storetab.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_Filecount_Storetab.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_Filecount_Storetab.Size = New System.Drawing.Size(83, 22)
        Me.txt_Filecount_Storetab.TabIndex = 63
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(324, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 15)
        Me.Label7.TabIndex = 62
        Me.Label7.Text = "Store"
        '
        'cmb_Customer_Storetab
        '
        Me.cmb_Customer_Storetab.EditValue = ""
        Me.cmb_Customer_Storetab.Location = New System.Drawing.Point(72, 14)
        Me.cmb_Customer_Storetab.Name = "cmb_Customer_Storetab"
        Me.cmb_Customer_Storetab.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Customer_Storetab.Properties.Appearance.Options.UseFont = True
        Me.cmb_Customer_Storetab.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Customer_Storetab.Properties.NullText = ""
        Me.cmb_Customer_Storetab.Size = New System.Drawing.Size(232, 22)
        Me.cmb_Customer_Storetab.TabIndex = 59
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 60
        Me.Label5.Text = "Customer:"
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style"
        '
        'PanelModify_Category
        '
        Me.PanelModify_Category.Controls.Add(Me.cmb_Modify_Category)
        Me.PanelModify_Category.Controls.Add(Me.lbl_Modify_Category)
        Me.PanelModify_Category.Controls.Add(Me.Category_Modify_Grid)
        Me.PanelModify_Category.Controls.Add(Me.btn_Modify_Exit)
        Me.PanelModify_Category.Controls.Add(Me.btn_ModifyCategory)
        Me.PanelModify_Category.Location = New System.Drawing.Point(523, 170)
        Me.PanelModify_Category.Name = "PanelModify_Category"
        Me.PanelModify_Category.Size = New System.Drawing.Size(290, 311)
        Me.PanelModify_Category.TabIndex = 77
        Me.PanelModify_Category.Visible = False
        '
        'cmb_Modify_Category
        '
        Me.cmb_Modify_Category.EditValue = ""
        Me.cmb_Modify_Category.Location = New System.Drawing.Point(70, 9)
        Me.cmb_Modify_Category.Name = "cmb_Modify_Category"
        Me.cmb_Modify_Category.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_Modify_Category.Properties.Appearance.Options.UseFont = True
        Me.cmb_Modify_Category.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Modify_Category.Properties.NullText = ""
        Me.cmb_Modify_Category.Size = New System.Drawing.Size(198, 22)
        Me.cmb_Modify_Category.TabIndex = 63
        '
        'lbl_Modify_Category
        '
        Me.lbl_Modify_Category.AutoSize = True
        Me.lbl_Modify_Category.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Modify_Category.Location = New System.Drawing.Point(8, 12)
        Me.lbl_Modify_Category.Name = "lbl_Modify_Category"
        Me.lbl_Modify_Category.Size = New System.Drawing.Size(44, 15)
        Me.lbl_Modify_Category.TabIndex = 64
        Me.lbl_Modify_Category.Text = "Genre:"
        '
        'Category_Modify_Grid
        '
        Me.Category_Modify_Grid.AllowUserToAddRows = False
        Me.Category_Modify_Grid.AllowUserToDeleteRows = False
        Me.Category_Modify_Grid.AllowUserToResizeRows = False
        DataGridViewCellStyle39.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black
        Me.Category_Modify_Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle39
        Me.Category_Modify_Grid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Category_Modify_Grid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(95, Byte), Integer))
        Me.Category_Modify_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Category_Modify_Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.Category_Modify_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle40.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle40.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle40.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Category_Modify_Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle40
        Me.Category_Modify_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Category_Modify_Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Category_Id, Me.Category_Name, Me.Category_Code})
        DataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle43.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle43.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle43.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        DataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle43.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Category_Modify_Grid.DefaultCellStyle = DataGridViewCellStyle43
        Me.Category_Modify_Grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Category_Modify_Grid.EnableHeadersVisualStyles = False
        Me.Category_Modify_Grid.Location = New System.Drawing.Point(6, 33)
        Me.Category_Modify_Grid.Name = "Category_Modify_Grid"
        Me.Category_Modify_Grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle44.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle44.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle44.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Category_Modify_Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle44
        Me.Category_Modify_Grid.RowHeadersVisible = False
        Me.Category_Modify_Grid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.Category_Modify_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Category_Modify_Grid.Size = New System.Drawing.Size(279, 241)
        Me.Category_Modify_Grid.TabIndex = 62
        '
        'Category_Id
        '
        Me.Category_Id.HeaderText = "Category ID"
        Me.Category_Id.Name = "Category_Id"
        Me.Category_Id.Visible = False
        '
        'Category_Name
        '
        DataGridViewCellStyle41.ForeColor = System.Drawing.Color.Black
        Me.Category_Name.DefaultCellStyle = DataGridViewCellStyle41
        Me.Category_Name.HeaderText = "Category Name"
        Me.Category_Name.Name = "Category_Name"
        Me.Category_Name.Width = 150
        '
        'Category_Code
        '
        DataGridViewCellStyle42.ForeColor = System.Drawing.Color.Black
        Me.Category_Code.DefaultCellStyle = DataGridViewCellStyle42
        Me.Category_Code.HeaderText = "Category Code"
        Me.Category_Code.Name = "Category_Code"
        Me.Category_Code.Width = 110
        '
        'btn_Modify_Exit
        '
        Me.btn_Modify_Exit.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Modify_Exit.Appearance.Options.UseFont = True
        Me.btn_Modify_Exit.Location = New System.Drawing.Point(181, 281)
        Me.btn_Modify_Exit.Name = "btn_Modify_Exit"
        Me.btn_Modify_Exit.Size = New System.Drawing.Size(104, 23)
        Me.btn_Modify_Exit.TabIndex = 32
        Me.btn_Modify_Exit.Text = "Exit"
        '
        'btn_ModifyCategory
        '
        Me.btn_ModifyCategory.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ModifyCategory.Appearance.Options.UseFont = True
        Me.btn_ModifyCategory.Location = New System.Drawing.Point(71, 281)
        Me.btn_ModifyCategory.Name = "btn_ModifyCategory"
        Me.btn_ModifyCategory.Size = New System.Drawing.Size(104, 23)
        Me.btn_ModifyCategory.TabIndex = 31
        Me.btn_ModifyCategory.Text = "Modify"
        '
        'frmLibrary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1343, 651)
        Me.Controls.Add(Me.PanelModify_Category)
        Me.Controls.Add(Me.Library_Tab_Control)
        Me.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.Name = "frmLibrary"
        Me.Text = "Library"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Library_Tab_Control, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Library_Tab_Control.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.Grp_Songs_History, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Grp_Songs_History.ResumeLayout(False)
        CType(Me.Grid_Songs_History, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Songs_Search.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SongsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopUpMenu.ResumeLayout(False)
        CType(Me.txt_SongsCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Genre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.Grp_Promos_History, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Grp_Promos_History.ResumeLayout(False)
        CType(Me.Grid_Promos_History, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Promos_Search.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PromosGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.popupMenuPromo.ResumeLayout(False)
        CType(Me.txt_PromosCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        Me.XtraTabPage3.PerformLayout()
        CType(Me.txt_Stores_Search.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Type_Storetab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StoresGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Store_Storetab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Filecount_Storetab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_Customer_Storetab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelModify_Category, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelModify_Category.ResumeLayout(False)
        Me.PanelModify_Category.PerformLayout()
        CType(Me.cmb_Modify_Category.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Category_Modify_Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Library_Tab_Control As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btn_SongActivate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cmb_Genre As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents txt_SongsCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SongsGrid As System.Windows.Forms.DataGridView
    Friend WithEvents cmb_Customer_Storetab As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmb_Store_Storetab As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btn_Go As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_Filecount_Storetab As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents StoresGrid As System.Windows.Forms.DataGridView
    Friend WithEvents cmb_Type_Storetab As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_Songs_Search As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_Add_Song As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_Stores_Search As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Promos_Search As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_AddPromo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PromosGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_PromosCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cmb_Customer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btn_ActivatePromo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Song_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Album As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Artist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FileName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Duration As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Genre_Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PopUpMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents TStrip_btn_Genre As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents popupMenuPromo As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents pop_Genre_Promo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PanelModify_Category As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmb_Modify_Category As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lbl_Modify_Category As System.Windows.Forms.Label
    Friend WithEvents Category_Modify_Grid As System.Windows.Forms.DataGridView
    Friend WithEvents Category_Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Category_Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Category_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn_Modify_Exit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_ModifyCategory As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TStrip_btn_Modify_Metadata As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pop_Promo_Metadata As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Grp_Songs_History As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_Songs_Close As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Grid_Songs_History As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Grp_Promos_History As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_Promos_Close As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Grid_Promos_History As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GenreId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_Title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_FileName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_Duration As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Promo_Normalization As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Start_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Stop_Date As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
