﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO
Imports DevExpress.Utils
Imports NAudio
Imports DevExpress.XtraEditors
Imports aTrimmer

Public Class frmAddSong
    Public CurRow, CurFirstRowIndex As Integer
    Private Sub frmAddSong_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not ReadConfig() Then
            Exit Sub
        End If
        Call LoadSettings()
        Call LoadGenres()
    End Sub
    Private Sub LoadGenres()
        Dim cmdGenre As New SqlCommand, adpGenre As New SqlDataAdapter, dsGenre As New DataSet

        Call OpenDBConnection()

        With cmdGenre
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdGenre.CommandText = "ProcGetGenresList"
        adpGenre.SelectCommand = cmdGenre
        adpGenre.Fill(dsGenre, "GenreList")

        cmb_Genre.Properties.DisplayMember = "Genre_Name"
        cmb_Genre.Properties.ValueMember = "Genre_ID"
        cmb_Genre.Properties.DataSource = dsGenre.Tables("GenreList")
        cmb_Genre.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Genre.Properties.PopulateColumns()
        cmb_Genre.Properties.Columns(0).Visible = False
        cmb_Genre.Properties.ShowFooter = False
        cmb_Genre.Properties.ShowHeader = False

        cmb_Genre.EditValue = ""


        ' If dsGenre.Tables("GenreList").Rows.Count <> 0 Then btn_SongActivate.Enabled = True Else btn_SongActivate.Enabled = False
        Call LoadNormalizationFactors()

        Call CloseDBConnection()

        dsGenre.Dispose()
        adpGenre.Dispose()

    End Sub
    Private Sub LoadNormalizationFactors()
        Dim cmdNormalization As New SqlCommand, adpNormalization As New SqlDataAdapter, dsNormalization As New DataSet

        With cmdNormalization
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
        End With
        cmdNormalization.CommandText = "ProcGetNormalizationsList"
        adpNormalization.SelectCommand = cmdNormalization
        adpNormalization.Fill(dsNormalization, "NormalizationList")

        cmb_Normalization.Properties.DisplayMember = "Normalization_Factor"
        cmb_Normalization.Properties.ValueMember = "Normalization_ID"
        cmb_Normalization.Properties.DataSource = dsNormalization.Tables("NormalizationList")
        cmb_Normalization.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Normalization.Properties.PopulateColumns()
        'cmb_Normalization.Properties.Columns(0).Visible = False
        cmb_Normalization.Properties.ShowFooter = False
        cmb_Normalization.Properties.ShowHeader = False

        cmb_Normalization.Text = "-6 db"

        dsNormalization.Dispose()
        adpNormalization.Dispose()
    End Sub
    Private Sub btn_Browse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Browse.Click
        BrowseFile.Filter = "Wave Files|*.wav"
        BrowseFile.ShowDialog()
        txt_AudioPath.Text = BrowseFile.FileName

    End Sub


    Private Sub btn_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Quit.Click

        If btn_Add.Text = "Save" Then
            Me.Close()
            Exit Sub
        End If


        MDIInstoreCentral.PlayerPanel.Visible = True
        MDIInstoreCentral.ResetPlayer()
        If Not cmb_Genre.EditValue = Nothing Then INSCModule.ActiveGenreId = Convert.ToInt32(cmb_Genre.EditValue.ToString)
        If Not cmb_Genre.Text = "" Then INSCModule.ActiveGenre = cmb_Genre.Text
        Me.Close()

        Dim w As New frmLibrary

        With w
            .MdiParent = MDIInstoreCentral
            .ControlBox = False
            .Text = [String].Empty
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            .Dock = DockStyle.Fill
            .Show()
            .Library_Tab_Control.SelectedTabPageIndex = 0
            .cmb_Genre.Text = ActiveGenre
            '.cmb_Genre.RefreshEditValue()
        End With

    End Sub

    Private Sub btn_Add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Add.Click

        If cmb_Genre.EditValue.ToString = "" Then
            XtraMessageBox.Show("Genre can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Genre.Focus()
            Exit Sub
        End If

        If cmb_Category.EditValue.ToString = "" Then
            XtraMessageBox.Show("Category can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmb_Category.Focus()
            Exit Sub
        End If

        If txt_SongID.Text = "" Then
            XtraMessageBox.Show("Song ID can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_SongID.Focus()
            Exit Sub
        End If

        If Not IsNumeric(txt_SongID.Text) Then
            XtraMessageBox.Show("Song ID can not numerics only", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_SongID.Focus()
            Exit Sub
        End If

        If txt_Album.Text = "" Then
            XtraMessageBox.Show("Album of the song can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_Album.Focus()
            Exit Sub
        End If

        If txt_Album.Text = "" Then
            XtraMessageBox.Show("Title of the song can not be blank", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_Album.Focus()
            Exit Sub
        End If


        If txt_AudioPath.Text = "" Then
            XtraMessageBox.Show("Song audio path is not selected", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_AudioPath.Focus()
            Exit Sub
        End If

        If Not File.Exists(txt_AudioPath.Text) Then
            XtraMessageBox.Show("Could not find 'Song audio' in the selected path", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_AudioPath.Focus()
            Exit Sub
        End If

        Dim SongFileName As String = ""
        Dim SongAACFileName As String = ""
        Dim FileDuration As String = ""

        Dim SongTempFile As String = ""

        If Not cmb_Genre.EditValue = Nothing Then INSCModule.ActiveGenreId = Convert.ToInt32(cmb_Genre.EditValue.ToString)
        If Not cmb_Genre.Text = "" Then INSCModule.ActiveGenre = cmb_Genre.Text

        If File.Exists(SongsArchivePath & "\" & txt_SongID.Text.Trim & ".aac") Then
            If btn_Add.Text <> "Save" Then
                If XtraMessageBox.Show("This song is already added, Do you want to overwrite?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    File.Delete(SongsArchivePath & "\" & txt_SongID.Text.Trim & ".aac")
                    Application.DoEvents()
                Else
                    Exit Sub
                End If
            Else
                If SongsArchivePath & "\" & txt_SongID.Text.Trim & ".aac" <> txt_AudioPath.Text Then
                    File.Delete(SongsArchivePath & "\" & txt_SongID.Text.Trim & ".aac")
                    Application.DoEvents()
                Else
                    FileDuration = lbl_Duration.Text
                    GoTo SaveToDB
                End If
            End If
        End If

        If Not File.Exists(AudioConverterpath) Then
            XtraMessageBox.Show("Audio converter not found (" & AudioConverterpath & ")", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        ConversionBar.Visible = True

        If Not Directory.Exists(Application.StartupPath & "\Temp") = True Then
            Directory.CreateDirectory(Application.StartupPath & "\Temp")
        End If

        SongFileName = Path.GetFileName(txt_AudioPath.Text)
        SongTempFile = Application.StartupPath & "\Temp\" & txt_SongID.Text & ".wav"
        SongAACFileName = txt_SongID.Text & ".aac" 'SongFileName.Replace(".wav", ".aac")

        lbl_Status.Text = "Status: Converting Song file"
        Application.DoEvents()

        If ReWriteWavFile(AudioConverterpath, txt_AudioPath.Text, SongTempFile) Then
            'silence trimming in the code
            lbl_Status.Text = "Status: Converting Song to .aac file"
            Application.DoEvents()

            If CheckforAACFile(SongFileName, txt_SongID.Text) = False Then
                FileDuration = GetWavFileDuration(SongTempFile)
                If FileDuration <> "Error while reading wav file" Then
                    If Convert_Wav_AACPlus(SongTempFile, txt_SongID.Text) Then
                    Else
                        XtraMessageBox.Show("Unable to convert Audio File", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                Else
                    XtraMessageBox.Show("Unable to retrieve audio file duration", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
            Else
                If FileDuration <> "Error while reading wav file" Then
                Else
                    XtraMessageBox.Show("Unable to retrieve audio file duration", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
            End If
        Else
            lbl_Status.Text = "Status: File normalization failed, could not add song"
            Application.DoEvents()
            XtraMessageBox.Show("File normalization failed, could not add song", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

SaveToDB:
        lbl_Status.Text = "Status: Saving to database"
        Application.DoEvents()

        Dim Cmd As New SqlCommand

        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcAddNewSong"

            .Parameters.AddWithValue("@Song_ID", Val(txt_SongID.Text)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Genre_ID", Convert.ToInt32(cmb_Genre.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Title", txt_Title.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Album", txt_Album.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Artist1", txt_Artist1.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Artist2", txt_Artist2.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@FileName", txt_SongID.Text.Trim & ".aac").Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Genre_Categrory_Id", Convert.ToInt32(cmb_Category.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Duration", FileDuration).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Normalization_ID", 7).Direction = ParameterDirection.Input
            '.Parameters.AddWithValue("@File_TimeStamp", Now).Direction = ParameterDirection.Input
            '.Parameters.AddWithValue("@Normalization_ID", Convert.ToInt32(cmb_Normalization.EditValue.ToString)).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output
            .ExecuteNonQuery()
        End With

        Dim strResult As String
        strResult = Cmd.Parameters("@Result").Value
        If strResult = "Success" Then
            'XtraMessageBox.Show("Song added successfully", "New Song", MessageBoxButtons.OK)
            lbl_Status.Text = "Status: Song added successfully"
            Application.DoEvents()
        Else
            'XtraMessageBox.Show(strResult, "New Song", MessageBoxButtons.OK)
            lbl_Status.Text = "Status: " & strResult
            Application.DoEvents()
        End If
        ConversionBar.Visible = False
        'lbl_Status.Text = ""

        If DeleteTempFolder(Application.StartupPath & "\Temp") Then
            'Write Log---Temp folder Delete Sucessfully
        Else
            'Write Log --- Error while deleteing temp folder
        End If
        ClearValues()
        Call CloseDBConnection()
        If btn_Add.Text = "Save" And lbl_Status.Text = "Status: Song added successfully" Then
            Dim f As frmLibrary = TryCast(Me.Owner, frmLibrary)
            f.RefreshSongs(CurRow, CurFirstRowIndex)
            Me.Close()
        End If
    End Sub
    Private Sub ClearValues()
        txt_AudioPath.Text = ""
        txt_SongID.Text = ""
        txt_Title.Text = ""
        txt_Album.Text = ""
        txt_Artist1.Text = ""
        txt_Artist2.Text = ""

    End Sub
    Public Function DeleteTempFolder(ByVal Temppath As String) As Boolean
        Try
            If Directory.Exists(Temppath) = True Then
                Directory.Delete(Temppath, True)
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function CheckforAACFile(ByVal Filename As String, ByVal Media_ID As String) As Boolean
        Dim returnval As Boolean
        Dim aacFilename As String
        aacFilename = Media_ID & "\" & ".aac"

        If File.Exists(SongsArchivePath & "\" & aacFilename) Then
            returnval = True
        Else
            returnval = False
        End If
        Return returnval
    End Function
    Public Function GetWavFileDuration(ByVal FileNameStr As String) As String
        Dim returnval As String
        If File.Exists(FileNameStr) Then
            Try
                Dim wd As NAudio.Wave.WaveFileReader = New Wave.WaveFileReader(FileNameStr)
                returnval = wd.TotalTime.ToString.Split(".")(0)
                wd.Close()
                wd = Nothing
            Catch ex As Exception
                returnval = "Error while reading wav file"
            End Try
        Else
            returnval = "Source File Not Found"
        End If
        Return returnval
    End Function
    Public Function Convert_Wav_AACPlus(ByVal FileNameStr As String, ByVal Media_ID As String) As Boolean
        Dim ConversionProcess As Process
        Dim StrOutput As String
        Dim finfo As FileInfo
        Dim wavfile As String
        Dim aacfile As String
        Dim frmStr As String
        Dim returnval As Boolean


        If File.Exists(FileNameStr) Then
            finfo = New FileInfo(FileNameStr)
            wavfile = finfo.FullName
            aacfile = SongsArchivePath & "\" & Media_ID & ".aac"
            ConversionProcess = New Process
            ConversionProcess.StartInfo.UseShellExecute = False
            ConversionProcess.StartInfo.RedirectStandardError = True
            ConversionProcess.StartInfo.CreateNoWindow = True
            ConversionProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            ConversionProcess.StartInfo.FileName = Application.StartupPath & "\enc_aacPlus.exe"
            ' ConversionProcess.EnableRaisingEvents = True
            frmStr = """" & wavfile & """" & " """ & aacfile & """ --br 128000 --he"
            ConversionProcess.StartInfo.Arguments = frmStr
            ConversionProcess.Start()

            ' StrOutput = ConversionProcess.StandardOutput.ReadToEnd
            Application.DoEvents()
            Do Until ConversionProcess.HasExited
                Application.DoEvents()
            Loop
            'ConversionProcess.WaitForExit()
            If File.Exists(aacfile) Then
                returnval = True
            Else
                returnval = False
            End If
        End If
        Return returnval
    End Function
    Public Function ReWriteWavFile(ByVal ExePath As String, ByVal InFile As String, ByVal OutFile As String) As Boolean
        Dim StrProcessPath As String = String.Empty
        Dim tempOutFile, tempPath As String

        tempPath = Path.GetDirectoryName(OutFile)
        tempOutFile = Path.GetFileNameWithoutExtension(OutFile) & "-trim" & Path.GetExtension(OutFile)

        StrProcessPath = """" & InFile & """ """ & tempPath & "\" & tempOutFile & "" & """ -c wav -sr 44100 -ch Stereo"
        If ShellandWait(ExePath, StrProcessPath) = True Then
            If File.Exists(tempPath & "\" & tempOutFile) = True Then
                Dim wa As New aTrim
                lbl_Status.Text = "Status: Trimming silence"
                Application.DoEvents()
                If (Not wa.StripSilence(tempPath & "\" & tempOutFile, OutFile)) Then
                    lbl_Status.Text = "Status: Trimming failed"
                    Application.DoEvents()
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If
        If File.Exists(tempPath & "\" & tempOutFile) Then
            File.Delete(tempPath & "\" & tempOutFile)
            Application.DoEvents()
        End If
    End Function
    Public Function ShellandWait(ByVal ProcessPath As String, ByVal Arguments As String) As Boolean
        Dim objProcess As System.Diagnostics.Process
        Dim returnval As Boolean
        Try
            objProcess = New System.Diagnostics.Process()
            objProcess.StartInfo.FileName = ProcessPath
            objProcess.StartInfo.Arguments = Arguments
            objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
            objProcess.Start()
            objProcess.WaitForExit()
            objProcess.Close()
            returnval = True
        Catch
            returnval = False
        End Try
        Return returnval
    End Function
    Private Sub LoadCategories(ByVal Genre_Id As Integer)
        Dim Cmd As New SqlCommand, dsStores As New DataSet, dapStores As New SqlDataAdapter
        Call OpenDBConnection()
        With Cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = DBConn
            .CommandTimeout = 500
            .CommandText = "ProcGetGenreCategoriesList"
            .Parameters.AddWithValue("@Genre_Id", Genre_Id).Direction = ParameterDirection.Input
        End With

        dapStores.SelectCommand = Cmd
        dapStores.Fill(dsStores, "CategoryList")


        'Creating Columns for Display in lookup
        cmb_Category.Properties.DisplayMember = "Category_Code"
        cmb_Category.Properties.ValueMember = "Genre_Category_ID"
        cmb_Category.Properties.DataSource = dsStores.Tables(0)
        cmb_Category.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        cmb_Category.Properties.PopulateColumns()
        cmb_Category.Properties.Columns(0).Visible = False
        cmb_Category.Properties.Columns(1).Visible = False
        cmb_Category.Properties.ShowFooter = False
        cmb_Category.Properties.ShowHeader = False
        cmb_Category.EditValue = ""

        'If dsStores.Tables(0).Rows.Count <> 0 Then btn_Save.Enabled = True Else btn_Save.Enabled = False

        dsStores.Dispose()
        dapStores.Dispose()


        Call CloseDBConnection()
    End Sub
    Private Sub cmb_Genre_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_Genre.EditValueChanged
        If cmb_Genre.EditValue.ToString <> "" Then Call LoadCategories(Convert.ToInt32(cmb_Genre.EditValue.ToString))
    End Sub
End Class