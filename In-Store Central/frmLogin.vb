﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.XtraEditors
Imports System.IO
Public Class frmLogin
    Private Function AppExpired() As Boolean
        Dim res As Boolean = True
        Dim LastExecDate As Date = "01/01/2012"
        Dim ExpiryDate As Date = "12/12/2020"
        If Not File.Exists(Application.StartupPath & "\Info.dat") Then
            'XtraMessageBox.Show("Invalid license, please contact administrator", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            res = True
        Else
            Try
                If DateDiff(DateInterval.Day, Now.Date, ExpiryDate) < 0 Then
                    'XtraMessageBox.Show("Invalid license, please contact administrator", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    res = True
                Else
                    Dim strRead As New StreamReader(Application.StartupPath & "\Info.dat")
                    LastExecDate = CDate(decryptString(strRead.ReadLine))
                    strRead.Close()
                    strRead = Nothing

                    If DateDiff(DateInterval.Day, LastExecDate, Now.Date) < 0 Then
                        'XtraMessageBox.Show("Invalid license, please contact administrator", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        res = True
                    Else
                        Dim strWrite As New StreamWriter(Application.StartupPath & "\Info.dat")
                        strWrite.WriteLine(encryptString(Now.Date.ToString("MM/dd/yyyy")))
                        strWrite.Close()
                        strWrite = Nothing
                        res = False
                    End If
                End If

            Catch ex As Exception
                'XtraMessageBox.Show("Invalid license, please contact administrator", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                res = False
            End Try
        End If
        Return res
    End Function

    Private Sub btn_Login_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Login.Click

        If AppExpired() Then
            End
            'Exit Sub
        End If

        If Not ReadConfig() Then Exit Sub

        If txt_LoginID.Text.Trim = "" Then
            XtraMessageBox.Show("Login Id is not entered", "Login Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_LoginID.Focus()
            Exit Sub
        End If

        If txt_Password.Text.Trim = "" Then
            XtraMessageBox.Show("Password is not entered", "Login Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txt_Password.Focus()
            Exit Sub
        End If

        Call OpenDBConnection()
        Dim cmdLogin As New SqlCommand

        With cmdLogin
            .Connection = DBConn
            .CommandTimeout = 300
            .CommandType = CommandType.StoredProcedure
            .CommandText = "ProcCheckPassword"
            .Parameters.AddWithValue("@User_Name", txt_LoginID.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.AddWithValue("@Password", txt_Password.Text.Trim).Direction = ParameterDirection.Input
            .Parameters.Add("@Result", SqlDbType.Bit).Direction = ParameterDirection.Output
            .ExecuteNonQuery()

            Dim strResult As Integer
            strResult = .Parameters("@Result").Value
            If strResult <> 0 Then
                Me.Hide()
                MDIInstoreCentral.Show()
            Else
                XtraMessageBox.Show("Invalid login id or password", "Login Alert", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txt_Password.Focus()
            End If
        End With

        cmdLogin = Nothing
        Call CloseDBConnection()
    End Sub

    Private Sub btn_Exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Exit.Click

        End
    End Sub


    Private Sub txt_Password_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Password.KeyPress
        If e.KeyChar = Chr(13) Then
            btn_Login.PerformClick()
        End If
    End Sub
    Private Sub lbl_ChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_ChangePassword.Click
        Me.Hide()
        frmChangePassword.Show()
    End Sub

    Private Sub lbl_ChangePassword_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_ChangePassword.MouseHover
        Cursor = Cursors.Hand
    End Sub

    Private Sub lbl_ChangePassword_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl_ChangePassword.MouseLeave
        Cursor = Cursors.Default
    End Sub

   

    Private Sub PictureEdit1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureEdit1.Click
        Process.Start("explorer.exe", "http://www.timbremedia.in")
    End Sub

  
    Private Sub txt_Password_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_Password.EditValueChanged

    End Sub
End Class